-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 05, 2014 at 02:18 PM
-- Server version: 5.5.36
-- PHP Version: 5.4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `joboard_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `candidate`
--

CREATE TABLE IF NOT EXISTS `candidate` (
  `candidate_id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(100) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `current_address` text,
  `phone` varchar(40) DEFAULT NULL,
  `mobile` varchar(40) DEFAULT NULL,
  `gender` enum('Male','Female') DEFAULT NULL,
  `dob` varchar(20) DEFAULT NULL,
  `marital_status` enum('Married','Unmarried') DEFAULT NULL,
  `religion` varchar(30) DEFAULT NULL,
  `nationality` varchar(100) DEFAULT '',
  `father_name` varchar(60) DEFAULT NULL,
  `mother_name` varchar(60) DEFAULT NULL,
  `cv_tbl_id` int(11) NOT NULL,
  `parmanent_address` text NOT NULL,
  `status` enum('0','1') NOT NULL COMMENT 'O for visitor and 1 for Candidate ',
  `city` text,
  `photo` text,
  `email` varchar(200) DEFAULT NULL,
  `alternative_email` varchar(200) DEFAULT NULL,
  `agreement` tinyint(1) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`candidate_id`),
  KEY `cv_tbl_id` (`cv_tbl_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=61 ;

--
-- Dumping data for table `candidate`
--

INSERT INTO `candidate` (`candidate_id`, `firstname`, `lastname`, `current_address`, `phone`, `mobile`, `gender`, `dob`, `marital_status`, `religion`, `nationality`, `father_name`, `mother_name`, `cv_tbl_id`, `parmanent_address`, `status`, `city`, `photo`, `email`, `alternative_email`, `agreement`, `created_at`, `updated_at`, `deleted_at`) VALUES
(7, 'mahabub', 'rahamn', NULL, NULL, NULL, 'Male', NULL, NULL, NULL, '', NULL, NULL, 0, '', '0', NULL, NULL, 'liveaccountmb@live.com', NULL, 0, '2014-05-30', '2014-05-30', NULL),
(8, 'himel', 'rahamn', NULL, NULL, NULL, 'Male', NULL, NULL, NULL, '', NULL, NULL, 0, '', '0', NULL, NULL, 'hsdfhsd@live.co', NULL, 0, '2014-05-30', '2014-05-30', NULL),
(59, 'Midad', NULL, 'Dhaka', '78651', '01477410', 'Male', '1990-01-02', 'Married', 'Islam', 'Bangladeshi', 'father', 'mother', 71, 'Dhaka r oduray', '1', NULL, '', 'email@mail.com', 'alter_email@mail.com', NULL, '2014-06-05', '2014-06-05', NULL),
(60, '', NULL, '', '', '', 'Male', '1970-01-01', 'Married', '', '', '', '', 72, '', '1', NULL, '', '', '', NULL, '2014-06-05', '2014-06-05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cv_edu`
--

CREATE TABLE IF NOT EXISTS `cv_edu` (
  `cv_edu_id` int(11) NOT NULL AUTO_INCREMENT,
  `cv_tbl_id` int(11) NOT NULL,
  `cv_edu_title` varchar(100) DEFAULT '',
  `major` varchar(100) DEFAULT '',
  `institution` varchar(100) DEFAULT '',
  `result` varchar(100) DEFAULT '',
  `passing_year` varchar(10) DEFAULT '',
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL,
  PRIMARY KEY (`cv_edu_id`),
  KEY `cv_tbl_id` (`cv_tbl_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=63 ;

-- --------------------------------------------------------

--
-- Table structure for table `cv_jobs`
--

CREATE TABLE IF NOT EXISTS `cv_jobs` (
  `cv_jobs_id` int(11) NOT NULL AUTO_INCREMENT,
  `cv_tbl_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `start` varchar(40) DEFAULT '',
  `end` varchar(40) DEFAULT '',
  `company_name` varchar(100) DEFAULT '',
  `designation` varchar(100) DEFAULT '',
  `department` varchar(100) DEFAULT '',
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL,
  PRIMARY KEY (`cv_jobs_id`),
  KEY `cv_tbl_id` (`cv_tbl_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=186 ;

-- --------------------------------------------------------

--
-- Table structure for table `cv_qualification`
--

CREATE TABLE IF NOT EXISTS `cv_qualification` (
  `cv_qualification_id` int(11) NOT NULL AUTO_INCREMENT,
  `cv_tbl_id` int(11) NOT NULL,
  `cv_q_title` varchar(100) DEFAULT '',
  `certificates` varchar(1000) DEFAULT '',
  `institution` varchar(200) DEFAULT '',
  `finished_year` varchar(10) DEFAULT '',
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL,
  PRIMARY KEY (`cv_qualification_id`),
  KEY `cv_tbl_id` (`cv_tbl_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=91 ;

-- --------------------------------------------------------

--
-- Table structure for table `cv_specialization`
--

CREATE TABLE IF NOT EXISTS `cv_specialization` (
  `cv_specialization_id` int(11) NOT NULL AUTO_INCREMENT,
  `cv_tbl_id` int(11) NOT NULL,
  `key` varchar(100) DEFAULT NULL,
  `value` varchar(40) NOT NULL,
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL,
  PRIMARY KEY (`cv_specialization_id`),
  KEY `cv_tbl_id` (`cv_tbl_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=175 ;

-- --------------------------------------------------------

--
-- Table structure for table `cv_tbl`
--

CREATE TABLE IF NOT EXISTS `cv_tbl` (
  `cv_tbl_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `mobile` varchar(100) DEFAULT NULL,
  `email` varchar(1000) NOT NULL,
  `dob` varchar(40) DEFAULT '',
  `gender` varchar(10) DEFAULT '',
  `year_of_exp` varchar(5) DEFAULT '',
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`cv_tbl_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=73 ;

--
-- Dumping data for table `cv_tbl`
--

INSERT INTO `cv_tbl` (`cv_tbl_id`, `name`, `mobile`, `email`, `dob`, `gender`, `year_of_exp`, `created_at`, `updated_at`) VALUES
(71, 'Midad', '01477410', 'email@mail.com', '1990-01-02', 'Male', '', '2014-06-05', '2014-06-05'),
(72, '', '', '', '1970-01-01', 'Male', '', '2014-06-05', '2014-06-05');

-- --------------------------------------------------------

--
-- Table structure for table `draft`
--

CREATE TABLE IF NOT EXISTS `draft` (
  `draft_id` int(11) NOT NULL AUTO_INCREMENT,
  `draft_date` date NOT NULL,
  `subject` varchar(200) NOT NULL,
  `body` mediumtext NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`draft_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `employer`
--

CREATE TABLE IF NOT EXISTS `employer` (
  `emp_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` int(11) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(40) NOT NULL,
  `email` varchar(200) DEFAULT NULL,
  `website` varchar(300) DEFAULT NULL,
  `contact_person` varchar(200) DEFAULT NULL,
  `contact_person_phone` varchar(40) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `job_category` text,
  `deleted_at` varchar(100) NOT NULL DEFAULT '0',
  PRIMARY KEY (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `inbox`
--

CREATE TABLE IF NOT EXISTS `inbox` (
  `inbox_id` int(11) NOT NULL AUTO_INCREMENT,
  `in_user_id` int(11) NOT NULL,
  `recv_user_id` int(11) NOT NULL,
  `date_of_recv` date NOT NULL,
  `status` int(11) NOT NULL,
  `subject` varchar(200) NOT NULL,
  `body` mediumtext NOT NULL,
  PRIMARY KEY (`inbox_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `interviews`
--

CREATE TABLE IF NOT EXISTS `interviews` (
  `interview_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(300) DEFAULT NULL,
  `topics` text,
  `duration` varchar(20) DEFAULT NULL,
  `type` varchar(150) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `delated_at` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`interview_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE IF NOT EXISTS `jobs` (
  `jobs_id` int(11) NOT NULL AUTO_INCREMENT,
  `no_vacancies` int(11) DEFAULT NULL,
  `job_responsibility` text,
  `job_nature` varchar(200) DEFAULT NULL,
  `eduactional_requirement` mediumtext,
  `experience_requirment` mediumtext,
  `extra_job_req` mediumtext,
  `salary_range` varchar(25) DEFAULT NULL,
  `other_benefite` mediumtext,
  `job_location` varchar(100) DEFAULT NULL,
  `job_source` text,
  `skill_id` int(11) DEFAULT NULL,
  `selected_skill_attributes` text,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` varchar(100) NOT NULL,
  `created_by` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  PRIMARY KEY (`jobs_id`),
  KEY `skill_id` (`skill_id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `option`
--

CREATE TABLE IF NOT EXISTS `option` (
  `option_id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(40) NOT NULL,
  `value` varchar(1000) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`option_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `option`
--

INSERT INTO `option` (`option_id`, `key`, `value`, `updated_at`, `created_at`) VALUES
(1, 'vendor_name', 'Carbon51 School Mangement System', '2014-05-18 10:19:00', '0000-00-00 00:00:00'),
(3, 'vendor_address', 'Banani', '2014-05-18 10:19:00', '2014-02-23 00:00:00'),
(4, 'vendor_code', 'Carbon51_2014', '2014-05-18 10:19:01', '2014-02-23 00:00:00'),
(5, 'vendor_email', 'sdf@email.com', '2014-05-18 10:19:00', '2014-02-23 00:00:00'),
(6, 'vendor_phone', '01714112912', '2014-05-18 10:19:00', '2014-02-23 00:00:00'),
(7, 'vendor_logo', 'data/logo/1396345423_logo.png', '2014-04-01 09:43:43', '2014-02-23 00:00:00'),
(8, 'attendance_default', 'Automatic', '2014-04-21 07:21:08', '2014-02-26 00:00:00'),
(9, 'medium', '', '2014-03-12 00:00:00', '2014-03-12 00:00:00'),
(10, 'aptitudes', 'Behavior, Dress, Hair', '2014-05-26 07:19:21', '2014-03-16 00:00:00'),
(12, 'terms', 'First Term,Second Term', '2014-05-26 07:19:21', '2014-03-19 00:00:00'),
(13, 'attendance_sheet_bind', '{"user_id":"0","in_time":"1","out_time":"2"}', '2014-04-21 07:21:08', '2014-04-21 00:00:00'),
(14, 'term_rules', '{"First Term":"50","Second Term":"50"}', '2014-05-19 07:25:24', '2014-05-19 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `outbox`
--

CREATE TABLE IF NOT EXISTS `outbox` (
  `outbox_id` int(11) NOT NULL AUTO_INCREMENT,
  `sent_user_id` int(11) NOT NULL,
  `recv_user_id` int(11) NOT NULL,
  `date_of_out` date NOT NULL,
  `status` int(11) NOT NULL,
  `subject` text NOT NULL,
  `body` text NOT NULL,
  PRIMARY KEY (`outbox_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `participants`
--

CREATE TABLE IF NOT EXISTS `participants` (
  `participant_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `interview_status` enum('hire','pending') DEFAULT NULL,
  `interview_mark` int(11) NOT NULL,
  `participant_date` date NOT NULL,
  `interview_id` int(11) NOT NULL,
  PRIMARY KEY (`participant_id`),
  KEY `interview_id` (`interview_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `schedule`
--

CREATE TABLE IF NOT EXISTS `schedule` (
  `avail_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `from_time` time NOT NULL,
  `to_time` time NOT NULL,
  `available_date` date NOT NULL,
  `deleted_at` varchar(300) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`avail_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE IF NOT EXISTS `skills` (
  `skill_id` int(11) NOT NULL AUTO_INCREMENT,
  `skill_name` varchar(200) NOT NULL,
  `skill_description` text,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `display_order` int(11) DEFAULT NULL,
  `deleted_at` varchar(100) NOT NULL DEFAULT '0',
  PRIMARY KEY (`skill_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL,
  `user_status` tinyint(1) NOT NULL,
  `user_type` enum('Admin','Employee','Candidate','Visitor') NOT NULL,
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL,
  `deleted_at` varchar(100) NOT NULL DEFAULT '0',
  `user_id` int(11) DEFAULT NULL COMMENT 'candidate_id, employer_id',
  `user_email` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=52 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `user_status`, `user_type`, `updated_at`, `created_at`, `deleted_at`, `user_id`, `user_email`) VALUES
(1, 'admin', '81dc9bdb52d04dc20036dbd8313ed055', 1, 'Admin', '2014-05-13', '2014-05-28', '0', NULL, NULL),
(2, 'asdg', '7e6a6a87bf3ffb29a6dd9f14afdc3b88', 0, 'Visitor', '2014-05-30', '2014-05-30', '0', 7, 'liveaccountmb@live.com'),
(3, 'himel', '81dc9bdb52d04dc20036dbd8313ed055', 0, 'Visitor', '2014-05-30', '2014-05-30', '0', 8, 'hsdfhsd@live.co'),
(50, '01477410', '73baa4ef06f5e910bcebe331240a1a97', 1, 'Candidate', '2014-06-05', '2014-06-05', '0', 59, NULL),
(51, '0', 'cfcd208495d565ef66e7dff9f98764da', 1, 'Candidate', '2014-06-05', '2014-06-05', '0', 60, NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cv_edu`
--
ALTER TABLE `cv_edu`
  ADD CONSTRAINT `cv_edu_ibfk_1` FOREIGN KEY (`cv_tbl_id`) REFERENCES `cv_tbl` (`cv_tbl_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cv_jobs`
--
ALTER TABLE `cv_jobs`
  ADD CONSTRAINT `cv_jobs_ibfk_1` FOREIGN KEY (`cv_tbl_id`) REFERENCES `cv_tbl` (`cv_tbl_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cv_qualification`
--
ALTER TABLE `cv_qualification`
  ADD CONSTRAINT `cv_qualification_ibfk_1` FOREIGN KEY (`cv_tbl_id`) REFERENCES `cv_tbl` (`cv_tbl_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cv_specialization`
--
ALTER TABLE `cv_specialization`
  ADD CONSTRAINT `cv_specialization_ibfk_1` FOREIGN KEY (`cv_tbl_id`) REFERENCES `cv_tbl` (`cv_tbl_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `jobs`
--
ALTER TABLE `jobs`
  ADD CONSTRAINT `jobs_ibfk_1` FOREIGN KEY (`skill_id`) REFERENCES `skills` (`skill_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `jobs_ibfk_2` FOREIGN KEY (`emp_id`) REFERENCES `employer` (`emp_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `participants`
--
ALTER TABLE `participants`
  ADD CONSTRAINT `participants_ibfk_1` FOREIGN KEY (`interview_id`) REFERENCES `interviews` (`interview_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `participants_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `schedule`
--
ALTER TABLE `schedule`
  ADD CONSTRAINT `schedule_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
