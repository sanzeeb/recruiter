-- Valentina Studio --
-- MySQL dump --
-- ---------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- ---------------------------------------------------------


-- CREATE DATABASE "jobsinbd_djit" -------------------------
CREATE DATABASE IF NOT EXISTS `jobsinbd_djit` CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `jobsinbd_djit`;
-- ---------------------------------------------------------


-- CREATE TABLE "agreements" -------------------------------
DROP TABLE IF EXISTS `agreements` CASCADE;

CREATE TABLE `agreements` ( 
	`agreement_id` Int( 11 ) AUTO_INCREMENT NOT NULL, 
	`description` Text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, 
	`amount` VarChar( 100 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, 
	`sender_id` Int( 11 ) NOT NULL, 
	`receiver_id` Int( 11 ) NOT NULL, 
	`approver_id` Int( 11 ) NOT NULL, 
	`status` Enum( 'Confirmed', 'Pending', 'Canceled', 'Declined', 'Accepted' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, 
	`rating` VarChar( 10 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL, 
	`rating_comment` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL, 
	`updated_at` Date NOT NULL, 
	`created_at` Date NOT NULL, 
	`agreement_title` VarChar( 100 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
	 PRIMARY KEY ( `agreement_id` )
 )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB
AUTO_INCREMENT = 2;
-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


