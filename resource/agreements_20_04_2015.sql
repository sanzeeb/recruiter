-- Valentina Studio --
-- MySQL dump --
-- ---------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- ---------------------------------------------------------


-- CREATE DATABASE "jobsinbd_djit" -------------------------
CREATE DATABASE IF NOT EXISTS `jobsinbd_djit` CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `jobsinbd_djit`;
-- ---------------------------------------------------------


-- CREATE TABLE "agreement_reply" --------------------------
DROP TABLE IF EXISTS `agreement_reply` CASCADE;

CREATE TABLE `agreement_reply` ( 
	`agreement_id` Int( 11 ) NOT NULL, 
	`reply` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL, 
	`created_at` Date NULL, 
	`updated_at` Date NULL
 )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB;
-- ---------------------------------------------------------


-- Dump data of "agreement_reply" --------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "agreement_id" -----------------------------
CREATE INDEX `agreement_id` USING BTREE ON `agreement_reply`( `agreement_id` );
-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


-- CREATE TABLE "agreements" -------------------------------
DROP TABLE IF EXISTS `agreements` CASCADE;

CREATE TABLE `agreements` ( 
	`agreement_id` Int( 11 ) AUTO_INCREMENT NOT NULL, 
	`agreement_title` VarChar( 100 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, 
	`description` Text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, 
	`amount` VarChar( 100 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, 
	`sender_id` Int( 11 ) NOT NULL, 
	`receiver_id` Int( 11 ) NOT NULL, 
	`approver_id` Int( 11 ) NOT NULL, 
	`status` Enum( 'Confirmed', 'Pending', 'Canceled', 'Declined', 'Accepted' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, 
	`rating` VarChar( 10 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL, 
	`rating_comment` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL, 
	`updated_at` Date NOT NULL, 
	`created_at` Date NOT NULL,
	 PRIMARY KEY ( `agreement_id` )
 )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB
AUTO_INCREMENT = 2;
-- ---------------------------------------------------------


-- Dump data of "agreements" -------------------------------
BEGIN;

INSERT INTO `agreements`(`agreement_id`,`agreement_title`,`description`,`amount`,`sender_id`,`receiver_id`,`approver_id`,`status`,`rating`,`rating_comment`,`updated_at`,`created_at`) VALUES ( '1', 'Android Developer', 'Responsibilities: Responsibilities 
 Academic QualificationEducational Requirements 
 Experience Experience Requirements ', '40000', '62', '11945', '1', 'Accepted', '', '', '2015-04-13', '2015-04-13' );
INSERT INTO `agreements`(`agreement_id`,`agreement_title`,`description`,`amount`,`sender_id`,`receiver_id`,`approver_id`,`status`,`rating`,`rating_comment`,`updated_at`,`created_at`) VALUES ( '2', 'sdf', 'Responsibilities: sdf
 Academic Qualificationwer
 Experience q', '1853', '62', '11945', '1', 'Pending', '', '', '2015-04-20', '2015-04-20' );
COMMIT;
-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


-- CREATE TABLE "aproval" ----------------------------------
DROP TABLE IF EXISTS `aproval` CASCADE;

CREATE TABLE `aproval` ( 
	`aprov_id` Int( 11 ) AUTO_INCREMENT NOT NULL, 
	`status` TinyInt( 4 ) NULL, 
	`created_at` Date NULL, 
	`updated_at` Date NULL, 
	`candidate_id` Int( 11 ) NULL, 
	`employer_id` Int( 11 ) NULL, 
	`deleted_at` TinyInt( 4 ) NULL, 
	`interview_request_id` Int( 11 ) NOT NULL,
	 PRIMARY KEY ( `aprov_id` )
 )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 25;
-- ---------------------------------------------------------


-- Dump data of "aproval" ----------------------------------
BEGIN;

INSERT INTO `aproval`(`aprov_id`,`status`,`created_at`,`updated_at`,`candidate_id`,`employer_id`,`deleted_at`,`interview_request_id`) VALUES ( '9', '1', '2014-06-22', '2014-06-22', '64', '2', NULL, '13' );
INSERT INTO `aproval`(`aprov_id`,`status`,`created_at`,`updated_at`,`candidate_id`,`employer_id`,`deleted_at`,`interview_request_id`) VALUES ( '10', '0', '2014-06-22', '2014-06-22', '63', '2', NULL, '14' );
INSERT INTO `aproval`(`aprov_id`,`status`,`created_at`,`updated_at`,`candidate_id`,`employer_id`,`deleted_at`,`interview_request_id`) VALUES ( '11', '1', '2014-06-22', '2014-06-22', '63', '2', NULL, '15' );
INSERT INTO `aproval`(`aprov_id`,`status`,`created_at`,`updated_at`,`candidate_id`,`employer_id`,`deleted_at`,`interview_request_id`) VALUES ( '12', '1', '2014-06-25', '2014-06-25', '63', '4', NULL, '16' );
INSERT INTO `aproval`(`aprov_id`,`status`,`created_at`,`updated_at`,`candidate_id`,`employer_id`,`deleted_at`,`interview_request_id`) VALUES ( '13', '1', '2014-06-25', '2014-06-25', '70', '4', NULL, '16' );
INSERT INTO `aproval`(`aprov_id`,`status`,`created_at`,`updated_at`,`candidate_id`,`employer_id`,`deleted_at`,`interview_request_id`) VALUES ( '14', '1', '2014-06-25', '2014-06-25', '64', '4', NULL, '17' );
INSERT INTO `aproval`(`aprov_id`,`status`,`created_at`,`updated_at`,`candidate_id`,`employer_id`,`deleted_at`,`interview_request_id`) VALUES ( '15', '1', '2014-06-25', '2014-07-07', '63', '4', NULL, '18' );
INSERT INTO `aproval`(`aprov_id`,`status`,`created_at`,`updated_at`,`candidate_id`,`employer_id`,`deleted_at`,`interview_request_id`) VALUES ( '16', '1', '2014-06-25', '2014-07-07', '64', '4', NULL, '18' );
INSERT INTO `aproval`(`aprov_id`,`status`,`created_at`,`updated_at`,`candidate_id`,`employer_id`,`deleted_at`,`interview_request_id`) VALUES ( '17', '1', '2014-06-25', '2014-07-05', '75', '4', NULL, '18' );
INSERT INTO `aproval`(`aprov_id`,`status`,`created_at`,`updated_at`,`candidate_id`,`employer_id`,`deleted_at`,`interview_request_id`) VALUES ( '18', '0', '2014-06-26', '2014-06-26', '64', '4', NULL, '19' );
INSERT INTO `aproval`(`aprov_id`,`status`,`created_at`,`updated_at`,`candidate_id`,`employer_id`,`deleted_at`,`interview_request_id`) VALUES ( '19', '0', '2014-06-26', '2014-06-26', '8', '4', NULL, '19' );
INSERT INTO `aproval`(`aprov_id`,`status`,`created_at`,`updated_at`,`candidate_id`,`employer_id`,`deleted_at`,`interview_request_id`) VALUES ( '20', '0', '2014-09-03', '2014-09-03', '6230', '15', NULL, '20' );
INSERT INTO `aproval`(`aprov_id`,`status`,`created_at`,`updated_at`,`candidate_id`,`employer_id`,`deleted_at`,`interview_request_id`) VALUES ( '21', '0', '2014-09-03', '2014-09-03', '6253', '15', NULL, '20' );
INSERT INTO `aproval`(`aprov_id`,`status`,`created_at`,`updated_at`,`candidate_id`,`employer_id`,`deleted_at`,`interview_request_id`) VALUES ( '22', '0', '2014-09-03', '2014-09-03', '6283', '15', NULL, '20' );
INSERT INTO `aproval`(`aprov_id`,`status`,`created_at`,`updated_at`,`candidate_id`,`employer_id`,`deleted_at`,`interview_request_id`) VALUES ( '23', '0', '2014-09-03', '2014-09-03', '6234', '14', NULL, '21' );
INSERT INTO `aproval`(`aprov_id`,`status`,`created_at`,`updated_at`,`candidate_id`,`employer_id`,`deleted_at`,`interview_request_id`) VALUES ( '24', '0', '2014-09-03', '2014-09-03', '6236', '14', NULL, '21' );
COMMIT;
-- ---------------------------------------------------------


-- CREATE INDEX "interview_request_id" ---------------------
CREATE INDEX `interview_request_id` USING BTREE ON `aproval`( `interview_request_id` );
-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


