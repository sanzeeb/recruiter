/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50616
Source Host           : localhost:3306
Source Database       : jobsinbd_djit

Target Server Type    : MYSQL
Target Server Version : 50616
File Encoding         : 65001

Date: 2014-11-13 17:34:45
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for skills
-- ----------------------------
DROP TABLE IF EXISTS `skills`;
CREATE TABLE `skills` (
  `skill_id` int(11) NOT NULL AUTO_INCREMENT,
  `skill_name` varchar(200) NOT NULL,
  `skill_description` text,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `skill_photo` varchar(70) DEFAULT NULL,
  `display_order` int(11) DEFAULT NULL,
  `deleted_at` varchar(100) NOT NULL DEFAULT '0',
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`skill_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of skills
-- ----------------------------
INSERT INTO `skills` VALUES ('1', 'php', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the.', '2014-06-09', '2014-11-13', 'PHP.jpg', '2', '0', '1');
INSERT INTO `skills` VALUES ('2', 'JavaScript', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the.', '0000-00-00', '2014-11-13', 'JavaScript.jpg', '4', '0', '0');
INSERT INTO `skills` VALUES ('3', 'microcontroller_programming', 'microcontroller_programming expert', '2014-06-15', '2014-08-20', 'micro.jpg', '6', '0', '0');
INSERT INTO `skills` VALUES ('4', 'asp.net', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the.', '2014-06-21', '2014-11-13', '3.jpg', '8', '0', '1');
INSERT INTO `skills` VALUES ('5', 'C#', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the.', '2014-06-21', '2014-11-13', 'C_.jpg', '1', '0', '0');
INSERT INTO `skills` VALUES ('6', 'oracle', 'oracle expert', '2014-06-21', '2014-09-09', 'oracle.jpg', '9', '0', '0');
INSERT INTO `skills` VALUES ('7', 'linux', 'linux expert', '2014-06-21', '2014-06-21', 'linux.jpg', '5', '0', '0');
INSERT INTO `skills` VALUES ('8', 'MySql', 'MySql Expert', '2014-06-21', '2014-06-21', 'mysql.jpg', '8', '0', '0');
INSERT INTO `skills` VALUES ('9', 'c++/vc++', 'System and Kernel Level programming. Network Security Application. Application and Game development', '2014-06-21', '2014-11-13', 'C++.jpg', '1', '0', '1');
INSERT INTO `skills` VALUES ('10', 'java', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the.', '2014-08-20', '2014-11-13', 'java.jpg', '3', '0', '1');
INSERT INTO `skills` VALUES ('11', 'Embedded', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the.', '2014-08-20', '2014-11-13', 'Embedded.jpg', '6', '0', '1');
INSERT INTO `skills` VALUES ('12', 'Game', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the.', '2014-08-20', '2014-11-13', 'Game.jpg', '5', '0', '1');
INSERT INTO `skills` VALUES ('13', 'iOS', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the.', '2014-08-27', '2014-11-13', 'iOS.jpg', '4', '0', '1');
INSERT INTO `skills` VALUES ('14', 'C', 'System and Kernel Level programming. Network Security Application. Application and Game development', '2014-11-13', '2014-11-13', 'C.png', '1', '0', '0');
INSERT INTO `skills` VALUES ('15', 'android', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the.', '2014-11-13', '2014-11-13', 'android.jpg', '7', '0', '1');
INSERT INTO `skills` VALUES ('16', 'photoshop', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the.', '2014-11-13', '2014-11-13', 'photoshop.png', '9', '0', '1');
