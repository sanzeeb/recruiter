/**
 * Created by HimelC51 on 11/17/14.
 */
 
$(function(){
    $("#approveBtn").click(function(){
        var obj = $(this);
        var formData = $("#approveInqueryFrm").serialize();
        $.ajax({
            type:"POST",
            url : BASE + 'dashboard/approve-inquery',
            data: formData,
            beforeSend:function(){
                $("#cancelBtn").attr('disabled','disabled');
                $("#approveBtn").attr('disabled','disabled');
            },
            success:function(e){
                window.location.reload();
            }
        });
    });

    $(".sent_disagree").click(function(){
        $(this).hide().after('Processing...');
    });

    $("#cancelBtn").click(function(){
        var obj = $(this);
        var formData = $("#approveInqueryFrm").serialize();
        $.ajax({
            type: "POST",
            url : BASE+ 'dashboard/cancel-inquiry',
            data: formData,
            beforeSend:function(){
                   $("#cancelBtn").hide().after('<p>Processing...</p>');
            },
            success:function(e)
            {
                window.location.reload();
                $("#cancelBtn").next().remove();
            }
        });
    });

    $("#replyBtn").click(function(){
        var obj = $(this);
        var formData = $("#approveInqueryFrm").serialize();
        var comment = $("textarea[name='reply']").val();
        
        var phoneWeburlEmail = /((^[0][1-9]|\d+){6,14}|http:\/\/.+\.([a-z]{3,}|[a-z]{3,}\.[a-z]{2,3})|[a-z]+\.[a-z]{3,}|[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-z]{2,})/
        if(phoneWeburlEmail.test(comment))
        {
                $("textarea[name='reply']").attr('placeholder', 'Sorry! You cannot write any phone no, email or web url').val('');
                return false;

        } else{ 

            if(comment != ""){
                $.ajax({
                    type:"POST",
                    url : BASE + 'dashboard/reply-inquery',
                    data: formData,
                    beforeSend:function(){
                        $("#replyBtn").hide().after('<p>Processing...</p>');
                    },
                    success:function(e){
                       window.location.reload();
                       $("#replyBtn").next().remove();
                       
                      
                    }
                });
                
            }else{
                $("textarea[name='reply']").css('border-color','red');
            }
            
         }
    });
});