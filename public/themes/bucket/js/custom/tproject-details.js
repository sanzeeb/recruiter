/**
 * Created by HimelC51 on 11/17/14.
 */
$(function(){
    $("#approveBtn").click(function(){
        var obj = $(this);
        var formData = $("#approveInqueryFrm").serialize();
        $.ajax({
            type:"POST",
            url : BASE + 'tproject/approve',
            data: formData,
            beforeSend:function(){
                $("#cancelBtn").attr('disabled','disabled');
                $("#approveBtn").attr('disabled','disabled');
            },
            success:function(e){
                window.location.reload();
                $("#approveBtn").removeAttr('disabled');
                $("#cancelBtn").removeAttr('disabled');
            }
        });
    });

    $("#approveReviewBtn").click(function(){
        var obj = $(this);

        var formData = $("#approveInqueryFrm").serialize();
        $.ajax({
            type: "POST",
            url : BASE + 'tproject/review-approve',
            data: formData,
            beforeSend: function(){
                $("#cancelReviewTproject").attr('disabled','disabled');
                $("#approveReviewBtn").attr('disabled','disabled');
            },
            success:function(e)
            {
                window.location.reload();
                $("#approveReviewBtn").removeAttr('disabled');
                $("#cancelReviewTproject").removeAttr('disabled');
            }
        });
    });

    $("#replyBtn").click(function(){
        var obj = $(this);
        var formData = $("#approveInqueryFrm").serialize();
        var ccomment = $("#ccomment").val();

        var phoneWeburlEmail = /((^[0][1-9]|\d+){6,14}|http:\/\/.+\.([a-z]{3,}|[a-z]{3,}\.[a-z]{2,3})|[a-z]+\.[a-z]{3,}|[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-z]{2,})/
        
        if(phoneWeburlEmail.test(ccomment))
        {
                $("#ccomment").attr('placeholder', 'Sorry! You cannot write any phone no, email or web url').val('');
                return false;

        } else{ 
            
            if(ccomment != ""){
                
                $.ajax({
                    type:"POST",
                    url : BASE + 'dashboard/reply-tproject',
                    data: formData,
                    beforeSend:function(){
                        $("#replyBtn").attr('disabled','disabled');
                    },
                    success:function(e){
                        $("#replyBtn").removeAttr('disabled');
                       window.location.reload();
                    }
                });

            }else{
                $("textarea[name='reply']").css('border-color','red');
            }
        }

    });

    $(".approveComment").click(function(){
        var obj = $(this);
        var formData = $("#approveInqueryFrm").serialize();

        $.ajax({
            type:"POST",
            url : BASE + 'tproject/reply-accept',
            data: formData,
            beforeSend:function()
            {
                $(".approveComment").attr('disabled','disabled');
            },
            success:function(e)
            {
               $(".approveComment").removeAttr('disabled');
               window.location.reload();
            }
        });
        
    });

    $("#cancelBtn").click(function(){
        var obj = $(this);
        var formData = $("#approveInqueryFrm").serialize();
        $.ajax({
            type:"POST",
            url : BASE + 'tproject/cancel',
            data: formData,
            beforeSend:function(){
                $("#cancelBtn").attr('disabled','disabled');
                $("#approveBtn").attr('disabled','disabled');
            },
            success:function(e){
                window.location.reload();
                $("#approveBtn").removeAttr('disabled');
                $("#cancelBtn").removeAttr('disabled');
            }
        });
    });

    $("#replyReasonFrm").submit(function(){
        var comment = $("#replyReasonFrm textarea[name='comment']").val();
        var phoneWeburlEmail = /((^[0][1-9]|\d+){6,14}|http:\/\/.+\.([a-z]{3,}|[a-z]{3,}\.[a-z]{2,3})|[a-z]+\.[a-z]{3,}|[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-z]{2,})/
        
        if(phoneWeburlEmail.test(comment))
        {
                $("#replyReasonFrm textarea[name='comment']").attr('placeholder', 'Sorry! You cannot write any phone no, email or web url').val('');
                return false;

        }
    });

    $("#cancelSubmissionFrm").submit(function(){
        var comment = $("#cancelSubmissionFrm textarea[name='comment']").val();
        var phoneWeburlEmail = /((^[0][1-9]|\d+){6,14}|http:\/\/.+\.([a-z]{3,}|[a-z]{3,}\.[a-z]{2,3})|[a-z]+\.[a-z]{3,}|[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-z]{2,})/
        
        if(phoneWeburlEmail.test(comment))
        {
                $("#cancelSubmissionFrm textarea[name='comment']").attr('placeholder', 'Sorry! You cannot write any phone no, email or web url').val('');
                return false;

        }
    });

    

    $("#cancelReplyFrm").submit(function(){
        var comment = $("#cancelReplyFrm textarea[name='comment']").val();
        var phoneWeburlEmail = /((^[0][1-9]|\d+){6,14}|http:\/\/.+\.([a-z]{3,}|[a-z]{3,}\.[a-z]{2,3})|[a-z]+\.[a-z]{3,}|[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-z]{2,})/
        
        if(phoneWeburlEmail.test(comment))
        {
                $("#cancelReplyFrm textarea[name='comment']").attr('placeholder', 'Sorry! You cannot write any phone no, email or web url').val('');
                return false;

        }
    });

});