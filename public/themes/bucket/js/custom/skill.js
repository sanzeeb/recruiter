/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


function editSkill(id) {

    $.post('./get-skill', {id: id}, function(data) {

        if (data == "500") {
            window.location.reload();

        } else {
            $("#eskill_name").val(data.skill_name);
            $("#eskill_description").val(data.skill_description);
            $("#eorder").val(data.display_order);
            $("#skill_id").val(id);
        }

    });

}



function editGa(id) {

    $.post('./get-ga', {id: id}, function(data) {

        if (data == "500") {
            window.location.reload();

        } else {
            $("#ga_title").val(data.title);
            $("#ga_description").val(data.description);

            $("#ga_id").val(id);
        }

    });

}

function deleteGa(id) {

    if (confirm("Are you sure?")) {

        $.post('./delete-ga', {id: id}, function(data) {

            if (data == "500") {
                window.location.reload();

            } else {
                alert("Delete succesfully");
                window.location.reload();
            }
        });
    }

}


function deleteSkill(id) {

    if (confirm("Are you sure?")) {

        $.post('./delete-skill', {id: id}, function(data) {

            if (data == "500") {
                window.location.reload();

            } else {
                alert("Delete succesfully");
                window.location.reload();
            }
        });
    }

}

function deleteClient(id) {

    if (confirm("Are you sure?")) {

        $.post('./delete-client', {id: id}, function(data) {

            if (data == "500") {
                window.location.reload();

            } else {
                alert("Delete succesfully");
                window.location.reload();
            }
        });
    }

}

function updateSkill() {

    var name = $("#eskill_name").val();
    var description = $("#eskill_description").val();
    var order = $("#eorder").val();
    var id = $("#skill_id").val();
    $.post('./update-skill', {id: id, name: name, description: description, order: order}, function(data) {
        if (data == "500") {
            window.location.reload();

        } else {

            window.location.reload();
        }

    });

}

function updateGallery() {

    var name = $("#ga_title").val();
    var description = $("#ga_description").val();

    var id = $("#ga_id").val();
    $.post('./update-ga', {id: id, name: name, description: description}, function(data) {
        if (data == "500") {
            window.location.reload();

        } else {

            window.location.reload();
        }

    });

}

$("input[name=status]").change(function(){
    var obj = $(this);

    $.ajax({
       type : "POST",
       url  : BASE + 'skills/update-status',
       data : {id:obj.attr('data-skill-id'),status:obj.val()},
       success:function(e)
       {

       }
    });
});
