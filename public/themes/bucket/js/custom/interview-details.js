/**
 * Created by HimelC51 on 11/17/14.
 */
$(function(){
    $("#cancelBtn").removeAttr('disabled');
    $("#approveBtn").removeAttr('disabled');
    $("#replyBtn").removeAttr('disabled');

    $("#approveBtn").click(function(){
        var obj = $(this);
        var formData = $("#approveInterviewFrm").serialize();
        $.ajax({
            type:"POST",
            url : BASE + 'dashboard/approve-interview',
            data: formData,
            beforeSend:function(){
                $("#cancelBtn").attr('disabled','disabled');
                $("#approveBtn").attr('disabled','disabled');
            },
            success:function(e){

                window.location.reload();
            }
        });
    });

    $("#approveAcceptBtn").click(function(){
        var obj = $(this);
        var formData = $("#approveInterviewFrm").serialize();
        $.ajax({
            type:"POST",
            url : BASE + 'dashboard/approve-accept-interview',
            data: formData,
            beforeSend:function(){
                $("#cancelBtn").attr('disabled','disabled');
                $("#approveBtn").attr('disabled','disabled');
            },
            success:function(e){

                window.location.reload();
            }
        });
    });

   /* $("#cancelBtn").click(function(){
        var obj = $(this);
        var formData = $("#approveInterviewFrm").serialize();
        $.ajax({
            type:"POST",
            url : BASE + 'dashboard/cancel-interview',
            data: formData,
            beforeSend:function(){
                $("#cancelBtn").attr('disabled','disabled');
            },
            success:function(e){
                $("#cancelBtn").removeAttr('disabled');
                window.location.reload();
            }
        });
    });*/

    $("#replyBtn").click(function(){
        var obj = $(this);
        var formData = $("#approveInterviewFrm").serialize();
        var comment = $("textarea[name='reply']").val();
        
        var phoneWeburlEmail = /((^[0][1-9]|\d+){6,14}|http:\/\/.+\.([a-z]{3,}|[a-z]{3,}\.[a-z]{2,3})|[a-z]+\.[a-z]{3,}|[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-z]{2,})/
        if(phoneWeburlEmail.test(comment))
        {
             

                $("textarea[name='reply']").attr('placeholder', 'Sorry! You cannot write any phone no, email or web url').val('');
                return false;

            
        } else{ 

            $.ajax({
                type:"POST",
                url : BASE + 'dashboard/reply-interview',
                data: formData,
                beforeSend:function(){
                    $("#replyBtn").attr('disabled','disabled');
                },
                success:function(e){
                    $("#replyBtn").removeAttr('disabled');
                   window.location.reload();
                }
            });
        }
    });

    $("#acceptBtn").click(function(){
        var obj = $(this);
        var formData = $("#approveInterviewFrm").serialize();
        $.ajax({
            type:"POST",
            url : BASE + 'dashboard/accept-interview',
            data: formData,
            beforeSend:function(){
                $("#acceptBtn").attr('disabled','disabled');
            },
            success:function(e){
                $("#acceptBtn").removeAttr('disabled');
                window.location.reload();
            }
        });
    });

    /*$("#declinedBtn").click(function(){
        var obj = $(this);
        var formData = $("#approveInterviewFrm").serialize();
        $.ajax({
            type:"POST",
            url : BASE + 'dashboard/decline-interview',
            data: formData,
            beforeSend:function(){
                $("#declinedBtn").attr('disabled','disabled');
            },
            success:function(e){
                $("#declinedBtn").removeAttr('disabled');
                window.location.reload();
            }
        });
    });*/

    $("#progressBtn").click(function(){
        var obj = $(this);
        var formData = $("#approveInterviewFrm").serialize();
        $.ajax({
            type:"POST",
            url : BASE + 'dashboard/progress-interview',
            data: formData,
            beforeSend:function(){
                $("#progressBtn").attr('disabled','disabled');
            },
            success:function(e){
                $("#progressBtn").removeAttr('disabled');
                window.location.reload();
            }
        });
    });

    $("#completedBtn").click(function(){
        var obj = $(this);
        var formData = $("#approveInterviewFrm").serialize();
        $.ajax({
            type:"POST",
            url : BASE + 'dashboard/completed-interview',
            data: formData,
            beforeSend:function(){
                $("#completedBtn").attr('disabled','disabled');
            },
            success:function(e){
                $("#completedBtn").removeAttr('disabled');
                window.location.reload();
            }
        });
    });

    $("#hiredBtn").click(function(){
        var obj = $(this);
        var formData = $("#approveInterviewFrm").serialize();
        var feedback = $('select[name=feedback]').val();
        if(feedback)
        {
            $.ajax({
                type:"POST",
                url : BASE + 'dashboard/hired-interview',
                data: formData,
                beforeSend:function(){
                    $("#hiredBtn").attr('disabled','disabled');
                },
                success:function(e){
                    $("#hiredBtn").removeAttr('disabled');
                    window.location = BASE + 'agreement/lists';
                }
            });
        }else{
            alert('Please select feedback before hire');
        }

    });

    $("#replyPreferedTime").click(function(){
        var my_date = $("input[name='your_date']").val();
        var my_time = $("input[name='your_time']").val();
        var id      = $("input[name='id']").val();
        var obj = $(this);
        
        var phoneWeburlEmail = /((^[0][1-9]|\d+){6,14}|http:\/\/.+\.([a-z]{3,}|[a-z]{3,}\.[a-z]{2,3})|[a-z]+\.[a-z]{3,}|[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-z]{2,})/

        if(phoneWeburlEmail.test(my_date))
        {
            $("input[name='your_date']").attr('placeholder', 'Sorry! You cannot write any phone no, email or web url').val('');
            return false;
        }
        if(phoneWeburlEmail.test(my_time))
        {
            $("input[name='your_time']").attr('placeholder', 'Sorry! You cannot write any phone no, email or web url').val('');
            return false;
        }

        $.ajax({
            type:"POST",
            url : BASE + 'interview/candidate-reply',
            data:{my_date:my_date,my_time:my_time,id:id},
            beforeSend:function()
            {
                obj.attr('disabled','disabled');
            },
            success:function(e)
            {
                window.location.reload();
                obj.removeAttr('disabled');
            }
        });
    });

    $("#approveReply").click(function(){
        var obj = $(this);
        $.ajax({
            type:"POST",
            url : BASE + "interview/accept-interview-reply",
            data: {ir_id:obj.attr('href')},
            success:function(e)
            {
                window.location.reload();
            }
        });
        return false;
    }); 

    $("input[name='interview_date']").datepicker({
         format: 'yyyy-mm-dd'
    });
    $("input[name='your_date']").datepicker({
        format: 'yyyy-mm-dd'
    });

    $("input[name='interview_time'],input[name='your_time']").timepicker({});

});