/**
 * Created by Himel on 6/9/14.
 */
$(function(){
    $("#addResponsibilityParam").live('click',function(){
        var obj = $(this);
        var htmlObj =  '<tr>';
        htmlObj += '<td><input type="text" class="form-control col-lg-2" name="responsibility[]" required/></td>';
        htmlObj += '<td><a class="delParam" href="#"><i class="fa fa-minus-circle"></i></a></td>';
        htmlObj += '</tr>';
        $("#ResponsibilityParamTbl tbody").append(htmlObj);
        return false;
    });



    $("#addEducationalParam").live('click',function(){
        var obj = $(this);
        var htmlObj =  '<tr>';
        htmlObj += '<td><input type="text" class="form-control col-lg-2" name="educational[]" required/></td>';
        htmlObj += '<td><a class="delParam" href="#"><i class="fa fa-minus-circle"></i></a></td>';
        htmlObj += '</tr>';
        $("#EducationalParamTbl tbody").append(htmlObj);
        return false;
    });

    $("#addExperienceParam").live('click',function(){
        var obj = $(this);
        var htmlObj =  '<tr>';
        htmlObj += '<td><input type="text" class="form-control col-lg-2" name="experience[]" required/></td>';
        htmlObj += '<td><a class="delParam" href="#"><i class="fa fa-minus-circle"></i></a></td>';
        htmlObj += '</tr>';
        $("#ExperienceParamTbl tbody").append(htmlObj);
        return false;
    });

    $("#addAdditionalParam").live('click',function(){
        var obj = $(this);
        var htmlObj =  '<tr>';
        htmlObj += '<td><input type="text" class="form-control col-lg-2" name="additional[]"/></td>';
        htmlObj += '<td><a class="delParam" href="#"><i class="fa fa-minus-circle"></i></a></td>';
        htmlObj += '</tr>';
        $("#AdditionalParamTbl tbody").append(htmlObj);
        return false;
    });

    $(".delParam").live('click',function(){
        var obj = $(this);
        obj.parent().parent().remove();
        if($("#paramTbl tbody tr").length == 0)
        {
            $("#saveParam").hide();
        }
        return false;
    });

    $(".job_del").live('click',function(){
        var obj = $(this);
        bootbox.confirm('Are you sure to delete this job',function(result){
            if(result)
            {
                console.log('here')
             $.ajax({
               type:"POST",
               url : BASE + 'job/del-job',
               data:{ job_id: obj.attr('href')},
               success:function(e)
               {
                   if(!e == 500)
                   {
                        obj.parent().parent().remove();
                        window.location.reload();
                   }
                   else
                       window.location.reload();
               }
            });
                
            }
        });

           
        
        return false;
    });

    $("#jobSearchFrm").submit(function(){
        var obj = $(this);

        $.ajax({
            type:"POST",
            url : obj.attr('action'),
            data: obj.serialize(),
            success:function(e)
            {
                
                var data = e;
                var trObj = '';
                if(data.length)
                {
                    $.each(data,function(i,el){
                        trObj +='<tr>';
                        trObj +='<td>'+(i+1)+'</td>';
                        trObj +='<td>'+data[i].job_title+'</td>';
                        trObj +='<td>'+data[i].skill.skill_name+'</td>';
                        trObj +='<td>'+data[i].job_vacancies+'</td>';
                        trObj +='<td>'+data[i].salary_range+'</td>';
                        trObj +='<td>';
                        trObj += '<a href="'+BASE+'job/edit/'+ data[i].jobs_id + '"class="grid-action-link job_edit"><i class="fa fa-pencil" title="Edit"></i></a>';
                        trObj += '<a href="'+BASE+'job/view/'+data[i].jobs_id+'" class="grid-action-link job_view"><i class="fa fa-eye" title="View"></i></a>';
                        trObj += '<a href="'+data[i].jobs_id+'" class="grid-action-link job_del"><i class="fa fa-trash-o" title="Delete"></i></a>';
                        trObj +='</td>';
                        trObj +='</tr>';
                    });
                }
                $("table.jobslists tbody").html(trObj);
            }
        });
        return false;
    });

    $("#saveJobFrm").submit(function(){
        var responsibilityCount =  $("table#ResponsibilityParamTbl tbody").children().length;
        var educationalCount =  $("table#EducationalParamTbl tbody").children().length;
        var experienceCount =  $("table#ExperienceParamTbl tbody").children().length;

        if(responsibilityCount == 0)
        {
            alert('Please add Responsibility for job post');
            return false;
        }
        if(educationalCount == 0)
        {
            alert('Please add Educational requirements for job post');
            return false;
        }
        if(experienceCount == 0)
        {
            alert('Please add Experience for job post');
            return false;
        }
    });

    

});