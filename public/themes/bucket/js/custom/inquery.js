/**
 * Created by HimelC51 on 11/16/14.
 */
$(function(){

    
    var optionHtml = '<option value="">Select Job</option>';
    $.each(jobs,function(i,elm){
        if(job && jobs[i].job_id == job)
            optionHtml += '<option selected="selected" value="'+jobs[i].job_id+'">'+jobs[i].title+'</option>';
        else
            optionHtml += '<option value="'+jobs[i].job_id+'">'+jobs[i].title+'</option>';
    });


    $("select[name=joblist]").html(optionHtml);

    var jobSelected = $("select[name=joblist]").val();
    if(jobSelected)
    {
       var jobDescription = jobs[jobSelected].description;
        $("#job_description").val(jobDescription);
    }   

    $("select[name=joblist]").change(function(){
        var jobDescription = jobs[$(this).val()].description;
        $("#job_description").val(jobDescription);
    });

    $("#inqueryFrm").submit(function(){
        var jobList = $("select[name=joblist]").val();
        var description = $("textarea[name=job_description]").val();
        var inquiry = $("textarea[name=inquiry]").val();

        if(jobList =="")
        {
            alert('Please select job');
            return false;
        }
        if (description == "")
        {
            alert('Please write job description');
            return false;
        }
        if(inquiry == "")
        {
            alert('Please write your inquiry messages');
            return false;
        }

    });




});