/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
var ratingStar = 0;
$(function() {

    $(".view-agree").live('click', function() {

        var obj = $(this);
        var dataId = obj.attr('data-id');
        $("#agreement_id").val(dataId);


        $.ajax({
            type: "POST",
            url: BASE + 'agreement/get-agreement',
            data: {agreement_id: dataId},
            success: function(e)
            {

                data = $.parseJSON(e);
                

                $('#eamount').val(data.amount);
                $('#edescription').val(data.description);
                $("#agreement_id").val(data.agreement_id);
                $('#etitle').val(data.agreement_title);
                $('#ecandidate').val(data.firstname + " " + data.lastname);
                $('#eemployer').val(data.title);

                //$("#agreement-form").html("Agreement sent successfully");
            }
        });
return false;

    });


    $(".disagree").live('click', function() {

        var obj = $(this);
        var dataId = obj.attr('data-id');
        $("#dis_agreement_id").val(dataId);

    });


    $(".agree").live('click', function() {


        var obj = $(this);
        var dataId = obj.attr('data-id');


        if (confirm("Are you sure  ?")) {
            $.ajax({
                type: "POST",
                url: BASE + 'agreement/update-agreement',
                data: {agreement_id: dataId},
                success: function(e)
                {

                   // alert("Agreement agreed successfully");
                   window.location.reload();

                    //$("#agreement-form").html("Agreement sent successfully");
                }
            });
        }

        return false;
    });
    
    
    
    $(".paid").live('click', function() {


        var obj = $(this);
        var dataId = obj.attr('data-id');

        if (confirm("Are you sure  ?")) {
            $.ajax({
                type: "POST",
                url: BASE + 'payment/paid-amount',
                data: {dataId: dataId},
                success: function(e)
                {

                    alert("Payment paid successfully");
                    window.location.reload();

                    //$("#agreement-form").html("Agreement sent successfully");
                }
            });
        }
     return false;
    });
    
    $(".cancel-confirm").live('click', function() {
        
    });
    $(".cancel").live('click', function() {


        var obj = $(this);
        var dataId = obj.attr('data-id');

        if (confirm("Are you sure  ?")) {
            $.ajax({
                type: "POST",
                url: BASE + 'payment/paid-cancel',
                data: {dataId: dataId},
                success: function(e)
                {

                    alert("Payment cancel successfully");
                    window.location.reload();

                    //$("#agreement-form").html("Agreement sent successfully");
                }
            });
        }
     return false;
    });

    $(".sent_disagree").live('click', function() {


        var disdescription = $("#disdescription").val();
        var dis_agreement_id = $("#dis_agreement_id").val();

        if (disdescription != "") {
            if (confirm("Are you sure ?")) {
                $.ajax({
                    type: "POST",
                    url: BASE + 'agreement/sent-disagreement',
                    data: {dis_agreement_id: dis_agreement_id, disdescription: disdescription},
                    success: function(e)
                    {

                        $('#disdescription').val("");
                      //  alert("Disagreement sent successfully");
                       window.location.reload();

                        //$("#agreement-form").html("Agreement sent successfully");
                    }
                });
            }
        } else {

            alert("Please enter description");
        }
        
        return false;
        
    });


    $(".starRate a").click(function(){
        var obj=$(this);
        obj.addClass('selected')
        obj.parent().prevAll().children().removeClass('selected');
        obj.parent().nextAll().children().addClass('selected');
        ratingStar = eval(obj.attr('href'));
        return false;
    });

    $("#clientRatingFrm").submit(function(){
        if(ratingStar > 0)
        {
            var comment = $("textarea[name=comment]").val();
            var agreementId = $("input[name=agreement_id]").val();
            $.ajax({
                type:"POST",
                url : BASE + 'agreement/agreement-rating',
                data: {rating:ratingStar, comment:comment,agreementId:agreementId },
                success:function(e)
                {
                    window.location.reload();
                }
            });

        }else{

            alert('Please select star for rating.');
        }
        return false;
    });



});