/**
 * Created by Himel on 6/19/14.
 */
$(function(){
   var checkMySchedule = $("input[name=checkMySchedule]");
    checkMySchedule.removeAttr('checked');
    checkMySchedule.change(function(){
      var obj = $(this);

        changeOption(obj);

   });

  function changeOption(obj)
  {
      if(obj.attr('checked'))
      {
          $("div.checkSchedule").removeClass('hidden');
          $("div.checkSchedule").find('input,select').removeAttr('disabled');
      }else{
          $("div.checkSchedule").addClass('hidden','hidden');
          $("div.checkSchedule").find('input,select').attr('disabled','disabled');
      }
  }


  $("select[name=recruiter]").change(function(){
      var obj = $(this);
      $.ajax({
        type:"POST",
        url : BASE + 'job/get-job',
        data:  {recruiter_id: obj.val()},
        beforeSend:function(){
          $("select[name=joblist]").children().remove();  
        },
        success:function(e)
        {
            var data = e;

          if(data.length > 0)
          {
              var opObj = '';
              $.each(data, function(i,elm){
                  opObj += '<option value="'+data[i].jobs_id+'">'+data[i].job_title+'</option>';
              });
              $("select[name=joblist]").html(opObj);
          }
        }
      });
  });
  
  $("#searchCandidateFrm").submit(function(){
    var obj = $(this);
    $.ajax({
        type:"POST",
        url : BASE+"interview/get-available-candidates",
        data: obj.serialize(),
        beforeSend:function()
        {
            var trObj = '';
            trObj += '<tr><td colspan="5" class="text-center">Loading</td></tr>';
            $("#candidateTable tbody").html(trObj);
        },
        success:function(e)
        {
            var trObj='';
            if(e.status == 200)
            {
                  var data = e.data;
                  
                  $.each(data,function(i,elm){
                      trObj += '<tr>';
trObj += '<td><input type="checkbox" name="candidates[]" class="candidates_chk" value="'+data[i].candidateObj.candidate_id+'"/></td>';
                      trObj += '<td>'+data[i].created_at+'</td>';
                      trObj += '<td>'+data[i].candidateObj.firstname+'</td>';
                      trObj += '<td>'+data[i].cv_tbl.year_of_exp+'</td>';
                      trObj += '<td>'+data[i].available_date+'</td>';
                      trObj += '<td>'+data[i].from_time+'-'+data[i].to_time+'</td>';
                      
                      trObj += '</tr>';
                  });
                  $("#candidateTable tbody").html(trObj);
            }


                        
                        
        }
    });
    return false;
  });
  
  
  $("#saveRecruiterRequestFrm").submit(function(){
      var recruiterVal = $("select[name=recruiter]").val();
      var jobVal = $("select[name=joblist]").val();
      var rowCount = $('#candidateTable >tbody >tr').length;
      var obj = $(this);
      if(recruiterVal)
      {
          if(rowCount>0 && $(".candidates_chk:checked").length){
              $.ajax({
                  type:"POSt",
                  url : BASE + 'interview/invite-for-interview',
                  data: obj.serialize(),
                  success:function(e)
                  {
                      if(e.status==200)
                      {
                        $(".candidates_chk:checked").parent().parent().remove();
                        CustomMessage.printMessage(e.status, e.msg, $("#msg"), null);
                      }
                  }
              });
          }else{
              alert("Please select candidate");   
          }
          
      }else{
          alert('Please Select recruiter');
      }
      return false;
  });
});