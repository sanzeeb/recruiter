/**
 * Created by HimelC51 on 11/18/14.
 */
$(function(){


    var optionHtml = '<option value="">Select Job</option>';
    $.each(jobs,function(i,elm){
        if(job && jobs[i].job_id == job)
            optionHtml += '<option selected="selected" value="'+jobs[i].job_id+'">'+jobs[i].title+'</option>';
        else
            optionHtml += '<option value="'+jobs[i].job_id+'">'+jobs[i].title+'</option>';

    });

    $("select[name=joblist]").html(optionHtml);
    var jobSelected = $("select[name=joblist]").val();
    if(jobSelected)
    {
       var jobResponsiblity = jobs[jobSelected].responsibilities;
        $("#responsibilities").val(jobResponsiblity);

        var jobEdu = jobs[jobSelected].edu;
        $("#edu_qualification").val(jobEdu);
        
        var jobExp = jobs[jobSelected].exp;
        $("#exp").val(jobExp);

        var jobSalary = jobs[jobSelected].salary;
        $("input[name=salary]").val(jobSalary);

        var jobnature = jobs[jobSelected].nature;
        $("input[name=nature]").val(jobnature);
    }  

    $("select[name=joblist]").change(function(){
        var jobResponsiblity = jobs[$(this).val()].responsibilities;
        var jobEdu = jobs[$(this).val()].edu;
        var jobExp = jobs[$(this).val()].exp;
        var jobSalary = jobs[$(this).val()].salary;
        var jobnature = jobs[$(this).val()].nature;
        $("#responsibilities").val(jobResponsiblity);
        $("#edu_qualification").val(jobEdu);
        $("#exp").val(jobExp);
        $("input[name=salary]").val(jobSalary);
        $("input[name=nature]").val(jobnature);
    });

    $("#interviewFrm").submit(function(){
        var message = $("select[name=message]").val();
        if(message =="")
        {
            alert('Please select job');
            return false;
        }
    });

    $("input[name='interview_date']").datepicker({
         dateFormat: 'yy-mm-dd'
    });

    $("input[name='interview_time']").timepicker({});

});