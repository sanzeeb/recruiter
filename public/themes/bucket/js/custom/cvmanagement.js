/**
 * Created by Himel on 6/5/14.
 */
$(function() {

    $("#addEduParam").live('click', function() {
        var obj = $(this);
        var htmlObj = '<tr>';
        htmlObj += '<td><input type="text" class="form-control col-lg-2" name="title[]"/></td>';
        htmlObj += '<td><input type="text" class="form-control col-lg-2" name="major[]"/></td>';
        htmlObj += '<td><input type="text" class="form-control col-lg-2" name="institute[]"/></td>';
        htmlObj += '<td><input type="text" class="form-control col-lg-2" name="result[]"/></td>';
        htmlObj += '<td><input type="text" class="form-control col-lg-2" name="passing_year[]"/></td>';
        htmlObj += '<td><a class="delParam" data-type="e" href="#"><i class="fa fa-minus-circle"></i></a></td>';
        htmlObj += '</tr>';
        $("#eduParamTbl tbody").append(htmlObj);
        return false;
    });

    $("#addWorkParam").live('click', function() {
        var obj = $(this);
        var htmlObj = '<tr>';
        htmlObj += '<td><input type="text" class="form-control col-lg-2" name="job_title[]"/></td>';
        htmlObj += '<td><input type="text" class="form-control col-lg-2" name="start_dt[]"/></td>';
        htmlObj += '<td><input type="text" class="form-control col-lg-2" name="end_dt[]"/></td>';
        htmlObj += '<td><input type="text" class="form-control col-lg-2" name="company[]"/></td>';
        htmlObj += '<td><input type="text" class="form-control col-lg-2" name="designation[]"/></td>';
        htmlObj += '<td><input type="text" class="form-control col-lg-2" name="department[]"/></td>';
        htmlObj += '<td><a class="delParam" data-type="j" href="#"><i class="fa fa-minus-circle"></i></a></td>';
        htmlObj += '</tr>';
        $("#workParamTbl tbody").append(htmlObj);
        return false;
    });

    $("#addPqParam").live('click', function() {
        var obj = $(this);
        var htmlObj = '<tr>';
        htmlObj += '<td><input type="text" class="form-control col-lg-2" name="q_title[]"/></td>';
        htmlObj += '<td><input type="text" class="form-control col-lg-2" name="q_institute[]"/></td>';
        htmlObj += '<td><input type="text" class="form-control col-lg-2" name="q_year[]"/></td>';
        htmlObj += '<td><a class="delParam" data-type="q" href="#"><i class="fa fa-minus-circle"></i></a></td>';
        htmlObj += '</tr>';
        $("#pqParamTbl tbody").append(htmlObj);
        return false;
    });

    $(".sent-agrrement").live('click', function() {

        var candidate_id = $("#candidate_id").val();

        var eamount = $('#eamount').val();
        var title = $('#etitle').val();
        var description = $('#edescription').val();

        if (candidate_id != "") {
            if (confirm("Are you sure ?")) {
                $.ajax({
                    type: "POST",
                    url: BASE + 'agreement/sent-agreement',
                    data: {candidate_id: candidate_id, title: title, description: description, amount: eamount},
                    success: function(e)
                    {

                        $('#eamount').val("");
                        $('#edescription').val("");
                        $("#candidate_id").val("");
                        $('#etitle').val("");
                        alert("Agreement sent successfully");
                        window.location.reload();

                        //$("#agreement-form").html("Agreement sent successfully");
                    }
                });
            }
        } else {
            alert("Select candidate and try again");
        }
        return false;
    });
    $(".offer-now").live('click', function() {

        var obj = $(this);
        var dataType = obj.attr('data-id');
        $("#candidate_id").val(dataType);

    });
    $(".delParam").live('click', function() {
        var obj = $(this);
        var dataType = obj.attr('data-type');
        $.ajax({
            type: "POST",
            url: BASE + 'resume/delete-attr',
            data: {type: dataType, id: obj.attr('href')},
            success: function(e)
            {

                obj.parent().parent().remove();

            }
        });
        return false;
    });

    if ($("#form-2").length) {
        $("#wizard").steps({
            headerTag: "h2",
            bodyTag: "section",
            transitionEffect: "fade",
            enableFinishButton: false,
            onStepChanging: function(event, currentIndex, newIndex) {

                var simpleValidate = new SimpleValidate();
                if (simpleValidate.isEmpty($("input"), $(".msg"), '')) {
                    $('body,html').animate({
                        scrollTop: 0
                    }, 800);
                    return false;
                } else {
                    return true;
                }
            }
        });
    }

    $("input[name=dob]").datepicker({
        dateFormat: 'yy-mm-dd',
        yearRange: '1940:-18',
        changeYear:true,
        changeMonth:true
    });
    
         $("#searchResumeFrm").submit(function() {

        $.ajax({
            type: "POST",
            url: BASE + 'resume/search-resume',
            data: $(this).serialize(),
            beforeSend: function() {
                $("#dynamic-table tbody").html('<tr><td colspan="5" class="text-center">Please wait, information loading</td></tr>');

            },
            success: function(e)
            {

                var data = e;
               // var data = Json.parse(e);


//            if(data.length)
//              {

                $("#dynamic-table tbody").children().remove(); // remove old table tbody children

                var dataTable = $("#dynamic-table").dataTable({
                    "bRetrieve": true, // retrieve old initiated dataTable object
                    "aoColumns": [// init with null value base on number of column need to show
                        {'bSortable': false},
                        null,
                        null,
                        null,
                        {'bSortable': false}

                    ]
                });



                var trObj = '';

                var oSettings = dataTable.fnSettings(); // get current settings of dataTable
                dataTable.fnClearTable(this);           // clear current dataTable data source


                var rank = 1;
                $.each(data, function(i, el) {


                    var td1 = '<input type="checkbox" class="candidates_chk" name="candidates[]" value="' + data[i].candidate_id + '" />';
                    if (data[i].photo == "") {
                        var td2 = '<img class="photo"   src="' + BASE + 'public/themes/bucket/images/placeholder.gif" />';
                    } else {
                        var td2 = '<img class="photo"  src="' + BASE + 'data/profile/' + data[i].photo + '" />';
                    }
                    var td3 = '<span class="name"><strong>' + data[i].name + '</strong></span><div class="starRate"><span class="dsg">' + data[i].designation + '</span>';
                    td3 += 'Feedback:<ul class="job-feedback">';
                    for (j = 5; j >= 1; j--) {
                        if (j <= data[i].rating) {
                            td3 += '<li><a class="selected" href="#"><span></span></a></li>';
                        } else {
                            td3 += '<li><a  href="#"><span></span></a></li>';
                        }
                    }
                    td3 += '</ul><br/>';


                    td3 += 'Djit:<ul class="skill-feedback">';
                    for (k = 5; k >= 1; k--) {

                        if (k <= data[i].djrating) {
                            td3 += '<li><a class="selected" href="#"><span></span></a></li>';
                        } else {
                            td3 += '<li><a  href="#"><span></span></a></li>';
                        }

                    }
                    td3 += '</ul>';

                    td3 += '</div><strong class="skills">Skills</strong>\n\<ul class="p-skills">';
                    var searchTxt = $("select[name=spec] option:selected").text();
                    for (s in data[i].key)
                    {
                        if (searchTxt.toLowerCase() == data[i].key[s])
                        {
                            td3 += '<li style="background:green;color:white;">' + data[i].key[s] + '</li>';
                        } else {
                            td3 += '<li>' + data[i].key[s] + '</li>';
                        }
                    }

                    td3 += '</ul>';


                    var td4 = '<span class="rank">Rank : (' + rank + ')</span><span class="yearofexp">Exp. year:' + data[i].year_of_exp + '</span>';

                    if (data[i].user_type == "Admin")
                        td4 += '<a target="_blank" style="margin-top:3px;" class="btn btn-send" href="' + BASE + 'feedback/index/' + data[i].cv_tbl_id + '">Feedback</a>';

                    var td5 = '<a target="_blank" style="margin-right:5px;" class="btn view-icon btn-primary" href="' + BASE + 'resume/display-resume/' + data[i].cv_tbl_id + '">View</a>';
                    td5+='<a  data-toggle="modal" style="margin-top:3px;" href="#myModal-1" data-id="' + data[i].id + '" class="btn btn-success offer offer-now">Offer now</a>';
                    td5 += '<form target="_blank" style="margin-top:3px;" method="post" action="' + BASE + 'resume/pdf' + '" class="col-sm-12"><input type="hidden" name="cv_tbl_id" value="' + data[i].cv_tbl_id + '"/><button type="submit" title="PDF Download" class="btn btn-primary col-lg-12" style="margin-right:5px;"><i class="fa fa-download"></i></button></form>';

                    var result = [td1, td2, td3, td4, td5];

                    dataTable.oApi._fnAddData(oSettings, result);  // add row as array

                    rank++;
                });

                oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();  // reinitialize settings for sort, pagination
                dataTable.fnDraw();                                     // redraw grid
//               }else{
//                 var  trObj = '<tr>';
//                   trObj += '<td colspan="5" class="text-center">Sorry no data found</td>';
//                   trObj += '</tr>';
//                   $(".searchResult tbody").html(trObj);
//               }

            }

        });
        return false;

    });

    $(".specs").change(function() {
        var obj = $(this);
        var key = obj.attr('data-key');
        var cvId = $("input[name=cv_id]").val();
        if (obj.attr('checked'))
        {
            $.ajax({
                type: "POST",
                url: BASE + 'resume/change-spec',
                data: {cv_id: cvId, key: key, status: 'add'},
                success: function(e)
                {

                }
            });
        } else {
            $.ajax({
                type: "POST",
                url: BASE + 'resume/change-spec',
                data: {cv_id: cvId, key: key, status: 'remove'},
                success: function(e)
                {

                }
            });
        }

    });

    $("select[name=recruiter]").change(function() {
        var obj = $(this);
        $.ajax({
            type: "POST",
            url: BASE + 'job/get-job',
            data: {recruiter_id: obj.val()},
            beforeSend: function() {
                $("select[name=joblist]").children().remove();
            },
            success: function(e)
            {
                var data = e;

                if (data.length > 0)
                {
                    var opObj = '';
                    $.each(data, function(i, elm) {
                        opObj += '<option value="' + data[i].jobs_id + '">' + data[i].job_title + '</option>';
                    });
                    $("select[name=joblist]").html(opObj);
                }
            }
        });
    });

    $("#sendRequestFrm").submit(function() {

        var recruiterVal = $("select[name=recruiter]").val();
        var rowCount = $('table.searchResult >tbody >tr').length;
        var obj = $(this);
        if (recruiterVal)
        {
            if (rowCount > 0 && $(".candidates_chk:checked").length) {
                $.ajax({
                    type: "POSt",
                    url: BASE + 'interview/invite-for-interview',
                    data: obj.serialize(),
                    success: function(e)
                    {
                        if (e.status == 200)
                        {
                            $(".candidates_chk:checked").parent().parent().remove();
                            CustomMessage.printMessage(e.status, e.msg, $("#msg"), null);
                        }
                    }
                });
            } else {
                alert('Please select candidate');
            }
        } else {
            alert('Please Select recruiter');
        }
        return false;
    });


});