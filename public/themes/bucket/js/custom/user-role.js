/**
 * Created by Himel on 6/12/14.
 */
$(function(){
     $(".permissionChk").live('change',function(){
        var obj = $(this);
        var userType  = $("select[name=user_type]").val();
        if(userType)
        {
            if(obj.attr('checked'))
            {

                $.ajax({
                    type:"POST",
                    url : BASE + 'users/update-role',
                    data:{"request-type":"add","module_id":obj.val(),"field":obj.attr('data-field'),"user_type":userType},
                    success:function(e)
                    {

                    }
                });
            }
             else
            {
                $.ajax({
                    type:"POST",
                    url : BASE + 'users/update-role',
                    data:{"request-type":"remove","module_id":obj.val(),"field":obj.attr('data-field'),"user_type":userType},
                    success:function(e)
                    {

                    }
                });
            }
        }else{
            alert("Please Select User Type");
            obj.removeAttr('checked');
        }
     });


    $("select[name=user_type]").change(function(){
        var obj = $(this);
        $.ajax({
           type:"POST",
           url : BASE + 'users/get-role',
           data:{user_type:obj.val()},
           beforeSend:function()
           {
               var trObj = '<tr><td colspan="5" class="text-center">Loading...</td></tr>';
               $(".permissionList tbody").html(trObj);   
           },
           success:function(e)
           {
               var data = e;
               var trObj = '';
               $.each(data, function(i,el){
                   console.log(i);
                   trObj += '<tr>';
                   trObj +='<td><input type="hidden" name="module_id['+'+data[i].module_id+'+']" value="'+data[i].module_id+'"/>'+data[i].module+'</td>';
                   // read
                   trObj += '<td>';
                    if(data[i].role && data[i].role.read)
                        trObj += '<input type="checkbox" class="permissionChk" data-field="read" name="read['+data[i].module_id+']" checked="checked" value="'+data[i].module_id+'"/>';
                    else
                        trObj += '<input type="checkbox" class="permissionChk" data-field="read" name="read['+data[i].module_id+']" value="'+data[i].module_id+'"/>';
                   trObj += '</td>';

                    // create
                   trObj += '<td>';
                   if(data[i].role && data[i].role.create)
                       trObj += '<input type="checkbox" class="permissionChk" data-field="create" name="create['+data[i].module_id+']" checked="checked" value="'+data[i].module_id+'"/>';
                   else
                       trObj += '<input type="checkbox" class="permissionChk" data-field="create" name="create['+data[i].module_id+']" value="'+data[i].module_id+'"/>';
                   trObj += '</td>';

                    // update
                   trObj += '<td>';
                   if(data[i].role && data[i].role.update)
                       trObj += '<input type="checkbox" class="permissionChk" data-field="update" name="update['+data[i].module_id+']" checked="checked" value="'+data[i].module_id+'"/>';
                   else
                       trObj += '<input type="checkbox" class="permissionChk" data-field="update" name="update['+data[i].module_id+']" value="'+data[i].module_id+'"/>';
                   trObj += '</td>';

                   // delete
                   trObj += '<td>';
                   if(data[i].role && data[i].role.delete)
                       trObj += '<input type="checkbox" class="permissionChk" data-field="delete" name="delete['+data[i].module_id+']" checked="checked" value="'+data[i].module_id+'"/>';
                   else
                       trObj += '<input type="checkbox" class="permissionChk" data-field="delete" name="delete['+data[i].module_id+']" value="'+data[i].module_id+'"/>';
                   trObj += '</td>';
                   trObj +='</tr>'
               });

               $(".permissionList tbody").html(trObj);
           }
        });
    });
});