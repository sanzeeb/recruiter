<?php
/**
 * Created by PhpStorm.
 * User: HimelC51
 * Date: 12/4/14
 * Time: 12:26 PM
 */

class JobCandidate extends Eloquent{

    /**
     * Table Name
     * @var string
     */
    protected $table = 'job_candidates';

    protected $fillable = array('job_id','candidate_id');

    public function Candidate()
    {
        return $this->belongsTo('Candidate','candidate_id','candidate_id');
    }

    public static function boot()
    {
        parent::boot();

        static::created(function($object){

            $job_id =  $object->job_id;
            $job = Jobs::find($job_id);
            if(count($job))
            {
                $userObj = User::where('user_id',$object->candidate_id)->where('user_type','Candidate')->first();
                $inbox = new Inbox();
                $inbox->sender_id = $userObj->id;
                $inbox->read_status = 0;

                if($object->replier_type == 'Candidate')
                {
                    $empUser = User::where('user_id',$job->created_by)->where('user_type','Employer')->first();
                    $inbox->receiver_id = $empUser->id;
                    $inbox->subject = 'Candidate'.$object->Candidate->firstname.' Applied to job '.$job->job_title;
                    $inbox->body = "<h5>Job Application</h5>";
                    $inbox->body .= "<p>{$inbox->subject}</p>";
                    $inbox->body .= " <br/> Visit job Link: <a href='".url(ASIAN_DOMAIN.'job/view/'.$job->jobs_id)."'>".url(ASIAN_DOMAIN.'job/view/'.$job->jobs_id)."</a>";
                    $inbox->in_type="Job Application";
                    $inbox->save();
                }
            }

        });
    }

} 