<?php
/**
 * Created by PhpStorm.
 * User: Himel
 * Date: 6/9/14
 * Time: 5:31 PM
 */

class Jobs extends Eloquent{

    /**
     * Table Name
     * @var string
     */
    protected $table = 'jobs';

    /**
     * Primary Key
     * @var string
     */
    protected $primaryKey = 'jobs_id';

    /**
     * @var array  only attributes that can be fill-able
     */
    protected $fillable = array('job_title','job_vacancies','job_responsibility','job_nature',
        'eduactional_requirement','experience_requirment','extra_job_req','salary_range','other_benefit',
    'job_location','job_source','skill_id','selected_skill_attributes','created_by','last_date');

    public function Skill()
    {
        return $this->belongsTo('Skills','skill_id','skill_id');
    }

    public function JobCreator()
    {
        $employer = Employer::find($this->created_by);
        if(count($employer))
        {
            return (count($employer)) ? $employer->title : '';
        }
        else
        {
            $admin = User::find($this->created_by);
            return (count($admin))? $admin->username : '';
        }
    }

    public function JobCandidate()
    {
        return $this->hasMany('JobCandidate','job_id','jobs_id');
    }
} 