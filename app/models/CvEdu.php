<?php
/**
 * Created by PhpStorm.
 * User: Himel
 * Date: 6/3/14
 * Time: 5:06 PM
 */

class CvEdu extends Eloquent{

    /**
     * Table Name
     * @var string
     */
    protected $table = 'cv_edu';

    /**
     * Primary Key
     * @var string
     */
    protected $primaryKey = 'cv_edu_id';

    /**
     * @var array  only attributes that can be fill-able
     */
    protected $fillable = array('cv_edu_title','major','institution','result','passing_year');

} 