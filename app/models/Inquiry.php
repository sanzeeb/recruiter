<?php
/**
 * Created by PhpStorm.
 * User: HimelC51
 * Date: 11/16/14
 * Time: 12:47 PM
 */

class Inquiry extends Eloquent{

    protected $table = 'inquery';

    protected $fillable = array('emp_id','candidate_id','job_id','description','message','status','ask_source_code','ask_academic_certificate');

    public static $Pending   = "Pending";
    public static $Approved  = "Approved";
    public static $Declined  = "Declined";
    public static $Replied   = "Replied";
    public static $Canceled  = "Canceled";

    public function Candidate()
    {
        return $this->belongsTo('Candidate','candidate_id','candidate_id');
    }

    public function Employer()
    {
        return $this->belongsTo('Employer','emp_id','emp_id');
    }

    public function Job()
    {
        return $this->belongsTo('Jobs','job_id','jobs_id');
    }

    public function InqueryResponse(){
        return $this->hasMany('InqueryResponse','inquery_id','id')->orderBy('id','DESC');
    }

    public function InqueryCanceledResponse()
    {
        return $this->hasMany('InqueryResponse','inquery_id','id')->where('status','Canceled')->orderBy('id','DESC');
    }

    public function InqueryRepliedResponse(){
        return $this->hasMany('InqueryResponse','inquery_id','id')->where('status','Approved')->orderBy('id','DESC');
    }
    public function InqueryRepliedAdminResponse(){
        return $this->hasMany('InqueryResponse','inquery_id','id')->where('status','Replied by admin')->orderBy('id','DESC');
    }

    public static function boot()
    {
        parent::boot();

        static::created(function($object){
            $sender = User::where('user_id',$object->emp_id)->where('user_type','Employer')->first();
            $emp =  Employer::find($sender->user_id);
            $receivers = User::where('user_type','Admin')->get();
            foreach($receivers as $receiver){
                $inbox = new Inbox();
                $inbox->sender_id = $sender->id;
                $inbox->receiver_id = $receiver->id;
                $inbox->read_status = 0;
                $inbox->subject = $emp->title.', Inquiry requested for '.$object->Candidate->firstname;
                $inbox->in_type = 'Inquiry';
                $inbox->body = '<h5>New Inquiry Request</h5>';
                $inbox->body .= $inbox->subject;
                $inbox->body .= ' <br/> Requested by : '.$object->Employer->title;
                $inbox->body .= ' <br/> Inquiry Link <a href="'.url(ASIAN_DOMAIN.'inquiry/details/'.$object->id).'">'.url(ASIAN_DOMAIN.'inquiry/details/'.$object->id).'</a>';
                $inbox->save();
            }
           
        });

        static::updated(function($object){
            $sender = Authenticate::check();
            if($sender->user_type=='Admin')
            {
                if($object->status == "Approved")
                {
                    $receiver = User::where('user_id',$object->candidate_id)->where('user_type','Candidate')->first();
                    $inbox = new Inbox();
                    $inbox->sender_id = $sender->id;
                    $inbox->receiver_id = $receiver->id;
                    $inbox->read_status = 0;
                    $inbox->subject = 'Inquiry requested from '.$object->Employer->title;
                    $inbox->in_type = 'Inquiry';
                    $inbox->body = '<h5>New Inquiry Request</h5>';
                    $inbox->body .= $inbox->subject;
                    $inbox->body .= ' <br/> Requested by : '.$object->Employer->title;
                    $inbox->body .= ' <br/> Inquiry Link <a href="'.url(JOB_DOMAIN.'inquiry/details/'.$object->id).'">'.url(JOB_DOMAIN.'inquiry/details/'.$object->id).'</a>';
                    $inbox->save();
                }
                if($object->status == 'Canceled')
                {
                    $receiver = User::where('user_id',$object->emp_id)->where('user_type','Employer')->first();
                    $inbox = new Inbox();
                    $inbox->sender_id = $sender->id;
                    $inbox->receiver_id = $receiver->id;
                    $inbox->read_status = 0;
                    $inbox->subject = 'Inquiry requested Canceled  by Admin';
                    $inbox->in_type = 'Inquiry';
                    $inbox->body = '<h5>Inquiry Request Canceled</h5>';
                    $inbox->body .= $inbox->subject;
                    $inbox->body .= ' <br/> We are so sorry to say that your inquiry request is against our inquiry rules so it was canceled';
                    $inbox->body .= ' <br/> Inquiry Link <a href="'.url(ASIAN_DOMAIN.'inquiry/details/'.$object->id).'">'.url(ASIAN_DOMAIN.'inquiry/details/'.$object->id).'</a>';
                    $inbox->save();

                    // Send E-mail functionality here
                    $employer = Employer::find($object->emp_id);
                    $emailReceiver = array(
                        'name'  => $receiver->username,
                        'email' => $employer->email
                    );
                    $messageBody = array(
                        'name' => $emailReceiver['name'],
                        'cancel' => "Your inquiry request canceled",
                        'body' => '<h2>Sorry, You Inquiry Canceled by admin.</h2> <p>You should create inquiry according to rules and convension of DJIT</p>'
                    );
                    //try{
                        Email::sendCancelMail($emailReceiver,$messageBody);
                    //}catch(Exception $ex)
                    //{}
                }
                if ($object->status == 'Replied by admin')
                {
                    $receiver = User::where('user_id',$object->emp_id)->where('user_type','Employer')->first();
                    $inbox = new Inbox();
                    $inbox->sender_id = $sender->id;
                    $inbox->receiver_id = $receiver->id;
                    $inbox->read_status = 0;
                    $inbox->subject = 'Inquiry requested Replied  by admin';
                    $inbox->in_type = 'Inquiry';
                    $inbox->body = '<h5>Inquiry Request Replied  by admin</h5>';
                    $inbox->body .= $inbox->subject;
                    $inbox->body .= ' <br/> We are so sorry to say that your inquiry request is against our inquiry rules so it was canceled';
                    $inbox->body .= ' <br/> Inquiry Link <a href="'.url(ASIAN_DOMAIN.'inquiry/details/'.$object->id).'">'.url(ASIAN_DOMAIN.'inquiry/details/'.$object->id).'</a>';
                    $inbox->save();
                }


            }
            /*else if($sender->user_type=='Candidate')
            {
                $receiver = User::where('user_id',$object->emp_id)->where('user_type','Employer')->first();
                $inbox = new Inbox();
                $inbox->sender_id = $sender->id;
                $inbox->receiver_id = $receiver->id;
                $inbox->read_status = 0;
                $inbox->subject = 'Inquery requested from '.$object->Candidate->firstname;
                $inbox->in_type = 'Inquery';
                $inbox->body = '<h5>New Inquery Request</h5>';
                $inbox->body .= $inbox->subject;
                $inbox->body .= 'Replied by : '.$object->Candidate->firstname;
                $inbox->body .= 'Inquery Link <a href="'.url('inquiry/details/'.$object->id).'">'.url('inquiry/details/'.$object->id).'</a>';
                $inbox->save();
            }*/
        });
    }
} 