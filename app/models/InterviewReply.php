<?php
/**
 * Created by PhpStorm.
 * User: HimelC51
 * Date: 11/17/14
 * Time: 4:09 PM
 */

class InterviewReply extends Eloquent{

    protected $table = "interviews_replies";

    protected $fillable = array('interview_id','replier_id','replier_type','reply','status');

    public function Interview()
    {
        return $this->belongsTo('Interview','interview_id','interview_id');
    }

    public static function boot()
    {
        parent::boot();

        static::created(function($object){

            $interview_id =  $object->interview_id;
            $interview = Interview::find($interview_id);
            if(count($interview))
            {
                $inbox = new Inbox();
                $domain = '';
                if($object->replier_type == 'Admin')
                {
                    $domain = ASIAN_DOMAIN;
                    return;
                }else if($object->replier_type == 'Candidate')
                {
                    $userObj = User::where('user_id',$object->replier_id)->where('user_type',$object->replier_type)->first();
                    
                    $inbox->sender_id = $userObj->id;
                    $inbox->read_status = 0;
                    if($object->status == "Pending")
                    {
                        $empUser = User::where('user_type','Admin')->first();
                        $inbox->receiver_id = $empUser->id;

                    }else{

                        $empUser = User::where('user_id',$interview->emp_id)->where('user_type','Employer')->first();
                        $inbox->receiver_id = $empUser->id;
                    }
                    $domain = ASIAN_DOMAIN;
                }
                else
                {
                    $userObj = User::where('user_id',$object->replier_id)->where('user_type',$object->replier_type)->first();
                   
                    $inbox->sender_id = $userObj->id;
                    $inbox->read_status = 0;
                   
                    $candUser = User::where('user_id',$interview->candidate_id)->where('user_type','Candidate')->first();
                    $inbox->receiver_id = $candUser->id;
                    $domain = JOB_DOMAIN;
                }

                if(($interview->status != "Declined") || !empty($object->status)){
                    $inbox->subject = 'Replied to Interview '.$interview->Job->job_title;
                    $inbox->body = "<h5>Replied To Interview</h5>";
                    $inbox->body .= " <br/> Visit Interview Link: <a href='".url($domain.'interview/details/'.$interview->interview_id)."'>".url($domain.'interview/details/'.$interview->interview_id)."</a>";
                    $inbox->in_type="Reply";
                    $inbox->save();
                }
            }

        });
    }
}