<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of InterviewRequest
 *
 * @author User
 */
class InterviewRequest extends Eloquent{
    //put your code here
    
    /**
     * Table Name
     * @var string
     */
    protected $table = 'interview_request';

    /**
     * Primary Key
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array  only attributes that can be fill-able
     */
    protected $fillable = array('emp_id','job_id','purpose');
}

?>
