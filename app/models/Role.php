<?php

class Role extends Eloquent
{
	/**
     * Table Name
     * @var string
     */
    protected $table = 'roles';

    /**
     * Primary Key
     * @var string
     */
    protected $primaryKey = 'role_id';


	/**
     * @var array  only attributes that can be fill-able
     */
    protected $fillable = array('role_name','editable');
}