<?php

class Module{
	public static function getModules()
	{
		return array(
			'Inquiry',
			'Favorites',
			'Interview',
			'Related Jobs',
			'Offers',
			'Test Project',
			'Manage skills',
			'Finance',
			'Payments',
			'Job posting',
			'Resume manage',
			'User',
			'Job Seeker Page',
			'Recruiter Page'
		);
	}
}