<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Candidate extends Eloquent {

    /**
     * Table Name
     * @var string
     */
    protected $table = 'candidate';

    /**
     * Primary Key
     * @var string
     */
    protected $primaryKey = 'candidate_id';

    /**
     * @var array  only attributes that can be fill-able
     */
    protected $fillable = array('email','firstname','lastname','current_address',
            'phone','mobile','agreement','gender','dob','marital_status',
            'religion','father_name','mother_name','parmanent_address','city','alternative_email','status','photo','csummary');

    /*** Himel ***/
    public function CvTable()
    {
        return $this->belongsTo('CvTable','cv_tbl_id','cv_tbl_id');
    }
    /*****Himel ***/
    
    public function UserAcc()
    {
        return $this->belongsTo('User','candidate_id','user_id');
    }

    public static function getAge($c_dob){
        $age = '';
        if(!empty($c_dob))
        {
            $dob = explode("-",$c_dob);

            $currentYear = date("Y");
            $currentMonth = date("m");

            if(!empty($dob[0]) && $dob[1] < $currentMonth)
            {
                $sYear = ($currentYear - 1);
                $age = ($sYear - trim($dob[0]));
            }

            if(!empty($dob[0]) && $dob[1] >= $currentMonth)
            {
                $age = ($currentYear - trim($dob[0]));
            }
        }

        //return $c_dob;
        return ($age>0)? $age : '';
    }
}
?>
