<?php
/**
 * Created by PhpStorm.
 * User: HimelC51
 * Date: 11/19/14
 * Time: 10:51 AM
 */

class TestProject extends Eloquent{

    public static $Pending   = "Pending";
    public static $Approved  = "Approved";
    public static $Canceled  = "Canceled";
    public static $Submitted  = "Submitted";
    public static $Review  = "Review";


    protected $table = "test_project";



    protected $fillable = array('emp_id','candidate_id','project_name','project_description','project_attachment','project_submission_link','project_zip','project_status');

    public function Employer()
    {
        return $this->belongsTo('Employer','emp_id','emp_id');
    }

    public function Candidate()
    {
        return $this->belongsTo('Candidate','candidate_id','candidate_id');
    }

    public static function boot()
    {
        parent::boot();

        static::created(function($object){
            $sender = User::where('user_id',$object->emp_id)->where('user_type','Employer')->first();
            $emp =  Employer::find($sender->user_id);
            $receivers = User::where('user_type','Admin')->get();
            foreach($receivers as $receiver){

                $inbox = new Inbox();
                $inbox->sender_id = $sender->id;
                $inbox->receiver_id = $receiver->id;
                $inbox->read_status = 0;
                $inbox->subject = $emp->title.' Test project requested for '.$object->Candidate->firstname;
                $inbox->in_type = 'TestProject';
                $inbox->body = '<h5>New Test Project Request</h5>';
                $inbox->body .= $inbox->subject;
                $inbox->body .= ' <br/> Requested by : '.$object->Employer->title;
                $inbox->body .= ' <br/> Test Project Link <a href="'.url(ASIAN_DOMAIN.'tproject/details/'.$object->id).'">'.url(ASIAN_DOMAIN.'tproject/details/'.$object->id).'</a>';
                $inbox->save();
            }
            
        });

        static::updated(function($object){
            $sender = Authenticate::check();
            if($sender->user_type=='Admin')
            {
                $objectAttr = $object->getAttributes();
                if($objectAttr['project_status'] == TestProject::$Approved)
                {

                    $receiver = User::where('user_id',$object->candidate_id)->where('user_type','Candidate')->first();
                    $inbox = new Inbox();
                    $inbox->sender_id = $sender->id;
                    $inbox->receiver_id = $receiver->id;
                    $inbox->read_status = 0;
                    $inbox->subject = 'Test project requested from '.$object->Employer->title;
                    $inbox->in_type = 'TestProject';
                    $inbox->body = '<h5>New Test Project Request</h5>';
                    $inbox->body .= $inbox->subject;
                    $inbox->body .= ' <br/> Requested by : '.$object->Employer->title;
                    $inbox->body .= ' <br/> Test Project Link <a href="'.url(JOB_DOMAIN.'tproject/details/'.$object->id).'">'.url(JOB_DOMAIN.'tproject/details/'.$object->id).'</a>';
                    $inbox->save();
                }

                if($objectAttr['project_status'] == TestProject::$Canceled)
                {
                    $receiver = User::where('user_id',$object->emp_id)->where('user_type','Employer')->first();
                    $inbox = new Inbox();
                    $inbox->sender_id = $sender->id;
                    $inbox->receiver_id = $receiver->id;
                    $inbox->read_status = 0;
                    $inbox->subject = 'Test project requested Canceled by'.$sender->username;
                    $inbox->in_type = 'TestProject';
                    $inbox->body = '<h5>Test Project Request Canceled</h5>';
                    $inbox->body .= $inbox->subject;
                    $inbox->body .= ' <br/> Requested by : '.$object->Employer->title;
                    $inbox->body .= ' <br/> Test Project Link <a href="'.url(ASIAN_DOMAIN.'tproject/details/'.$object->id).'">'.url(JOB_DOMAIN.'tproject/details/'.$object->id).'</a>';
                    $inbox->save();

                    $emailReceiver = array(
                        'name'  => $receiver->username,
                        'email' => $receiver->email
                    );
                    $messageBody = array(
                        'name' => $emailReceiver['name'],
                        'body' => '<h2>Sorry, You Test project Canceled by admin.</h2> <p>You should create test project according to rules and convension of DJIT</p>'
                    );
                    try{
                        Email::sendCancelMail($emailReceiver,$messageBody);
                    }catch(Exception $ex)
                    {}
                }
            }

            if($sender->user_type == 'Candidate')
            {
                if($object->project_status == "Review")
                {
                    $receivers = User::where('user_type','Admin')->get();
                    foreach($receivers as $receiver){
                        $inbox = new Inbox();
                        $inbox->sender_id = $sender->id;
                        $inbox->receiver_id = $receiver->id;
                        $inbox->read_status = 0;
                        $inbox->subject = 'Test project Submitted By '.$object->Candidate->firstname;
                        $inbox->in_type = 'TestProject';
                        $inbox->body = '<h5>Test Project Submitted</h5>';
                        $inbox->body .= $inbox->subject;
                        $inbox->body .= ' <br/> Submitted by : '.$object->Candidate->firstname;
                        $inbox->body .= ' <br/> Test Project Link <a href="'.url(ASIAN_DOMAIN.'tproject/details/'.$object->id).'">'.url(ASIAN_DOMAIN.'tproject/details/'.$object->id).'</a>';
                        $inbox->save();
                    }
                }else{
                
                }
            }
        });


    }

    public function TestProjectReply()
    {
        return $this->hasMany('TestProjectReply','tproject_id','id')->orderBy('id','DESC');
    }


} 