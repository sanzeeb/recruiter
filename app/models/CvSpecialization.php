<?php
/**
 * Created by PhpStorm.
 * User: Himel
 * Date: 6/3/14
 * Time: 5:06 PM
 */

class CvSpecialization extends Eloquent{

    /**
     * Table Name
     * @var string
     */
    protected $table = 'cv_specialization';

    /**
     * Primary Key
     * @var string
     */
    protected $primaryKey = 'cv_specialization_id';

    /**
     * @var array  only attributes that can be fill-able
     */
    protected $fillable = array('specialization');

} 