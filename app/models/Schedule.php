<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Schedule extends Eloquent {

    protected $table = 'schedule';
    
    protected $primaryKey = 'avail_id';
    
    protected $fillable = array('avail_id','user_id','title','created_at','updated_at','deleted_at','from_time','to_time','timestamp_from','timestamp_to','available_date');


    /*** Himel ***/
    public function User()
    {
        return $this->belongsTo('User','user_id','id');
    }
    /*****Himel ***/
    
}
?>
