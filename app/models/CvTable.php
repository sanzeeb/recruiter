<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class CvTable extends Eloquent {

    /**
     * Table Name
     * @var string
     */
    protected $table = 'cv_tbl';

    /**
     * Primary Key
     * @var string
     */
    protected $primaryKey = 'cv_tbl_id';

    /**
     * @var array  only attributes that can be fill-able
     */
    protected $fillable = array('name', 'mobile', 'email', 'dob', 'gender', 'year_of_exp');

    public function getCv($fromYear, $toYear, $spec) {

        //DB::select(DB::raw("UPDATE cv_tbl SET year_of_exp = replace(year_of_exp, ':', '') WHERE year_of_exp LIKE '%:%'"));

        $queryObj = DB::table($this->table)
                ->leftJoin('cv_specialization', $this->table . '.cv_tbl_id', '=', 'cv_specialization.cv_tbl_id')
                ->leftJoin('candidate', $this->table . '.cv_tbl_id', '=', 'candidate.cv_tbl_id')
                ->leftJoin('users', 'candidate.candidate_id', '=', 'users.user_id')
                ->leftJoin('cv_jobs', $this->table . '.cv_tbl_id', '=', 'cv_jobs.cv_tbl_id');

        if ($fromYear) {
            $queryObj->where('year_of_exp', '>=', $fromYear);
        }
        if ($toYear) {
            $queryObj->where('year_of_exp', '<', $toYear);
        }
        if ($spec)
            $queryObj->where('key', $spec)->where('value', 'yes');
        $_userSession = Authenticate::check();
        if ((!empty($fromYear)) || (!empty($toYear)) || (!empty($spec))) {
            $result = $queryObj->groupBy('candidate.cv_tbl_id')->orderBy('cv_tbl.year_of_exp', 'DESC')->get();
            $dataSet = array();
            if (count($result)) {
                //Helpers::LastQuery();
                foreach ($result as $i => $r) {
                    $dataSet[$r->cv_tbl_id]['candidate_id'] = $r->candidate_id;
                    $dataSet[$r->cv_tbl_id]['id'] = $r->id;
                    $dataSet[$r->cv_tbl_id]['designation'] = $r->designation;
                    $dataSet[$r->cv_tbl_id]['department'] = $r->department;
                    $dataSet[$r->cv_tbl_id]['firstname'] = $r->firstname;
                    $dataSet[$r->cv_tbl_id]['lastname'] = $r->lastname;
                    $dataSet[$r->cv_tbl_id]['name'] = $r->name;
                    $dataSet[$r->cv_tbl_id]['cv_tbl_id'] = $r->cv_tbl_id;
                    $dataSet[$r->cv_tbl_id]['photo'] = $r->photo;
                    $dataSet[$r->cv_tbl_id]['year_of_exp'] = $r->year_of_exp;
                    $dataSet[$r->cv_tbl_id]['user_type'] = $_userSession->user_type;

                    $agreements = Agreements::where("receiver_id", $r->id)
                                    ->where("status", "Accepted")->where("rating", '!=', "")->get();

                    $countAgr = count($agreements);
                    $rating = 0;

                    foreach ($agreements as $agr) {
                        $rating+=$agr->rating;
                    }

                    if ($countAgr > 0 && $rating != 0) {
                        $rating = ceil($rating / $countAgr);
                    }
                    $dataSet[$r->cv_tbl_id]['rating'] = $rating;


                    $specs = (CvSpecialization::where('cv_tbl_id', $r->cv_tbl_id)->get());

                    $countSkill = count($specs);
                    $djrating = 0;

                    foreach ($specs as $skill) {

                        if ($skill->rating != 0) {
                            $djrating+=$skill->rating;
                        } else {
                            $djrating+=0;
                        }
                    }

                    if ($countSkill > 0) {
                        $djrating = ceil($djrating / $countSkill);
                    }

                    $dataSet[$r->cv_tbl_id]['djrating'] = $djrating;
                    if (count($specs)) {
                        foreach ($specs as $s) {
                            $dataSet[$r->cv_tbl_id]['key'][] = $s->key;
                        }
                    } else {
                        $dataSet[$r->cv_tbl_id]['key'] = array();
                    }
                }
            }
            return $dataSet;
            // Helpers::debug($result);
            // die();
        }
        else
            return array();
    }


    public function getBySkill($spec,$name="",$age="",$year_of_exp="",$offset=0,$limit=20,$count=false) {
       $skills = '';
       $filter = array();
       
       if(is_array($spec))
        {    
            foreach($spec as $sp)
            {
                if(!in_array($sp,$filter))
                    $filter[] = $sp;
            }
            foreach($filter as $i=> $f)
            {
                $skills .= "'".$f."'";
                if($i<(count($filter)-1))
                    $skills.=",";
         
            }
        }else{
         
            $filter = (!empty($spec))? array($spec) : array();
            $skills = (!empty($spec))? "'".$spec."'" : '';
        }

       

            $sql = "SELECT * FROM (SELECT *, COUNT(r.cvTblId) AS skill_count 
                      FROM (SELECT candidate.photo, candidate.csummary, candidate.cv_tbl_id, cv_tbl.year_of_exp,  candidate.firstname,candidate.lastname,cv_tbl.cv_tbl_id AS cvTblId, cv_specialization.key 
                                      FROM `cv_tbl` 
                                          LEFT JOIN `cv_specialization` 
                                              ON `cv_tbl`.`cv_tbl_id` = `cv_specialization`.`cv_tbl_id` 
                                          LEFT JOIN `candidate` 
                                              ON `cv_tbl`.`cv_tbl_id` = `candidate`.`cv_tbl_id` 
                                          LEFT JOIN `users` 
                                              ON `candidate`.`candidate_id` = `users`.`user_id`";

            if(!empty($skills))
                $sql .=" WHERE `key` IN (".$skills.") AND `value` = 'yes'";

           
           
            $sql .= ") AS r GROUP BY r.cvTblId ";
            
            if(!empty($filter))
                $sql .=" HAVING skill_count = ".count($filter);

            $sql .=" ORDER BY r.cvTblId ASC) as b";
            
            if(!empty($name) || !empty($year_of_exp))
                $sql .= " WHERE ";

            if(!empty($name))
            {
                   
                $sql .= " b.firstname LIKE '%$name%' OR b.lastname LIKE '%$name%' ";
            }   

            if(!empty($year_of_exp))
            {
                if(!empty($name))
                    $sql .= " AND ";
                $sql .= " b.year_of_exp >= '".$year_of_exp."'";
            }

             if(!empty($offset) || !empty($limit))
                $sql .=" LIMIT ".$offset.", ".$limit;

            $query = DB::select(DB::raw($sql));

          // Helpers::LastQuery();
            //Helpers::debug($query);
        
       // Helpers::LastQuery();
       // $queryObj = DB::table();
                /*->select(DB::raw('count(*) as user_count, status'))
                ->leftJoin('cv_specialization', $this->table . '.cv_tbl_id', '=', 'cv_specialization.cv_tbl_id')
                ->leftJoin('candidate', $this->table . '.cv_tbl_id', '=', 'candidate.cv_tbl_id')
                ->leftJoin('users', 'candidate.candidate_id', '=', 'users.user_id');*/

                /*SELECT *, COUNT(r.cvTblId) AS skill_count FROM (SELECT candidate.firstname,cv_tbl.cv_tbl_id AS cvTblId, cv_specialization.key FROM `cv_tbl` 
LEFT JOIN `cv_specialization` ON `cv_tbl`.`cv_tbl_id` = `cv_specialization`.`cv_tbl_id` 
LEFT JOIN `candidate` ON `cv_tbl`.`cv_tbl_id` = `candidate`.`cv_tbl_id` 
LEFT JOIN `users` ON `candidate`.`candidate_id` = `users`.`user_id` WHERE `key` IN  ('php','javascript')  AND `value` = 'yes') AS r  GROUP BY r.cvTblId HAVING skill_count = 2  ORDER BY  r.cvTblId ASC*/
        
      

        


       
            //$result = $queryObj->groupBy('candidate.cv_tbl_id')->orderBy('candidate.csummary', 'DESC')->paginate(30);
            $result = $query;
           //Helpers::debug($result);
            return ($count == true)? count($result) : $result;
       
    }

    public function _old_getBySkill($spec,$name="",$age="",$year_of_exp="") {

        $queryObj = DB::table($this->table)
                ->leftJoin('cv_specialization', $this->table . '.cv_tbl_id', '=', 'cv_specialization.cv_tbl_id')
                ->leftJoin('candidate', $this->table . '.cv_tbl_id', '=', 'candidate.cv_tbl_id')
                ->leftJoin('users', 'candidate.candidate_id', '=', 'users.user_id');

        if(!empty($spec))
        {
            if (is_array($spec))
            {
                foreach($spec as $i=> $spc)
                {
                    /*if($i == 0)
                        $queryObj->where('key','=',urldecode($spc));   
                    else
                        $queryObj->where('key','=',urldecode($spc),'OR');*/
               
                }  
                $queryObj->where('value', 'yes');
            }else
                $queryObj->where('key', $spec)->where('value', 'yes');
        }
        

        if(!empty($name))
        {
            
            $queryObj->where('candidate.firstname','like',"%".$name."%");
        }   

        if(!empty($year_of_exp))
        {
            $queryObj->where($this->table.'.year_of_exp','Like',$year_of_exp."%");
        }


        if ((!empty($spec)) || (!empty($name)) || (!empty($yxp)))
        {
            $result = $queryObj->groupBy('candidate.cv_tbl_id')->orderBy('candidate.csummary', 'DESC')->paginate(30);
            //Helpers::LastQuery();
            return $result;
        }   
        else
            return array();
    }

    public function FavoriteCv($emp_id,$spec,$name="",$age="",$year_of_exp="") {
       $skills = '';
       $filter = array();
       
       if(is_array($spec))
        {    
            foreach($spec as $sp)
            {
                
                if(!in_array($sp,$filter))
                    $filter[] = $sp;
                
            }
            foreach($filter as $i=> $f)
            {
                $skills .= "'".$f."'";
                if($i<(count($filter)-1))
                    $skills.=",";
         
            }
        }else{
         
            $filter = (!empty($spec))? array($spec) : array();
            $skills = (!empty($spec))? "'".$spec."'" : "";
        }

        

            $sql = "SELECT * FROM (SELECT *, COUNT(r.cvTblId) AS skill_count 
                      FROM (SELECT candidate.photo, candidate.csummary, candidate.cv_tbl_id, cv_tbl.year_of_exp,  candidate.firstname,cv_tbl.cv_tbl_id AS cvTblId, cv_specialization.key 
                                      FROM `cv_tbl` 
                                          LEFT JOIN `cv_specialization` 
                                              ON `cv_tbl`.`cv_tbl_id` = `cv_specialization`.`cv_tbl_id` 
                                          LEFT JOIN `candidate` 
                                              ON `cv_tbl`.`cv_tbl_id` = `candidate`.`cv_tbl_id` 
                                          LEFT JOIN `employer_favorites`
                                              ON `employer_favorites`.`candidate_id` = `candidate`.`candidate_id`
                                          LEFT JOIN `users` 
                                              ON `candidate`.`candidate_id` = `users`.`user_id`";

            $sql .= " WHERE  employer_favorites.emp_id = ".$emp_id;
            if((!empty($skills)))
                $sql.=" AND `key` IN (".$skills.") AND `value` = 'yes'";
            

            $sql .= " ) AS r GROUP BY r.cvTblId ";
            if(count($filter))
                $sql .= " HAVING skill_count = ".count($filter);

            $sql .= " ORDER BY r.cvTblId ASC) AS b";
            
            if(!empty($name) || !empty($year_of_exp))
                $sql .= " WHERE ";

            if(!empty($name))
            {
                   
                $sql .= " b.firstname LIKE '%$name%'";
            }   

            if(!empty($year_of_exp))
            {
                if(!empty($name))
                    $sql .= " AND ";
                $sql .= " b.year_of_exp >= '".$year_of_exp."'";
            }
            

            $query = DB::select(DB::raw($sql));

            //Helpers::LastQuery();
        


       
            //$result = $queryObj->groupBy('candidate.cv_tbl_id')->orderBy('candidate.csummary', 'DESC')->paginate(30);
            $result = $query;
           
            return $result;
       
    }

    public function getAge(){
        $age = '';
        if(!empty($this->dob))
        {
            $dob = explode("-",$this->dob);
            $currentYear = date("Y");
            $currentMonth = date("m");

            if(!empty($dob[0]) && $dob[1]<$currentMonth)
            {
                $currentYear = ($currentYear - 1);
                $age = ($currentYear - $dob[0]);
            }

            if(!empty($dob[0]) && $dob[1]>$currentMonth)
            {
                $age = ($currentYear - $dob[0]);
            }
        }
        return $age;
    }

    public function CvJob() {
        return $this->hasMany('CvJob', 'cv_tbl_id', 'cv_tbl_id')->orderBy('cv_jobs_id', 'DESC');
    }

    public function CvEdu() {
        return $this->hasMany('CvEdu', 'cv_tbl_id', 'cv_tbl_id')->orderBy('passing_year', 'DESC');
    }

    public function CvQualification() {
        return $this->hasMany('CvQualification', 'cv_tbl_id', 'cv_tbl_id')->orderBy('cv_qualification_id', 'DESC');
    }

    public function CvSpecialization() {
        return $this->hasMany('CvSpecialization', 'cv_tbl_id', 'cv_tbl_id');
    }

    public function Candidate() {
        return $this->belongsTo('Candidate', 'cv_tbl_id', 'cv_tbl_id');
    }

}

?>