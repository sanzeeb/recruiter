<?php
/**
 * Created by PhpStorm.
 * User: Himel
 * Date: 6/3/14
 * Time: 5:06 PM
 */

class CvQualification extends Eloquent{

    /**
     * Table Name
     * @var string
     */
    protected $table = 'cv_qualification';

    /**
     * Primary Key
     * @var string
     */
    protected $primaryKey = 'cv_qualification_id';

    /**
     * @var array  only attributes that can be fill-able
     */
    protected $fillable = array('cv_q_title','certificates','institution','finished_year');

} 