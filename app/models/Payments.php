<?php
/**
 * Created by PhpStorm.
 * User: Himel
 * Date: 7/20/14
 * Time: 3:20 PM
 */

class Payments extends Eloquent{

    protected $table = 'payments';

    protected $primaryKey = 'payment_id';

    protected $fillable = array('agreement_id','amount','sender_id','receiver_id','status');

    public function ReceiverUserAcc()
    {
        return $this->belongsTo('User','receiver_id','id');
    }

    public function SenderUserAcc()
    {
        return $this->belongsTo('User','sender_id','id');
    }
    
    public function AgreementAcc()
    {
        return $this->belongsTo('Agreements','agreement_id','agreement_id');
    }
} 