<?php
/**
 * Created by PhpStorm.
 * User: Himel
 * Date: 2/18/14
 * Time: 2:30 PM
 */

class Authenticate {



    public static function login($user,$pass,$remember_me=0)
    {


        try{

            $user =  User::where('username',$user)->where('password',md5($pass))->first();
            if(count($user))
            {
                if($remember_me)
                {

                    //Helpers::debug(Config::get('session'));die();
                    $user->remember_me = $remember_me;
                    $user->save();
                }else{
                    $user->remember_me = $remember_me;
                    $user->save();
                }
                if($user->user_status == 0)
                    return 2;
                if((($user->user_type == "Candidate") && (preg_match('/'.$_SERVER['SERVER_NAME'].'/',ASIAN_DOMAIN))) ||
                    (($user->user_type == "Admin" || $user->user_type == 'Employer') && preg_match('/'.$_SERVER['SERVER_NAME'].'/', JOB_DOMAIN)))
                {
                    return array('type'=>$user->user_type);
                }
                //Helpers::debug($user);die();
                Session::put('id', $user->id);

                Session::put('user',$user);
              
                return 1;
            }else{
                return 0;
            }
        }catch(Exception $e)
        {
           // Helpers::debug($e->getMessage());die();
            throw new DbConnectionException('<h3 style="text-align:center;color:tomato;">Db connection failed. Please contact with provider to check is db running?</h3>');
        }


    }


    public static function check()
    {
        $user = Session::get('user');

        if(empty($user))
        {

            header('Location:'.Request::root());
            exit();
        }
        else
        {
            return $user;
        }
    }

    public static function hasSession()
    {
        return Session::get('user');
    }

    public static function getUserType()
    {
        $user = Session::get('user');

        if(!empty($user))
        {
           return $user->user_type;
        }
        return null;
    }

    public static function getPermission($user)
    {
        $userRoles = UserRole::where('user_type',$user->user_type)->get();
        if(count($userRoles))
        {
            foreach($userRoles as $role)
            {
                if($role->Module->route == Request::segment(1))
                    return $role;
            }
        }
    }


} 