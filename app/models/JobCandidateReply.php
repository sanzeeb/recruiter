<?php
/**
 * Created by PhpStorm.
 * User: HimelC51
 * Date: 12/14/14
 * Time: 4:06 PM
 */

class JobCandidateReply extends Eloquent{

    /**
     * Table Name
     * @var string
     */
    protected $table = 'job_candidate_reply';

    protected $fillable = array('job_id','replier_type','replier_id','reply');

    public static function boot()
    {
        parent::boot();

        static::created(function($object){

            $job_id =  $object->job_id;
            $job = Jobs::find($job_id);
            if(count($job))
            {


                    $uObj = User::where('user_id',$object->replier_id);
                if($object->replier_type == "Employer")
                    $uObj = $uObj->where('user_type','Employer');
                else
                    $uObj = $uObj->where('user_type','Candidate');
                $userObj= $uObj->first();
                $inbox = new Inbox();
                $inbox->sender_id = $userObj->id;
                $inbox->read_status = 0;

                if($object->replier_type == 'Candidate')
                {
                    $empUser = User::where('user_id',$object->reply_to)->where('user_type','Employer')->first();
                    $inbox->receiver_id = $empUser->id;
                    $inbox->subject = 'Candidate replied to job '.$job->job_title;
                    $inbox->body = "<h5>Replied to job application</h5>";
                    $inbox->body .= "<p>{$inbox->subject}</p>";
                    $inbox->body .= " <br/> Visit job Link: <a href='".url(ASIAN_DOMAIN.'job/view/'.$job->jobs_id)."'>".url(ASIAN_DOMAIN.'job/view/'.$job->jobs_id)."</a>";
                    $inbox->in_type="Job Application";

                }else{
                    $candidateUser = User::where('user_id',$object->reply_to)->where('user_type','Candidate')->first();
                    $inbox->receiver_id = $candidateUser->id;
                    $inbox->subject = 'Employer replied to job '.$job->job_title;
                    $inbox->body = "<h5>Replied to job application</h5>";
                    $inbox->body .= "<p>{$inbox->subject}</p>";
                    $inbox->body .= " <br/> Visit job Link: <a href='".url(JOB_DOMAIN.'job/view/'.$job->jobs_id)."'>".url(JOB_DOMAIN.'job/view/'.$job->jobs_id)."</a>";
                    $inbox->in_type="Job Application";
                }

                $inbox->save();
            }

        });
    }

} 