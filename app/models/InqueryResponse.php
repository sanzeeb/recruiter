<?php
/**
 * Created by PhpStorm.
 * User: HimelC51
 * Date: 11/16/14
 * Time: 12:47 PM
 */

class InqueryResponse extends Eloquent{

    protected $table = 'inquery_response';


    protected $fillable = array('inquery_id','replier_id','replier_type','inquery_reason','status','cancel_by');


    public static function boot()
    {
        parent::boot();

        static::created(function($object){

           $inquery_id =  $object->inquery_id;
           $inquery = Inquiry::find($inquery_id);
           $user  = Authenticate::check();
           if($user->user_type=='Admin')
           {

           }elseif($user->user_type == 'Candidate'){

             if(count($inquery))
             {
                  $userObj = User::where('user_id',$object->replier_id)->where('user_type',$object->replier_type)->first();
                  
                  $domain = '';
                  if($object->replier_type == 'Candidate')
                  {

                      $adminUser = User::where('user_type','Admin')->get();
                      foreach($adminUser as $admin){
                        $inbox = new Inbox();
                        $inbox->sender_id = $userObj->id;
                        $inbox->read_status = 0;
                        $inbox->receiver_id = $admin->id;
                        $domain = ASIAN_DOMAIN;
                        $candidate = $inquery->Candidate;
                        $inbox->subject = $candidate->firstname.' Replied to Inquiry '.$inquery->Job->job_title;
                        $inbox->body = "<h5>{$candidate->firstname} Replied To Inquiry</h5>";
                        $inbox->body .= "Visit Inquiry Link: <a href='".url($domain.'inquiry/details/'.$inquery->id)."'>".url($domain.'inquiry/details/'.$inquery->id)."</a>";
                        $inbox->in_type="Reply";
                        $inbox->save();
                      }
                  }
                  else
                  { 
                      $inbox = new Inbox();
                      $inbox->sender_id = $userObj->id;
                      $inbox->read_status = 0;
                      $candUser = User::where('user_id',$inquery->candidate_id)->where('user_type','Candidate')->first();
                      $inbox->receiver_id = $candUser->id;
                      $domain = JOB_DOMAIN;
                      $candidate = $inquery->Candidate;
                      $inbox->subject = $candidate->firstname.' Replied to Inquiry '.$inquery->Job->job_title;
                      $inbox->body = "<h5>{$candidate->firstname} Replied To Inquiry</h5>";
                      $inbox->body .= "Visit Inquiry Link: <a href='".url($domain.'inquiry/details/'.$inquery->id)."'>".url($domain.'inquiry/details/'.$inquery->id)."</a>";
                      $inbox->in_type="Reply";
                      $inbox->save();
                  }
                 
             }
            }
        });

        static::updated(function($object){

            if($object->status == "Canceled")
            {
              $inquery = $object->Inquiry;
              $inbox = new Inbox();
              $candUser = User::where('user_id',$object->replier_id)->where('user_type',$object->replier_type)->first();
              $adminUser = Authenticate::check();
              $inbox->sender_id = $adminUser->id;
              $inbox->read_status = 0;
              $inbox->receiver_id = $candUser->id;
             
              $domain = JOB_DOMAIN;
              $inbox->subject = 'Reply was canceled of Inquiry '.$inquery->Job->job_title;
              $inbox->body = "<h5>Your reply was canceled to Inquiry {$inquery->Job->job_title} By Admin</h5>";
              $inbox->body .= "Visit Inquiry Link: <a href='".url($domain.'inquiry/details/'.$inquery->id)."'>".url($domain.'inquiry/details/'.$inquery->id)."</a>";
              $inbox->in_type="Reply";
              $inbox->save();  
            }
            elseif($object->status == 'Approved')
            {
              $inquery = $object->Inquiry;
              $inbox = new Inbox();
              $employerUser = User::where('user_id',$inquery->emp_id)->where('user_type','Employer')->first();
              $adminUser = Authenticate::check();
              $inbox->sender_id = $adminUser->id;
              $inbox->read_status = 0;
              $inbox->receiver_id = $employerUser->id;
              $domain = ASIAN_DOMAIN;
              $inbox->subject = 'Candidate Replied to Inquiry '.$inquery->Job->job_title;
              $inbox->body = "<h5>Candidate Replied To Inquiry By Candidate</h5>";
              $inbox->body .= "Visit Inquiry Link: <a href='".url($domain.'inquiry/details/'.$inquery->id)."'>".url($domain.'inquiry/details/'.$inquery->id)."</a>";
              $inbox->in_type="Reply";
              $inbox->save();  
            }

        });
    }

    public function Inquiry()
    {
      return $this->belongsTo('Inquiry','inquery_id','id');
    }



} 