<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
        protected $primaryKey='id';

        /**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');


	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the token value for the "remember me" session.
	 *
	 * @return string
	 */
	public function getRememberToken()
	{
		return $this->remember_token;
	}

	/**
	 * Set the token value for the "remember me" session.
	 *
	 * @param  string  $value
	 * @return void
	 */
	public function setRememberToken($value)
	{
		$this->remember_token = $value;
	}

	/**
	 * Get the column name for the "remember me" token.
	 *
	 * @return string
	 */
	public function getRememberTokenName()
	{
		return 'remember_token';
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}
        
    /**
     * @var array  only attributes that can be fill-able
     */
    protected $fillable = array('username','user_type','password','user_id','user_status','user_email','id','reset_token','token_expire','token','role_id','remember_me','created_by');
    
    
    /**
     * Get Candidate Profile
     * @return type
     */
    public function Candidate()
    {
        return $this->belongsTo('Candidate','user_id','candidate_id');
    }
    
    
    /**
     * Get Employer profile
     * @return type
     */
    public function Employer()
    {
        return $this->belongsTo('Employer','user_id','emp_id');
    }

    public function Permissions()
    {
        return $this->hasMany('UserRole','user_type','user_type');
    }
    
    
    public function Schedules()
    {
        return $this->hasMany('Schedule','user_id','id');
    }

   
   	public function Role()
    {
        return $this->belongsTo('Role','role_id','role_id');
    }
    

}
