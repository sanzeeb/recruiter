<?php

class Permission extends Eloquent
{
	/**
     * Table Name
     * @var string
     */
    protected $table = 'permissions';

    /**
     * Primary Key
     * @var string
     */
    protected $primaryKey = 'permission_id';


	/**
     * @var array  only attributes that can be fill-able
     */
    protected $fillable = array('role_id','module','action');

    public static function getModulePermissions($roleId)
    {
    	$permissions = array();
    	$results = self::where('role_id',$roleId)->get();
    	if(count($results))
    	{
    		foreach($results as $result)
    		{
    			$permissions[$result->module]['action'] = json_decode($result->action);
    		}
    	}

    	return $permissions;
    }

    public static function getPermission($roleId,$module,$action)
    {
        $result = self::where('role_id',$roleId)->where('module',$module)->first();
        
        if(!empty($result))
        {
            $actions = json_decode($result->action);
            if(isset($actions->$action))
                return 1;
            else
                return 0;
        }
        return 0;
    }
}