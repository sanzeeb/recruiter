<?php
/**
 * Created by PhpStorm.
 * User: HimelC51
 * Date: 12/4/14
 * Time: 3:17 PM
 */

class TestProjectReply extends Eloquent{

    public static $Pending   = "Pending";
    public static $Approved  = "Approved";
    public static $Canceled  = "Canceled";
    public static $Submitted  = "Submitted";
    public static $Replied  = "Replied";

    protected $table = "test_project_reply";

    protected $fillable = array('tproject_id','replier_type','replier_id','reply','status','reason');


    public function TestProject()
    {
        return $this->belongsTo('TestProject','tproject_id','id');
    }

    public static function boot()
    {
        parent::boot();

        static::created(function($object){

            $tproject_id =  $object->tproject_id;
            $tproject = TestProject::find($tproject_id);
            $user  = Authenticate::check();
            if(count($tproject))
            {
                if($user->user_type=='Admin')
                {
                    
                    $userObj = User::where('user_type','Admin')->first();
                    $empObj = User::where('user_id',$tproject->emp_id)->where('user_type','Employer')->first();
                    $candidateObj = User::where('user_id',$tproject->candidate_id)->where('user_type','Candidate')->first();
                    $inbox = new Inbox();
                    $inbox->sender_id = $userObj->id;
                    $inbox->read_status = 0;
                    if($object->status == "Replied by admin")
                    {
                        $domain = ASIAN_DOMAIN;
                        $inbox->receiver_id = $empObj->id;
                        $inbox->subject = 'Test Project '.$tproject->project_name . ' Replied by admin';
                        $inbox->body =  " <br/> <h5>$inbox->subject</h5>";
                        $inbox->body .= " <br/> Visit Test Project Link: <a href='".url($domain.'tproject/details/'.$tproject->id)."'>".url($domain.'tproject/details/'.$tproject->id)."</a>";
                        $inbox->in_type="Reply";
                        $inbox->save();

                    }else if($object->status == "Canceled")
                    {
                        $domain = JOB_DOMAIN;
                        $inbox->receiver_id = $empObj->id;
                        $inbox->subject = 'Test Project '.$tproject->project_name . ' was Canceled';
                        $inbox->body =  " <br/> <h5>Your reply to '.$inbox->subject.' by admin </h5>";
                        $inbox->body .= " <br/> Visit Test Project Link: <a href='".url($domain.'tproject/details/'.$tproject->id)."'>".url($domain.'tproject/details/'.$tproject->id)."</a>";
                        $inbox->in_type="Reply";
                        $inbox->save();

                    }else if($object->status == "Replied")
                    {
                        $domain = ASIAN_DOMAIN;
                        $inbox->receiver_id = $empObj->id;
                        $inbox->subject = 'Replied to Test Project '.$tproject->project_name;
                        $inbox->body =  " <br/> <h5>'.$candidateObj->firstname .' '.$inbox->subject .'</h5>";
                        $inbox->body .= " <br/> Visit Test Project Link: <a href='".url($domain.'tproject/details/'.$tproject->id)."'>".url($domain.'tproject/details/'.$tproject->id)."</a>";
                        $inbox->in_type="Reply";
                        $inbox->save();
                    }

                }else if($user->user_type == "Candidate"){

                    $receivers = User::where('user_type','Admin')->get();

                    foreach($receivers as $receiver)
                    {
                        $inbox = new Inbox();
                        $inbox->sender_id = $user->user_id;
                        $inbox->read_status = 0;
                        $domain = ASIAN_DOMAIN;
                        $inbox->receiver_id = $receiver->id;
                        

                        $inbox->subject = 'Replied to Test Project '.$tproject->project_name;
                        $inbox->body =  " <br/> <h5>Replied To Test Project</h5>";
                        $inbox->body .= " <br/> Visit Test Project Link: <a href='".url($domain.'tproject/details/'.$tproject->id)."'>".url($domain.'tproject/details/'.$tproject->id)."</a>";
                        $inbox->in_type="Reply";
                        $inbox->save();
                    }
               
                   
                
                }else{

                }

            }

        });
        
        static::updated(function($object){

            $tproject_id =  $object->tproject_id;
            $tproject = TestProject::find($tproject_id);
            $empObj = User::where('user_id',$tproject->emp_id)->where('user_type','Employer')->first();
            $candidateObj = User::where('user_id',$tproject->candidate_id)->where('user_type','Candidate')->first();
            $user  = Authenticate::check();
            if($user->user_type=="Admin")
            {
                if($object->status == "Canceled")
                {
                    $inbox = new Inbox();
                    $inbox->sender_id = $user->id;
                    $inbox->read_status = 0;
                    $domain = JOB_DOMAIN;
                    $inbox->receiver_id = $candidateObj->id;
                    $inbox->subject = 'Reply on test project '.$tproject->project_name . ' was canceled';
                    $inbox->body =  " <br/> <h5>Your reply to $inbox->subject by admin </h5>";
                    $inbox->body .= " <br/> Visit Test Project Link: <a href='".url($domain.'tproject/details/'.$tproject->id)."'>".url($domain.'tproject/details/'.$tproject->id)."</a>";
                    $inbox->in_type="Reply";
                    $inbox->save();

                }elseif($object->status == "Approved"){

                }

            }else if($user->user_type=="Candidate")
            {

            }
        });
    }
} 