
<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Agreements extends Eloquent {

    protected $table = 'agreements';
    protected $primaryKey = 'agreement_id';
    protected $fillable = array('agreement_id', 'agreement_title', 'description', 'created_at', 'updated_at', 'amount', 'sender_id', 'receiver_id', 'approver_id', 'status', 'rating', 'rating_comment');

    public function ReceiverUserAcc() {
        return $this->belongsTo('User', 'receiver_id', 'id');
    }

    public function SenderUserAcc() {
        return $this->belongsTo('User', 'sender_id', 'id');
    }

    public function Payments() {
        return $this->hasMany('Payments', 'agreement_id', 'agreement_id');
    }

}

?>
