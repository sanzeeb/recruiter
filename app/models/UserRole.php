<?php
/**
 * Created by PhpStorm.
 * User: Himel
 * Date: 6/12/14
 * Time: 11:12 AM
 */

class UserRole extends Eloquent{

    /**
     * Table Name
     * @var string
     */
    protected $table = 'user_role';

    /**
     * Primary key
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     *
     * @var array
     */
    protected $fillable = array('user_type','module_id','module','route','create','update','delete','read');

    public function Module()
    {
        return $this->belongsTo("Modules","module_id","module_id");
    }

} 