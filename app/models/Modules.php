<?php
/**
 * Created by PhpStorm.
 * User: Himel
 * Date: 6/12/14
 * Time: 12:47 PM
 */

class Modules extends Eloquent{

    /**
     * Table Name
     * @var string
     */
    protected $table = 'modules';

    /**
     * Primary key
     * @var string
     */
    protected $primaryKey = 'module_id';

    /**
     *
     * @var array
     */
    protected $fillable = array('module','status');
} 