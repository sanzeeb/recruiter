<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Aproval extends Eloquent {

    protected $table = 'aproval';
    
    protected $primaryKey = 'aprov_id';
    
    protected $fillable = array('avail_id','status','employer_id','candidate_id','interview_request_id','job_id','created_at','updated_at','deleted_at');


    /*** Himel ***/
    public function User()
    {
        return $this->belongsTo('User','user_id','id');
    }
    /*****Himel ***/
    
    public function Candidate()
    {
        return $this->belongsTo('Candidate','candidate_id','candidate_id');
    }

    public function InterviewRequest()
    {
        return $this->belongsTo('InterviewRequest','interview_request_id','id');
    }
    
}
?>
