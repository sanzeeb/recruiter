<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Employer extends Eloquent {

    protected $table = 'employer';
    
    protected $primaryKey = 'emp_id';
    
    protected $fillable = array('email','title','address','phone','website','contact_person','contact_person_phone');

    public function UserAcc()
    {
        return $this->belongsTo('User','candidate_id','user_id');
    }
    public function UserEAcc()
    {
        return $this->belongsTo('User','emp_id','user_id');
    }

    public function Favorites(){
        return $this->hasMany('EmployerFavorite','emp_id','emp_id');
    }
}
?>
