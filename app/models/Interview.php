<?php
/**
 * Created by PhpStorm.
 * User: HimelC51
 * Date: 11/17/14
 * Time: 4:09 PM
 */

class Interview extends Eloquent{

    public static $Pending   = "Pending";
    public static $Accepted   = "Accepted";
    public static $AcceptedPending   = "Accepted Pending";
    public static $Offered   = "Offered";
    public static $Approved  = "Approved";
    public static $Declined  = "Declined";
    public static $Replied   = "Replied";
    public static $Canceled  = "Canceled";
    public static $Completed  = "Completed";
    public static $Progressing  = "Progressing";

    protected $table = "interviews";

    protected $primaryKey = "interview_id";

    protected $fillable = array('emp_id','candidate_id','job_id','topics','salary','type','interview_date','interview_time','interview_duration','status','message');

    public function Candidate()
    {
        return $this->belongsTo('Candidate','candidate_id','candidate_id');
    }

    public function Employer()
    {
        return $this->belongsTo('Employer','emp_id','emp_id');
    }

    public function Job()
    {
        return $this->belongsTo('Jobs','job_id','jobs_id');
    }

    public function InterviewReplies(){
        return $this->hasMany('InterviewReply','interview_id','interview_id')->orderBy('id','DESC');
    }

    public function Feedback()
    {
        return $this->hasOne("InterviewFeedback",'interview_id','interview_id');
    }

    public static function boot()
    {
        parent::boot();

        static::created(function($object){
            $sender = User::where('user_id',$object->emp_id)->where('user_type','Employer')->first();
            $emp =  Employer::find($sender->user_id);
            $receivers = User::where('user_type','Admin')->get();
            foreach($receivers as $receiver){
                $inbox = new Inbox();
                $inbox->sender_id = $sender->id;
                $inbox->receiver_id = $receiver->id;
                $inbox->read_status = 0;
                $inbox->subject = $emp->title.' Interview requested for '.$object->Candidate->firstname;
                $inbox->in_type = 'Interview';
                $inbox->body = '<h5>New Interview Request</h5>';
                $inbox->body .= $inbox->subject;
                $inbox->body .= ' <br/> Requested by : '.$object->Employer->title;
                $inbox->body .= ' <br/> Interview request Link <a href="'.url(ASIAN_DOMAIN.'interview/details/'.$object->interview_id).'">'.url(ASIAN_DOMAIN.'interview/details/'.$object->interview_id).'</a>';
                $inbox->save();
            }
        });

        static::updated(function($object){
            $sender = Authenticate::check();
            if($sender->user_type=='Admin')
            {
                if($object->status ==  "Approved")
                {
                    
                    $emp      = User::where('user_id',$object->emp_id)->where('user_type','Employer')->first();
                    $receiver = User::where('user_id',$object->candidate_id)->where('user_type','Candidate')->first();
                   
                    $inbox = new Inbox();
                    $inbox->sender_id   = $emp->id;
                    $inbox->receiver_id = $receiver->id;
                    $inbox->read_status = 0;
                    $inbox->subject = 'Interview request send by '.$object->Employer->title;
                    $inbox->in_type = 'Interview';
                    $inbox->body    = '<h5>Interview request received from </h5>';
                    $inbox->body .= $inbox->subject;

                    $inbox->body .= ' <br/> Interview Link <a href="'.url(JOB_DOMAIN.'interview/details/'.$object->interview_id).'">'.url(ASIAN_DOMAIN.'interview/details/'.$object->interview_id).'</a>';
                    $inbox->save();

                }else if($object->status ==  "Accepted")
                {
                    $attributes = $object->getAttributes();
                    if($attributes['status'] == 'Canceled')
                        return;
                    $receiver = User::where('user_id',$object->emp_id)->where('user_type','Employer')->first();
                    $inbox = new Inbox();
                    $inbox->sender_id   = $sender->id;
                    $inbox->receiver_id = $receiver->id;
                    $inbox->read_status = 0;
                    $inbox->subject = 'Interview accepted by '.$object->Candidate->firstname;
                    $inbox->in_type = 'Interview';
                    $inbox->body    = '<h5>Interview accepted</h5>';
                    $inbox->body .= $inbox->subject;
                    $inbox->body .= ' <br/> Accepted by : '.$object->Candidate->firstname;
                    $inbox->body .= ' <br/> Interview Link <a href="'.url(ASIAN_DOMAIN.'interview/details/'.$object->interview_id).'">'.url(ASIAN_DOMAIN.'interview/details/'.$object->interview_id).'</a>';
                    $inbox->save();

                }else if($object->status == "Replied"){

                    $attributes = $object->getAttributes();
                    if($attributes['status'] == 'Canceled')
                        return;
                    $receiver = User::where('user_id',$object->emp_id)->where('user_type','Employer')->first();
                    $inbox = new Inbox();
                    $inbox->sender_id   = $sender->id;
                    $inbox->receiver_id = $receiver->id;
                    $inbox->read_status = 0;
                    $inbox->subject = $object->Candidate->firstname.' Replied to Interview';
                    $inbox->in_type = 'Interview';
                    $inbox->body    = '<h5>Replied to your interview</h5>';
                    $inbox->body .= $inbox->subject;
                    $inbox->body .= ' <br/> Replied by : '.$object->Candidate->firstname;
                    $inbox->body .= ' <br/> Interview Link <a href="'.url(ASIAN_DOMAIN.'interview/details/'.$object->interview_id).'">'.url(ASIAN_DOMAIN.'interview/details/'.$object->interview_id).'</a>';
                    $inbox->save();

                }elseif($object->status == "Replied by admin"){
                   
                    
                    $receiver = User::where('user_id',$object->emp_id)->where('user_type','Employer')->first();
                    $inbox = new Inbox();
                    $inbox->sender_id = $sender->id;
                    $inbox->receiver_id = $receiver->id;
                    $inbox->read_status = 0;
                    $inbox->subject = 'Replied by '.$sender->username.' to your Interview requested ';
                    $inbox->in_type = 'Interview';
                    $inbox->body  = "<h5>{$inbox->subject}</h5>";
                    $inbox->body .= $inbox->subject;
                 
                    $inbox->body .= ' <br/> Interview Link <a href="'.url(ASIAN_DOMAIN.'interview/details/'.$object->interview_id).'">'.url(JOB_DOMAIN.'interview/details/'.$object->interview_id).'</a>';
                    $inbox->save(); 

                }elseif($object->status == "Canceled"){
                   
                    
                    $receiver = User::where('user_id',$object->emp_id)->where('user_type','Employer')->first();
                    $inbox = new Inbox();
                    $inbox->sender_id = $sender->id;
                    $inbox->receiver_id = $receiver->id;
                    $inbox->read_status = 0;
                    $inbox->subject = 'Interview requested Canceled by '.$sender->username;
                    $inbox->in_type = 'Interview';
                    $inbox->body  = "<h5>{$inbox->subject}</h5>";
                    $inbox->body .= $inbox->subject;
                 
                    $inbox->body .= ' <br/> Interview Link <a href="'.url(ASIAN_DOMAIN.'interview/details/'.$object->interview_id).'">'.url(JOB_DOMAIN.'interview/details/'.$object->interview_id).'</a>';
                    $inbox->save(); 

                    $employer = Employer::find($object->emp_id);

                    $emailReceiver = array(
                        'name'  => $receiver->username,
                        'email' => $employer->email
                    );
                    $messageBody = array(
                        'name' => $emailReceiver['name'],
                        'body' => '<h2>Sorry, You Interview Canceled by admin.</h2> <p>You should create Interview according to rules and convension of DJIT</p>'
                    );
                    try{
                    Email::sendCancelMail($emailReceiver,$messageBody);
                    }catch(Exception $ex)
                    {}
                }
                

            }
            else if($sender->user_type=='Candidate')
            {

                $attributes = $object->getAttributes();
                $receivers = User::where('user_type','Admin')->get();
                foreach($receivers as $receiver)
                {
                    $inbox = new Inbox();
                    $inbox->sender_id = $sender->id;
                    $inbox->receiver_id = $receiver->id;
                    $inbox->read_status = 0; 
                    $inbox->subject = 'Interview '.$attributes['status'].' by '.$object->Candidate->firstname;
                    $inbox->in_type = 'Interview';
                    $inbox->body = '<h5>Interview '.$attributes['status'].' for '.$object->Candidate->firstname.' </h5>';
                    $inbox->body .= $inbox->subject;
                    $inbox->body .= 'Replied by : '.$object->Candidate->firstname;
                    $inbox->body .= 'Interview Link <a href="'.url(ASIAN_DOMAIN.'interview/details/'.$object->interview_id).'">'.url(ASIAN_DOMAIN.'interview/details/'.$object->interview_id).'</a>';
                    $inbox->save();
                }
                
            }
            else if($sender->user_type=='Employer')
            {
                $attributes = $object->getAttributes();
                if($attributes['status'] == 'Offered')
                {
                    $receiver = User::where('user_id',$object->candidate_id)->where('user_type','Candidate')->first();
                    $inbox = new Inbox();
                    $inbox->sender_id = $sender->id;
                    $inbox->receiver_id = $receiver->id;
                    $inbox->read_status = 0;
                    $inbox->subject = 'Candidate '.$attributes['status'].' by '.$object->Candidate->firstname;
                    $inbox->in_type = 'Interview';
                    $inbox->body = '<h5>Interview Process Completed and '.$attributes['status'].' </h5>';
                    $inbox->body .= $inbox->subject;
                    $inbox->body .= 'Interview Link <a href="'.url(JOB_DOMAIN.'interview/details/'.$object->interview_id).'">'.url(JOB_DOMAIN.'interview/details/'.$object->interview_id).'</a>';
                    //$inbox->save();
                }
            }
        });
    }

} 