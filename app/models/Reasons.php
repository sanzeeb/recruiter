<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Reasons extends Eloquent {

    protected $table = 'reasons';
    
    protected $primaryKey = 'reason_id';
    
    protected $fillable = array('reason_id','description','created_at','updated_at','agreement_id','rejected_by');


}
?>
