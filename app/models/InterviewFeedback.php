<?php
/**
 * Created by PhpStorm.
 * User: HimelC51
 * Date: 11/17/14
 * Time: 4:09 PM
 */

class InterviewFeedback extends Eloquent{

    protected  $table = "interviews_feedback";

    protected $fillable = array('interview_id','feedback');
} 