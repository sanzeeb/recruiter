<?php
/**
 * Created by PhpStorm.
 * User: Himel
 * Date: 6/3/14
 * Time: 5:06 PM
 */

class CvJob extends Eloquent{

    /**
     * Table Name
     * @var string
     */
    protected $table = 'cv_jobs';

    /**
     * Primary Key
     * @var string
     */
    protected $primaryKey = 'cv_jobs_id';

    /**
     * @var array  only attributes that can be fill-able
     */
    protected $fillable = array('title','start','end','company_name','designation','department');

} 