<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Skills extends Eloquent {

    protected $table = 'skills';
    
    protected $primaryKey = 'skill_id';
    
    protected $fillable = array('skill_id','skill_name','skill_description','created_at','updated_at','deleted_at','display_order','skill_photo');


}
?>
