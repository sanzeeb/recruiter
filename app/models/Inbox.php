<?php
/**
 * Created by PhpStorm.
 * User: Himel
 * Date: 6/22/14
 * Time: 12:51 PM
 */

class Inbox extends Eloquent{

    /**
     * Table Name
     * @var string
     */
    protected $table = 'inbox';

    /**
     * Primary Key
     * @var string
     */
    protected $primaryKey = 'inbox_id';

    /**
     * @var array  only attributes that can be fill-able
     */
    protected $fillable = array('sender_id','receiver_id','read_status','subject','body');

    public function User()
    {
       return $this->belongsTo('User','sender_id','id');
    }
} 