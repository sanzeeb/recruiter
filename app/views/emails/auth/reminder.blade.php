<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>

		<h2>Hi {{$user->username}}, Reset Password</h2>

		<div>
			To reset your password, complete this form: <a href="{{url($link)}}">{{url($link)}}</a>
            After 24 hrs later link will expire.
		</div>
        <p>
           If you did not sent request for password reset then ignore this email.
        </p>
	</body>
</html>
