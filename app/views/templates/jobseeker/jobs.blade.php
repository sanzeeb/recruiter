@extends('templates.jobseeker.jobseeker')



@section('wrapper')	

		
<div class="inner-pag-title"><div class="inner">About Us</div></div>


		<div id="page-content"><!-- start content -->

			<div class="content-about">

				<div class="container"><!-- container -->

				<div class="spacer-1">&nbsp;</div>

					<div class="row clearfix">

						<div class="col-md-7">

								<h1 class="page-title">{{{$about->title or ''}}} </h1>

								<p>

								{{{$about->content or ''}}}

                                </p>

						</div>



                        <div class="container">
                            <h1>Find Jobs</h1>
                            <p>Skill: {{$skill}}</p>
                            <!--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>-->

                            <br/>
                            @if(count($jobs))
                                @foreach($jobs as $job)
                            <div class="job-line">
                                @if(Session::get('id')!="")
                                <?php $link = url('job/view/'.$job->jobs_id); ?>
                                @else
                                <?php $link = url('jobseeker/register-job-seeker/?ref='.url('job/view/'.$job->jobs_id)); ?>
                                @endif
                                <span class="c-title">{{{$job->job_title}}}</span>
                                <a href="{{$link}}" class="c-apply">Apply</a></a>

                                <span class="c-date">{{Helpers::dateTimeFormat('j F, Y',$job->created_at)}}</span>
                                <span class="c-by">{{$job->JobCreator()}}</span>
                            </div>

                                @endforeach
                            @endif
                            <!--<div class="job-line">

                                <span class="c-title">Lorem Ipsum is simply dummy text 5and typesetting industry.</span>
                                <span class="c-by">carbon51</span>
                                <span class="c-date">1 / Dec / 2014</span>
                                <a href="#" class="c-apply">Apply</a></a>
                            </div>

                            <div class="job-line">

                                <span class="c-title">Lorem Ipsum is simplytypesetting industry.</span>
                                <span class="c-by">carbon51</span>
                                <span class="c-date">1 / Dec / 2014</span>
                                <a href="#" class="c-apply">Apply</a></a>
                            </div>

                            <div class="job-line">

                                <span class="c-title">Lorem Ipsum is simply dummy text of the printtting industry.</span>
                                <span class="c-by">carbon51</span>
                                <span class="c-date">1 / Dec / 2014</span>
                                <a href="#" class="c-apply">Apply</a></a>
                            </div>

                            <div class="job-line">

                                <span class="c-title">Lorem Ipsum is simply dummy text of inting and typesetting industry.</span>
                                <span class="c-by">carbon51</span>
                                <span class="c-date">1 / Dec / 2014</span>
                                <a href="#" class="c-apply">Apply</a></a>
                            </div>

                            <div class="job-line">

                                <span class="c-title">Lorem Ipsum is simply dunting and typesetting industry.</span>
                                <span class="c-by">carbon51</span>
                                <span class="c-date">1 / Dec / 2014</span>
                                <a href="#" class="c-apply">Apply</a></a>
                            </div>-->

                            {{$jobs->links()}}

                        </div>

					</div>



					<div class="spacer-1">&nbsp;</div>

				</div><!-- container -->





				<div id="cs"><!-- CS -->

					<div class="container">

					<div class="spacer-1">&nbsp;</div>

						<h1>If You Have Any Quries?</h1>

						<p>

							Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintockContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock

						</p>

						<h1 class="phone-cs">Call: +88 01713 493 282</h1>

					</div>

				</div><!-- CS -->

			</div><!-- end content -->

		</div><!-- end page content -->





@stop

