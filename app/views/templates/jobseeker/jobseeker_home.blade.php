@extends('templates.jobseeker.jobseeker')

@section('wrapper')		

<div class="main-slider"><!-- start main-headline section -->
    <div class="slider-nav">
        <a class="slider-prev"><i class="fa fa-chevron-circle-left"></i></a>
        <a class="slider-next"><i class="fa fa-chevron-circle-right"></i></a>
    </div>
    <div id="home-slider" class="owl-carousel owl-theme">
       <div class="item-slide">
            <img src="{{ $theme }}images/upload/home_01_01.JPG" class="img-responsive" alt="dummy-slide" />
        </div>
        <div class="item-slide">
            <img src="{{ $theme }}images/upload/home_02_01.JPG" class="img-responsive" alt="dummy-slide" />
        </div>
     <div class="item-slide">
            <img src="{{ $theme }}images/upload/home_03_01.JPG" class="img-responsive" alt="dummy-slide" />
        </div>
    </div>
</div>		

<div class="recent-job"><!-- Start Recent Job -->
    <div class="container">
        <div class="row">


            <section class=" wrapper_x site-min-height_x">
                <div class="row_x">
                    <div class="col-lg-12"> <p class="hd-title_x pop-tex">Looking for IT Jobs?</p></div>

                </div>
                
                
                <!--<div class="col-lg-4">
                        <section class="panel_x" style="border:1px solid #e6e6e6;border-top:solid 3px #BCD74A;">
                            <div class="panel-body_x" style="text-align:left;">
                                <a style="float:left; margin-right:8px; padding:2px; border:solid 1px #CCC;" href="#">                                
                                    <img height='60' alt="thumbnail" src="{{$theme}}images/c-logo.png" /></a>
                                <p align="justify" style="font-size:13px; font-family:Arial; line-height:normal;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the.</p>
                                <div class="a_clearfix"></div>
                            </div>
                        </section>
                    </div>
                    
                <div class="col-lg-4">
                        <section class="panel_x" style="border:1px solid #e6e6e6;border-top:solid 3px #BCD74A;">
                            <div class="panel-body_x" style="text-align:left;">
                                <a style="float:left; margin-right:8px; padding:2px; border:solid 1px #CCC;" href="#">                                
                                    <img height='60' alt="thumbnail" src="{{$theme}}images/java.jpg" /></a>
                                <p align="justify" style="font-size:13px; font-family:Arial; line-height:normal;"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the.</p>
                                <div class="a_clearfix"></div>
                            </div>
                        </section>
                    </div>
                    
                <div class="col-lg-4">
                        <section class="panel_x" style="border:1px solid #e6e6e6;border-top:solid 3px #BCD74A;">
                            <div class="panel-body_x" style="text-align:left;">
                                <a style="float:left; margin-right:8px; padding:2px; border:solid 1px #CCC;" href="#">                                
                                    <img height='60' alt="thumbnail" src="{{$theme}}images/javascript_logo.png" /></a>
                                <p align="justify" style="font-size:13px; font-family:Arial; line-height:normal;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the.</p>
                                <div class="a_clearfix"></div>
                            </div>
                        </section>
                    </div>-->
                
                

                <div class="row_x" style="margin:0;">
                    
                    
                    @foreach($skills as $skill)
                    <div class="col-lg-4">
                        <section class="panel_x" style="border:1px solid #e6e6e6;border-top:solid 3px #BCD74A;">
                            <div class="panel-body_x" style="text-align:left;">
                                <?php $skill_name=base64_encode($skill->skill_name); ?>
                                <a style="float:left; margin-right:8px; padding:2px; border:solid 1px #CCC;" href="{{{url('jobseeker/jobs')}}}/{{{$skill_name}}}">
                                    @if(!empty($skill->skill_photo) && file_exists('data/skill/'.$skill->skill_photo))
                                    <img height='60' alt="thumbnail" alt="{{{$skill->skill_photo or ''}}}" src="{{{url('/')}}}/data/skill/{{{$skill->skill_photo}}}"/>
                                    @else
                                    <img height='60' alt="thumbnail" alt="thumbnail" src="{{$theme}}images/placeholder.gif"/>
                                    @endif
                                </a>
                                <p align="justify" style="font-size:13px; line-height:normal;">
                                    {{$skill->skill_description}}
                                </p>

                                <!--<a href="{{{url('recruiter/top-engineers')}}}/{{{$skill_name}}}">
                                @if($skill->skill_name=="microcontroller_programming")
                                    micro programming
                                @else
                                    {{{$skill->skill_name}}}
                                @endif
                                </a>-->
                                <div class="a_clearfix"></div>
                            </div>
                        </section>
                    </div>
                <!--<div class="col-lg-4" style="text-align:center;">
                        <section class="panel_x">
                            <div class="panel-body_x">
                                <a href="{{{url('jobseeker/register-job-seeker')}}}">
@if(!empty($skill->skill_photo) && file_exists('data/skill/'.$skill->skill_photo))
                <img height='60' alt="{{{$skill->skill_photo or ''}}}" src="{{{url('/')}}}/data/skill/{{{$skill->skill_photo}}}"/>
                @else
                <img height='60' alt="thumbnail" src="{{$theme}}images/placeholder.gif"/>
                @endif
                </a>
                <br/>
                   <a href="{{{url('jobseeker/register-job-seeker')}}}"> 
                   
                                                   @if($skill->skill_name=="microcontroller_programming")
                                micro programming
                                @else
                                {{{$skill->skill_name}}}
                                @endif
                                             
                                
                                </a></div>
                        </section>
                    </div>-->
                    
                    @endforeach
                    
                    
                </div>

            
            </section>
            <div class="clearfix"></div>
        </div>
    </div>
</div><!-- end Recent Job -->
<!--<div class="job-status">
    <div class="container">
        <h1>Jobs Stats Updates</h1>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        
        <br/>
        <div class="job-line">    
        <span class="c-title">
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.<a href="#" class="link-color">...more</a></p>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. <a href="#" class="link-color">...less</a></p></span>
        <span class="c-by">carbon51</span>
        <span class="c-date">1 / Dec / 2014</span>
        <a href="#" class="c-apply">Apply</a></a>
        </div>
        
        <div class="job-line">
        <span class="c-title">Lorem Ipsum is simply dummy text 5and typesetting industry.</span>
        <span class="c-by">carbon51</span>
        <span class="c-date">1 / Dec / 2014</span>
        <a href="#" class="c-apply">Apply</a></a>
        </div>
        
        <div class="job-line">
        <span class="c-title">Lorem Ipsum is simplytypesetting industry.</span>
        <span class="c-by">carbon51</span>
        <span class="c-date">1 / Dec / 2014</span>
        <a href="#" class="c-apply">Apply</a></a>
        </div>
        
        <div class="job-line">
        <span class="c-title">Lorem Ipsum is simply dummy text of the printtting industry.</span>
        <span class="c-by">carbon51</span>
        <span class="c-date">1 / Dec / 2014</span>
        <a href="#" class="c-apply">Apply</a></a>
        </div>
        
        <div class="job-line">
        <span class="c-title">Lorem Ipsum is simply dummy text of inting and typesetting industry.</span>
        <span class="c-by">carbon51</span>
        <span class="c-date">1 / Dec / 2014</span>
        <a href="#" class="c-apply">Apply</a></a>
        </div>
        
        <div class="job-line">
        <span class="c-title">Lorem Ipsum is simply dunting and typesetting industry.</span>
        <span class="c-by">carbon51</span>
        <span class="c-date">1 / Dec / 2014</span>
        <a href="#" class="c-apply">Apply</a></a>
        </div>

        <div class="counter clearfix">
            <div class="counter-container">
                <div class="counter-value">{{{$jobs}}}</div>
                <div class="line"></div>
                <p>job posted</p>
            </div>


            <div class="counter-container">
                <div class="counter-value">{{{$resumes}}}</div>
                <div class="line"></div>
                <p>Resume posted</p>
            </div>

            <div class="counter-container">
                <div class="counter-value">{{{$employers}}}</div>
                <div class="line"></div>
                <p>Companies</p>
            </div>

            <div class="counter-container">
                <div class="counter-value">{{{$candidates}}}</div>
                <div class="line"></div>
                <p>Job Seeker</p>
            </div>
        </div>

    </div>
</div>-->
<div class="step-to">
    <div class="container">
        <h1 style="margin-top:30px;">Easiest Way To Use</h1>
        <p>
            when an unknown printer took a galley of type and scrambled it to make a type specimen book.
        </p>

        <div class="step-spacer"></div>
        <div id="step-image">
            <div class="step-by-container">
                <div class="step-by">
                    First Step
                    <div class="step-by-inner">
                        <div class="step-by-inner-img">
                            <img src="{{ $theme }}images/step-icon-1.png" alt="step" />
                        </div>
                    </div>
                    <h5>Register with us</h5>
                </div>

                <div class="step-by">
                    Second Step
                    <div class="step-by-inner">
                        <div class="step-by-inner-img">
                            <img src="{{ $theme }}images/step-icon-2.png" alt="step" />
                        </div>
                    </div>
                    <h5>Create your profile</h5>
                </div>

                <div class="step-by">
                    Third Step
                    <div class="step-by-inner">
                        <div class="step-by-inner-img">
                            <img src="{{ $theme }}images/step-icon-3.png" alt="step" />
                        </div>
                    </div>
                    <h5>Upload your resume</h5>
                </div>

                <div class="step-by">
                    Now it's our turn
                    <div class="step-by-inner">
                        <div class="step-by-inner-img">
                            <img src="{{ $theme }}images/step-icon-4.png" alt="step" />
                        </div>
                    </div>
                    <h5>Now take rest :)</h5>
                </div>

            </div>
        </div>
        <div class="step-spacer"></div>
    </div>
</div>

<div id="company-post">
    <div class="container">

        <h1 style="margin:25px 0 20px 0;">Companies Who Have Posted Jobs</h1>
        <div style="margin:0px 0 25px 0;" id="company-post-list" class="owl-carousel company-post">
            @foreach($company as $comp)            
            <div class="company">                           
                @if(!empty($comp->photo_name) && file_exists('data/employer/'.$comp->photo_name))
                 <img height="50" width="300" class="img-responsive" alt="{{{$comp->photo_name or ''}}}" src="{{{url('/')}}}/data/employer/{{{$comp->photo_name}}}"/>
                 @else
                 <img height="50" width="300" class="img-responsive" alt="thumbnail" src="{{$theme}}images/placeholder.gif"/>
                 @endif
            
            </div>            

            @endforeach
        </div>
    </div>
</div>

@stop