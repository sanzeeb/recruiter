@extends('templates.jobseeker.jobseeker')



@section('wrapper')



<div class="inner-pag-title"><div class="inner">Forget your password</div></div>

<div class="pagemid">

    <div class="inner">
    <style>
    .reset-pass-box h2{font-size:14px; font-weight:400; color:#333; line-height:normal; padding:20px 0;}
	.reset-pass-box{-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px; border:2px solid #a6ce39; padding:15px 20px; width:70%; margin:100px auto;}
    .reset-pass-box h1{background:#a6ce39; color:#FFF; font-size:14px; line-height:40px; text-indent:20px; text-transform:uppercase; margin:-15px -20px 0; min-height:40px;}
    </style>

        <div id="main">
                <div class="entry-content reset-pass-box">
                <h1>Recover Password</h1>
                <h2>Your Reset password request sent to you email address. Please check inbox. if not found check to the spam folder and check that email as not spam</h2>
            </div>
        </div>
    </div>
</div>
@stop