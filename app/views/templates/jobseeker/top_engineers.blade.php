@extends('templates.recruit.recruit')



@section('wrapper')		



<div id="subheader">

    <div class="inner">

        <div class="subdesc">

            <h1 class="page-title">Top Ranked Engineers</h1>

            <div class="customtext"><h2></h2></div>

        </div>

        <div class="breadcrumbs"><!-- Breadcrumb NavXT 5.0.1 -->

            <a class="home" href="#" title="Go to Hostme v2.">DJIT</a> <a href="#" title="Go to Pages." > </a> &gt;Top Ranking Engineers

        </div>

    </div>

</div><!-- #subheader -->



<div class="pagemid">

    <div class="inner">



        <div id="main">

            <div class="entry-content">





                <div class="clear"></div>

                <div class="divider thin" style="margin:40px 0 15px 0;"></div>



                <div style="font-size:26px;text-align:center;">Joined by an awesome team to take over the world!</div>

                <div class="divider thin" style="margin:15px 0 60px 0;"></div>



                @foreach($expertises as $expert)





                <?php $jobseeker = CvTable::find($expert->cv_tbl_id); ?>





                <div class="one_fifth">

                    <div  data-id="bounce" class="bio iva_anim">

                        <?php $candidate = Candidate::where('cv_tbl_id', $expert->cv_tbl_id)->first(); ?>

                        @if(!empty($candidate->photo) && file_exists('data/profile/'.$candidate->photo))

                        <img alt="{{{$candidate->photo or ''}}}" src="{{{url('/')}}}/data/profile/{{{$candidate->photo}}}"/>

                        @else

                        <img alt="thumbnail" src="{{$theme}}images/placeholder.gif"/>

                        @endif

                        

                        <div class="details bio_meta middl">

                            <div class="rounded"><h3 class="rounded_in">{{{$expert->cv_tbl_id}}}</h3></div><h4 class="clor">{{{$jobseeker->name}}}</h4><span class="staff-role"> <?php

                                $title = CvJob::where('cv_tbl_id', $expert->cv_tbl_id)->first();

                                echo $title->title;

                                ?></span>

                        </div>

                        <div class="sociables">

                            @if(Session::get('id')!="")

                            <?php //echo Session::get('id');  ?>

                            <a href="{{{url('resume/display-resume')}}}/{{{$expert->cv_tbl_id}}}"><span class="staff-role">View more</span></a>

                        @else

                           <a href="{{{url('jobseeker/register-job-seeker')}}}/{{{$expert->cv_tbl_id}}}"><span class="staff-role">View more</span></a>

   

                        @endif

                        </div>

                    </div>

                    <div class="clear"></div>

                </div><!-- .one_fourth -->

                

                @endforeach







                <div class="divider thin" style="margin:15px 0 60px 0;"></div>





                <div class="clear"></div>				









            </div><!-- .entry-content -->

        </div><!-- #main -->

        <div class="clear"></div>



    </div><!-- .inner -->

</div><!-- .pagemid -->

@stop