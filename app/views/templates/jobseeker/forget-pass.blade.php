@extends('templates.jobseeker.jobseeker')
@section('wrapper')
<div class="inner-pag-title">
  <div class="inner">Forget your password</div>
</div>
<div class="pagemid">
  <div class="inner">
    <div id="main">
    <span style="padding-top:150px; color:#F00;">{{{$Msg}}}</span>
      <div class="entry-content" style="height:220px; padding-top:20px;">
        <form id="forgetPassFrm" action="{{url('jobseeker/forget-pass')}}" method="post">
          <p>Enter Your e-Mail Address<br>
            <span class="wpcf7-form-control-wrap your-name" style="float:left; width:50%;">
            <input type="text" class="form-control forgot-field" name="email"/>
            </span> <span>
            <input type="submit" class="btn  reg-log-btn green forgot-pass-btn" value="Request Reset Url"/>
            </span> </p>
        </form>
      </div>
    </div>
  </div>
</div>
@stop