<!DOCTYPE html>

<html lang="en">

  <head>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Jobs Engineers</title>

    <!-- Bootstrap -->

    <link href="{{ $theme }}bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<!-- Bootstrap -->



	<!-- Main Style -->

    <link href="{{ $theme }}style.css" rel="stylesheet">

	<!-- Main Style -->

    

	<link href="{{ $theme }}css/cs.css" rel="stylesheet">

    

	<!-- fonts -->

    <link href='http://fonts.googleapis.com/css?family=Nunito:300,400,700' rel='stylesheet' type='text/css'>

	<link href='http://fonts.googleapis.com/css?family=Raleway:400,300,500,600,700' rel='stylesheet' type='text/css'>

	<link href='{{ $theme }}font-awesome/css/font-awesome.css' rel="stylesheet" type="text/css">

  

     <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

	<!-- fonts -->



	<!-- Owl Carousel -->

	<link href="{{ $theme }}css/owl.carousel.css" rel="stylesheet">

    <link href="{{ $theme }}css/owl.theme.css" rel="stylesheet">

	<link href="{{ $theme }}css/owl.transitions.css" rel="stylesheet">



	<!-- Owl Carousel -->



	<!-- Form Slider -->

	<link rel="stylesheet" href="{{ $theme }}css/jslider.css" type="text/css">

	<link rel="stylesheet" href="{{ $theme }}css/jslider.round.css" type="text/css">

            <link href="{{ $theme }}css/bs.css" rel="stylesheet">

	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

  </head>

  <body>

	<div id="wrapper"><!-- start main wrapper -->

		<div id="header"><!-- start main header -->

			<div class="top-line">&nbsp;</div>

			<div class="top"><!-- top -->

				<div class="container">

					<div class="media-top-right">

						<ul class="media-top clearfix">

							<li class="item"><a href="#" target="blank"><i class="fa fa-twitter"></i></a></li>

							<li class="item"><a href="#" target="blank"><i class="fa fa-facebook"></i></a></li>

							<li class="item"><a href="#" target="blank"><i class="fa fa-linkedin"></i></a></li>

							<li class="item"><a href="#" target="blank"><i class="fa fa-rss"></i></a></li>

							<li class="item"><a href="#" target="blank"><i class="fa fa-google-plus"></i></a></li>

						</ul>

						<ul class="media-top-2 clearfix">

                                                       @if(Session::get('id')!="")
                            <li><a href="{{url('dashboard')}}" class="btn btn-default btn-green btn-sm"><?php $user=User::find(Session::get('id'));
						echo $user->username;
						?></a></li>
							<li><a href="{{{url('home/logout')}}}" class="btn btn-default btn-blue btn-sm">Log Out</a></li>

							@else

                                                        <li><a href="{{{url('jobseeker/register-job-seeker')}}}" class="btn btn-default btn-blue btn-sm">REGISTER</a></li>

							<li><a href="{{{url('jobseeker/register-job-seeker')}}}" class="btn btn-default btn-green btn-sm" >LOG IN</a></li>

                                                        

                                                        @endif

						</ul>

						<div class="clearfix"></div>

					</div>

				</div>

			</div><!-- top -->

			<div class="container"><!-- container -->

				<div class="row">

					<div class="col-md-4"><!-- logo -->

						<a href="{{{url('jobseeker/index')}}}" title="Job Board" rel="home">

							<img class="main-logo" src="{{ $theme }}images/logo.png" alt="job board" />

						</a>

					</div><!-- logo -->

					<div class="col-md-8 main-nav"><!-- Main Navigation -->

						<a id="touch-menu" class="mobile-menu" href="#"><i class="fa fa-bars fa-2x"></i></a>

						<nav>

							<ul class="menu">

								<li><a href="{{{url('jobseeker/index')}}}">HOME</a>

						

								</li>

								<li><a  href="{{{url('jobseeker/about')}}}">ABOUT US</a></li>

								<li><a  href="{{{url('jobseeker/register-job-seeker')}}}">REGISTER NOW</a></li>

								

								<li><a  href="{{{url('jobseeker/contact')}}}">CONTACT US</a></li>

								

							</ul>

						</nav>

					</div><!-- Main Navigation -->

					<div class="clearfix"></div>

				</div>

			</div><!-- container -->

		</div><!-- end main header -->

	

                                <!--Content start-->

                

                    @yield('wrapper')

                

                <!-- Content end -->

                

                

                

                	<div id="footer"><!-- Footer -->

			<div class="container"><!-- Container -->

				<div class="row">

					<div class="col-md-3 footer-widget"><!-- Text Widget -->

						<h6 class="widget-title">DJIT Daffodil JAPAN IT Ltd</h6>

						<div class="textwidget">

							<p style="text-align:justify;">DJIT, Daffodil Japan IT Ltd is a joint venture between Daffodil and BBP Japan. We have signed this joint venture in September 2013 We seek for the new business to train Bangladeshi Engineers by international level class, and try to coordinate the job opportunities in USA, JAPAN and any other developped countries</p>

						</div>

					</div><!-- Text Widget -->

					

					<div class="col-md-2 footer-widget"><!-- Footer Menu Widget -->

						<h6 class="widget-title">PAGES</h6>

						<div class="footer-widget-nav">

							<ul>
                            <li><a href="{{{url('jobseeker/index')}}}">HOME</a></li>

								<li><a  href="{{{url('jobseeker/about')}}}">ABOUT US</a></li>

								<li><a  href="{{{url('jobseeker/register-job-seeker')}}}">REGISTER NOW</a></li>								

								<li><a  href="{{{url('jobseeker/contact')}}}">CONTACT US</a></li>

							</ul>

						</div>

					</div><!-- Footer Menu Widget -->

					

					<div class="col-md-4 footer-widget"><!-- Recent Tweet Widget -->

						<h6 class="widget-title">LOCATE US</h6>

						<div class="recent-twitt">

							<p>BDBL Bhaban, 12 (6th floor) Kawran Bazaar, Dhaka, Bangladesh.<br/>
                            Phone : 01713493282, 01713493299, 01713493278, 01856991018 <br/>
                            Mail : info@djit.ac 


							</p>


						</div>

					</div><!-- Recent Tweet Widget -->



					<div class="col-md-3 footer-widget"><!-- News Leter Widget -->

						<h6 class="widget-title">Subscribe to Our Newsletter</h6>

						<div class="textwidget">

							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>

						</div>



						<form role="form">

							<div class="form-group">

								<input type="email" class="input-newstler">

							</div>

							<button type="button" class="btn-newstler">Sign Up</button>

						</form>

						<a href="#" target="blank"><i class="media-footer footer-twitt"></i></a>

						<a href="#" target="blank"><i class="media-footer footer-fb"></i></a>

						<a href="#" target="blank"><i class="media-footer footer-linkedin"></i></a>

						<a href="#" target="blank"><i class="media-footer footer-yahoo"></i></a>

						<a href="#" target="blank"><i class="media-footer footer-blog"></i></a>

						<a href="#" target="blank"><i class="media-footer footer-rss"></i></a>

					</div><!-- News Leter Widget -->

					<div class="clearfix"></div>

				</div>



				<div class="footer-credits"><!-- Footer credits -->

					2014 &copy; - WWW.DJIT.AC All Rights Reserved.

				</div><!-- Footer credits -->

				

			</div><!-- Container -->

		</div><!-- Footer -->

	</div><!-- end main wrapper -->



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

    

    <!-- Include all compiled plugins (below), or include individual files as needed -->

    <script src="{{ $theme }}bootstrap/js/bootstrap.min.js"></script>



	<!-- Tabs -->

	<script src="{{ $theme }}js/jquery.easytabs.min.js" type="text/javascript"></script>

	<script src="{{ $theme }}js/modernizr.custom.49511.js"></script>

	<!-- Tabs -->



	<!-- Owl Carousel -->

	<script src="{{ $theme }}js/owl.carousel.js"></script>

	<!-- Owl Carousel -->



	<!-- Form Slider -->

	<script type="text/javascript" src="{{ $theme }}js/jshashtable-2.1_src.js"></script>

	<script type="text/javascript" src="{{ $theme }}js/jquery.numberformatter-1.2.3.js"></script>

	<script type="text/javascript" src="{{ $theme }}js/tmpl.js"></script>

	<script type="text/javascript" src="{{ $theme }}js/jquery.dependClass-0.1.js"></script>

	<script type="text/javascript" src="{{ $theme }}js/draggable-0.1.js"></script>

	<script type="text/javascript" src="{{ $theme }}js/jquery.slider.js"></script>

	<!-- Form Slider -->

	

	<!-- Map -->

	<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

	<!-- Map -->



	<script src="{{ $theme }}js/job-board.js"></script>



  </body>

</html>