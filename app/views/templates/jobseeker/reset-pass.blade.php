@extends('templates.jobseeker.jobseeker')



@section('wrapper')



<div class="inner-pag-title"><div class="inner">Reset password</div></div>

<div class="pagemid">
<style>
.popo-reg h1{font-size:14px; font-weight:400; color:#333; line-height:normal; padding:20px 0;}
.popo-reg h1 a{-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px; text-align:center; padding:7px 15px; color:#FFF; background:#1abc9c; margin-left:50px;}
.popo-reg{-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px; border:2px solid #C30; padding:15px 20px; width:70%; margin:100px auto;}
.popo-reg h2{background:#C30; color:#FFF; font-size:14px; line-height:40px; text-indent:20px; text-transform:uppercase; margin:-15px -20px 0; min-height:40px;}
</style>
    <div class="inner">

        <div id="main">
            
            <div class="entry-content col-lg-7" style="padding:50px 0;" >
                @if(Input::old('newpass'))
                    <h3>New password and confirm password not matched.</h3>
                @endif
                @if(empty($Msg))
                <div class="col-lg-12" style="padding:15px 0 15px 25px; border:solid 1px #F60; margin-bottom:20px; -webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;">
                    <ul style="margin:0px;font-size:12px; color:#F60;">
                        <li>Minimum 8 character long</li>
                        <li>Combination with small , capital letter and numbers [a-zA-Z0-9]</li>
                        <li>Special character (@ , #)</li>
                    </ul>
                </div>
                <form id="forgetPassFrm" action="{{url('jobseeker/update-password')}}" method="post">
                    <p>New Password<br>
                        <span class="wpcf7-form-control-wrap your-name">
                            <input type="password" class="form-control" name="newpass"/>

                        </span>
                        <span>Strength : <strong class="passStrength"></strong></span>
                    </p>
                    <p>Confirm Password<br>
                        <span class="wpcf7-form-control-wrap your-name">
                            <input type="password" class="form-control" name="confirmPass"/>

                        </span>
                    </p>
                    <p>
                        <input type="hidden" name="id" value="{{$user->id}}"/>
                        <input type="hidden" name="token" value="{{$user->reset_token}}"/>
                        <input type="submit" class="btn  reg-log-btn green" style="width:160px!important;" value="Request Passowrd"/>
                    </p>

                    </div>
                </form>
            @else
            <div class="popo-reg">
            <h2>Sorry token expired.</h2>
            <h1>{{$Msg}}</h1>
            </div>
            @endif
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var weekPass = false;
    var password = ''
    $("input[name='newpass']").keyup(function(){
       var userName = $('input[name="username"]').val();
       password = $(this).val();
       var pattern = /(\w|\W){8,}/;
       if(pattern.test(password)){
            if(password.match(/@/) && password.match(/#/))
                $(".passStrength").html('Strong').css('color','green');
            else if(password.match(/@/))
                $(".passStrength").html('Medium').css('color','green');
            else
                $(".passStrength").html('Medium').css('color','green');
            weekPass = false;
       }else if(pattern.test(userName))
       {
            weekPass = true;
            $(".passStrength").html('Week').css('color','red');

       }else{
            weekPass = true;
            $(".passStrength").html('Week').css('color','red');
       }
       
    });

    $("#forgetPassFrm").submit(function(){
        if(weekPass)
            return false;
        
        var re_password = $("input[name='confirmPass']").val();
        if(password != re_password)
        {    $(".passStrength").html('Not Matched').css('color','red');
            return false;
        }else{
            $(".passStrength").html('');
        }
    });
</script>
@stop