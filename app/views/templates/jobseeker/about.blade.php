@extends('templates.jobseeker.jobseeker')



@section('wrapper')	

		
<div class="inner-pag-title"><div class="inner">About Us</div></div>


		<div id="page-content"><!-- start content -->

			<div class="content-about">

				<div class="container"><!-- container -->
                
                
                
                <div style="text-align:justify;">
						<div class="headline-stripe-wrapper padding-bottom24">
							<div class="headline-border">
								<h4 style="margin-bottom:20px; text-transform:uppercase;color:#2991d6; font-weight:600;">DJIT Daffodil JAPAN IT Ltd<span class="headline-middle-line"></span>

                                </h4>
							</div>
						</div>
                        <img src="{{{$theme}}}images/upload/aboutus_slider1.jpg" style="float:left; margin:0 15px 10px 0; padding:1px; border:solid 1px #ddd;" width="550" class="img-responsive" alt="page-slider" />
                      
						<p>{{$about->content or ''}}</p>
                         </div>
                
                
                <p style="margin-bottom:12px;">DJIT Limited is a leading global IT recruiting firm that has been supporting the success and growth of companies and enhancing the careers of professionals. We are dedicated for expert IT recruitment. DJIT Limited is based on Dhaka, Bangladesh having satellite office in different countries. We recruit capable IT expert for international companies from ASIA, and throughout the globe but also cooperate and coordinate with IT recruitment/placement agencies and companies all around the world.</p>
                        <p>It is to be understood that since ages, Bangladesh has been affluent with Skilled IT individual and experts who are held as loyal, diligent and hardworking. In the past decade, there has been a rapid growth in education and resources for professional training to establish a higher education system and skilled IT expert in the country.</p>

                   <p style="margin-bottom:12px;">Thousands of skilled IT professional with high qualification and vast amount of experience are ready to be a part of the solution for any IT company aiming to reach the pinnacle of success. Hence, DJIT has made a keen effort to establish and provide a collection of Professional, skilled personnel for various companies.</p>
                   <p>Owing to our specialized, strict, diligent and responsible working attitude, as well as high effective teamwork spirit, experience, and the network throughout Asia, Europe and other regions, we are confident to provide quality services and satisfactory cooperation to our clients.</p>
				



					<div class="spacer-1">&nbsp;</div>

				</div><!-- container -->


<div class="full-width-container pale-gray-bg pale-border-bottom pale-border-top inner-side">
			<div class="container padding-bottom48">
				<div class="row services-icons-wrap icons-left light-services-icons">
					<div class="padding-top48">
						<div class="relative text-center">
							<i class="iconalt-svg194"></i>
							<div class="services-content text-left">
                             <h5 style="font-weight:600; text-transform:uppercase;">Daffodil Group</h5>
								<p class="padding-top12">has started the journey in 1990 as a computer distributor and assembler. Daffodil International University was started in 2002. Now Daffodil Group has over 17 concerns in ICT, education, and training. Also Daffodil has the operation in major divisions in Bangladesh. Furthermore, daffodil has started global operation in UK, USA, Malaysia, and Dubai.</p>

                                <h5 style="font-weight:600; text-transform:uppercase;">BBP japan</h5>
								<p class="padding-top12">have set up &quot;Bangla business partner japan, IT ltd&quot; for promoting this Bangladesh opportunities to the develop countries such as; USA, CANADA and JAPAN.  BBP Japan, IT consultation firm regulated by expert and professional Japanese individual.</p>

                                <h5 style="font-weight:600; text-transform:uppercase;">DJIT</h5>
								<p class="padding-top12">is a joint venture Ltd company for training and recruitment. DJIT, Daffodil Japan IT Ltd is a joint venture between Daffodil group and BBP Japan. DJIT have train Bangladeshi Engineers and different category individuals by international level class, and try to coordinate the job opportunities in USA, CANADA, JAPAN and any other developed countries. BPP & DIU has created DJIT as a way to combat unemployment/dissatisfaction and help the Expert IT professional to find suitable position.</p><br />

                                <h5 style="font-weight:600; text-transform:uppercase;">DJIT</h5>
								<p class="padding-top12">brings fresh information about expert IT professional, uncover IT expert buzzwords and customize recruitment package to help building the modern IT expert database with offering simple strategic solution for the clients.</p>
                     
							</div>
						</div>
					</div>

					<div class="clearfix"></div>
					
				</div>
			</div>
            
		</div>


<div class="full-width-container pale-border-bottom pale-border-top inner-side">
        <div class="container" style="padding-top:30px;">
        <h4 style="margin-bottom:20px; text-transform:uppercase;color:#2991d6; font-weight:600;">OUR MISSION<span class="headline-middle-line"></span></h4>
		</div>	
            <div class="container padding-bottom48 po-para" style="padding-top:0px;">		
                
                <div class="clearfix"></div>
                <p style="padding-left:0;">
                DJIT' mission is to provide top-notch, customized executive search services that will accomplish our clients' goals quickly, efficiently, and professionally. DJIT can help you bridge the gap between your placement needs and the best talent the industry has to offer. Our reward structure is team-driven and built on a foundation of client service and candidate care. Growing special relationship with professional consultants with professional qualifications. Your employees are one of your most important assets. The continued growth of successful companies depends on the shared vision of its key employees. As experienced IT industry recruiters, we know that in the long run, our business is not simply about filling positions. It's about building long-term relationships with people who share your vision. 
			</div>
            
<div class="spacer-1">&nbsp;</div>
		</div>







				
			</div><!-- end content -->

		</div><!-- end page content -->





@stop

