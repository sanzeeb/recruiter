@extends('templates.jobseeker.jobseeker')

@section('wrapper')
<script type="text/javascript">
var weekPass = false;
var password=''
$(function(){

    $("input[name='password']").keyup(function(){
       var userName = $('input[name="username"]').val();
       password  = $(this).val();
       var pattern = /(\w|\W){8,}/;
       if(pattern.test(password)){
            if(password.match(/@/) && password.match(/#/))
                $(".passStrength").html('Strong').css('color','green');
            else if(password.match(/@/))
                $(".passStrength").html('Medium').css('color','green');
            else
                $(".passStrength").html('Medium').css('color','green');
            weekPass = false;
       }else if(pattern.test(userName))
       {
            weekPass = true;
            $(".passStrength").html('Week').css('color','red');

       }else{
            weekPass = true;
            $(".passStrength").html('Week').css('color','red');
       }
       
    });

    $("#registerFrm").submit(function(){
        if(weekPass)
            return false;
        
        var re_password = $("input[name='re_password']").val();
        
        if(password != re_password)
        {    $(".passStrength").html('Not Matched').css('color','red');
            return false;
        }else{
            $(".passStrength").html('');
        }
    });
});
</script>
<div class="main-page-title" style="background: none repeat scroll 0 0 #2991d6 !important; padding:35px 0 0 !important;"><!-- start main page title -->
    <div class="container">

        <div>
            <h4 class="reg-pag-title">Stay with us</h4>


            {{ Form::open(array('url'=>'jauth','class'=>'form-signin', 'autocomplete'=>'off','method'=>'post')) }}

            <h6 style='color: white; float: none; line-height: 30px; margin-bottom:-22px; margin-left: 378px; margin-top: -60px; width: auto;'>@if (Session::has('message'))
                <span>{{ Session::get('message') }}</span>
                <?php Session::forget('message'); ?>
                @endif
            </h6>
            <a class="forgot-pass" href="{{url('jobseeker/forget-pass')}}">Forget password</a>
            <div class="login">
                <p style="margin-top:21px;"><button type="submit" value="submit" name="contactsubmit" id="" class="reg-log-btn" />Login</button></p>
                <div class="remember_me">
                    <input type="checkbox"  name="remember"/> Remember Me
                </div>
            </div>

            <div class="form-group col-md-3" style="float:right; margin-top:25px;">
                <input tabindex="2" type="password" class="form-control form-dark2 reg-logi"  id="name" name="pass" placeholder="Enter password">
            </div>
            <input type="hidden" name="cv_tbl_id" value="{{{$cv_id}}}"/>
            <input type="hidden"  value="jobseeker" name="log_type" />
            <input type="hidden"  value="{{$ref}}" name="ref" />
            <div class="form-group col-md-3" style="float:right; margin-top:25px;">
                <input tabindex="1" type="text" class="form-control form-dark2 reg-logi" id="userId" name="userId" placeholder="Enter username">
            </div>


            {{ Form::close() }}

        </div>
    </div>
</div><!-- end main page title -->

<div id="page-content"><!-- start content-->
    <div class="content-about" style="padding-top:35px;">
        <div class="row clearfix">
            <div class="col-lg-6" style="float:left; margin-left:6%; margin-right:0%; margin-bottom:2%;">
            
            <h4 class="hs_heading"><strong>Register An Jobseeker Account</strong></h4>
            
            <div class="hs_comment_form">

        <div class="row">
            <h3 style="padding:10px; font-size:20px;">{{{$msg or ''}}} </h3>
            {{ Form::open(array('url'=>'jobseeker/register-job-seeker','id'=>'registerFrm','class'=>'wpcf7-form contact_form','autocomplete'=>'off', 'method'=>'post')) }}
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <span class="btn btn-success"><i class="fa fa-pencil"></i></span>
                        </span>
                        <input type="text" placeholder="First name (required)" aria-invalid="false" aria-required="true"
                               class="form-control wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email requiredField " size="40" value="" name="firstname" id="contact_name">
                    </div>
                    <!-- /input-group -->
                </div>
                <input type="hidden" name="cv_tbl_id" value="{{{$cv_id}}}"/>
                <!-- /.col-lg-6 -->

                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <span class="btn btn-success"><i class="fa fa-pencil"></i></span>
                        </span>
                        <input type="text" aria-invalid="false" aria-required="true" placeholder="Last name (required)" class="form-control wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email requiredField email"
                               size="40" value="" name="lastname" id="contact_email">
                    </div>
                </div>



            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="input-group gender">
                        <span class="input-group-btn">
                            <span class="btn btn-success"><i class="fa fa-user"></i></span>
                        </span> (required) <input name="gender" type="radio" value="Male" /> Male <input name="gender" type="radio" value="Female" />Female 
                    <!--<input type="text" aria-invalid="false" aria-required="true" placeholder="Gender"
                           class="form-control requiredField contactpersonname" size="40" value="" name="contact_person" id="contact_person">-->
                </div>
            </div>
                <!--<div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <span class="btn btn-success"><i class="fa fa-phone"></i></span>
                        </span>
                        <input type="text" aria-invalid="false" aria-required="true"  placeholder="Phone" class="form-control requiredField phone"
                               size="40" value="" name="phone" id="phone">
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <span class="btn btn-success"><i class="fa fa-globe"></i></span>
                        </span>
                        <input type="text" aria-invalid="false" aria-required="true" placeholder="Website" class="form-control requiredField website"
                               size="40" value="" name="website" id="website">
                    </div>
                </div>-->



                <!--<div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <span class="btn btn-success"><i class="fa fa-phone"></i></span>
                        </span>
                        <input type="text" aria-invalid="false" aria-required="true" placeholder="Contact Person Phone"
                               class="form-control requiredField contactpersonphone" size="40" value="" name="contact_person_phone" id="contact_person_phone">
                    </div>
                </div>-->

                <div class="col-lg-12">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <span class="btn btn-success"><i class="fa fa-user"></i></span>
                        </span>
                        <input type="text" aria-invalid="false" aria-required="true"  placeholder="User Name (required)" class="form-control requiredField username"
                               size="40" name="username" id="username">
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="input-group">
                            <span class="input-group-btn">
                                <span class="btn btn-success"><i class="fa fa-envelope"></i></span>
                            </span>
                        <input type="text" aria-required="true" placeholder="Email (required)" class="form-control wpcf7-form-control wpcf7-text wpcf7-validates-as-required requiredField address"
                               size="40" value="" name="email" id="email">
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <span class="btn btn-success"><i class="fa fa-unlock-alt"></i></span>
                        </span>

                        <input type="password" aria-invalid="false" placeholder="Password (required)" class="form-control requiredField password"
                               size="40" name="password">
                    </div>
                    <div class="form-group">
                        <span class="input-group">
                            <ul style="font-size:10px;">
                                <li>Minimum 8 character long</li>
                                <li>Combination with small , capital letter and numbers [a-zA-Z0-9]</li>
                                <li>Special character (@ , #)</li>
                            </ul>
                        </span>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <span class="btn btn-success"><i class="fa fa-unlock-alt"></i></span>
                        </span>
                        <input type="password" aria-invalid="false" placeholder="Re-Password (required)" class="form-control requiredField password"
                               size="40" value="" name="re_password">
                        
                    </div>
                    <span>Strength : <strong class="passStrength"></strong></span>
                </div>

                <div class="col-lg-10">
                    <div class="input-group" style="font-size:14px; line-height:17px; margin:11px 0 0 0;">
                        <input name="agreement" type="checkbox" value="I agree to the Terms of Service and Privacy Policy"/> I
                        agree to the Terms of Service and Privacy Policy

                    </div>
                </div>

                <!--<div class="col-lg-12">
                  <div class="form-group"><textarea rows="8" class="form-control requiredField" name="contact-message" id="message"></textarea></div>
                  </div>-->

                <p id="err"></p>

                <div class="form-group" style="float:right;">
                    <div class="col-sm-12">

                        <button type="submit" class="btn btn-success pull-right" id="em_sub">Register</button>

                    </div>

                </div>
            {{Form::close()}}
        </div>


    </div>
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
                <!--<h3>{{{$msg or ''}}} </h3>
                {{ Form::open(array('url'=>'jobseeker/register-job-seeker','class'=>'wpcf7-form contact_form', 'method'=>'post')) }}

                <h3 style="line-height:normal;">Registration</h3>
                <div class="form-group">
                    <p>First name:<br>
                        <input type="text" class="form-control form-dark" id="firstname" name="firstname" placeholder="Enter name">
                    </p>
                </div>
                <input type="hidden" name="cv_tbl_id" value="{{{$cv_id}}}"/>
                <div class="form-group">
                    <p>Last name:<br>
                        <input type="text" class="form-control form-dark" id="lastname" name="lastname" placeholder="Enter Last name">
                    </p>
                </div>
                <div class="form-group">
                    <p>Gender:<br>
                        <input type="radio" value="male"  name="gender" id="gender">
                        Male<input type="radio"  size="10" value="female" name="gender" id="gender2">Female
                    </p> </div>
                <div class="form-group">
                    <p>Username:<br>
                        <input type="text" class="form-control form-dark" id="username" name="username" placeholder="Enter username">
                    </p>
                </div>
                <div class="form-group">
                    <p>Email:<br>
                        <input type="email" name="email" class="form-control form-dark" id="email" placeholder="Email">
                    </p>
                </div>




                <div class="form-group">
                    <p>Password:<br>
                        <input type="password" name="password" class="form-control form-dark" id="password" placeholder="Password">
                    </p>
                </div>



                <div class="form-group">
                    <p>Retype Password:<br>
                        <input type="password" class="form-control form-dark" name="re_password" id="re_password" placeholder="Retype password">
                    </p>
                </div>

                <div class="form-group">

                    <p>I agree to the Terms of Service and Privacy Policy<br/>
                        <input type="checkbox" name="agreement" value="agree this condition">
                    </p>
                </div>
                <button class="btn btn-default btn-blue btn-lg" type="submit">Submit</button>

                {{ Form::close() }}-->

            </div>

            <div class="col-lg-4 col-md-5 col-sm-5">

    <h4 class="hs_heading"><strong>DJIT Headquarters</strong></h4>

    <div class="hs_contact">

        <ul>

            <li><i class="fa fa-map-marker"></i>

                <p>BDBL Bhaban, 12 (6th floor) Kawran Bazaar, Dhaka, Bangladesh.</p>

            </li>

            <li><i class="fa fa-phone"></i>

                <p>01713493282, 01713493299, 01713493278, 01856991018</p>

                <p></p>

            </li>

            <li style="display:flex;"><i class="fa fa-envelope"></i>

                <p><a href="Mailto:info@djit.ac">info@djit.ac</a></p>

            </li>

            <li><i class="fa fa-globe"></i>

                <p><a href="http://www.djit.ac/" target="_blank">www.djit.ac</a></p>
            </li>

        </ul>

    </div>

    <!--<div class="hs_contact_social">

        <div class="hs_profile_social">

            <ul>
                <li><a href=""><i class="fa fa-facebook"></i></a></li>

                <li><a href=""><i class="fa fa-twitter"></i></a></li>

                <li><a href=""><i class="fa fa-google-plus"></i></a></li>

            </ul>

        </div>

    </div>-->

</div>

        </div>

        <!--<div id="cs">
            <div class="container">
                <div class="spacer-1">&nbsp;</div>
                <h1>If You Have Any Quries?</h1>
                <p>
                    At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt.
                </p>
                <h1 class="phone-cs">Call: +88 01713 493 282</h1>
            </div>
        </div> CS -->
    </div><!-- end content-->
</div><!-- end page content -->

@stop