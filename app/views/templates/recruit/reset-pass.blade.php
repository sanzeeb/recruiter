@extends('templates.recruit.recruit')



@section('wrapper')



<div class="inner-pag-title"><div class="inner">Reset password</div></div>

<div class="pagemid">

    <div class="inner">

        <div id="main">
            
            <div class="entry-content">
                @if(Input::old('newpass'))
                    <h3>New password and confirm password not matched.</h3>
                @endif
                @if(empty($Msg))
                <div class="col-lg-12">
                    <ul style="font-size:10px;">
                        <li>Minimum 8 character long</li>
                        <li>Combination with small , capital letter and numbers [a-zA-Z0-9]</li>
                        <li>Special character (@ , #)</li>
                    </ul>
                </div>
                <form id="forgetPassFrm" action="{{url('recruiter/update-password')}}" method="post">
                    <p>New Password<br>
                        <span class="wpcf7-form-control-wrap your-name">
                            <input type="password" class="form-control" name="newpass"/>
                        </span>
                        <span>Strength : <strong class="passStrength"></strong></span>
                    </p>
                    <p>Confirm Password<br>
                        <span class="wpcf7-form-control-wrap your-name">
                            <input type="password" class="form-control" name="confirmPass"/>
                        </span>
                    </p>
                    <p>
                        <input type="hidden" name="id" value="{{$user->id}}"/>
                        <input type="hidden" name="token" value="{{$user->reset_token}}"/>
                        <input type="submit" class="btn  reg-log-btn green" value="Request Passowrd"/>
                    </p>

                    </div>
                </form>
            @else
                <h1>{{$Msg}}</h1>
            @endif
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var weekPass = false;
    var password = '';
    $("input[name='newpass']").keyup(function(){
       var userName = $('input[name="username"]').val();
       password = $(this).val();
       var pattern = /(\w|\W){8,}/;
       if(pattern.test(password)){
            if(password.match(/@/) && password.match(/#/))
                $(".passStrength").html('Strong').css('color','green');
            else if(password.match(/@/))
                $(".passStrength").html('Medium').css('color','green');
            else
                $(".passStrength").html('Medium').css('color','green');
            weekPass = false;
       }else if(pattern.test(userName))
       {
            weekPass = true;
            $(".passStrength").html('Week').css('color','red');

       }else{
            weekPass = true;
            $(".passStrength").html('Week').css('color','red');
       }
       
    });

    $("#forgetPassFrm").submit(function(){
        if(weekPass)
            return false;
        
        var re_password = $("input[name='confirmPass']").val();
        if(password != re_password)
        {    $(".passStrength").html('Not Matched').css('color','red');
            return false;
        }
        else{
            $(".passStrength").html('');
        }
    });
</script>
@stop