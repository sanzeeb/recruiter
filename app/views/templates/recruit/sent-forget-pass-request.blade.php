@extends('templates.recruit.recruit')



@section('wrapper')



<div class="inner-pag-title"><div class="inner">Forget your password</div></div>

<div class="pagemid">

    <div class="inner">

        <div id="main">

                <div class="entry-content">
                <h2>Your Reset password request sent to you email address. Please check inbox. if not found check to the spam folder and check that email as not spam</h2>
            </div>
        </div>
    </div>
</div>
@stop