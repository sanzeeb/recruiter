@extends('templates.recruit.recruit')

@section('wrapper')		

<div id="subheader">
    <div class="inner">
        <div class="subdesc">
            <h1 class="page-title">Top Ranked Engineers Of Bangladesh</h1>
            <div class="customtext"><h2></h2></div>
        </div>
        <div class="breadcrumbs"><!-- Breadcrumb NavXT 5.0.1 -->
            <a class="home" href="#" title="Go to Hostme v2.">DJIT</a><a href="#" title="Go to Pages."></a> &gt;Top Ranking Engineers
        </div>
    </div>
</div><!-- #subheader -->

<div class="pagemid">
    <div class="inner">

        <div id="main">
        <div class="top-con-ser">
        <h2>{{strtoupper($skill)}}</h2>
        <form action="{{Request::url()}}" class="font-ser" method="get">
        <label>Search :</label>
        <label><input name="name" placeholder="Name" type="text" /></label>
<!--         <label><input name="age" placeholder="Age" type="text" /></label> -->
        <label><input name="yxp" placeholder="Year of Exp" type="text" /></label>
        <input type="hidden" name="skill" value="{{$skill}}"/>
        <input name="" class="srbtn" type="submit" value="search" />
        </form>
        </div>
        
        <div class="entry-content">


    <!--<div class="new-top-en">
        <div class="lef"><span class="avator"><img src="{{ $theme }}image/img2.jpg"  /></span></div>
        <div class="rig">
        <a href="#" class="name">Shane Wilson</a>
        <a href="#" class="p_button">Favorite</a>
        <a href="#" class="p_button">Inquery</a>
        <a href="#" class="p_button">Interview</a>
        <a href="#" class="p_button">Test Project</a>
        <div class="clear"></div>
        <span><strong><span>Age :</span> 33</strong> &nbsp;&nbsp;&nbsp;<strong><span>Year of Exp :</span> 9</strong> &nbsp;&nbsp;&nbsp;<strong><span>Rank :</span> 1</strong></span>   <p>A Computer Science and Engineering graduate have more than four years? of experience in related field. Currently working as a Software Engineer in an IT based organization and core responsibility is ensuring the quality of coding user interface design and development projects using technologies such as HTML5, CSS3, html, CSS, JavaScript library (jQuery), PHP (CakePHP), Codeigniter, Yii, Zend, XML, Joomla, Wordpress<a href="">...more</a></p>
        <ul class="cat-link">
                <li><a>PHP</a></li>
                <li><a>Photoshop</a></li>
                <li><a>C++</a></li>
                <li><a>CSS 3</a></li>
                <li><a>C#</a></li>
            </ul>
        </div>
        
        </div>
        <div class="clear"></div>
        
        
        
        <div class="divider thin" style="margin:30px 0;"></div>-->

                
            <?php $rank=Request::get('id'); $page=Request::get('page');
            $rank = (!empty($rank))? $rank : 1;
            $page=!empty($page)? $page : 1;
                    if($page==1){
                        $rank=1;
                    }
                // $rank=$rank * $page;
            ?>
            <?php $rankTop = (count($expertises)*$page)+1; ?>
                @if(!is_array($expertises))
                <div class="pag">
                    {{$expertises ->appends(array('id'=>$rankTop))->links()}}
                </div>
                @endif
                <div class="clear"></div>
                <div class="divider thin" style="margin:15px 0 60px 0;"></div>
                @foreach($expertises as $j=> $expert)

                <?php   $jobseeker = CvTable::find($expert->cv_tbl_id);
                        $candidate = Candidate::where('cv_tbl_id',$expert->cv_tbl_id)->first();
                $link = '';
                ?>

            <div class="new-top-en">
                <div class="lef"><span class="avator">
                        @if(!empty($expert->photo))
                            <img src="{{url(JOB_DOMAIN.'data/profile/'.$expert->photo)}}" alt="{{$expert->photo}}" />
                        @else
                            <img src="{{ $theme }}image/avatar.jpg"  alt="Avatar"/>
                        @endif
                    </span></div>
                <div class="rig">
                    @if(Session::get('id')!="")
                    <?php $link = ''; ?>
                    <a href="{{url('resume/display-resume/'.$candidate->cv_tbl_id)}}" class="name">{{str_replace(array("_")," ",strtoupper($jobseeker->name))}}</a>
                    @else
                    <?php $link = url('recruiter/register-recruiter/'.$expert->cv_tbl_id); ?>
                    <a href="{{$link}}" class="name">{{str_replace(array("_")," ",strtoupper($jobseeker->name))}}</a>
                    @endif


                    @if(empty($user) || ($user->user_type == 'Employer'))
                        @if(empty($link))

                            @if(!empty($user) && ($user->user_type == 'Employer'))
                            <form target="_blank" method="post" action="{{url('resume/add-to-favorite')}}">
                                <input type="hidden" name="cid" value="{{$jobseeker->Candidate->candidate_id}}"/>
                                <input type="hidden" name="cv_tbl_id" value="{{$candidate->cv_tbl_id}}"/><button type="submit" title="Add to favorite" class="p_button">Favourite</button>
                            </form>
                            @endif
                            <form target="_blank" method="post" action="{{url('inquiry/create/'.$candidate->candidate_id)}}">
                                <input type="hidden" name="cv_tbl_id" value="{{$candidate->cv_tbl_id}}"/><button type="submit" title="Inquery" class="p_button">Inquery</button>
                            </form>
                            <form target="_blank" method="post" action="{{url('interview/create/'.$candidate->candidate_id)}}">
                                <input type="hidden" name="cv_tbl_id" value="{{$candidate->cv_tbl_id}}"/><button type="submit" title="Interview" class="p_button">Interview</button>
                            </form>
                            @if(!empty($candidate->source_code))
                                <a href="{{$candidate->source_code}}" target="_blank" class="p_button">Source Code</a>
                            @endif
                            @if(!empty($candidate->academic_certificate) && File::exists('data/academic_certificate/'.$candidate->academic_certificate))
                                <a target="_blank" href="{{url('data/academic_certificate/'.$candidate->academic_certificate)}}" class="p_button">Academic Certificate</a>
                            @endif
                            <a href="{{url('tproject/create/'.$candidate->candidate_id)}}" target="_blank" class="p_button">Test Project</a>

                        @else
                        <a href="{{$link}}" class="p_button">Favorite</a>
                        <a href="{{$link}}" class="p_button">Inquery</a>
                        <a href="{{$link}}" class="p_button">Interview</a>
                        <a href="{{$link}}" class="p_button">Test Project</a>
                        @endif
                    @else
                        <a href="{{url('resume/display-resume/'.$expert->cv_tbl_id)}}" target="_blank" class="p_button">View Details</a>
                        @if(!empty($candidate->source_code))
                        <a href="{{$candidate->source_code}}" target="_blank" class="p_button">Source Code</a>
                        @endif
                        @if(!empty($candidate->academic_certificate) && File::exists('data/academic_certificate/'.$candidate->academic_certificate))
                        <a target="_blank" href="{{url('data/academic_certificate/'.$candidate->academic_certificate)}}" class="p_button">Academic Certificate</a>
                        @endif
                    @endif



                    <div class="clear"></div>
                    <span><strong><span>Age :</span> {{Candidate::getAge($jobseeker->dob)}}</strong> &nbsp;&nbsp;&nbsp;<strong><span>Year of Exp :</span> {{$expert->year_of_exp or ''}}</strong> &nbsp;&nbsp;&nbsp;<strong><span>Rank :</span> {{$rank}} <?php $rank++; ?></strong></span>
                    @if(!empty($candidate->csummary))
                    <div class="n-content-box">
                        <p class="summary">
                            {{Str::words($candidate->csummary,50)}}
                            <a href="#" class="summary">...more</a>
                        </p>

                        <p class="details">
                            {{$candidate->csummary}}
                            <a href="#" class="details">...less</a>
                        </p>
                    </div>
                    @endif
                    <?php $cvSpecs = CvSpecialization::where('cv_tbl_id',$expert->cv_tbl_id)->get(); ?>

                    @if(count($cvSpecs))
                    <ul class="cat-link">
                        @foreach($cvSpecs as $specs)
                        <li><a>{{str_replace("_"," ",strtoupper($specs->key))}}</a></li>
                        @endforeach

                    </ul>
                    @endif
                </div>

            </div>
            <div class="clear"></div>
            <div class="divider thin" style="margin:30px 0;"></div>

                <!--<div class="top-engi-box">
                    <span>
                        <div class="left">
                            <span><b>Name : </b>
                                @if(Session::get('id')!="")

                                <?php /*$link = url('resume/display-resume/'.$expert->cv_tbl_id)*/?>
                                 @else
                                <?php /*$link = url('recruiter/register-recruiter/'.$expert->cv_tbl_id)*/?>
                                @endif
                                <a href="{{{$link}}}">{{$jobseeker->name}}</a>
                            </span>
                            <span><b>Rank : </b> <strong>{{$rank}}</strong></span> <?php /*$rank++; */?>
                            <span><b>Age : </b> <strong>{{Candidate::getAge($jobseeker->dob)}}</strong></span>
                            <span><b>Year of exp :</b> <strong>{{$expert->year_of_exp or ''}}</strong></span>
                        </div>
                            <div class="mid"><p>A Computer Science and Engineering graduate have more than four years? of experience in related field. Currently working as a Software Engineer in an IT based organization and core responsibility is ensuring the quality of coding user interface design and development projects using technologies such as HTML5, CSS3, html,  CSS, JavaScript library (jQuery), PHP (CakePHP), Codeigniter, Yii, Zend, XML, Joomla, Wordpress, MySQL. Previously worked as a web developer where there was an involvement in Interface design, documentation, implementation and support functions, generated projects more than ten. Also has knowledge on Joomla structure and coding.</p></div>
                            <div class="right">
                                <p><a href="{{{$link}}}" class="push_button">Favorite</a></p>
                                <p><a href="{{{$link}}}" class="push_button2">Inquery</a></p>
                                <p><a href="{{{$link}}}" class="push_button3">Interview</a></p>
                                <p><a href="{{{$link}}}" class="push_button4">Source Code</a></p>
                                <p><a href="{{{$link}}}" class="push_button5">Test Project</a></p>
                                <p><a href="{{{$link}}}" class="push_button6">Academic Paper</a></p>
                            </div>
                    </span>
                </div>-->


                
                @endforeach



                <div class="divider thin" style="margin:15px 0 60px 0;"></div>
@if(!is_array($expertises))
<div class="pag">
{{$expertises ->appends(array('id'=>$rank))->links()}}
</div>
@endif
                <div class="clear"></div>				




            </div><!-- .entry-content -->
        </div><!-- #main -->
        <div class="clear"></div>

    </div><!-- .inner -->
</div><!-- .pagemid -->
<script type="text/javascript">
    $(function(){
        $('a.summary').click(function(){
           var obj = $(this);
           obj.parent().next().slideDown().children('a.details').show();
           obj.parent().hide();
            return false;
        });

        $('a.details').click(function(){
            var obj = $(this);
            obj.parent().slideUp('fast').prev().slideDown();
            return false;
        });
    });
</script>
@stop