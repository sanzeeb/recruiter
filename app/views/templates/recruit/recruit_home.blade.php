@extends('templates.recruit.recruit')

@section('wrapper')		

<div id="featured_slider">
			<div class="slider_stretched">
			<script type="text/javascript">
				jQuery(document).ready(function($) {
					jQuery(".flexslider").flexslider({
						animation: "fade",
						controlsContainer: ".flex-container",
						slideshow: true,
						slideshowSpeed: 3000,
						animationDuration: 400,
						directionNav: true,
						controlNav: false,
						mousewheel: false,
						smoothHeight :true,
						start: function(slider) {
							jQuery(".total-slides").text(slider.count);
						},
						after: function(slider) {
							jQuery(".current-slide").text(slider.currentSlide);
						}
					});
				});	
			</script>
				<div class="flexslider">
					<ul class="slides">
						 <li>
							<img src="{{ $theme }}images/1.jpg" alt="" />
							<div class="flex-caption">
								<div class="flex-title">
									<h5><span>Slider 0</span></h5>
								</div>
							</div>
						</li>
<li>
							<img src="{{ $theme }}images/2.jpg" alt="" />
							<div class="flex-caption">
								<div class="flex-title">
									<h5><span>Slider 1</span></h5>
								</div>
							</div>
						</li>
						<li>
							<img src="{{ $theme }}images/3.jpg" alt="" />
							<div class="flex-caption">
								<div class="flex-title">
									<h5><span>Slider 2</span></h5>
								</div>
								</div>
						</li>
						<li><img src="{{ $theme }}images/4.jpg" alt="" />
							<div class="flex-caption">
								<div class="flex-title">
									<h5><span>Slider 3</span></h5>
								</div>
							</div>
						</li>
                       <!-- <li><img src="{{ $theme }}images/5.jpg" alt="" />
							<div class="flex-caption">
								<div class="flex-title">
									<h5><span>Slider 4</span></h5>
								</div>
							</div>
						</li>-->
					</ul>
					<ul class="flex-direction-nav">
						<li><a href="#" class="flex-prev">Previous</a></li>
						<li><a href="#" class="flex-next">Next</a></li>
					</ul>
				</div><!-- .flex_slider -->
			</div><!-- .slider_stretched -->
		</div><!-- #featured_slider -->
		
		<div class="pagemid_section">
			<script type="text/javascript">
			jQuery(document).ready(function() {
				MySlider(3000,"testimonial70");
			});
			</script>
			
			<!--<div class="section_row clearfix" style="padding:20px 0;color:#ffffff; background:url( {{ $theme }}image/texture_2.png) no-repeat #5a7ea0; ">-->
<!--				<div class="section_row clearfix" style="padding:30px 0;color:#ffffff; background:#107fc9;border-top:4px solid #0d67a3; ">
					<div class="section_inner">
						<div data-id="fadeInDown" class="custom-animation iva_anim">
							<div  class="callOutBox iva_anim" style="padding:0!important;" >
								<div class="teaser_Content">
									<div class="callOut_Text">
										<h2 class="callOut_Heading">Take Control Of <strong>Your Recruitment Today!</strong></h2>
										<p>To secure the right candidate you need a strategic hiring solution that seamlessly combines expert knowledge...</p>
									</div>
				<div class="callOut_Button"><a style="background: linear-gradient(160deg, #fff 0px, #fff 50%, #efefef 51%, #efefef 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
    border: 3px solid #0d67a3; border-radius: 0; color: #696969; cursor: pointer; display: inline-block; font-family: inherit; font-size: 17px; height: 49px; line-height:38px;
    margin:0px 0 0; outline: medium none; padding: 0 36px; position: relative; text-shadow: none; text-transform: uppercase; transition: all 0.3s ease 0s;"
    href="{{{url('recruiter/register-recruiter')}}}"><span>Get Started</span></a></div>
								</div>
							</div>
						</div>
					</div>
			</div>-->
			
	   <div style="padding-top:0!important;" class="aboutpage_section">
          <div class="a_clearfix"></div>
          <div class="our_team" style="background:#fff;">
                               
          <section class="wrapper_x site-min-height_x" >
        <div class="row_x">
            <div class="col-lg-12"> <p class="hd-title_x">Top Ranked Engineers In Bangladesh</p></div>
            
        </div>
        
        

        

         <div class="row_x">
                          
             @foreach($skills as $skill)
                    <div class="col-lg-4">
                        <section class="panel_x re-hom-box">
                        
                            <div class="panel-body_x" style="text-align:left; position:relative;">
                            <?php $skill_name=base64_encode($skill->skill_name); ?>
                                <a style="float:left; margin-right:8px; padding:2px; border:solid 1px #CCC;">
                                @if(!empty($skill->skill_photo) && file_exists('data/skill/'.$skill->skill_photo))
                                    <img height='60' alt="thumbnail" alt="{{{$skill->skill_photo or ''}}}" src="{{{url('/')}}}/data/skill/{{{$skill->skill_photo}}}"/>
                                @else
                                    <img height='60' alt="thumbnail" alt="thumbnail" src="{{$theme}}images/placeholder.gif"/>
                                @endif
                                </a>
                                <a href="{{{url('recruiter/top-engineers')}}}/{{{$skill_name}}}" style="position:absolute; top:0; right:0; left:0; bottom:0;"></a>
                                <p align="justify" style="font-size:13px; line-height:normal;">
                                    {{$skill->skill_description}}
                                </p>

                                <!--<a href="{{{url('recruiter/top-engineers')}}}/{{{$skill_name}}}">
                                @if($skill->skill_name=="microcontroller_programming")
                                    micro programming
                                @else
                                    {{{$skill->skill_name}}}
                                @endif
                                </a>-->
                                <div class="a_clearfix"></div>
                            </div>
                            
                        </section>
                    </div>

                    @endforeach
                   
            

        </div>

        
       
    </section>
          </div><!-- end our team -->
          <div class="a_clearfix"></div>
 <section class="what-we-do wrapper_x section-features  site-min-height_xx">
 <div class="row_x">
 <div class="col-lg-12">
 <h1 style="text-align:center; margin-bottom:0;"><strong style="color:#90bd00; text-shadow:#FFF 0 1px 0; font-size:33px;">What You Can Do?</strong></h1>
 
 <div class="col-sm-12 col-md-6 col-lg-4 respon-bbp">
 <article class="h-article feature-rotate-second ">
        <div class="box-inner">						
            <header>
                <h4 class="entry-title"><a target="_blank" href="{{url('recruiter/'.$homeCountry->slug)}}">{{$homeCountry->title}}</a></h4>
                <h6 class="sub-info">( parmanent or recruit immigration )</h6>
            </header>
            <div class="separator-box"></div>
            <div class="entry-summary">{{Str::words($homeCountry->content,20)}}</div>
        </div> 
    </article>                
 </div>
 <div class="col-sm-12 col-md-6 col-lg-4 respon-bbp">
  <article class="h-article feature-rotate-second ">
        <div class="box-inner">						
            <header>
                <h4 class="entry-title"><a target="_blank" href="{{url('recruiter/'.$remoteProgrammer->slug)}}">{{$remoteProgrammer->title}}</a></h4>
                <h6 class="sub-info">( full time freelancer )</h6>
            </header>
            <div class="separator-box"></div>
            <div class="entry-summary">{{Str::words($remoteProgrammer->content,20)}}</div>
        </div> 
    </article>                
 </div>
 <div class="col-sm-12 col-md-6 col-lg-4 respon-bbp">
  <article class="h-article feature-rotate-second ">
        <div class="box-inner">						
            <header>
                <h4 class="entry-title"><a target="_blank" href="{{url('recruiter/'.$partTimeProgrammer->slug)}}">{{$partTimeProgrammer->title}}</a></h4>
                <h6 class="sub-info">( part time freelancer )</h6>
            </header>
            <div class="separator-box"></div>
            <div class="entry-summary">{{Str::words($partTimeProgrammer->content,20)}}</div>
        </div> 
    </article>                
 </div>
</div></div>
 <div class="a_clearfix"></div>
 </section>
 
 
 
  <div class="a_clearfix"></div>
 <section class="easiest wrapper_x section-features  site-min-height_xx">
 <div class="row_x">
  <div id="job-step">
	<div class="container">
		<h1 class="job-step-title">Easiest Way to Use</h1>
		<div class="job-step-wrapper row">
			<div class="col-sm-12 col-md-6 col-lg-4">
				<div class="step-number">First Step</div>
				<div class="step-image"><img title="Register with Us" alt="First Step" src="{{ $theme }}css/img/step-icon-2.png"></div><!-- /.step-image -->
				<h3 class="step-title">Register or Sign in</h3>
                <span class="ho-po-bo">( 4 ) <br/>Wait For The Application</span>
			</div><!-- /.col-sm-3 -->
			
			<div class="col-sm-12 col-md-6 col-lg-4">
				<div class="step-number">Second Step</div>
				<div class="step-image"><img title="Create Your Profile" alt="Second Step" src="{{ $theme }}css/img/step-icon-1.png"></div><!-- /.step-image -->
				<h3 class="step-title">Choose The Category</h3>               
                <span class="ho-po-bo2"> <div class="line"></div>( 5 )<br/>Contact with the candidate <div class="line2"></div></span>
                
			</div><!-- /.col-sm-3 -->
			
		
			<div class="col-sm-12 col-md-6 col-lg-4">
				<div class="step-number">Third Step</div>
				<div class="step-image step-last"><img title="Now take a rest" alt="First Step" src="{{ $theme }}css/img/step-icon-4.png"></div><!-- /.step-image -->
				<h3 class="step-title">Post Your Jobs!</h3>
                <span class="ho-po-bo">( 4 ) <br/>Search From Database Directory</span>
			</div><!-- /.col-sm-3 -->
            <div class="a_clearfix"></div>
            
            <div class="support">
            <span>Message and Queries<br/><span class="i1"></span></span>
            <span>nterview Request<br/><span class="i2"></span></span>
            <span>Source Code Request<br/><span class="i3"></span></span>
            <span>Test Request<br/><span class="i4"></span></span>
            <span>Academic Paper Request<br/><span class="i5"></span></span>
            <div class="a_clearfix"></div>
            
            <div class="mid-btn">
            <div class="left">DJIT SUPPORT</div>
            <div class="mid"></div>
            <div class="right">Offer the work and start the project</div>
            </div>
            
            
            </div>
            
            
		</div><!-- /.job-step-wrapper -->
	</div><!-- /.container -->
</div>

 </div>
 </section>
 
 
          <div class="a_clearfix"></div>
          <div data-speed="10" data-type="background" class="bg_parralax" id="parallax_01">
            <div class="b_container">
              <div class="parallax_sec1">
                <div class="b_container">
                  <h1 style="text-transform:capitalize;">Get to<strong style="color:#F26522; text-transform:uppercase;"> Know us!</strong></h1>
                  <h2>Find the Benefits of Using Recruitment Service Of DJIT.</h2>
                  <div class="a_clearfix mar_top3"></div>
                  <a class="but_transp" href="{{{url('recruiter/why-djit')}}}">Look For Us!</a> </div>
              </div>
            </div>
          </div>
     </div>
<!--added end-->
<!---------------------------------------------------------------------------------------------------------------------------------------->
			<div class="blog_part" style="height:450px;">
            		<div class="section_bg2"></div>
					<div class="section_inner2">
						<div class="one_half" style="margin-right:30px; margin-top:30px; padding-right:30px; border-right: 1px dashed #a3a3a3;">
							<h3 style="margin-bottom:10px; text-transform:uppercase; color:#107fc9; font-weight:400;">latest news</h3>
							<div id="accordion3" class="ac_wrap iva_anim">
                                @if(count($feeds))
                                    @foreach($feeds as $feed)
		<div class="ac_title" style="background:#FFF; border-left:solid 1px #e0e0e0;border-top:solid 1px #e0e0e0; font-size:14px; font-weight:normal;">
        <span class="arrow" style="background-color:#FFF; border-left:solid 1px #e0e0e0; border-right:solid 1px #e0e0e0;" ></span>
            <a target="_blank" href="{{$feed['link']}}">{{$feed['title']}}</a>
        </div>

		<div class="ac_content" style="border-left:solid 1px #e0e0e0; border-right:solid 1px #e0e0e0;border-top:solid 1px #e0e0e0; font-size:13px;">{{$feed['desc']}}<br /><a target="_blank" href="{{$feed['link']}}">Read More</a></div>
                                    @endforeach
                                @endif
	<!--<div class="ac_title" style="background:#FFF; border-left:solid 1px #e0e0e0; font-size:14px; font-weight:normal;">
    <span class="arrow" style="background-color:#FFF; border-left:solid 1px #e0e0e0; border-right:solid 1px #e0e0e0;"></span>Dress to impress for your interview</div>
								<div class="ac_content" style="border-left:solid 1px #e0e0e0; border-right:solid 1px #e0e0e0;border-top:solid 1px #e0e0e0; font-size:13px;">With Valentine’s Day just around the corner, many people are digging out their glad rags ready for what will hopefully be a loved up date.<br /></div>
	<div class="ac_title" style="background:#FFF; border-left:solid 1px #e0e0e0;font-size:14px; font-weight:normal;">
    <span class="arrow" style="background-color:#FFF; border-left:solid 1px #e0e0e0; border-right:solid 1px #e0e0e0;"></span>Are Recruitment Interviews a Waste of Time?</div>
								<div class="ac_content" style="border-left:solid 1px #e0e0e0; border-right:solid 1px #e0e0e0;border-top:solid 1px #e0e0e0; font-size:13px;">Have you stopped to think whether that vacancy you’re advertising even needs an interview? Or are you just doing it because it’s what you know?<br /></div>-->
							</div>
						</div><!-- .one_half -->
						
						<div class="one_half last2" style="margin-bottom:60px; margin-top:30px;">
							<h3 style="margin-bottom:10px; text-transform:uppercase; color:#107fc9; font-weight:400;">What Our Clients Say</h3>
							<div  data-id="bounceIn" class="custom-animation iva_anim">
								<div id="testimonial70" class="testimonial_list">
									<ul class="testimonials">
                                        @if(count($comments))
                                        @foreach($comments as $comment)
										<li>
											<div class="testimonial-box" style="padding:15px; display:inline-block;">
                                                <div style="float:left; text-align:center;">
                                                    @if(!empty($comment->image_name) && file_exists('data/gallery/'.$comment->image_name))
                                                        <img width="75" height="75" style="border-radius:35px;" src="{{ url('/') }}/data/gallery/{{$comment->image_name}}" alt="0"  />   <br />
                                                    <!--<img alt="{{{$comment->image_name or ''}}}" width="75" height="55" src="{{{url('/')}}}/data/gallery/{{{$comment->image_name}}}"/>-->
                                                    @else
                                                        <a href="#"><img width="75" height="75" title="" style="border-radius:35px;" alt="A photo on Flickr" src="{{ $theme }}images/flickr-img1.jpg"></a>
                                                    @endif

                                                    <strong style="font-weight:normal; font-size:13px;">{{$comment->name}}</strong>
                                                </div>
                                                <div class="testimonial-content" style="padding-left:30px; padding-top:8px; padding-right:20px; font-size:12px;">
                                                    <p align="justify" style="line-height:normal;">{{$comment->description}} </p>
                                                </div>

                                                <div class="client-meta">

                                                </div>
													
											</div>
										</li>
                                        @endforeach
                                        @endif
									</ul>
								</div>
								<div class="clear"></div>
							</div>
							<div class="clear"></div>
						</div><!-- .one_half last -->
						
						<div class="clear"></div>
					</div>
            		
            </div><!-- .section_row -->
<!---------------------------------------------------------------------------------------------------------------------------------------->
		</div><!-- .pagemid_section -->

@stop