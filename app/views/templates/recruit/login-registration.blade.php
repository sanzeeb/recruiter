@extends('templates.recruit.recruit')

@section('wrapper')

<div id="subheader" style="padding:25px 0 30px!important;background:#2991d6!important;">
    <div class="inner">
        <div class=" bann"><p class="reg-pag-title">Get in touch</p></div>
        {{ Form::open(array('url'=>'auth','class'=>'form-signin','autocomplete'=>'off', 'method'=>'post')) }}

        <h6 style='margin-left: 260px;'>@if (Session::has('message'))
            <span class="alert alert-info">{{ Session::get('message')}}</span>
            <?php Session::forget('message'); ?>
            @endif
        </h6>
        <a class="forgot-pass" href="{{url('recruiter/forget-pass')}}">Forget password</a>



        <div class="login">
            <p>
                <button class="btn btn-success reg-log-btn" id="butto" name="contactsubmit" value="submit"
                        type="submit">Login
                </button>
            </p>
            <div class="remember_me">
                <input type="checkbox"  name="remember"/> Remember Me
            </div>
        </div>
        <div class="login" style="margin-top:-8px;">
            <p style="color:#FFF;">Password<br>
                <span class="wpcf7-form-control-wrap your-subject"><input type="password" tabindex="2"
                                                                          aria-invalid="false" class="logi reg-logi"
                                                                          size="28" value="" name="pass"></span></p>
            <input type="hidden" name="cv_tbl_id" value="{{{$cv_id}}}"/>
            <input type="hidden" value="recruit" name="log_type"/>
        </div>
        <div class="login" style="margin-top:-8px;">
            <p style="color:#FFF;">Username<br>
                <span>
                    <input type="text" aria-invalid="false" tabindex="1" aria-required="true" class="logi reg-logi"
                             size="28" value="<?php echo $userId; ?>" name="userId" ></span></p>

        </div>

        {{ Form::close() }}

    </div>
</div>


<div class="pagemid">
<div class="inner">
<div id="main">
<div class="entry-content">

<!-- .POST -->

<div class="row">

<div class="col-lg-8 col-md-7 col-sm-7">

    <h4 class="hs_heading"><strong>Register An Recruiter Account</strong></h4>

    <div class="hs_comment_form">

        <div class="row">


           <!-- <form method="post" action="" class="contact-form" id="contact_form">-->
                {{ Form::open(array('url'=>'recruiter/register-recruiter','class'=>'contact-form', 'autocomplete'=>'off', 'id'=>'registerFrm', 'method'=>'post')) }}
                @if(!empty($msg))
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <h3 style="color:tomato;font-weight:600;">{{{$msg or ''}}} </h3>
                    <br/>
                </div>
                @endif
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <span type="button" class="btn btn-success bttl"><i class="fa fa-pencil"></i></span>
                        </span>
                        <input type="text" placeholder="Title (required)" aria-invalid="false" aria-required="true"
                               class="form-control wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email requiredField " size="40" value="" name="title" id="contact_name">
                    </div>
                    <!-- /input-group -->
                </div>

                <!-- /.col-lg-6 -->

                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <span class="btn btn-success bttl"><i class="fa fa-envelope"></i></span>
                        </span>
                        <input type="email" aria-invalid="false" aria-required="true" placeholder="Email (required)" class="form-control wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email requiredField email"
                               size="40" value="" name="email" id="contact_email">
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <span class="btn btn-success bttl"><i class="fa fa-file-text-o"></i></span>
                        </span>
                        <input type="text" aria-required="true" placeholder="Address (required)" class="form-control wpcf7-form-control wpcf7-text wpcf7-validates-as-required requiredField address"
                               size="40" value="" name="address" id="contact_name">
                    </div>
                </div>


                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <span class="btn btn-success bttl"><i class="fa fa-phone"></i></span>
                        </span>
                        <input type="text" aria-invalid="false" aria-required="true"  placeholder="Phone (required)" class="form-control requiredField phone"
                               size="40" value="" name="phone" id="phone">
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <span class="btn btn-success bttl"><i class="fa fa-globe"></i></span>
                        </span>
                        <input type="text" aria-invalid="false" aria-required="true" placeholder="Website" class="form-control requiredField website"
                               size="40" value="" name="website" id="website">
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <span class="btn btn-success bttl"><i class="fa fa-user"></i></span>
                        </span>
                        <input type="text" aria-invalid="false" aria-required="true" placeholder="Contact Person Name (required)"
                               class="form-control requiredField contactpersonname" size="40" value="" name="contact_person" id="contact_person">
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <span class="btn btn-success bttl"><i class="fa fa-phone"></i></span>
                        </span>
                        <input type="text" aria-invalid="false" aria-required="true" placeholder="Contact Person Phone (required)"
                               class="form-control requiredField contactpersonphone" size="40" value="" name="contact_person_phone" id="contact_person_phone">
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <span class="btn btn-success bttl"><i class="fa fa-user"></i></span>
                        </span>
                        <input type="text" aria-invalid="false" aria-required="true" placeholder="User Name (required)" class="form-control requiredField username"
                               size="40" value="" name="username" id="username">
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <span class="btn btn-success bttl"><i class="fa fa-unlock-alt"></i></span>
                        </span>

                        <input type="password" aria-invalid="false" placeholder="Password (required)" class="form-control requiredField password"
                               size="40" name="password">
                    </div>
                    <div class="form-group">
                        <span class="input-group">
                            <ul style="font-size:10px;">
                                <li>Minimum 8 character long</li>
                                <li>Combination with small , capital letter and numbers [a-zA-Z0-9]</li>
                                <li>Special character (@ , #)</li>
                            </ul>
                        </span>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <span class="btn btn-success bttl"><i class="fa fa-unlock-alt"></i></span>
                        </span>
                        <input type="password" aria-invalid="false" placeholder="Re-Password (required)" class="form-control requiredField password"
                               size="40" value="" name="re_password">

                    </div>
                    <span>Strength : <strong class="passStrength"></strong></span>
                </div>

                <div class="col-lg-10">
                    <div class="input-group" style="font-size:14px; line-height:17px; margin:11px 0 0 0;">
                        <input name="agreement" type="checkbox" value="I agree to the Terms of Service and Privacy Policy"/> I
                        agree to the Terms of Service and Privacy Policy

                    </div>
                </div>

                <!--<div class="col-lg-12">
                  <div class="form-group"><textarea rows="8" class="form-control requiredField" name="contact-message" id="message"></textarea></div>
                  </div>-->

                <p id="err"></p>

                <div class="form-group" style="float:right;">
                    <div class="col-sm-12">

                        <input type="submit" class="btn popo-btn btn-success pull-right" style=""/ value="Register">

                    </div>

                </div>

            {{Form::Close()}}
        </div>


    </div>

</div>

<div class="col-lg-4 col-md-5 col-sm-5">

    <h4 class="hs_heading"><strong>DJIT Headquarters</strong></h4>

    <div class="hs_contact">

        <ul>

            <li><i class="fa fa-map-marker"></i>

                <p>BDBL Bhaban, 12 (6th floor) Kawran Bazaar, Dhaka, Bangladesh.</p>

            </li>

            <li><i class="fa fa-phone"></i>

                <p>01713493282, 01713493299, 01713493278, 01856991018</p>

                <p></p>

            </li>

            <li style="display:flex;"><i class="fa fa-envelope"></i>

                <p><a href="Mailto:info@djit.ac">info@djit.ac</a></p>

            </li>

            <li><i class="fa fa-globe"></i>

                <p><a href="http://www.djit.ac/" target="_blank">www.djit.ac</a></p>
            </li>

        </ul>

    </div>

    <!--<div class="hs_contact_social">

        <div class="hs_profile_social">

            <ul>
                <li><a href=""><i class="fa fa-facebook"></i></a></li>

                <li><a href=""><i class="fa fa-twitter"></i></a></li>

                <li><a href=""><i class="fa fa-google-plus"></i></a></li>

            </ul>

        </div>

    </div>-->

</div>

</div>

<div class="clear"></div>

</div>
<!-- .entry-content -->
</div>
<!-- .main -->
</div>
<!-- .inner -->
</div>
<script type="text/javascript">
    var weekPass = false;
    var password = '';
    $("input[name=userId]").val('').focus();
    $("input[name='password']").keyup(function(){
       var userName = $('input[name="username"]').val();
       password = $(this).val();
       var pattern = /(\w|\W){8,}/;
       if(pattern.test(password)){
            
            if(password.match(/@/) && password.match(/#/))
                $(".passStrength").html('Strong').css('color','green');
            else if(password.match(/@/))
                $(".passStrength").html('Medium').css('color','green');
            else
                $(".passStrength").html('Medium').css('color','green');
            weekPass = false;
       }else if(pattern.test(userName))
       {
            weekPass = true;
            $(".passStrength").html('Week').css('color','red');

       }else{
            weekPass = true;
            $(".passStrength").html('Week').css('color','red');
       }
       
    });

    $("#registerFrm").submit(function(){
        if(weekPass)
            return false;

        
        var re_password = $("input[name='re_password']").val();
        
        if(password != re_password)
        {    $(".passStrength").html('Not Matched').css('color','red');
            return false;
        }else{
            $(".passStrength").html('');
        }
        
    });
</script>
@stop