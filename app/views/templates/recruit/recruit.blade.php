<!DOCTYPE html>

<!--[if IE 7]>

<html class="ie ie7" lang="en-US">

<![endif]-->

<!--[if IE 8]>

<html class="ie ie8" lang="en-US">

<![endif]-->

<!--[if !(IE 7) | !(IE 8) ]><!-->

<html lang="en-US">

    <!--<![endif]-->





    <head>

        <meta name="viewport" content="width=device-width, user-scalable=no">

        <title>Asian-Engineers</title>



        <!--[if lt IE 9]>

        <script src="js/html5.js" type="text/javascript"></script>

        <![endif]-->

        <link href="{{ $theme }}css/st.css" rel="stylesheet">

        <link rel='stylesheet' href='{{ $theme }}css/sty.css' type='text/css' />

        <link rel='stylesheet' href='{{ $theme }}css/style.css' type='text/css' />

        <link rel='stylesheet' href='{{ $theme }}css/shortcodes.css' type='text/css' />

        <link rel='stylesheet' href='{{ $theme }}css/animate.css' type='text/css' />

        <link rel='stylesheet' href='{{ $theme }}css/prettyPhoto.css' type='text/css' />

        <link rel='stylesheet' href='{{ $theme }}css/responsive.css' type='text/css' />

        <link rel='stylesheet' href='{{ $theme }}css/jplayer.blue.monday.css' type='text/css' />

        <link rel='stylesheet' href='{{ $theme }}css/flexslider.css' type='text/css' />



        <!-- Light Skin -->

        <link rel='stylesheet' href='{{ $theme }}css/light.css' type='text/css' />

<link rel="stylesheet" type="text/css" href="{{$theme}}assets/select2-master/select2.css"/>

        <!-- FontAwesome CSS -->

        <link rel="stylesheet" type="text/css" media="all" href="{{ $theme }}fontawesome/css/font-awesome.css" />

        <link rel="stylesheet" type="text/css" href="{{ $theme }}css/sty.css" />



        <script src="{{ $theme }}js/jquery.min.js"></script>

        <script type='text/javascript' src='{{ $theme }}js/jquery-migrate.min.js'></script>

        <script type='text/javascript' src='{{ $theme }}js/jquery.easing.1.3.js'></script>

        <script type='text/javascript' src='{{ $theme }}js/hoverIntent.js'></script>
        
        <script src="{{$theme}}assets/select2-master/select2.js"></script>

        <script type='text/javascript' src='{{ $theme }}js/superfish.js'></script>

        <script type='text/javascript' src='{{ $theme }}js/jquery.jplayer.min.js'></script>

        <script type='text/javascript' src='{{ $theme }}js/jquery.preloadify.min.js'></script>

        <script type='text/javascript' src='{{ $theme }}js/jquery.prettyPhoto.js'></script>

        <script type='text/javascript' src='{{ $theme }}js/jquery.fitvids.js'></script>

        <script type='text/javascript' src='{{ $theme }}js/jquery.flexslider.js'></script>

        <script type='text/javascript' src='{{ $theme }}js/waypoints.js'></script>

        <!-- Google Fonts -->

        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet" type="text/css" />
    </head>


    <body>

        <!-- LayoutWrap -->

        <div id="stretched" class="fullwidth">

            <!-- bodyoverlay -->
            <div class="bodyoverlay"></div>

            <div id="sticky">
                <div style="margin-left: 65%;">
                </div>
            </div>
            <!-- #wrapper -->
            <div id="wrapper">
                <div id="header" class="header clearfix">
                    <div class="topbar">

                        <div class="inner">

                            <div class="one_half"><i class="fa fa-envelope"></i> info@djit.ac</div>

                            <div class="one_half last">

                                <div id="social-icons">
                    @if(Session::get('id')!="")

                    

                        <a href="{{url('dashboard')}}" style="background:#12A948; color:#FFF; padding:5px 15px; font-size:12px; text-transform:uppercase; margin-top:-3px;"><?php $user=User::find(Session::get('id'));
						echo $user->username;
						?></a>

                        <a href="{{{url('home/logout')}}}" class='re-logour popo-ablog btn small belizehole flat iva_anim' ><span >Logout</span></a> 

                   

                    @else

                    <a href="{{{url('recruiter/register-recruiter')}}}"  class=' btn  small  greensea  flat  iva_anim '><span>REGISTER</span></a>

                    <a href="{{{url('recruiter/register-recruiter')}}}" class="btn  small  belizehole  flat  iva_anim" ><span>LOG IN</span></a>



                    @endif</div>

                            </div>

                        </div>

                    </div><!-- .topbar -->

                    <div class="clear"></div>



                    <div class="inner">



                        <div class="logo">

                            <a href="{{{url('recruiter/index')}}}" title="DJIT"><img src="{{ $theme }}images/LOGO.jpg" alt="DJIT" /></a>

                        </div><!-- .logo -->



                        <div class="primarymenu menuwrap">

                            <ul id="atp_menu" class="sf-menu">

                                <?php
                                    $home = $about = $ourService = $whyDjit = $regRecruiter = $contact = '';
                                    $contrl = Request::segment(1);
                                    $method = Request::segment(2);
                                    if($contrl == 'recruiter' && ($method == '' || $method == 'index'))
                                    {
                                        $home = 'class="sfHover"';
                                    }
                                    if($contrl == 'recruiter' && ($method == 'about'))
                                    {
                                        $about = 'class="sfHover"';
                                    }
                                    if($contrl == 'recruiter' && ($method == 'our-service'))
                                    {
                                        $ourService = 'class="sfHover"';
                                    }
                                    if($contrl == 'recruiter' && ($method == 'why-djit'))
                                    {
                                        $whyDjit = 'class="sfHover"';
                                    }
                                    if($contrl == 'recruiter' && ($method == 'register-recruiter'))
                                    {
                                        $regRecruiter = 'class="sfHover"';
                                    }
                                    if($contrl == 'recruiter' && ($method == 'contact'))
                                    {
                                        $contact = 'class="sfHover"';
                                    }


                                ?>
                                <li {{$home or ''}} ><a href="{{{url('recruiter/index')}}}">Home</a></li>

                                <li {{$about or ''}}><a href="{{{url('recruiter/about')}}}">About Us</a></li>
								<li {{$ourService or ''}}><a href="{{{url('recruiter/our-service')}}}">Our Service</a></li>
								<li {{$whyDjit or ''}}><a href="{{{url('recruiter/why-djit')}}}">Why Djit</a></li>
                                <li {{$regRecruiter or ''}}><a href="{{{url('recruiter/register-recruiter')}}}">Registration</a></li>
                                <li {{$contact or ''}}><a href="{{{url('recruiter/contact')}}}">Contact Us</a></li>

                            </ul>

                        </div><!-- .primarymenu -->



                    </div><!-- .inner-->

                </div><!-- #header -->



                <div class="clear"></div>



                <!--Content start-->



                @yield('wrapper')



                <!-- Content end -->





                <div id="footer" style="background:url( {{ $theme }}images/dark-map.jpg) no-repeat;">
                    <div class="inner">
                        <div class="footer-sidebar">
                            <div class="one_half" style="margin-bottom:0!important;">
                                <div class="one_half"  style="margin-bottom:0!important;">
                                    <aside class="contactinfo-wg">
                                        <h3 class="widget-title">LOCATE US</h3>
                                        <div class="contactinfo-wrap" style="margin-bottom:20px;">
                                           <p><span class="icon"><i class="fa fa-map-marker"></i></span>
<span class="details">BDBL Bhaban, 12 (6th floor) Kawran Bazaar, Dhaka, Bangladesh. </span></p>
                                            <p class="phone">
                                                <span class="icon"><i class="fa fa-phone"></i></span>
                                                <span class="details" style="font-size:12px;">01713493282, 01713493299 <br> 01713493278, 01856991018</span>
                                            </p>

                                            <p>
                                                <span class="icon"><i class="fa fa-envelope"></i></span>
                                                <span class="details"><a href="mailto:info@djit.ac">info@djit.ac</a></span>
                                            </p>

                                        </div>
                                    </aside>

                                </div><!-- .one_half -->

                                <div class="one_half last" style="margin-bottom:-10px;">
                                    <aside class="widget_text">
                                        <h3 class="widget-title" style="text-transform:uppercase;">Follow US</h3>
                                        <div class="textwidget">
                                            <ul class="r-foot social">
                                            
                                            	<li><a href="{{$fb}}" target="_blank"><img src="{{ $theme }}image/f.png" alt="facebook" /></a></li>
                                                <li><a href="{{$twitter}}" target="_blank"><img src="{{ $theme }}image/t.png" alt="twitter" /></a></li>
                                                <li><a href="{{$gplus}}" target="_blank"><img src="{{ $theme }}image/g.png" alt="google" /></a></li>
                                                <li><a href="{{$linkedin}}" target="_blank"><img src="{{ $theme }}image/l.png" alt="linkedin" /></a></li>
                                                

                                            </ul>

                                        </div>

                                    </aside>

                                </div><!-- .one_half last -->

                            </div><!-- .one_half -->



                            <div class="one_half last">

                                <aside class="widget_text">

                                    <div class="textwidget footer-cont">

                                        <figure><img  src="{{ $theme }}images/LOGO.png" alt="" /></figure>

                                        <div class="demo_space"></div> Daffodil Japan IT Ltd., (DJIT) is a joint venture organization between Daffodil and BBP Japan. DJIT provide top level Education and Recruitment service. Job Placement support and try to coordinate the job opportunities in USA, JAPAN and any other developed countries.</div>

                                </aside>

                                <aside class="flickr-wg">
 <!--  
                                    <h3 class="widget-title"> {{$gallery->title}}</h3>

                                 

                                   <?php  $images= Images:: where('ga_id',$gallery->ga_id)->get(); ?>

                                    

                                    @foreach($images as $img)

                                    <div id="flickr_badge_image1" class="flickr_badge_image">

                                        @if(!empty($img->image_name) && file_exists('data/gallery/'.$img->image_name))

                                    <img alt="{{{$img->image_name or ''}}}" src="{{{url('/')}}}/data/gallery/{{{$img->image_name}}}"/>

                                    @else

                                         <a href="#"><img width="75" height="75" title="" alt="A photo on Flickr" src="{{ $theme }}images/flickr-img1.jpg"></a>

                                   

                                    @endif

                                    

                                    </div>

                                    

                                   @endforeach

                                   <a title="click here to view all" href='{{{url("recruiter/gallery")}}}' >View Our Gallery</a>-->

                                </aside>

                            </div><!-- .one_half last -->

                        </div><!-- .footer-sidebar -->

                        <div class="clear"></div>



                        <div class="copyright clearfix">

                            <div class="copyright_left">

                                &copy; Copyright - 2014 - <a href="#">WWW.DJIT.AC</a> All Rights Reserved.<br />

                            </div>

                            <div class="copyright_right">Designed and Developed by Carbon51</div>

                        </div><!-- .copyright -->

                    </div><!-- .inner -->

                </div><!-- #footer -->

            </div><!-- #wrapper -->

        </div>

        <!-- #layout -->



        <div id="back-top"><a href="#header"><span></span></a></div>

        <script type='text/javascript' src='{{ $theme }}js/sys_custom.js'></script>



    </body>





</html>

