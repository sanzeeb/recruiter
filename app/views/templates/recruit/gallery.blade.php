@extends('templates.recruit.recruit')

@section('wrapper')		

<div id="subheader">
    <div class="inner">
        <div class="subdesc">
            <h1 class="page-title">Photo Gallery</h1>
            <div class="customtext"><h2></h2></div>
        </div>
        <div class="breadcrumbs"><!-- Breadcrumb NavXT 5.0.1 -->
            <a class="home" href="#" title="carbon51.">DJIT</a><a href="#" title="Go to Pages."></a> &gt; Photo Gallery
        </div>
    </div>
</div><!-- #subheader -->

<div class="pagemid">
    <div class="inner">

        <div id="main">
            <div class="entry-content">
                <h1>Our Gallery</h1>

                <div class="clear"></div>
                <div class="demo_space" style="height:40px"></div>


                @for($i=0; $i < count($galleries); $i++)
                <?php //echo $galleries[$i]->title; ?>
                <div class="one_third gallery">
                    <div >
                        <h4>{{$galleries[$i]->title}} </h4></br>
                        <a href="{{{url('recruiter/gallery-dtl')}}}/{{$galleries[$i]->ga_id}}">
                            <?php $images = Images::where('ga_id', $galleries[$i]->ga_id)->first(); ?>
                            @if(!empty($images->image_name) && file_exists('data/gallery/'.$images->image_name))
                            <img alt="{{{$images->image_name or ''}}}" height="230" width="320" src="{{{url('/')}}}/data/gallery/{{{$images->image_name}}}"/>
                            @else
                            <img src="{{$theme}}images/520x300_img1.jpg" alt="galary_image" height="170" width="220"/>

                            @endif


                        </a>
                        <p><a style="font-size:14px;font-style:italic;" href="{{{url('recruiter/gallery-dtl')}}}/{{$galleries[$i]->ga_id}}">View more&#8230;</a>
                    </div>
                </div><!-- .one_fourth -->
                @endfor



                <div class="clear">

                </div>


                <div class="sociables">
                    <ul class="atpsocials">
                        <li><a class="dribbble" href="#"></a><span class="ttip">Dribbble</span></li>
                        <li><a class="facebook" href="#"></a><span class="ttip">Facebook</span></li>
                        <li><a class="linkedin" href="#"></a><span class="ttip">Linkedin</span></li>
                        <li><a class="pinterest" href="#"></a><span class="ttip">Pinterest</span></li>
                        <li><a class="twitter" href="#"></a><span class="ttip">Twitter</span></li>
                    </ul>
                </div>
            </div><div class="clear"></div>
        </div><!-- .one_fourth last-->
        <div class="clear"></div>
    </div><!-- .entry-content -->
</div><!-- #main -->
<div class="clear"></div>

</div><!-- .inner -->
</div><!-- .pagemid -->
</div>
</div>
@stop	