@extends('templates.recruit.recruit')

@section('wrapper')
<div id="subheader">
    <div class="inner">
        <div class="subdesc">
            <h1 class="page-title">Photo gallery</h1>
            <div class="customtext"><h2></h2></div>
        </div>
        <div class="breadcrumbs"><!-- Breadcrumb NavXT 5.0.1 -->
            <a title="Go to " href="#" class="home">djit</a> &gt; <a title="Go to Blog." href="#" > Gallery</a> &gt Gallery Details
        </div>
    </div>
</div>
<!-- #subheader -->

<div class="pagemid">
    <div class="inner">
        <div id="main">
            <div class="entry-content">
                <div class="post" id="post">
                    <div class="post_content">
                        <h2 class="entry-title">
                            <a href="#" rel="bookmark" title="Permanent Link to Image Gallery Type Format">{{{$galleries->title}}}</a>
                        </h2>
                        <div class="post-info">

                        </div><!-- post-info -->	
                        <div class="flexslider">
                            <ul class="slides">
                                <?php $images = Images::where('ga_id', $galleries->ga_id)->get(); ?>

                                @foreach($images as $img)
                                @if(!empty($img->image_name) && file_exists('data/gallery/'.$img->image_name))

                                <li>  <img height='550' class="image_" alt="{{{$img->image_name or ''}}}"  src="{{{url('/')}}}/data/gallery/{{{$img->image_name}}}"/></li>

                                @else

                                <li><img src="{{$theme}}images/520x300_img1.jpg" alt="galary_image" /></li>

                                @endif@endforeach
                            </ul>
                        </div>
                        <div class="clear"></div>
                        <div class="post-entry">
                            <p>
                                {{ $galleries->description}}
                            </p>

                        </div>
                    </div><!-- .post_content -->					
                </div><!-- /post -->




            </div><!-- .entry-content -->
        </div><!-- #main -->


        <div class="clear"></div>

    </div><!-- .inner -->
</div><!-- .pagemid -->

@stop
