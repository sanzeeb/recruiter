@extends('templates.recruit.recruit')

@section('wrapper')
<div class="inner-pag-title"><div class="inner">About Us</div></div>
<div class="pagemid">
			<div class="inner">
			
				<div id="main">
					<div class="entry-content">						
                            
                            <div style="text-align:justify;">
						<div class="headline-stripe-wrapper padding-bottom24">
							<div class="headline-border">
								<h4 style="margin-bottom:20px; text-transform:uppercase;color:#2991d6; font-weight:600;">DJIT Daffodil JAPAN IT Ltd<span class="headline-middle-line"></span>

                                </h4>
							</div>
						</div>
                        <img src="{{{$theme}}}images/reqsimg.jpg" style="float:left; margin:0 15px 10px 0; padding:1px; border:solid 1px #ddd;" width="550" class="img-responsive" alt="page-slider" />
						<p>{{$about->content or ''}}</p>
                         </div>
																
									</div>
           
							</div><!-- .one_fourth last-->
							<div class="clear"></div>
					</div><!-- .entry-content -->
				</div><!-- #main -->
                
                
                
       <div class="full-width-container pale-gray-bg pale-border-bottom pale-border-top inner-side">
			<div class="container padding-bottom48">
				<div class="row services-icons-wrap icons-left light-services-icons">
					<div class="padding-top48">
						<div class="relative text-center">
							<i class="iconalt-svg194"></i>
							<div class="services-content text-left">{{$midContent->content}}</div>
						</div>
					</div>

					<div class="clearfix"></div>
					
				</div>
			</div>
		</div>
        
        
        <div class="full-width-container pale-border-bottom pale-border-top inner-side">
        <div class="container" style="padding-top:30px;">
        <h4 style="margin-bottom:20px; text-transform:uppercase;color:#2991d6; font-weight:600;">OUR MISSION<span class="headline-middle-line"></span></h4>
		</div>	
            <div class="container padding-bottom48 po-para" style="padding-top:0px;">
				<div class="clearfix"></div>
                <p style="padding-left:0;">{{$ourMission->content}}</div>
		</div>
        
        
        <div class="full-width-container pale-border-bottom">

            <div class="container padding-bottom48" style="padding-top:30px;">

                <p class="col-md-3">
	
                <span style="margin-bottom:20px; text-transform:uppercase;color:#2991d6; font-weight:600;">{{strtoupper($specialists->title)}}<span class="headline-middle-line" style="width:57%;"></span></span><br />
                {{Str::words($specialists->content,40)}}<br />
                <a href="{{url('recruiter/specialists')}}" style="border:dashed 1px #aaa; padding:3px 25px; margin-top:15px; float:left; color:#333; font-size:12px;">More...</a>
                </p>
                
                <p class="col-md-3">
	
                <span style="margin-bottom:20px; text-transform:uppercase;color:#2991d6; font-weight:600;">{{strtoupper($trusted->title)}}<span class="headline-middle-line" style="width:61%;"></span></span><br />
                    {{Str::words($trusted->content,40)}}<br />
                 <a href="{{url('recruiter/trusted')}}" style="border:dashed 1px #aaa; padding:3px 25px; margin-top:15px; float:left; color:#333; font-size:12px;">More...</a>
                </p>
                
                
<p class="col-md-3">
	
 <span style="margin-bottom:20px; text-transform:uppercase;color:#2991d6; font-weight:600;">{{strtoupper($ourTeam->title)}}<span class="headline-middle-line" style="width:74%;"></span></span><br />{{Str::words($ourTeam->content,40)}}<br />
 <a href="{{url('recruiter/our-team')}}" style="border:dashed 1px #aaa; padding:3px 25px; margin-top:15px; float:left; color:#333; font-size:12px;">More...</a>        
                </p>
                
<div class="clearfix"></div>
                
			</div>
		</div>
        

                
				<div class="clear"></div>

			</div><!-- .inner -->
		</div><!-- .pagemid -->
		
                
      @stop          