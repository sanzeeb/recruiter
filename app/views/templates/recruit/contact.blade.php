@extends('templates.recruit.recruit')



@section('wrapper')	



<div class="inner-pag-title"><div class="inner">Contact Us</div></div>

	<div class="pagemid">

			<div class="inner">

				<div id="main">

					<div class="entry-content">

						<div>

							<script type="text/javascript">

								/* <![CDATA[ */

								jQuery(document).ready(function() {

									jQuery("#g_map987").gMap({

										icon: {

											image: "images/gmap_marker.png",

											iconsize: [37, 51],

											iconanchor: [20, 51],

											infowindowanchor: [-5, 20],

											},

										zoom:13,

										 scrollwheel: true,

										zoomControl :true,

										scaleControl: true,

										maptype: google.maps.MapTypeId.ROADMAP,

										markers:[{

											latitude:-23.749763,

											longitude:90.39264

											}, {

											address:"2 Elizabeth St, Melbourne Victoria 3000 Australia",

											popup :false,

											html:"2 Elizabeth St, Melbourne Victoria 3000 Australia" } ],

										controls: false,

										styles: [

											{

												stylers: [

													{ hue: "#8ba0b6", },

													{ saturation: -20 }

												]

											},{

												featureType: "road",

												elementType: "geometry",

												stylers: [

													{ lightness: 100 },

													{ visibility: "simplified" }

												]

											},{

												featureType: "road",

												elementType: "labels",

												stylers: [

													{ visibility: "on" }

												]

											}

										]

									});

								});	

								/* ]]&gt; */

								</script>

							<div class="atpmap"   style="height:400px"><iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d14600.557862943642!2d90.40991064389253!3d23.813639198346305!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2s!4v1403182327882" width="600" height="450" frameborder="0" style="border:0"></iframe></div>

							<div class="demo_space"  style="height:60px"></div>

							

							<div class="two_third last">

								<h3 class="fancy-title textleft"  style="line-height:normal;">Our Head Office</h3>

						<p>DJIT Ltd</p>
                        <p>Address: BDBL Bhaban, 12 (6th floor) Kawran Bazaar, Dhaka, Bangladesh.</p>
                        <p>Phone: 01713493282, 01713493299, 01713493278, 01856991018</p>
                        <p>Email: info@DJIT.ac<br>
                         Website: www.djit.ac<br></p>

							</div><!-- .one_third-->

							<div class="clear"></div>

							

						</div><!-- .POST -->

						<div class="clear"></div>

	

					</div><!-- .entry-content -->

				</div><!-- .main -->

			</div><!-- .inner -->

		</div><!-- .pagemid -->

	







@stop