@extends('templates.recruit.recruit')

@section('wrapper')
<div class="inner-pag-title"><div class="inner">{{$page->title}}</div></div>

        
        <div class="full-width-container pale-border-bottom pale-border-top">

            <div class="container padding-bottom48 po-para" style="padding-top:30px;">
                <div class="clearfix"></div>
                <p style="padding-left:0;">{{$page->content}}</p>
			</div>
		</div>
        <div class="clear"></div>

			</div><!-- .inner -->
		</div><!-- .pagemid -->
		
                
      @stop          