@extends('templates.recruit.recruit')

@section('wrapper')
<div class="inner-pag-title"><div class="inner">SERVICES PROVIDED BY DJIT</div></div>
<div class="pagemid">
			<div class="inner">
			
				<div id="main">
					<div class="entry-content services-content">
						
							<!--<div class="one_half">
                            <h1 class="page-title">{{{$about->title or ''}}}</h1>
							<p>{{{$about->content or ''}}}</p>
							</div>--><!-- .one_half -->
							
							<!--<div class="one_half last">
								<div class="video-stage">
									 @if(!empty($about->photo_name) && file_exists('data/cv/'.$about->photo_name))
                                    <img height="250" width="100%" style="-webkit-border-radius:4px; -moz-border-radius:4px; border-radius:4px;" alt="{{{$about->photo_name or ''}}}" class="img-responsive"  src="{{{url('/')}}}/data/cv/{{{$about->photo_name}}}"/>
                                    @else
                                    	<img src="{{{$theme}}}images/upload/aboutus_slider1.jpg" class="img-responsive" alt="page-slider" />
									
                                    @endif
								</div>
							</div>--><!-- .one_half -->
                            
                          
                           
                           <div class="clear"></div>
                           
                           
                           
                           
                         <div class="row">
				<div class="col-md-12">
					<div data-animated="0" id="timeline-wrap" class="col-md-12 animated-in">
                        <img src="{{url('/')}}/public/themes/recruit/image/{{$ourService->photo_name}}" class="servi-img img-responsive" alt="page-slider" />
						{{$ourService->content}}
                        <!--<ol id="timeline">
                        
							<li data-animated="0" class="t-left animated-in" style="width:48%;">
								<div class="t-time">Recruitment and selection of a permanent staff</div>
								<p align="justify">DJIT strongly believe employees are the most valuable internal asset for the company, as a result it is very much important to ensure that a new incumbent will complement the existing culture and structures within an organization.
Classic recruitment and selection carried out by our consultants is a direct search, supported by the DJIT&rsquo;s enrich database and, if necessary, online advertisements and / or press releases. An employer selects the best applicant according to his/her needs and hires the applicant on a permanent basis.</p>
							</li>
                            
							<li data-animated="0" class="t-right animated-in" style="width:48%; float:left;">
								<div class="t-time">Recruitment of temporary staff</div>
								<p align="justify">Flexible and modern solution which is assignment for DJIT Ltd. DJIT Ltd is responsible to do the recruitment on behalf and under the supervision of the organization. We offer cross border recruitment facility as well in some extent. Employers employed the candidate on a contract basis and falls under the client&rsquo;s HR. We also conduct mass recruitment that covers a large number of employees with similar qualifications.</p>
							</li>
                            

                            
							<li data-animated="0" class="t-left animated-in" >
								<div class="t-time">Direct search for key professionals, executive and managers</div>
								<p align="justify">Executive Placements is for the recruitment of middle, senior management employees and Executives, including CEOs of medium to large companies. Active and direct search for candidates is perfect for senior management positions. This method is used when it is not appropriate to disclose company's recruitment and expectations towards candidates are defined and often very high. This executive search reduces miss-hires in companies which are very expensive. Miss-hire can turn out as disappointments for the employer; the ultimate effect is really harmful for any IT recruitment agency. In our minds business change is happening globally and companies employing best practice in the hiring process will in fact be the only ones that will grow succeed and become the top listed entities of the future. That&rsquo;s why DJIT always be on the right track to be the ultimate solution for any global MNS with making any damage by following top quality hiring. Employer these mentioned facility. <br />
The coordinators delivering DJIT Ltd solutions are dedicated to the only one customer. This means increased effectiveness and efficiency of services, quick response to the changing needs and reduction of administration costs. DJIT facilitate the upright and verification check services as well besides support of staff.</p>
							</li> 
                            
                            <div class="clear"></div>
                            
							<li data-animated="0" class="t-right animated-in">
								<div class="t-time">The Review and Evaluation of Candidates</div>
								<p align="justify">The method of competency assessment and professional potential of the candidates for the job are formulated based on the observation of a potential employee in the performance of tasks similar to those occurring in the organization. To further assist our client in their decision making process our recruitment agency facilitates additional tailored recruiting assessments. This could be the completion of a case study to assess thought process or knowledge on a subject, or compiling an article to assess writing and articulation skills. We would discuss this with the client prior to commencing the recruiting project.</p>
							</li>
                            
                            
							<li data-animated="0" class="t-left animated-in">
								<div class="t-time">Training Solution for all Candidates</div>
								<p align="justify">Just to meet the customer expectations, we offer a full range of services and best practices. We always search for an individual approach to each client, because we treat human resources as the greatest good of the company. DJIT organise &quot; IT-TALENT CONTEST &quot; to filter the best people in different IT sector and train them again in our own training institute to make them more perfect and dynamic. As a truly global recruitment consultancy we can provide truly scalable solutions - whether you are a multi-national or a smaller regional or local organization, we have the global coverage and local knowledge to meet any recruitment need. We offer help and advice in the field of Information Technology and we guarantee that we will execute any order. Take advantage of our great experience and support you would expect from the leader in the branch.</p>
							</li>
                            
                            
                            
							<li data-animated="0" class="t-right animated-in">
								<div class="t-time">International Recruitment for Multinational Organization</div>
								<p align="justify">In Bangladesh, we recruit expert IT individual to work in different MNS&rsquo;s all around the Globe. We have long-term experience, a dedicated team and specialist tools to offer companies interested in IT recruitment the best candidates from Bangladesh. We are tenacious in our approach to advertising jobs and have dedicated a resource within the business to focus on creating the widest online exposure for jobs we are approaching. We probe to ensure that the reference is of relevance to the client&rsquo;s requirements and will assist them in making the best decision. Valuing our reputation as a reputable employment IT agency we interview candidates knowledgeably. We probe the candidate&rsquo;s needs, goals, aspirations and values. DJIT obtain feedback from both parties after the interview. We maintain contact with the candidate and employer both throughout the guarantee period to ensure that any issues arising is managed with finesse. Companies with another culture and that reckons that its ok to only have certain parent of the total workforce as high performers.</p>
							</li>
                            
                            
							<li data-animated="0" class="t-left animated-in">
								<div class="t-time">Assessment of the employees, determination of the potential and direction of development</div>
								<p align="justify">DJIT asses the IT expert at the very beginning and find their strength and weakness and train them accordingly. The tool is used to diagnose the level of competence of employees and allows you to explore their strengths and weaknesses. As a result, the organizations obtain information on the employee development plan and what the training needs of the company are. DJIT provides a dedicated recruitment specialist to work solely for vacancy of a particular candidate. We provide individual support for preparation of personal profiles (CVs) and in competency based interviewing for international organisations.</p>
							</li>
                            
                            
							<li data-animated="0" class="t-right animated-in">
								<div class="t-time">Outplacement Program</div>
								<p align="justify">DJIT offer outplacement program for the candidate .Outplacement is a system of support provided by the company and constructed for the needs of leaving employees. This includes psychological counselling, assistance in finding a new job as well as an assistance plan defined and developed for each employee individually. Finally we provide an atmosphere of trust and honesty with pioneering technologies and original solutions that helps to make DJIT, The most reliable company to work with for all multinational companies around the globe. We work at a faster pace and at a higher level of accuracy than any other firm.</p>
							</li>
                            
                            
                          <li data-animated="0" class="t-left animated-in">
								<div class="t-time">Management and Coordination with other IT expert recruiter</div>
								<p align="justify">It is to provide employees and simultaneous coordination of work of all other staff providers by DJIT Ltd. Thanks to this solution the customer is assured of a level of control over skills, tasks and processes and maintains contact with DJIT Limited. DJIT subscribes to the highest ethical business practice. Our businesses are high performing entities driven by centres of excellence. We pride ourselves on our service delivery. DJIT understand the lingo and requirements of different technical job roles in order to become effectual. DJIT can help you bridge the gap between your placement needs and the best talent the industry has to offer.</p>
							</li>
                            
                            
							<li data-animated="0" class="t-right animated-in">
								<div class="t-time">Additional Services with Support of Staff</div>
								<p align="justify">There is some additional services provide by DJIT besides its major role. As a truly global recruitment consultancy we can provide truly scalable solutions - whether you are a multi-national or a smaller regional or local organization, we have the global coverage and local knowledge to meet any recruitment need. Reference checking should be regarded as another form of interviewing. References help you get the full picture of the candidate's skills, work habits, and personality. DJIT always careful about the mentioned issue and provide.</p>
							</li>  
                            
                            
                            
						</ol>-->
					</div>
				</div>
			</div>  
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                            
                            
																
						</div>
           
							</div><!-- .one_fourth last-->
							<div class="clear"></div>
					</div><!-- .entry-content -->
				</div><!-- #main -->
				<div class="clear"></div>

			</div><!-- .inner -->
		</div><!-- .pagemid -->
		
                
      @stop          