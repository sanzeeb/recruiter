@extends('templates.bucket.bucket')

@section('wrapper')
<section class="wrapper">
    {{{Helpers::showMessage()}}}
    <!--mini statistics start-->
    <?php $seskills =  Input::get('skills');?>
    <div class="row">

        <div class="col-sm-12">
            <section class="panel">
                <div class="panel-body">
                
        <div class="top-con-ser" style="margin-bottom:10px;">
        <h2><span style="color:#06f;">TOP ENGINEER </span> : {{strtoupper($skill)}}</h2><br/><br/>
        <form action="{{Request::url()}}" class="font-ser col-sm-12" style="padding:0;" method="get">
        
        <label class="po-drop">
            
        <select placeholder="Skill"  class="form-control multi-skills" name="skills[]" multiple="multiple">
            @foreach($disSkills as $s)
                @if(!empty($seskills))
                    @if((in_array($s,$seskills)))
                        <option selected="selected" value="{{$s}}">{{$s}}</option>
                    @else
                    <option value="{{$s}}">{{$s}}</option>
                    @endif
                @else
                    <option value="{{$s}}">{{$s}}</option>
                @endif
            @endforeach
        </select>
        </label>
        <label><input name="name" placeholder="Name" type="text" value="{{Request::get('name')}}"/></label>
        <label><input name="yxp" placeholder="Year of Exp" type="text" value="{{Request::get('yxp')}}"/></label>
        <input type="hidden" name="selectedSkill" value="{{$skill}}"/>
        <input name="" class="srbtn" type="submit" value="search" />
        <a href="{{Request::url()}}" class="btn srbtn btn-xs btn-info" style="width:80px;">Reset</a>
        </form>
        </div>
                  
                

<!--<div class="new-top-en">
        <div class="lef"><span class="avator"><img src="{{ $theme }}image/img2.jpg"  /></span></div>
        <div class="rig">
        <a href="#" class="name">Shane Wilson</a>
        <a href="#" class="p_button">Favorite</a>
        <a href="#" class="p_button">Inquery</a>
        <a href="#" class="p_button">Interview</a>
        <a href="#" class="p_button">Test Project</a>
        <div class="clear"></div>
        <span><strong><span>Age :</span> 33</strong> &nbsp;&nbsp;&nbsp;<strong><span>Year of Exp :</span> 9</strong> &nbsp;&nbsp;&nbsp;<strong><span>Rank :</span> 1</strong></span>   <p>A Computer Science and Engineering graduate have more than four years? of experience in related field. Currently working as a Software Engineer in an IT based organization and core responsibility is ensuring the quality of coding user interface design and development projects using technologies such as HTML5, CSS3, html, CSS, JavaScript library (jQuery), PHP (CakePHP), Codeigniter, Yii, Zend, XML, Joomla, Wordpress<a href="">...more</a></p>
        <ul class="cat-link">
                <li><a>PHP</a></li>
                <li><a>Photoshop</a></li>
                <li><a>C++</a></li>
                <li><a>CSS 3</a></li>
                <li><a>C#</a></li>
            </ul>
        </div>
        
        </div>
        <div class="clear"></div>-->












                    <?php $rank=Request::get('id'); $page=Request::get('page');
                    $rank = (!empty($rank))? $rank : 1;
                    $page=!empty($page)? $page : 1;
                    if($page==1){
                        $rank=1;
                    }
                    
                    // $rank=$rank * $page;
                    ?>
                    <?php $rankTop = (count($expertises)*$page)+1; ?>
                    <?php 
                        
                        $name =  Input::get('name');
                        $yxp =  Input::get('yxp');
                        $queryString = (!empty($queryString))? $queryString : '';
                        $paginateData = array('id'=>$rankTop);
                        if(!empty($seskills))
                            $paginateData['skills'] = $seskills;
                        if(!empty($name))
                            $paginateData['name'] = $name;
                        if(!empty($yxp))
                            $paginateData['yxp'] = $yxp;
                                             ?>
                    @if(!is_array($expertises))
                    <div class="pag">
                        {{$expertises ->appends($paginateData)->links()}}
                    </div>
                    @endif
                    
                    @foreach($expertises as $j=> $expert)

                    <?php 
                       $flag = 0;
                        $cvSpecs = $skills[$expert->cv_tbl_id]; //CvSpecialization::where('cv_tbl_id',$expert->cv_tbl_id)->get(); ?>
                    <?php 
                    $expertSkills = '<ul class="cat-link" >'; 
                    foreach($cvSpecs as $specs){
                         
                       if(!empty($searchedSkills)){
                            if(trim(strtolower($specs->key)) == trim(strtolower($skill)))
                            {
                                $flag = 1;

                            }
                        }
                        $expertSkills .= '<li ';
                            if(strtolower($skill) == strtolower($specs->key))
                            { 
                                 $expertSkills .= 'style="background:green !important;"';
                            }elseif(!empty($seskills) && (in_array($specs->key,$seskills)))
                            {
                                $expertSkills .= 'style="background:#b1f79b !important;"';
                            }
                        $expertSkills .='>';
                        $expertSkills .= '<a ';
                            if(strtolower($skill) == strtolower($specs->key))
                            {
                                $expertSkills .= 'style="color:#fff;"';
                            }
                        $expertSkills .='>'.str_replace("_"," ",strtoupper($specs->key)).'</a></li>';
                                    
                    }
                    if(!empty($searchedSkills) && !empty($skill))
                    {
                        if($flag == 0)
                        {
                            continue;
                        }
                    }
                    $expertSkills .= '</ul>';
                            
                    ?>
                    <?php $candidate = Candidate::where('cv_tbl_id', $expert->cv_tbl_id)->first(); ?>
                    <div class="new-top-en">
                        <div class="lef"><span class="avator">
                                @if(!empty($expert->photo))
                                    <?php try{ ?>
                                       
                                    <img src="{{JOB_DOMAIN.'data/profile/'.$expert->photo}}" alt="{{$expert->photo}}" />
                                    <?php }catch(Exception $ex){ ?>
                                         <img src="{{ $theme }}images/avatar.jpg"  alt="Avatar"/>
                                    <?php } ?>
                                    
                                    
                                    
                                @else
                                    <img src="{{ $theme }}images/avatar.jpg"  alt="Avatar"/>
                                @endif
                            </span></div>
                        <div class="rig">
                            @if(Session::get('id')!="")

                            <?php $link = ''; ?>
                            <a href="{{url('resume/display-resume/'.$candidate->cv_tbl_id)}}" class="name">{{str_replace(array("_")," ",strtoupper($candidate->firstname))}}</a>
                            @else
                            <?php $link = url('recruiter/register-recruiter/'.$expert->cv_tbl_id); ?>
                            <a href="{{$link}}" class="name">{{str_replace(array("_")," ",strtoupper($candidate->firstname))}}</a>
                            @endif


                            @if(empty($user) || ($user->user_type == 'Employer'))
                                @if(empty($link))
                                    @if(!empty($user) && ($user->user_type == 'Employer'))
                                    <form target="_blank" method="post" action="{{url('employer/remove-favorite')}}">
                                        <input type="hidden" name="cid" value="{{$candidate->candidate_id}}"/>
                                        <input type="hidden" name="cv_tbl_id" value="{{$candidate->cv_tbl_id}}"/><button type="submit" title="Remove favorite" class="p_button"><i class="fa fa-trash"></i>Remove Favourite</button>
                                    </form>
                                    @endif
                                    <form target="_blank" method="post" action="{{url('inquiry/create/'.$candidate->candidate_id)}}">
                                        <input type="hidden" name="cv_tbl_id" value="{{$candidate->cv_tbl_id}}"/><button type="submit" title="Inquery" class="p_button">Inquiry</button>
                                    </form>
                                    <form target="_blank" method="post" action="{{url('interview/create/'.$candidate->candidate_id)}}">
                                        <input type="hidden" name="cv_tbl_id" value="{{$candidate->cv_tbl_id}}"/><button type="submit" title="Interview" class="p_button">Interview</button>
                                    </form>
                                    @if(!empty($candidate->source_code))
                                    <?php 
                                        $sourceLink = '';
                                        
                                        if(preg_match('/http/',$candidate->source_code))
                                            $souceLink  = $candidate->source_code;
                                        else
                                            $sourceLink = 'http://'.$candidate->source_code; 
                                    ?>
                                    <a href="{{url($sourceLink)}}" target="_blank" class="p_button">Source Code</a>
                                    @endif
                                    @if(!empty($candidate->academic_certificate) && File::exists('data/academic_certificate/'.$candidate->academic_certificate))
                                    <a target="_blank" href="{{url('data/academic_certificate/'.$candidate->academic_certificate)}}" class="p_button">Academic Certificate</a>
                                    @endif
                                    <a href="{{url('tproject/create/'.$candidate->candidate_id)}}" target="_blank" class="p_button">Test Project</a>
                                @else
                                    <a href="{{$link}}" class="p_button">Favorite</a>
                                    <a href="{{$link}}" class="p_button">Inquery</a>
                                    <a href="{{$link}}" class="p_button">Interview</a>
                                    <a href="{{$link}}" class="p_button">Test Project</a>
                                @endif
                            @else
                                <a href="{{url('resume/display-resume/'.$expert->cv_tbl_id)}}" target="_blank" class="p_button">View Details</a>
                                @if(!empty($candidate->source_code))
                                    <?php 
                                        $sourceLink = '';
                                        
                                        if(preg_match('/http/',$candidate->source_code))
                                            $souceLink  = $candidate->source_code;
                                        else
                                            $sourceLink = 'http://'.$candidate->source_code; 
                                    ?>
                                    <a href="{{url($sourceLink)}}" target="_blank" class="p_button">Source Code</a>
                                @endif
                                @if(!empty($candidate->academic_certificate) && File::exists('data/academic_certificate/'.$candidate->academic_certificate))
                                    <a target="_blank" href="{{url('data/academic_certificate/'.$candidate->academic_certificate)}}" class="p_button">Academic Certificate</a>
                                @endif
                            @endif

                            <div class="clear"></div>
                            <span><strong><span>Age :</span> {{Candidate::getAge($candidate->dob)}}</strong> &nbsp;&nbsp;&nbsp;<strong><span>Year of Exp :</span> {{$expert->year_of_exp}}</strong> &nbsp;&nbsp;&nbsp;<strong><span>Rank :</span> {{$rank}} <?php $rank++; ?></strong></span>
                            @if(!empty($candidate->csummary))
                            <div class="n-content-box">
                                <p class="summary">{{Str::words($candidate->csummary,50)}}<a href="" class="summary">...more</a></p>

                                <p class="details">{{$candidate->csummary}}<a href="" class="details">...less</a></p>
                            </div>
                            @endif
                            

                            @if(count($cvSpecs))
                                {{$expertSkills}}
                            @endif
                        </div>

                    </div>
                    <div class="clear"></div>
                    <div class="divider thin" style="margin:30px 0;"></div>

                    <!--<div class="top-engi-box">
                    <span>
                        <div class="left">
                            <span><b>Name : </b>
                                @if(Session::get('id')!="")

                                <?php /*$link = url('resume/display-resume/'.$expert->cv_tbl_id)*/?>
                                @else
                                <?php /*$link = url('recruiter/register-recruiter/'.$expert->cv_tbl_id)*/?>
                                @endif
                             <a href="">{{$candidate->firstname}}</a>
                            </span>
                            <span><b>Rank : </b> <strong>{{$rank}} <?php /*$rank++; */?></strong></span><span><b>Age : </b> <strong>{{Candidate::getAge($candidate->dob)}}</strong></span>
                            <span><b>Year of exp :</b> <strong>: {{$expert->year_of_exp}}</strong></span>
                        </div>
                            <div class="mid"><p>A Computer Science and Engineering graduate have more than four years? of experience in related field. Currently working as a Software Engineer in an IT based organization and core responsibility is ensuring the quality of coding user interface design and development projects using technologies such as HTML5, CSS3, html,  CSS, JavaScript library (jQuery), PHP (CakePHP), Codeigniter, Yii, Zend, XML, Joomla, Wordpress, MySQL. Previously worked as a web developer where there was an involvement in Interface design, documentation, implementation and support functions, generated projects more than ten. Also has knowledge on Joomla structure and coding.</p></div>
                            <div class="right">
                                <p><a href="{{{$link}}}" class="push_button">Favorite</a></p>
                                <p><a href="{{{$link}}}" class="push_button2">Inquery</a></p>
                                <p><a href="{{{$link}}}" class="push_button3">Interview</a></p>
                                <p><a href="{{{$link}}}" class="push_button4">Source Code</a></p>
                                <p><a href="{{{$link}}}" class="push_button5">Test Project</a></p>
                                <p><a href="{{{$link}}}" class="push_button6">Academic Paper</a></p>
                            </div>
                    </span>
                    </div>-->
                    @endforeach

                    @if(!is_array($expertises))
                    <div class="pag">
                            {{$expertises ->appends($paginateData)->links()}}
                    </div>
                    @endif

                </div>
            </section>
        </div>
    </div>

</section>
<script type="text/javascript">

    $(function(){
        $('a.summary').click(function(){
            var obj = $(this);
            obj.parent().next().slideDown().children('a.details').show();
            obj.parent().hide();
            return false;
        });

        $('a.details').click(function(){
            var obj = $(this);
            obj.parent().slideUp('fast').prev().slideDown();
            return false;
        });
    });

    $("select.multi-skills").select2();


</script>
@stop