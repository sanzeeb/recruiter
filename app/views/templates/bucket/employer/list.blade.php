@extends('templates.bucket.bucket')

@section('wrapper') 
<section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        Employer list
                        
                             <span class="tools pull-right">

                            {{ Form::open(array('url'=>'employer/create-employer','method'=>'get')) }}
                                <button class="btn btn-primary panel-btn" type="submit"><i class="fa fa-plus-circle"></i> Add New</button>
                            {{ Form::close() }}
                        
                         </span>
                    </header>
                    <div class="panel-body">
                        @if (Session::has('message'))
                <div class="alert alert-info">{{ Session::get('message') }}</div>
                        @endif
                        <table class="table  table-hover general-table">
                            <thead>
                            <tr>
                                <th>Created at</th>
                               
                                <th class="hidden-phone">Title</th>
                                <th> Username</th>
                                <th> Email</th>
                                <th> Phone</th>
                                <th>Actions</th>

                            </tr>
                            </thead>
                            <tbody>
                                
                                @foreach($users as $userow)
                                
                            <tr>
                                <td>{{{Helpers::dateTimeFormat("F j, Y",$userow->created_at)}}} </td>

                                <td><a href="#">{{{$userow->Employer->title}}}</a></td>
                                

                                <td><a href="#">{{{$userow->username}}}</a></td>
                                
                                <td>{{{$userow->user_email}}}</td>
                                <td>{{{$userow->Employer->phone}}}</td>
                                

                                <td>
                                    

            <a  href="{{{url('employer/edit-employer/')}}}/{{{$userow->id}}}">Edit</a> |
            <a onclick="deleteEmp('{{{$userow->id}}}')" href="javascript:void(0)">Delete</a> |
                                                           
                                                     
                                               
                                                
                                                  


                                </td>
                            </tr>
                                    @endforeach

                            </tbody>
                        </table>
                        <div class="row-fluid"><div class="span6"><div id="hidden-table-info_info" class="dataTables_info">{{$users->links()}}</div></div><div class="span6"><div class="dataTables_paginate paging_bootstrap pagination"></div></div></div>
                    </div>
                </section>
            </div>
        </div>
        
        <!-- page end-->
        </section>

  <div class="modal fade in" id="myModal-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title">User</h4>
                                    </div>
                                    <div class="modal-body">

                                        <form role="form" class="form-horizontal">
                                            <div class="form-group">
                                                <label class="col-lg-2 col-sm-2 control-label" for="inputEmail1">Email</label>
                                                <div class="col-lg-10">
                                                    <input type="email" placeholder="Email" id="inputEmail4" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-2 col-sm-2 control-label" for="inputPassword1">Password</label>
                                                <div class="col-lg-10">
                                                    <input type="password" placeholder="Password" id="inputPassword4" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-offset-2 col-lg-10">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"> Remember me
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-offset-2 col-lg-10">
                                                    <button class="btn btn-default" type="submit">Sign in</button>
                                                </div>
                                            </div>
                                        </form>

                                    </div>

                                </div>
                            </div>
                        </div>
<script src="{{ $theme }}js/custom/emp.js"></script>
  
@stop