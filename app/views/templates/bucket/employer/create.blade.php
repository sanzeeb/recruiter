@extends('templates.bucket.bucket')

@section('wrapper')  
<section class="wrapper">
        <!-- page start-->
<h3>{{{$msg or ''}}} </h3>
     {{ Form::open(array('url'=>'employer/create-employer','enctype'=>'multipart/form-data','class'=>'wpcf7-form contact_form', 'method'=>'post')) }}   
    <div class="row">
    <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            Employee create form
                            
                        </header>
                        <div class="panel-body">
                            <div class="position-center">

                                <div class="form-group">
                                     <label for="title">Title</label>
                                       <input type="text" value="" name="title" id="title" data-val-required="The User name field is required." data-val="true" class="form-control">
                                </div>
                                <div class="form-group">
                                     <label for="address">Address</label>
                                      <textarea name="address" class="form-control">
                                      </textarea>
                                </div>
                                                        <div class="form-group">
                                     <label for="Phone">Phone</label>
                                       <input type="text" value="" name="phone" id="phone" data-val-required="The User name field is required." data-val="true" class="form-control">
                                </div>
                              <div class="form-group">
                                     <label for="email">Email</label>
                                       <input type="email" value="" name="email" id="Email" data-val-required="Email field is required." data-val="true" class="form-control">
                                </div>
                               <div class="form-group">
                                     <label for="website">Website</label>
                                       <input type="text" value="" name="website" id="website" data-val-required="Email field is required." data-val="true" class="form-control">
                                </div>
                               
                                  <div class="form-group">
                                     <label for="Contact">Contact person</label>
                                       <input type="text" value="" name="contact_person" id="contact_person" data-val-required="The User name field is required." data-val="true" class="form-control">
                                </div>
                                  <div class="form-group">
                                     <label for="Contact">Contact person phone</label>
                                       <input type="text" value="" name="contact_person_phone" id="contact_person_phone" data-val-required="The User name field is required." data-val="true" class="form-control">
                                </div>
                                
                                
                                <div class="form-group">
                                     <label for="UserName">User name</label>
                                       <input type="text" value="" name="username" id="UserName" data-val-required="The User name field is required." data-val="true" class="form-control">
                                </div>
                                
                                <div class="form-group">
                                      <label for="Password">Password</label>
                                     <input type="password" name="password" id="Password" data-val-required="The Password field is required." data-val-length-min="6" data-val-length-max="100" data-val-length="The Password must be at least 6 characters long." data-val="true" class="form-control">
                                </div>
                                
                               <div class="form-group">
                                       <label for="ConfirmPassword">Confirm password</label>
                <input type="password" name="re_password" id="ConfirmPassword" data-val-equalto-other="*.Password" data-val-equalto="The password and confirmation password do not match." data-val="true" class="form-control">
                                </div>

                                    <div class="form-group">
                                    <label class="control-label col-md-3">Photo</label>
                                    <div class="controls col-md-5">
                                        <div class="fileupload-new thumbnail" style="width:110px; float:left;">
                                            <img alt="" src="{{$theme}}images/placeholder.gif"/>

                                        </div>
                                        <div class="fileupload fileupload-new" style="float:right;" data-provides="fileupload">
                                            <span class="btn btn-white btn-file">
                                            <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Browse file</span>
                                            <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                            <input type="file" class="default" name="emp_photo"/>
                                            </span>
                                            <span class="fileupload-preview" style="margin-left:5px;"></span>
                                            <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                        </div>
                                    </div>
                                </div>
                                    
                                <button type="submit" style="float:left; clear:both;" class="btn btn-info">Submit</button>
                            
                            </div>

                        </div>
                    </section>

            </div>

</div>     


     {{ Form::close() }}
        <!-- page end-->
        </section>
@stop