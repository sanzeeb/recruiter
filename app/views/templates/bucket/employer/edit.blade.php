@extends('templates.bucket.bucket')

@section('wrapper')  
<section class="wrapper">
    <!-- page start-->
    <h3>{{{$msg or ''}}} </h3>
    {{ Form::open(array('url'=>'employer/edit-employer','class'=>'wpcf7-form contact_form','enctype'=>'multipart/form-data', 'method'=>'post')) }}   
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Employee edit form

                </header>
                <div class="panel-body">
                    <div class="position-center">
                        <input type="hidden" name="id" value="{{{$users->id}}}" />
                        <input type="hidden" name="user_id" value="{{{$users->user_id}}}" />
                        <input type="hidden" name="username" value="{{{$users->username}}}" />
                        
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" value="{{{$users->Employer->title}}}" name="title" id="title" data-val-required="The User name field is required." data-val="true" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="address">Address</label>
                            <textarea name="address" class="form-control">
                            {{{$users->Employer->address}}}
                            </textarea>
                        </div>
                        <div class="form-group">
                            <label for="Phone">Phone</label>
                            <input type="text" value="{{{$users->Employer->phone}}}" name="phone" id="phone" data-val-required="The User name field is required." data-val="true" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" value="{{{$users->Employer->email}}}" name="email" id="Email" data-val-required="Email field is required." data-val="true" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="website">Website</label>
                            <input type="text" value="{{{$users->Employer->website}}}" name="website" id="website" data-val-required="Email field is required." data-val="true" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="Contact">Contact person</label>
                            <input type="text" value="{{{$users->Employer->contact_person}}}" name="contact_person" id="contact_person" data-val-required="The User name field is required." data-val="true" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="Contact">Contact person phone</label>
                            <input type="text" value="{{{$users->Employer->contact_person_phone}}}" name="contact_person_phone" id="contact_person_phone" data-val-required="The User name field is required." data-val="true" class="form-control">
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3">Photo</label>
                            <div class="controls col-md-5">
                                <div class="fileupload-new thumbnail" style="width:110px; float:left;">
                                    @if(!empty($users->Employer->photo_name) && file_exists('data/profile/'.$users->Employer->photo_name))
                                    <img alt="{{{$users->Employer->photo_name or ''}}}" src="{{{url('/')}}}/data/employer/{{{$users->Employer->photo_name}}}"/>
                                    @else
                                    <img alt="thumbnail" src="{{$theme}}images/placeholder.gif"/>
                                    @endif
                                </div>
                                <div class="fileupload fileupload-new" style="float:right;" data-provides="fileupload">
                                    <span class="btn btn-white btn-file">
                                        <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Browse file</span>
                                        <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                        <input type="file" class="default" name="emp_photo"/>
                                    </span>
                                    <span class="fileupload-preview" style="margin-left:5px;"></span>
                                    <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                </div>
                            </div>
                        </div>

                        <button type="submit" style="float:left; clear:both;" class="btn btn-info">Update</button>

                    </div>

                </div>
            </section>

        </div>

    </div>          {{ Form::close() }}
    <!-- page end-->
</section>
@stop