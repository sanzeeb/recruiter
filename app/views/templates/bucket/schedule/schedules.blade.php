@extends('templates.bucket.bucket')

@section('wrapper') 
    <link rel="stylesheet" href="({$theme})tasks/plugins/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="({$theme})tasks/plugins/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="({$theme})tasks/css/main.css">
		<link rel="stylesheet" href="({$theme})tasks/css/main-responsive.css">
		<link rel="stylesheet" href="({$theme})tasks/plugins/iCheck/skins/all.css">
		<link rel="stylesheet" href="({$theme})tasks/plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css">
		<link rel="stylesheet" href="({$theme})tasks/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
		<link rel="stylesheet" href="({$theme})tasks/css/print.css" type="text/css" media="print"/>
		<link rel="stylesheet" href="({$theme})tasks/plugins/fullcalendar/fullcalendar/fullcalendar.css">
<section class="wrapper">

                                                	<div class="row">
						<div class="col-sm-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<i class="clip-calendar"></i>
									Schedules
									
								</div>
								<div class="panel-body">
									<div id='calendar'></div>
								</div>
							</div>
						</div>

					</div>
                        <!-- page end-->
                    
               
        <!-- page end-->
        </section>

<script src="{{$theme}}tasks/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
		<script src="{{$theme}}tasks/plugins/bootstrap/js/bootstrap.min.js"></script>
	
		<script src="{{$theme}}tasks/js/main.js"></script>
		<!-- end: MAIN JAVASCRIPTS -->
		<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script src="{{$theme}}tasks/plugins/fullcalendar/fullcalendar/fullcalendar.js"></script>
		<script src="{{$theme}}tasks/js/index.js"></script>
		<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script>
			jQuery(document).ready(function() {
				Main.init();
				Index.init();
			});
		</script>

@stop