@extends('templates.bucket.bucket')

@section('wrapper') 

<section class="wrapper">
        <!-- page start-->
<h3>{{{$msg or ''}}} </h3>
   
        <div class="row">
           <div class="col-md-12">
                
                <section class="panel">
                    <header class="panel-heading">
                     Create   Available time
                              <span class="tools pull-right">
                            <a class="fa fa-chevron-down" href="javascript:;"></a>
                            <a class="fa fa-cog" href="javascript:;"></a>
                            <a class="fa fa-times" href="javascript:;"></a>
                         </span>
                    </header>
                    <div class="panel-body">
                       
                              {{ Form::open(array('url'=>'schedule/create-schedule','class'=>'form-horizontal', 'method'=>'post')) }}   
   
                            <div class="form-group">
                                <label class="control-label col-md-3">Enter title</label>
                                <div class="col-md-6 col-xs-11">
                                    <input class="form-control form-control-inline input-medium " name="title"  size="16" type="text" value="" />
                                 
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Select Available Date</label>
                                <div class="col-md-4 col-xs-11">
                                    <input class="form-control form-control-inline input-medium default-date-picker" name="available_date"  size="16" type="text" value="" />
                                    <span class="help-block">Select date</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Select from time</label>
                                <div class="col-md-5">
                                    <div class="input-group bootstrap-timepicker">
                                        <input  name="availtime" type="text" class="form-control timepicker-default">
                                                <span class="input-group-btn">
                                                <button class="btn btn-default" style="margin-right: 13px; " type="button"><i class="fa fa-clock-o"></i></button>
                                             
                                                
                                                
                                                </span>
                                        
                                        
                                    </div>
                                    
                                            </div>
                            </div>
 <div class="form-group">
                                <label class="control-label col-md-3">Select to time</label>
                                <div class="col-md-5">
                                    <div class="input-group bootstrap-timepicker">
                                        <input  name="availtimeto" type="text" class="form-control timepicker-default">
                                                <span class="input-group-btn">
                                                <button class="btn btn-default" style="margin-right: 13px; " type="button"><i class="fa fa-clock-o"></i></button>
                                             
                                                
                                                
                                                </span>
                                        
                                        
                                    </div>
                                    
                                    
                                </div>
                                
                                                <button  class="btn btn-success add-time" type="submit">Save</button>
                            </div>
               

                       {{Form::close() }}
                    </div>
                </section>
              
            </div>
            
      
            
      
            
        </div>



        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                     Available  time
                        <span class="tools pull-right">
                            
                            
                         </span>
                    </header>
                    <div class="panel-body">
       
                        <table class="table  table-hover general-table">
                            <thead>
                            <tr>
      
                                <th>Created at</th>
                               
                                <th class="hidden-phone">Title</th>
                                <th> Available date</th>
                                <th> Time</th>
                          <th> Actions</th>

                                

                            </tr>
                            </thead>
                            <tbody>
                                
                                @foreach($schedules as $skill)
                                
                            <tr>
                                <td>{{{Helpers::dateTimeFormat("F j, Y",$skill->created_at)}}} </td>

                                <td><a href="#">{{{$skill->title}}}</a></td>
                                

                                <td><a href="#">{{{$skill->available_date}}}</a></td>
                                
                                                          
                                
                                <td class="hidden-phone">{{{$skill->from_time}}}-{{{$skill->to_time}}}</td>
                                
                                
                                <td>   
            
            <a  data-toggle="modal" href="#myModal-1" onclick="editSchedule('{{{$skill->avail_id}}}')" >Edit</a> |
                                    &nbsp; &nbsp;
            <a href="javascript:void(0)" onclick="deleteSchedule('{{{$skill->avail_id}}}')">Delete</a> |
            
                                </td>
                            </tr>
                                    @endforeach

                            </tbody>
                        </table>
                        <div class="row-fluid"><div class="span6"><div id="hidden-table-info_info" class="dataTables_info">{{$schedules->links()}}</div></div><div class="span6"><div class="dataTables_paginate paging_bootstrap pagination"></div></div></div>
                    </div>
                </section>
            </div>
        </div>
      
        <!-- page end-->
        </section>
    

  <div class="modal fade in" id="myModal-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title">Schedule Edit form </h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="msg"></div>
                                        <form role="form" class="form-horizontal">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Title</label>
                                                <div class="col-md-4 col-xs-11">
                                                    <input type="hidden" name="etitle_id" id="etitle_id"  />
                                                    <input type="text" name="etitle" placeholder="Enter title" id="etitle" class="form-control">
                                                </div>
                                            </div>
                                          
                                              <div class="form-group">
                                <label class="control-label col-md-3">Select Available Date</label>
                                <div class="col-md-4 col-xs-11">
                                    <input class="form-control form-control-inline input-medium default-date-picker" name="eavailable_date" id="eavailable_date"  size="16" type="text" value="" />
                                    <span class="help-block">Select date</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Select from time</label>
                                <div class="col-md-5">
                                    <div class="input-group bootstrap-timepicker">
                                        <input  id="eavailtime" name="eavailtime" type="text" class="form-control timepicker-default">
                                                <span class="input-group-btn">
                                                <button class="btn btn-default" style="margin-right: 13px; " type="button"><i class="fa fa-clock-o"></i></button>
                                             
                                                
                                                
                                                </span>
                                        
                                        
                                    </div>
                                    
                                            </div>
                            </div>
 <div class="form-group">
                                <label class="control-label col-md-3">Select to time</label>
                                <div class="col-md-5">
                                    <div class="input-group bootstrap-timepicker">
                                        <input  id="eavailtimeto" name="eavailtimeto" type="text" class="form-control timepicker-default">
                                                <span class="input-group-btn">
                                                <button class="btn btn-default" style="margin-right: 13px; " type="button"><i class="fa fa-clock-o"></i></button>
                                             
                                                
                                                
                                                </span>
                                        
                                        
                                    </div>
                                    
                                    
                                </div>
                                
                                                <button onclick="updateSchedule()"  class="btn btn-success add-time" type="button">Update</button>
                            </div>
               
                                            
                                        </form>

                                    </div>

                                </div>
                            </div>
                        </div>

<script src="{{ $theme }}js/custom/schedule.js"></script>
@stop