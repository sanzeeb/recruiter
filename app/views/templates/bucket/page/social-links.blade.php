@extends('templates.bucket.bucket')

@section('wrapper')
<section class="wrapper">
    <!-- page start-->
    <h3>{{{$msg or ''}}} </h3>
    {{ Form::open(array('url'=>'page/social-links','class'=>'wpcf7-form contact_form', 'method'=>'post')) }}
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Social Links

                </header>
                <div class="panel-body">
                    <div class="position-center">

                        <div class="form-group">
                            <label class="col-lg-3 col-sm-2 control-label" for="eskill_name">Faceboook</label>

                            <input type="hidden" value="{{{$fb}}}" name="key[fb]" id="title"  />
                            <input type="text" value="{{{$fb}}}" name="key[fb]" id="title" class="form-control">

                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 col-sm-2 control-label" for="eskill_name">Google +</label>

                            <input type="hidden" value="{{{$gplus}}}" name="key[gplus]" id="title"  />
                            <input type="text" value="{{{$gplus}}}" name="key[gplus]" id="title" class="form-control">

                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 col-sm-2 control-label" for="eskill_name">Twitter</label>

                            <input type="hidden" value="{{{$twitter}}}" name="key[twitter]" id="title"  />
                            <input type="text" value="{{{$twitter}}}" name="key[twitter]" id="title" class="form-control">

                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 col-sm-2 control-label" for="eskill_name">Linked In</label>

                            <input type="hidden" value="{{{$linkedin}}}" name="key[linkedin]" id="title"  />
                            <input type="text" value="{{{$linkedin}}}" name="key[linkedin]" id="title" class="form-control">

                        </div>

                        <div class="form-group col-lg-offset-3">
                            <button type="submit" class="btn btn-info">Submit</button>
                        </div>

                    </div>

                </div>
            </section>

        </div>

    </div>          {{ Form::close() }}


    <!-- page end-->
</section>

<script src="{{ $theme }}js/custom/skill.js"></script>

@stop