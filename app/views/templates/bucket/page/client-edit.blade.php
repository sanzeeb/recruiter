@extends('templates.bucket.bucket')

@section('wrapper')  
<section class="wrapper">
        <!-- page start-->
<h3>{{{$msg or ''}}} </h3>
     {{ Form::open(array('url'=>'page/edit-client-say','class'=>'wpcf7-form contact_form','enctype'=>'multipart/form-data', 'method'=>'post')) }}
    <div class="row">
    <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            Client says form
                            
                        </header>
                        <div class="panel-body">
                            <div class="position-center">
                                

                                <div class="form-group">
                                                <label class="col-lg-3 col-sm-2 control-label" for="eskill_name">Name of client</label>
                                                 
                                                    <input type="text"  name="title" placeholder="Enter title" id="title" value="{{$client->name}}" class="form-control">
                                                
                                            </div>
                                <div class="form-group">
                                     <label class="col-lg-3 col-sm-2 control-label">Client comment</label>
                                     <textarea class="form-control" name="content_description" >{{$client->description}}</textarea>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3">Select Photo</label>
                                    <div class="controls col-md-5">
                                        <div class="fileupload-new thumbnail" style="width:110px; float:left;">
                                        @if(File::exists('data/gallery/'.$client->image_name))
                                            <img alt="thumbnail" src="{{url('data/gallery').'/'.$client->image_name}}"/>
                                        @else
                                            <img alt="thumbnail" src="{{$theme}}images/placeholder.gif"/>
                                        @endif
                                        </div>
                                        <div class="fileupload fileupload-new" style="float:right;" data-provides="fileupload">
                                            <span class="btn btn-white btn-file">
                                            <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Browse file</span>
                                            <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                            <input type="file" class="default" name="photo"/>
                                            </span>
                                            <span class="fileupload-preview" style="margin-left:5px;"></span>
                                            <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                        </div>
                                    </div>
                                </div>

                                <input type="hidden" name="cid" value="{{$client->cc_id}}"/>
                                <button type="submit" style="float:left; clear:both;" class="btn btn-info">Update</button>
                            
                            </div>

                        </div>
                    </section>

            </div>

</div>          {{ Form::close() }}




       

        <!-- page end-->
        </section>

<script src="{{ $theme }}js/custom/skill.js"></script>
  
@stop