@extends('templates.bucket.bucket')

@section('wrapper')  
<section class="wrapper">
        <!-- page start-->
<h3>{{{$msg or ''}}} </h3>
     {{ Form::open(array('url'=>'page/specialists','class'=>'wpcf7-form contact_form', 'method'=>'post')) }}
    <div class="row">
    <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            WE'RE SPECIALISTS
                            
                        </header>
                        <div class="panel-body">
                            <div class="position-center" style="width:70%;">
                                
                                <div class="form-group">
                                    <label class="col-lg-3 col-sm-2 control-label" for="eskill_name">Content title</label><br />

                                        <input type="hidden" value="{{{$page->page_id}}}" name="page_id" id="title"  />
                                        <input type="text" value="{{{$page->title}}}" name="title" placeholder="Enter title" id="title" class="form-control">

                                </div>

                                <div class="form-group">
                                     <label class="col-lg-3 col-sm-2 control-label">Page description</label><br />
                                     <textarea class="form-control" name="content_description" id="trumbowyg" >{{{$page->content or ''}}}</textarea>
                                </div>



                                <div class="form-group col-lg-offset-3">
                                    <button type="submit" class="btn btn-info">Submit</button>
                                </div>
                            
                            </div>

                        </div>
                    </section>

            </div>

</div>          {{ Form::close() }}


        <!-- page end-->
        </section>

<script src="{{ $theme }}js/custom/skill.js"></script>
<script type="text/javascript">
	$("#trumbowyg").trumbowyg();
</script>
@stop