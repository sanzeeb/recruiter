@extends('templates.bucket.bucket')

@section('wrapper')  
<section class="wrapper">
        <!-- page start-->
<h3>{{{$msg or ''}}} </h3>
     {{ Form::open(array('url'=>'page/why-djit','class'=>'wpcf7-form contact_form','enctype'=>'multipart/form-data', 'method'=>'post')) }}
    <div class="row">
    <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            Why DJIT
                        </header>
                        <div class="panel-body">
                            <div class="position-center" style="width:70%;" >
                                
                                <div class="form-group">
                                    <label class="col-lg-3 col-sm-2 control-label" for="eskill_name">Content title</label><br />

                                        <input type="hidden" value="{{{$page->page_id}}}" name="page_id" id="title"  />
                                        <input type="text" value="{{{$page->title}}}" name="title" placeholder="Enter title" id="title" class="form-control">

                                </div>

                                <div class="form-group">
                                     <label class="col-lg-3 col-sm-2 control-label">Page description</label><br />
                                     <textarea class="form-control" name="content_description" id="trumbowyg" >{{{$page->content or ''}}}</textarea>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Photo</label>
                                    <div class="controls col-md-5">
                                        <div class="fileupload-new thumbnail" style="width:110px; float:left;">
                                            @if(!empty($page->photo_name) && File::exists('public/themes/recruit/image/'.$page->photo_name))
                                            <img alt="{{{$page->photo_name or ''}}}" src="{{{url('/')}}}/public/themes/recruit/image/{{{$page->photo_name}}}"/>
                                            @else
                                            <img alt="thumbnail" src="{{$theme}}images/placeholder.gif"/>
                                            @endif
                                        </div>
                                        <div class="fileupload fileupload-new" style="float:right;" data-provides="fileupload">
                                            <span class="btn btn-white btn-file">
                                            <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Browse file</span>
                                            <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                            <input type="file" class="default" name="photo_name"/>
                                            </span>
                                            <span class="fileupload-preview" style="margin-left:5px;"></span>
                                            <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                        </div>
                                    </div>
                                </div>
                                <br class="clear"/>
                                <div class="form-group col-lg-offset-3">
                                    <button type="submit" style="float:left; clear:both;" class="btn btn-info">Submit</button>
                                </div>
                            
                            </div>

                        </div>
                    </section>

            </div>

</div>          {{ Form::close() }}


        <!-- page end-->
        </section>

<script src="{{ $theme }}js/custom/skill.js"></script>
    <script type="text/javascript">
	$("#trumbowyg").trumbowyg();
</script>
@stop