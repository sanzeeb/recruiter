@extends('templates.bucket.bucket')

@section('wrapper')  
<section class="wrapper">
        <!-- page start-->
<h3>{{{$msg or ''}}} </h3>
     {{ Form::open(array('url'=>'page/client-say','class'=>'wpcf7-form contact_form','enctype'=>'multipart/form-data', 'method'=>'post')) }}   
    <div class="row">
    <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            Client says form
                            
                        </header>
                        <div class="panel-body">
                            <div class="position-center" style="width:70%;">
                                

                                <div class="form-group">
                                                <label class="col-lg-3 col-sm-2 control-label" for="eskill_name">Name of client</label><br />
                                                 
                                                    <input type="text"  name="title" placeholder="Enter title" id="title" class="form-control">
                                                
                                            </div>
                                <div class="form-group">
                                     <label class="col-lg-3 col-sm-2 control-label">Client comment</label><br />
                                     <textarea class="form-control" name="content_description" id="trumbowyg" >

                                     </textarea> 
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3">Select Photo</label>
                                    <div class="controls col-md-5">
                                        <div class="fileupload-new thumbnail" style="width:110px; float:left;">
                                         
                                    <img alt="thumbnail" src="{{$theme}}images/placeholder.gif"/>
                                    
                                        </div>
                                        <div class="fileupload fileupload-new" style="float:right;" data-provides="fileupload">
                                            <span class="btn btn-white btn-file">
                                            <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Browse file</span>
                                            <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                            <input type="file" class="default" name="photo"/>
                                            </span>
                                            <span class="fileupload-preview" style="margin-left:5px;"></span>
                                            <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                        </div>
                                    </div>
                                </div>
                              
                            
                                <button type="submit" style="float:left; clear:both;" class="btn btn-info">Submit</button>
                            
                            </div>

                        </div>
                    </section>

            </div>

</div>          {{ Form::close() }}



        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        Comment list
                        <span class="tools pull-right">
                            
                            
                         </span>
                    </header>
                    <div class="panel-body">
       
                        <table class="table  table-hover general-table">
                            <thead>
                            <tr>
                                <th>Created at</th>
                                <th> photo</th>                               
                                <th class="hidden-phone">Name</th>
                                <th>Comment</th>

                                

                            </tr>
                            </thead>
                            <tbody>
                                
                                @foreach($comments as $skill)
                                
                            <tr>
                                <td>{{{Helpers::dateTimeFormat("F j, Y",$skill->created_at)}}} </td>

                                <td class="hidden-phone">
                                        @if(!empty($skill->image_name) && file_exists('data/gallery/'.$skill->image_name))
                           
                                    <img alt="{{{$skill->image_name or ''}}}" height='75' width='75' src="{{{url('/')}}}/data/gallery/{{{$skill->image_name}}}"/>
                                    @else
                                    <img alt="thumbnail" height='75' width='75'  src="{{$theme}}images/placeholder.gif"/>
                                    @endif
                                    
                                </td>
                                <td><a href="#">{{{$skill->name}}}</a></td>
                                

                                <td><a href="#">{{{$skill->description}}}</a></td>
                                
                                
                                <td>   
            
                                <a href="{{url('page/edit-client-say/'.$skill->cc_id)}}">Edit</a> |
                                <a href="javascript:void(0)" onclick="deleteClient('{{{$skill->cc_id}}}')">Delete</a> |

                                </td>
                            </tr>
                                    @endforeach

                            </tbody>
                        </table>
                        <div class="row-fluid"><div class="span6"><div id="hidden-table-info_info" class="dataTables_info">{{$comments->links()}}</div></div><div class="span6"><div class="dataTables_paginate paging_bootstrap pagination"></div></div></div>
                    </div>
                </section>
            </div>
        </div>
       

        <!-- page end-->
        </section>

<script src="{{ $theme }}js/custom/skill.js"></script>
      <script type="text/javascript">
	$("#trumbowyg").trumbowyg();
</script>
@stop