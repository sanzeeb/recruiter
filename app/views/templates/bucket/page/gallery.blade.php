@extends('templates.bucket.bucket')

@section('wrapper')  
<section class="wrapper">
    <!-- page start-->
    <h3>{{{$msg or ''}}} </h3>
    {{ Form::open(array('url'=>'page/gallery','class'=>'wpcf7-form contact_form', 'method'=>'post')) }}   
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Create your gallery

                </header>
                <div class="panel-body">
                    <div class="position-center" style="width:70%;">

                        <div class="form-group">
                            <label class="col-lg-3 col-sm-2 control-label" for="eskill_name">Gallery title</label><br />


                            <input type="text"  name="title" placeholder="Enter title" id="title" class="form-control">

                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 col-sm-2 control-label">Description</label><br />
                            <textarea class="form-control" name="content_description" id="trumbowyg" >
                            </textarea> 
                        </div>


                        <button type="submit" class="btn btn-info">Submit</button>

                    </div>

                </div>
            </section>

        </div>

    </div>          {{ Form::close() }}



    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    Gallery list
                    <span class="tools pull-right">


                    </span>
                </header>
                <div class="panel-body">

                    <table class="table  table-hover general-table">
                        <thead>
                            <tr>
                                <th>Created at</th>

                                <th class="hidden-phone">Title</th>
                                <th> Description</th>

                                <th>Actions</th>

                            </tr>
                        </thead>
                        <tbody>

                            @foreach($gallery as $userow)

                            <tr>
                                <td>{{{Helpers::dateTimeFormat("F j, Y",$userow->created_at)}}} </td>


                                <td class="hidden-phone">{{{$userow->title}}}</td>
                                <td>{{{$userow->description}}}</td>


                                <td>
                                    <a  data-toggle="modal" href="#myModal-1" onclick="editGa('{{{$userow->ga_id}}}')">Edit</a> |
                                    <a onclick="deleteGa('{{{$userow->ga_id}}}')" href="javascript:void(0)">Delete</a> 

                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                    <div class="row-fluid"><div class="span6"><div id="hidden-table-info_info" class="dataTables_info">{{$gallery->links()}}</div></div><div class="span6"><div class="dataTables_paginate paging_bootstrap pagination"></div></div></div>
                </div>
            </section>
        </div>
    </div>




    {{ Form::open(array('url'=>'page/images-up','class'=>'wpcf7-form contact_form','enctype'=>'multipart/form-data', 'method'=>'post')) }}   
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Upload gallery images

                </header>
                <div class="panel-body">
                    <div class="position-center">

                        <div class="form-group " >
                            <label class="col-lg-3 col-sm-2 control-label">Select gallery </label>

                            <select name="ga_id" class="schedule_title form-control" required style="width: 240px"  >
                                <option value="">Select gallery</option>
                                @foreach($galleries as $schedule)

                                <option value="{{{$schedule->ga_id}}}">{{{$schedule->title}}}</option>

                                @endforeach

                            </select>

                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Photo</label>
                            <div class="controls col-md-5">
                                <div class="fileupload-new thumbnail" style="width:110px; float:left;">

                                    <img alt="thumbnail" src="{{$theme}}images/placeholder.gif"/>

                                </div>
                                <div class="fileupload fileupload-new" style="float:right;" data-provides="fileupload">
                                    <span class="btn btn-white btn-file">
                                        <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Browse file</span>
                                        <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                        <input type="file" class="default" name="photo"/>
                                    </span>
                                    <span class="fileupload-preview" style="margin-left:5px;"></span>
                                    <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                </div>
                            </div>
                        </div>


                        <button type="submit" style="float:left; clear:both;" class="btn btn-info">Submit</button>

                    </div>

                </div>
            </section>

        </div>

    </div>          {{ Form::close() }}

    <!-- page end-->



    <div class="modal fade in" id="myModal-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Gallery Edit form </h4>
                </div>
                <div class="modal-body">
                    <div class="msg"></div>
                    <form role="form" class="form-horizontal">
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label" for="eskill_name">Gallery title</label>
                            <div class="col-lg-10">
                                <input type="hidden" name="ga_id" id="ga_id"  />
                                <input type="text" name="ga_title" placeholder="Enter title" id="ga_title" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label" for="ga_description">Description</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="enter description" id="ga_description" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-default" onclick="updateGallery()" type="button">Update</button>
                            </div>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>


</section>

<script src="{{ $theme }}js/custom/skill.js"></script>
 <script type="text/javascript">
	$("#trumbowyg").trumbowyg();
</script>
@stop