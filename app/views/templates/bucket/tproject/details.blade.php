@extends('templates.bucket.bucket')

@section('wrapper')
<section class="wrapper">
    <!-- page start-->
    {{Helpers::showMessage()}}
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">Test Project Details
                    @if($user->user_type=='Employer')
                    <span class="tools pull-right">
                        <form action="{{url('tproject/lists')}}">
                            <button class="btn btn-info" style="padding:4px 12px;font-size:13px;margin-top:-4px;" type="submit">Back</button>
                        </form>
                    </span>
                    @endif
                </header>
                <div class="panel-body">
                    @if($user->user_type == 'Admin')
                        <form method="post" id="approveInqueryFrm" action="{{url('dashboard/approve-tproject')}}" class="form-horizontal bucket-form">

                            <div class="form-group">
                                <label class="col-sm-3 control-label"><b>Company Name :</b></label>
                                <div class="col-sm-6">
                                    <p>{{$tproject->Employer->title}}</p>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label"><b>Candidate Name :</b></label>
                                <div class="col-sm-6">
                                    <p><a href="{{url('resume/display-resume/'.$tproject->Candidate->cv_tbl_id)}}">{{$tproject->Candidate->firstname}}</a></p>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label"><b>Title :</b></label>
                                <div class="col-sm-6">
                                    <p>{{$tproject->project_name or 'Title not found'}}</p>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label"><b>Description :</b></label>
                                <div class="col-sm-6">
                                    <p><?php echo str_replace("\n","<br/><br/>",$tproject->project_description); ?></p>
                                </div>
                            </div>

                            @if(!empty($tproject->project_attachment))
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><b>Attachment :</b></label>
                                <div class="col-sm-6">
                                   <a href="{{url('data/tproject/attachment/'.$tproject->project_attachment)}}">{{$tproject->project_attachment}}</a>
                                </div>
                            </div>
                            @endif

                            @if(in_array($tproject->project_status,array('Review','Submitted')))
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><b>Submitted Project :</b></label>
                                <div class="col-sm-6">
                                    <a href="{{JOB_DOMAIN.'data/tproject/project/'.$tproject->project_zip}}">{{$tproject->project_zip}}</a>
                                </div>
                            </div>

                            @endif

                            @if(in_array($user->user_type,array('Admin','Employer')))
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><b>Status :</b></label>
                                <div class="col-sm-6">
                                    {{$tproject->project_status}}
                                </div>
                            </div>
                            @endif


                            <div class="form-group">
                                <div class="col-lg-offset-3 col-lg-6">
                                    <input type="hidden" name="id" value="{{$tproject->id}}"/>
                                    @if($tproject->project_status == TestProject::$Pending)
                                    <button id="approveBtn" type="button" class="btn btn-success">Approve</button>

                                    <a data-toggle="modal" href="#cancelTproject"  type="button" class="btn btn-danger">Cancel</a>
                                    @endif

                                    @if($tproject->project_status == TestProject::$Review)
                                        <button id="approveReviewBtn" type="button" class="btn btn-success">Review Approve</button>

                                        <a data-toggle="modal" href="#cancelReviewTproject"  type="button" class="btn btn-danger">Cancel</a>
                                    @endif
                                </div>
                            </div>
                            @if($tproject->project_status == TestProject::$Pending)
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><b>Reply :</b></label>
                                <div class="col-sm-6">
                                    <textarea id="ccomment" class="form-control " name="reply" required=""></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-3 col-lg-6">
                                    <input type="hidden" name="id" value="{{$tproject->id}}"/>
                                    <button id="replyBtn" class="btn btn-warning" type="button">Reply</button>
                                </div>
                            </div>
                            @endif
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><b>Replies :</b></label>
                                <div class="col-sm-6">
                                    <?php $tprojectReply =  $tproject->TestProjectReply; $pendingComment =''; ?>
                                    @if(count($tprojectReply))
                                    @foreach($tprojectReply as $ir)

                                    <?php
                                    if($user->user_type != "Admin")
                                    {
                                        $userObj = User::where('user_id',$ir->replier_id)->where('user_type',$ir->replier_type)->first();
                                        $replier = '';
                                        if($userObj->user_type == 'Candidate')
                                        {
                                            $candidate = Candidate::find($userObj->user_id);
                                            $replier = $candidate->firstname;
                                        }else{
                                            $emp = Employer::find($userObj->user_id);
                                            $replier = $emp->title;
                                        }
                                    }
                                    ?>
                                    <i class="fa fa-user"></i>&nbsp;{{$replier or ''}} <small><em>{{Helpers::dateTimeFormat('j F, Y',$ir->created_at)}}</em></small>
                                    <p align="justify">{{$ir->reply}}
                                    @if($ir->status == "Canceled")
                                        - {{ $ir->status}} by Admin
                                    @endif
                                    </p>

                                    @if($ir->status == "Pending")
                                        <?php $pendingComment = $ir->id; ?>
                                            <input type="button" data-id="{{$ir->id}}" class="approveComment btn btn-success" value="Approve"/>
                                            <input type="hidden" name="ir_id" value="{{$pendingComment}}"/>
                                            <a data-toggle="modal" href="#cancelReply" class="btn btn-danger" >Cancel</a>
                                    @endif
                                    <span style="height:1px; float:left; width:100%; margin:10px 0;background:#eff2f7;"></span>
                                    @endforeach
                                    @endif

                                </div>
                            </div>


                        </form>
                        <div class="modal fade in" id="cancelTproject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title">Canceling Reason</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="msg"></div>
                                        <input type='hidden' id='dis_agreement_id' name='dis_agreement_id' />
                                        <form role="form" id='reasonFrm' action="{{url('tproject/cancel')}}" class="form-horizontal" method="POST">


                                            <div class="form-group">
                                                <label class="control-label col-md-3">Give a reason</label>
                                                <div class="col-md-9">
                                                    <textarea rows="7" style="resize:none;" class='form-control' id='disdescription' name='comment'></textarea>

                                                </div>
                                                
                                            </div>
                                            <div class="form-group">
                                               

                                                <input type="hidden" name="id" value="<?php echo $tproject->id; ?>"/>
                                                <div class="col-md-3 col-md-offset-3">
                                                    <button  class="btn btn-success add-time sent_disagree" type="submit">Send</button>
                                                </div>
                                            </div>

                                        </form>

                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="modal fade in" id="cancelReply" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title">Canceling Reason</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="msg"></div>
                                        <input type='hidden' id='dis_agreement_id' name='dis_agreement_id' />
                                        <form role="form" id='cancelReplyFrm' action="{{url('tproject/reply-cancel')}}" class="form-horizontal" method="POST">


                                            <div class="form-group">
                                                <label class="control-label col-md-3">Give a reason</label>
                                                <div class="col-md-9">
                                                    <textarea rows="7" style="resize:none;" class='form-control' id='disdescription' name='comment'></textarea>

                                                </div>
                                                
                                            </div>
                                            <div class="form-group">
                                               

                                                <input type="hidden" name="id" value="<?php echo $tproject->id; ?>"/>
                                                <input type="hidden" name="ir_id" value="<?php echo $pendingComment; ?>"/>
                                                <div class="col-md-3 col-md-offset-3">
                                                    <button  class="btn btn-success add-time sent_disagree" type="submit">Send</button>
                                                </div>
                                            </div>

                                        </form>

                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="modal fade in" id="cancelReviewTproject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title">Canceling Project Submittion</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="msg"></div>
                                        <input type='hidden' id='dis_agreement_id' name='dis_agreement_id' />
                                        <form role="form" id='cancelSubmissionFrm' action="{{url('tproject/submittion-cancel')}}" class="form-horizontal" method="POST">


                                            <div class="form-group">
                                                <label class="control-label col-md-3">Give a reason</label>
                                                <div class="col-md-9">
                                                    <textarea rows="7" style="resize:none;" class='form-control' id='disdescription' name='comment'></textarea>

                                                </div>
                                                
                                            </div>
                                            <div class="form-group">
                                               

                                                <input type="hidden" name="id" value="<?php echo $tproject->id; ?>"/>
                                                <input type="hidden" name="ir_id" value="<?php echo $pendingComment; ?>"/>
                                                <div class="col-md-3 col-md-offset-3">
                                                    <button  class="btn btn-success add-time sent_disagree" type="submit">Send</button>
                                                </div>
                                            </div>

                                        </form>

                                    </div>

                                </div>
                            </div>
                        </div>
                    @endif
                    @if($user->user_type == 'Employer')
                    <form method="post" id="approveInqueryFrm" class="form-horizontal bucket-form">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Company Name :</b></label>
                            <div class="col-sm-6">
                                <p>{{$tproject->Employer->title}}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Candidate Name :</b></label>
                            <div class="col-sm-6">
                                <p>{{$tproject->Candidate->firstname}}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Title :</b></label>
                            <div class="col-sm-6">
                                <p>{{$tproject->project_name}}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Description :</b></label>
                            <div class="col-sm-6">
                                <p><?php echo str_replace("\n","<br/><br/>",$tproject->project_description); ?></p>
                            </div>
                        </div>

                        @if(!empty($tproject->project_attachment))
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Attachment :</b></label>
                            <div class="col-sm-6">
                                <a href="{{url('data/tproject/attachment/'.$tproject->project_attachment)}}">{{$tproject->project_attachment}}</a>
                            </div>
                        </div>
                        @endif

                        @if($tproject->project_status == TestProject::$Submitted)
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Submitted Project :</b></label>
                            <div class="col-sm-6">
                                <a href="{{JOB_DOMAIN.'data/tproject/project/'.$tproject->project_zip}}">{{$tproject->project_zip}}</a>
                            </div>
                        </div>

                        @endif
                        @if(0)
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Reply :</b></label>
                            <div class="col-sm-6">
                                <textarea id="ccomment" class="form-control " name="reply" required=""></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-3 col-lg-6">
                                <input type="hidden" name="id" value="{{$tproject->id}}"/>
                                <button id="replyBtn" class="btn btn-warning" type="button">Reply</button>
                            </div>
                        </div>
                        @endif
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Replies :</b></label>
                            <div class="col-sm-6">
                                <?php $tprojectReply =  $tproject->TestProjectReply; ?>
                                @if(count($tprojectReply))
                                @foreach($tprojectReply as $ir)

                                <?php
                                if($ir->status == "Replied")
                                        {

                                    $userObj = User::where('user_id',$ir->replier_id)->where('user_type',$ir->replier_type)->first();
                                    $replier = '';
                                    if($userObj->user_type == 'Candidate')
                                    {
                                        $candidate = Candidate::find($userObj->user_id);
                                        $replier = $candidate->firstname;
                                    }else{
                                        $emp = Employer::find($userObj->user_id);
                                        $replier = $emp->title;
                                    }
                                }
                                ?>
                                    @if($ir->status == "Replied" || $ir->status == "Replied by admin")
                                    <i class="fa fa-user"></i>&nbsp;{{$replier or 'admin'}} <small><em>{{Helpers::dateTimeFormat('j F, Y',$ir->created_at)}}</em></small>
                                    <p align="justify">{{$ir->reply}}</p>
                                    <span style="height:1px; float:left; width:100%; margin:10px 0;background:#eff2f7;"></span>
                                    @endif
                                @endforeach
                                @endif

                            </div>
                        </div>


                    </form>
                    @endif
                    @if($user->user_type == 'Candidate')
                    <form method="post"  action="{{url('tproject/submitted')}}"  enctype="multipart/form-data" class="form-horizontal bucket-form">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Company Name :</b></label>
                            <div class="col-sm-6">
                                <p>{{$tproject->Employer->title}}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Candidate Name :</b></label>
                            <div class="col-sm-6">
                                <p>{{$tproject->Candidate->firstname}}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Title :</b></label>
                            <div class="col-sm-6">
                                <p>{{$tproject->project_name}}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Description :</b></label>
                            <div class="col-sm-6">
                                <p><?php echo str_replace("\n","<br/><br/>",$tproject->project_description); ?></p>
                            </div>
                        </div>
                        @if(!empty($tproject->project_attachment))
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Attachment :</b></label>
                            <div class="col-sm-6">
                                <a href="{{url('data/tproject/attachment/'.$tproject->project_attachment)}}">{{$tproject->project_attachment}}</a>
                            </div>
                        </div>
                        @endif

                        @if($tproject->project_status != TestProject::$Submitted && $tproject->project_status != TestProject::$Review)
                            @if($anyApprovedReply == 0)
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><b>Source Link :</b></label>
                                    <div class="col-sm-6">
                                        <input class="form-control " name="source_link" required=""/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><b>Project :</b></label>
                                    <div class="col-sm-6">
                                        <input type="file" name="project" />
                                        <span>(only .zip file supported)</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <input type="hidden" name="id" value="{{$tproject->id}}"/>
                                        <button class="btn btn-warning" type="submit">Submit</button>
                                    </div>
                                </div>
                            @endif
                        @else
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><b>Submitted Project :</b></label>
                                <div class="col-sm-6">
                                    <a href="{{url('data/tproject/project/'.$tproject->project_zip)}}">{{$tproject->project_zip}}</a>
                                    @if($tproject->project_status == TestProject::$Review)
                                        <p><strong class="text-danger">Under Review</strong></p>
                                    @endif
                                </div>
                            </div>
                        @endif
                    </form>
                    <br/>
                    
                    <form method="post" id="approveInqueryFrm" class="form-horizontal bucket-form">
                        @if($tproject->project_status != TestProject::$Submitted && $tproject->project_status != TestProject::$Review)
                            @if($anyApprovedReply == 0)
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><b>Reply :</b></label>
                                <div class="col-sm-6">
                                    <textarea id="ccomment" class="form-control " name="reply" required=""></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-3 col-lg-6">
                                    <input type="hidden" name="id" value="{{$tproject->id}}"/>
                                    <button id="replyBtn" class="btn btn-warning" type="button">Reply</button>
                                </div>
                            </div>
                            @endif
                        @endif
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Replies :</b></label>
                            <div class="col-sm-6">
                                <?php $tprojectReply =  $tproject->TestProjectReply; ?>
                                @if(count($tprojectReply))
                                @foreach($tprojectReply as $ir)

                                <?php
                                if($ir->status != "Replied by admin" && $ir->status != "Submit Canceled"){
                                $userObj = User::where('user_id',$ir->replier_id)->where('user_type',$ir->replier_type)->first();
                                $replier = '';
                                if($userObj->user_type == 'Candidate')
                                {
                                    $candidate = Candidate::find($userObj->user_id);
                                    $replier = $candidate->firstname;
                                }else{
                                    $emp = Employer::find($userObj->user_id);
                                    $replier = $emp->title;
                                }
                                 }
                                ?>

                                <i class="fa fa-user"></i>&nbsp;{{$replier or ''}} <small><em>{{Helpers::dateTimeFormat('j F, Y',$ir->created_at)}}</em></small>
                                <p align="justify">{{$ir->reply}}</p>
                                <p>{{$ir->reason}}</p>
                                <span style="height:1px; float:left; width:100%; margin:10px 0;background:#eff2f7;"></span>
                                @endforeach
                                @endif

                            </div>
                        </div>
                    </form>

                    @endif
                </div>
            </section>
        </div>
    </div>
</section>
<script type="text/javascript" src="{{$theme}}js/custom/tproject-details.js"></script>
@stop