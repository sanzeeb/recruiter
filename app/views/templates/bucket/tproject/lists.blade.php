@extends('templates.bucket.bucket')

@section('wrapper')
<style type="text/css">
    #searchBtn{float:right; right:170px;}
</style>
<section class="wrapper" ng-app="tproject">
    <!-- page start-->
    {{Helpers::showMessage()}}
    <div class="row" ng-controller="tprojectCtrl">
        <div class="col-sm-12">

            <section class="panel">
                <header class="panel-heading">
                    Test Project list
                         <span class="tools pull-right">

                         </span>
                </header>
                <div class="panel-body cmxform form-horizontal">
                    @if(count($tprojects))
                            <button id="searchBtn" ng-click="search()" class="btn btn-xs btn-info">Search By Date</button>
                            <table ng-table="tableParams" show-filter="true" class="table ng-table-responsive">
                                <tr ng-repeat="tproject in $data">
                                    <td align="center" data-title="'Sl'">@{{($index+1)}}</td>
                                    @if($user->user_type != 'Candidate')
                                        <td align="center" data-title="'Candidate Name'" filter="{'candidate.firstname':'text'}">@{{tproject.candidate.firstname}}</td>
                                    @else
                                        <td align="center" data-title="'Company Name'" filter="{'employer.title':'text'}">@{{tproject.employer.title}}</td>
                                    @endif
                                <td align="center" data-title="'Project Title'" filter="{'project_name':'text'}">@{{tproject.project_name}}</td>
                                    <td align="center" data-title="'Status'"  sortable="'id'" filter="{ 'project_status': 'select' }" filter-data="statuses($column)">
                                        
                                        <span ng-if="tproject.project_status == 'Approved'">Delivered</span>
                                        <span ng-if="tproject.project_status != 'Approved'">@{{tproject.project_status}}</span>

                                    </td>
                                    <td align="center" data-title="'Date'" sortable="'created_at'" filter="{'created_at':'text'}">@{{tproject.created_at}}</td>
                                    <td data-title="'Actions'">
                                        <a href="{{url('tproject/details')}}/@{{tproject.id}}" style="margin-bottom:5px;" class="btn btn-warning  btn-xs pull-left" >View Details</a>
                                        @if($user->user_type == "Employer")
                                        <form ng-if="(tproject.project_status=='Submitted') || (tproject.project_status=='Replied') || (tproject.project_status == 'Canceled') || (tproject.project_status == 'Replied by admin')" target="_blank" method="post" action="{{url('tproject/create')}}/@{{tproject.candidate_id}}" style="float:left; width:140px; margin-right:5px;">
                                             <input type="hidden" name="job_id" value="@{{inquiry.job_id}}"/>
                                            <button type="submit"  style="margin-bottom:5px;" class="btn btn-success btn-xs">New Test Project</button>
                                        </form>
                                        @endif
                                    </td>
                                </tr>
                            </table>
                    @else

                        <div style="width:100%;padding:35px 0; text-align:center;"><img src="{{$theme}}images/no-test.jpg" /></div>
                    @endif
                       
                </div>
            </section>
        </div>
    </div>
</section>
<script type="text/javascript">

$(function(){   
            $("input[name='last_date'],input[name='created_at']").datepicker({
                changeMonth: true,
                dateFormat:"yy-mm-dd",
				yearRange: '-15:+15',
                changeYear: true,
				onSelect: function() {
					$("#searchBtn").addClass("po-show");
				}
            });
        });
   
        var app = angular.module('tproject', ['ngTable']).controller('tprojectCtrl', function($scope, $http, $filter, $q, NgTableParams) {

        var data = {{$tprojects}};
        
        $scope.search = function(){
            var searchDate = angular.element('input[name="created_at"]').val();
  

            $http({
                    url : BASE + 'tproject/get_tproject_by_date',
                    method: "POST",
                    data: {'date':searchDate}
                }).success(function(response,status){
                    data = response;
                	angular.element('#searchBtn').removeClass('po-show');
                    $scope.tableParams.reload();
                });
            
            
        }

        $scope.tableParams = new NgTableParams({
            page: 1,            // show first page
            count: 10           // count per page
        }, {
            total: data.length, // length of data
            getData: function($defer, params) {
                // use build-in angular filter

                var orderedData = params.sorting() ?
                        $filter('orderBy')(data, params.orderBy()) :
                        data;
                orderedData = params.filter() ?
                        $filter('filter')(orderedData, params.filter()) :
                        orderedData;

                params.total(orderedData.length); // set total for recalc pagination
                $defer.resolve($scope.users = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        });

        var inArray = Array.prototype.indexOf ?
                function (val, arr) {
                    return arr.indexOf(val)
                } :
                function (val, arr) {
                    var i = arr.length;
                    while (i--) {
                        if (arr[i] === val) return i;
                    }
                    return -1
                };
            $scope.statuses = function(column) {
                var def = $q.defer(),
                    arr = [],
                    statuses = [];
                angular.forEach(data, function(item){
                  
                   /*var status = (item.status == 'Approved') ? 'Delivered' : item.status; */
                    if (inArray(item.project_status, arr) === -1) {
                        arr.push(item.project_status);
                        statuses.push({
                         'id': item.project_status,
                         'title':  ((item.project_status == 'Approved') ? 'Delivered' : item.project_status)
                        });
                    }
                });
                def.resolve(statuses);
                return def;
            };
            
        });
</script>
@stop