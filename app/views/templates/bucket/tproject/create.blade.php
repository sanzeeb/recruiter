@extends('templates.bucket.bucket')

@section('wrapper')
<section class="wrapper">
    <!-- page start-->
    {{Helpers::showMessage()}}
    <div class="row">
        <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">Test Project
                        <span class="tools pull-right"></span>
                    </header>
                    <div class="panel-body cmxform form-horizontal">

                        <div class=" form">
                            <form id="tprojectCreateFrm" action="{{url('tproject/send-request')}}" method="post" class="cmxform form-horizontal " enctype="multipart/form-data" novalidate="novalidate">
                                <div class="form-group ">
                                    <label class="control-label col-lg-3" for="cname">Company Name : <span class="required">*</span></label>
                                    <div class="col-lg-6">
                                        <input type="hidden" name="eid" value="{{$employer->emp_id}}"/>

                                        <input type="text" required="" minlength="2" name="ename" class=" form-control" readonly value="{{$employer->title}}">
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-3" for="cemail">Candidate Name : <span class="required">*</span></label>
                                    <div class="col-lg-6">
                                        <input type="hidden" name="cid" value="{{$candidate->candidate_id}}"/>
                                        <input type="text" required name="cname"  class="form-control" readonly value="{{$candidate->firstname}}" >
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-3" for="curl">Title : <span class="required">*</span></label>
                                    <div class="col-lg-6">
                                         @if(count($jobs))
                                        <select name="project_title" class="form-control" required>
                                           
                                                @foreach($jobs as $job)
                                                    
                                                    @if(isset($jobID) && ($job['job_id'] == $jobId))
                                                        <option value="{{$job['title']}}" selected="selected">{{$job['title']}}</option>
                                                    @else
                                                        <option value="{{$job['title']}}">{{$job['title']}}</option>
                                                    @endif
                                                @endforeach
                                            
                                        </select>
                                        @else
                                           No job created yet <a href="{{url('job/create')}}"><strong>Create Job</strong></a>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-3" for="ccomment">Description : <span class="required">*</span></label>
                                    <div class="col-lg-6">
                                        <textarea required name="project_description"  class="form-control "></textarea>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-3" for="ccomment">File input :</label>
                                    <div class="col-lg-6">
                                        <input id="exampleInputFile" type="file" name="attachment">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <input type="submit" class="btn btn-success" value="Send Request"/>

                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </section>
            </div>
        </div>
    </section>
<script type="text/javascript">
    $(function(){
        $("#tprojectCreateFrm").submit(function(){
            var project_title = $("select[name='project_title']").val()
            var project_description = $("textarea[name='project_description']").val()
            

            if(project_title.trim() == null)
            {
                alert("Please select job");
                return false;

            }
            if(project_description.trim() == "")
            {
                alert("Please type descriptions");
                return false;
            }   

            var phoneWeburlEmail = /((^[0][1-9]|\d+){6,14}|http:\/\/.+\.([a-z]{3,}|[a-z]{3,}\.[a-z]{2,3})|[a-z]+\.[a-z]{3,}|[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-z]{2,})/

            if(phoneWeburlEmail.test(project_description))
            {
                $("textarea[name='project_description']").attr('placeholder', 'Sorry! You cannot write any phone no, email or web url').val('');
                return false;
            }  
            
            
            
            
        });
    });
</script>
@stop