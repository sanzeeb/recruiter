@extends('templates.bucket.bucket')

@section('wrapper')  
<section class="wrapper">
        <!-- page start-->
<h3>{{{$msg or ''}}} </h3>
     {{ Form::open(array('url'=>'skills/create-skill','class'=>'wpcf7-form contact_form','enctype'=>'multipart/form-data', 'method'=>'post')) }}   
    <div class="row">
    <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            Skills create form
                            
                        </header>
                        <div class="panel-body">
                            <div class="position-center">

                                <div class="form-group">
                                     <label for="skill_name">Skill name</label>
                                       <input type="text" value="" name="skill_name" id="skill_name" data-val-required="The User name field is required." data-val="true" class="form-control">
                                </div>
                                <div class="form-group">
                                     <label for="skill_description">Skill description</label>
                                     <textarea class="form-control" name="skill_description" ></textarea> 
                                </div>
                              <div class="form-group">
                                     <label for="display_order">Display order</label>
                                       <input type="text" value="" name="display_order" id="display_order" data-val-required="The User name field is required." data-val="true" class="form-control">
                                </div>
                          
                                    <div class="form-group">
                                    <label class="control-label col-md-3">Photo</label>
                                    <div class="controls col-md-5">
                                        <div class="fileupload-new thumbnail" style="width:110px; float:left;">
                                            <img alt="" src="{{$theme}}images/placeholder.gif"/>

                                        </div>
                                        <div class="fileupload fileupload-new" style="float:right;" data-provides="fileupload">
                                            <span class="btn btn-white btn-file">
                                            <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Browse file</span>
                                            <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                            <input type="file" class="default" name="skill_photo"/>
                                            </span>
                                            <span class="fileupload-preview" style="margin-left:5px;"></span>
                                            <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                        </div>
                                    </div>
                                </div>
                                    
                                <button type="submit" style="float:left; clear:both;" class="btn btn-info">Submit</button>
                            
                            </div>

                        </div>
                    </section>

            </div>

</div>          {{ Form::close() }}


        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        Skill list
                        <span class="tools pull-right">
                            
                            
                         </span>
                    </header>
                    <div class="panel-body">
       
                        <table class="table  table-hover general-table">
                            <thead>
                            <tr>
                                <th>Created at</th>
                               
                                <th class="hidden-phone">Name</th>
                                <th> Description</th>
                                <th> Display order</th>
                                <th> Display</th>
                                

                            </tr>
                            </thead>
                            <tbody>
                                
                                @foreach($skills as $skill)
                                
                            <tr>
                                <td>{{{Helpers::dateTimeFormat("F j, Y",$skill->created_at)}}} </td>

                                <td><a href="#">{{{$skill->skill_name}}}</a></td>
                                

                                <td><a href="#">{{{$skill->skill_description}}}</a></td>
                                <td class="hidden-phone">{{{$skill->display_order}}}</td>
                                <td class="hidden-phone">
                                    @if($skill->status)
                                        <input type="checkbox" data-skill-id="{{{$skill->skill_id}}}" name="status" checked="checked" value="0"/>
                                    @else
                                        <input type="checkbox" data-skill-id="{{{$skill->skill_id}}}" name="status" value="1"/>
                                    @endif
                                </td>

                                <td>   
            <a  data-toggle="modal" href="#myModal-1" onclick="editSkill('{{{$skill->skill_id}}}')" >Edit</a> |
                                    &nbsp; &nbsp;
            <a href="javascript:void(0)" onclick="deleteSkill('{{{$skill->skill_id}}}')">Delete</a> |
                                </td>
                            </tr>
                                    @endforeach

                            </tbody>
                        </table>
                        <div class="row-fluid"><div class="span6"><div id="hidden-table-info_info" class="dataTables_info">{{$skills->links()}}</div></div><div class="span6"><div class="dataTables_paginate paging_bootstrap pagination"></div></div></div>
                    </div>
                </section>
            </div>
        </div>
       
        <!-- page end-->
        </section>

  <div class="modal fade in" id="myModal-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title">Skill Edit form </h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="msg"></div>
                                        <form role="form" class="form-horizontal">
                                            <div class="form-group">
                                                <label class="col-lg-2 col-sm-2 control-label" for="eskill_name">Skill name</label>
                                                <div class="col-lg-10">
                                                    <input type="hidden" name="skill_id" id="skill_id"  />
                                                    <input type="text" name="eskill_name" placeholder="Enter skill" id="eskill_name" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-2 col-sm-2 control-label" for="eskill_description">Description</label>
                                                <div class="col-lg-10">
                                                    <input type="text" placeholder="enter description" id="eskill_description" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                  <label class="col-lg-2 col-sm-2 control-label" for="eorder">Display order</label>
                                              
                                                <div class="col-lg-10">
                                                    <input type="text" name="eorder" placeholder="Enter order" id="eorder" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-offset-2 col-lg-10">
                                                    <button class="btn btn-default" onclick="updateSkill()" type="button">Update</button>
                                                </div>
                                            </div>
                                        </form>

                                    </div>

                                </div>
                            </div>
                        </div>

<script src="{{ $theme }}js/custom/skill.js"></script>
  
@stop