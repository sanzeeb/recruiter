@extends('templates.bucket.bucket')

@section('wrapper')  
<section class="wrapper">
    <!-- page start-->
    <h3>{{{$msg or ''}}} </h3>
    {{ Form::open(array('url'=>'employer/edit-employer','class'=>'wpcf7-form contact_form','enctype'=>'multipart/form-data' ,'method'=>'post')) }}   
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">Employer profile</header>
                <div class="panel-body">
                    <div class="position-center" style="width:100%;">
                    <div class="col-lg-4" style="padding-left:0;">
                            <div class="form-group">
                                    <div class="controls col-md-12" style="padding-left:0;">
                                        <div class="fileupload-new thumbnail popo-imgr">
                                            @if(!empty($users->Employer->photo_name) && file_exists('data/employer/'.$users->Employer->photo_name))
                                            <img alt="{{{$users->Employer->photo_name or ''}}}" src="{{{url('/')}}}/data/employer/{{{$users->Employer->photo_name}}}"/>
                                            @else
                                            <img alt="thumbnail" src="{{$theme}}images/placeholder.gif"/>
                                            @endif
                                        </div>
                                               <div class="fileupload fileupload-new" data-provides="fileupload">
                                                    <span class="btn btn-sm btn-white btn-file">
                                                        <span class="fileupload-new"><i class="fa fa-paper-clip"></i>Edit My Profile Photo</span>
                                                        <span class="fileupload-exists"><i class="fa fa-undo"></i>Change</span>
                                                        <input type="file" class="default" name="emp_photo"/>
                                                    </span>
                                                    
                                         <!--<span class="btn btn-sm btn-danger pull-right"><a href="{{ url('logout') }}"><i class="fa fa-key"></i> Log Out</a></span>-->
                                                    
                                                    <span class="fileupload-preview" style="margin-left:5px;"></span>
                                                    <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                                </div> 
                                    </div>
                                </div>
                            
                       </div>
                    <div class="col-lg-6">
                        <input type="hidden" name="id" value="{{{$users->id}}}" />
                        <input type="hidden" name="user_id" value="{{{$users->user_id}}}" />
                        <input type="hidden" name="username" value="{{{$users->username}}}" />
                        <div class="form-group">
                            <label for="title">Title <span class="required">*</span></label>
                            <input type="text" value="{{{$users->Employer->title}}}" name="title" id="title" data-val-required="The User name field is required." data-val="true" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="address">Address <span class="required">*</span></label>
                        <textarea name="address" class="form-control">{{{$users->Employer->address}}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="Phone">Phone <span class="required">*</span></label>
                            <input type="text" value="{{{$users->Employer->phone}}}" name="phone" id="phone" data-val-required="The User name field is required." data-val="true" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="email">Email <span class="required">*</span></label>
                            <input type="email" value="{{{$users->Employer->email}}}" name="email" id="Email" data-val-required="Email field is required." data-val="true" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="website">Website</label>
                            <input type="text" value="{{{$users->Employer->website}}}" name="website" id="website" data-val-required="Email field is required." data-val="true" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="Contact">Contact person</label>
                            <input type="text" value="{{{$users->Employer->contact_person}}}" name="contact_person" id="contact_person" data-val-required="The User name field is required." data-val="true" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="Contact">Contact person phone</label>
                            <input type="text" value="{{{$users->Employer->contact_person_phone}}}" name="contact_person_phone" id="contact_person_phone" data-val-required="The User name field is required." data-val="true" class="form-control">
                        </div>


                        <!--<div class="form-group">
                            <label class="control-label col-md-3">Change Photo</label>
                            <div class="controls col-md-2">
                                
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <span class="btn btn-white btn-file">
                                        <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Browse file</span>
                                        <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                        <input type="file" class="default" name="emp_photo"/>
                                    </span>
                                    <span class="fileupload-preview" style="margin-left:5px;"></span>
                                    <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                </div>
                            </div>
                        </div>-->
                        <div class="form-group">
                            <div class="col-lg-12" style="padding:10px 0 20px 0;">
                                <button type="submit" class="btn btn-info">Update</button>
                                <a href="{{url('dashboard')}}" class="btn btn-danger">Cancel</a>
                            </div>

                        </div>

                    </div><!-- position-center -->

                </div>
            </section>

        </div>

    </div>{{ Form::close() }}
    <!-- page end-->
</section>
@stop