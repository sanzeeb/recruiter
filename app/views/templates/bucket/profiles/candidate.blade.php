@extends('templates.bucket.bucket')

@section('wrapper')

<section class="wrapper">
    <!-- page start-->
    {{Helpers::showMessage()}}
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <div class="msg"></div>
                <header class="panel-heading">
                    Candidate  profile info
                </header>
                <div class="panel-body">
                    {{Form::open(array('id'=>'resumeEditFrm','url'=>'resume/update-resume','class'=>'cmxform form-horizontal','enctype'=>'multipart/form-data','method'=>'post'))}}
                    <div id="wizard">

                        <h4>Personal Detail</h4>

                        <section>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Name <span class="text-danger">*</span></label>
                                <div class="col-lg-5">
                                    <input type="text" name="name" class="form-control" title="Name" value="{{{$cvInfo->name or ''}}}" required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Career Summary</label>
                                <div class="col-lg-5">
                                    <textarea type="text" name="csummary" class="form-control" title="Carrier Summary">{{{$candidate->csummary or ''}}}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-3">Total Year of Experience <span class="text-danger">*</span></label>
                                <div class="col-lg-2">
                                    <input type="text" name="year_of_exp" title="Total Work Experience" class="form-control" value="{{{$cvInfo->year_of_exp or '' }}}" required/>
                                    <input type="hidden" name="cv_id" value="{{{$cvInfo->cv_tbl_id or '' }}}"/>
                                </div>
                            </div>
                            <hr/>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Father's Name <span class="text-danger">*</span></label>
                                <div class="col-lg-5">
                                    <input type="text" name="father_name" class="form-control" title="Father's Name" value="{{{$candidate->father_name or '' }}}" required/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Mother's Name <span class="text-danger">*</span></label>
                                <div class="col-lg-5">
                                    <input type="text" name="mother_name" class="form-control" title="Mother's Name" value="{{{$candidate->mother_name or '' }}}" required/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Date of Birth <span class="text-danger">*</span></label>
                                <div class="col-lg-3">
                                    <input type="text" name="dob" class="form-control" title="Date of Birth" value="{{{$candidate->dob or '' }}}" required/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Gender <span class="text-danger">*</span></label>
                                <div class="col-lg-8">
                                    <select name="gender" class="form-control-select2" required>
                                        @foreach($genders as $g)
                                        @if($candidate->gender == $g)
                                        <option selected="selected" value="{{$g}}">{{$g}}</option>
                                        @else
                                        <option value="{{$g}}">{{$g}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Marital Status </label>
                                <div class="col-lg-8">
                                    <select name="marital_status" class="form-control-select2">
                                        @foreach($marital_status as $mt)
                                        @if($candidate->marital_status == $mt)
                                        <option selected="selected" value="{{$mt}}">{{$mt}}</option>
                                        @else
                                        <option value="{{$mt}}">{{$mt}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>



                            <div class="form-group">
                                <label class="col-lg-3 control-label">Nationality</label>
                                <div class="col-lg-3">
                                    <input type="text" name="nationality" class="form-control" value="{{{$candidate->nationality or ''}}}"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Religion</label>
                                <div class="col-lg-3">
                                    <input type="text" name="religion" class="form-control" value="{{{$candidate->religion or ''}}}"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Present Address</label>
                                <div class="col-lg-8">
                                    <input type="text" name="present_address" class="form-control" value="{{{$candidate->current_address or ''}}}"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Permanent Address</label>
                                <div class="col-lg-8">
                                    <input type="text" name="permanent_address" class="form-control" value="{{{$candidate->parmanent_address or ''}}}"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Home Phone</label>
                                <div class="col-lg-3">
                                    <input type="text" name="home_phone" class="form-control" value="{{{$candidate->phone or ''}}}"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Mobile <span class="text-danger">*</span></label>
                                <div class="col-lg-3">
                                    <input type="text" name="mobile" class="form-control" title="Mobile number" value="{{{$candidate->mobile or ''}}}" required/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Email</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control" name="email" value="{{{$candidate->email or ''}}}"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Alternate Email</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control" name="alternate_email" value="{{{$candidate->alternative_email or ''}}}" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Photo</label>
                                <div class="controls col-md-2">
                                    <div class="fileupload-new thumbnail">
                                        @if(!empty($candidate->photo))
                                        <img alt="{{{$candidate->photo or ''}}}" src="{{JOB_DOMAIN.'/data/profile/'.$candidate->photo}}"/>
                                        @else
                                        <img alt="thumbnail" src="{{$theme}}images/placeholder.gif"/>
                                        @endif
                                    </div>
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <span class="btn btn-white btn-file">
                                            <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Browse file</span>
                                            <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                            <input type="file" class="default" name="photo"/>
                                        </span>
                                        <span class="fileupload-preview" style="margin-left:5px;"></span>
                                        <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                    </div>
                                </div>
                            </div>
                        </section>

                        
                        <section>
                            <div class="form-group col-lg-12">
                               <h3 style="float:left;">Education Info</h3>
                        <a style="float:right;" class="btn btn-primary newAddBtn" id="addEduParam" href="#"><i class="fa fa-plus-circle"> Add More</i></a> 
                                <table width="100%" id="eduParamTbl">
                                    <thead>
                                        <tr>
                                            <th>Exam Title</th>
                                            <th>Major</th>
                                            <th>Institution</th>
                                            <th>Result</th>
                                            <th>Passing Year</th>
                                            <th><i class="fa fa-trash-o"></i></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($cvEdu))
                                        @foreach($cvEdu as $cE)
                                        <tr>
                                            <td><input type="text" name="title[{{{$cE->cv_edu_id}}}]" class="form-control col-lg-2" value="{{{$cE->cv_edu_title or ''}}}"/></td>
                                            <td><input type="text" name="major[{{{$cE->cv_edu_id}}}]" class="form-control col-lg-2" value="{{{$cE->major or ''}}}"/></td>
                                            <td><input type="text" name="institute[{{{$cE->cv_edu_id}}}]" class="form-control col-lg-2" value="{{{$cE->institution or ''}}}"/></td>
                                            <td><input type="text" name="result[{{{$cE->cv_edu_id}}}]" class="form-control col-lg-2" value="{{{$cE->result or ''}}}"/></td>
                                            <td><input type="text" name="passing_year[{{{$cE->cv_edu_id}}}]" class="form-control col-lg-2" value="{{{$cE->passing_year or ''}}}"/></td>
                                            <td><a class="delParam" data-type='e' href="{{{$cE->cv_edu_id}}}"><i class="fa fa-minus-circle"></i></a></td>
                                        </tr>
                                        @endforeach
                                        @else
                                        <tr>
                                            <td><input type="text" name="title[]" class="form-control col-lg-2"/></td>
                                            <td><input type="text" name="major[]" class="form-control col-lg-2" /></td>
                                            <td><input type="text" name="institute[]" class="form-control col-lg-2" /></td>
                                            <td><input type="text" name="result[]" class="form-control col-lg-2" /></td>
                                            <td><input type="text" name="passing_year[]" class="form-control col-lg-2" /></td>
                                            <td><a class="delParam" data-type='e' href=""><i class="fa fa-minus-circle"></i></a></td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>

                        </section>
<hr style="float:left; width:100%;" />
                        
                        <section>
                            <div class="form-group col-lg-12">
<h3 style="float:left;">Work Experience</h3>
                                <a style="float:right;" class="btn btn-primary newAddBtn" id="addWorkParam" href="#"><i class="fa fa-plus-circle"> Add More</i></a>
                                <table width="100%" id="workParamTbl">
                                    <thead>
                                        <tr>
                                            <th>Job Title</th>
                                            <th>Start Date</th>
                                            <th>End Date <a href="javascript:;" title="If present leave this field blank"><i class="fa fa-question-circle" ></i></a></th>
                                            <th>Company Name</th>
                                            <th>Designation</th>
                                            <th>Department</th>
                                            <th><i class="fa fa-trash-o"></i></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($cvJob))
                                        @foreach($cvJob as $cJ)
                                        <tr>
                                            <td><input type="text" name="job_title[{{{$cJ->cv_jobs_id}}}]" class="form-control col-lg-2" value="{{{$cJ->title or ''}}}"/></td>
                                            <td><input type="text" name="start_dt[{{{$cJ->cv_jobs_id}}}]" class="form-control col-lg-2 startDate" value="{{{$cJ->start or ''}}}"/></td>
                                            <td><input type="text" name="end_dt[{{{$cJ->cv_jobs_id}}}]" class="form-control col-lg-2 endDate" value="{{{$cJ->end or ''}}}"/></td>
                                            <td><input type="text" name="company[{{{$cJ->cv_jobs_id}}}]" class="form-control col-lg-2" value="{{{$cJ->company_name or ''}}}"/></td>
                                            <td><input type="text" name="designation[{{{$cJ->cv_jobs_id}}}]" class="form-control col-lg-2" value="{{{$cJ->designation or ''}}}"/></td>
                                            <td><input type="text" name="department[{{{$cJ->cv_jobs_id}}}]"class="form-control col-lg-2" value="{{{$cJ->department or ''}}}"/></td>
                                            <td><a class="delParam" data-type='j' href="{{{$cJ->cv_jobs_id}}}"><i class="fa fa-minus-circle"></i></a></td>
                                        </tr>
                                        @endforeach
                                        @else
                                        <tr>
                                            <td><input type="text" name="job_title[]" class="form-control col-lg-2" /></td>
                                            <td><input type="text" name="start_dt[]" class="form-control col-lg-2 startDate" /></td>
                                            <td><input type="text" name="end_dt[]" class="form-control col-lg-2 endDate" /></td>
                                            <td><input type="text" name="company[]" class="form-control col-lg-2" /></td>
                                            <td><input type="text" name="designation[]" class="form-control col-lg-2" /></td>
                                            <td><input type="text" name="department[]"class="form-control col-lg-2" /></td>
                                            <td><a class="delParam" data-type='j' href=""><i class="fa fa-minus-circle"></i></a></td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>

                        </section>
<hr style="float:left; width:100%;" />
                        
                        <section>

                            <div class="form-group col-lg-12">
                            <h3 style="float:left;">Professional Qualification</h3>
                                <a style="float:right;" class="btn btn-primary newAddBtn" id="addPqParam" href="#"><i class="fa fa-plus-circle"> Add More</i></a>
                                <table width="100%" id="pqParamTbl">
                                    <thead>
                                        <tr>
                                            <th>Qualification Title</th>
                                            <th>Institution</th>
                                            <th>Finished Year</th>
                                            <th><i class="fa fa-trash-o"></i></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($cvQ))
                                        @foreach($cvQ as $cq)
                                        <tr>
                                            <td><input type="text" name="q_title[{{{$cq->cv_qualification_id}}}]" class="form-control col-lg-2" value="{{{$cq->cv_q_title or ''}}}"/></td>
                                            <td><input type="text" name="q_institute[{{{$cq->cv_qualification_id}}}]" class="form-control col-lg-2" value="{{{$cq->institution or ''}}}"/></td>
                                            <td><input type="text" name="q_year[{{{$cq->cv_qualification_id}}}]" class="form-control col-lg-2" value="{{{$cq->finished_year or ''}}}"/></td>
                                            <td><a class="delParam" data-type='q' href="{{{$cq->cv_qualification_id}}}"><i class="fa fa-minus-circle"></i></a></td>
                                        </tr>
                                        @endforeach
                                        @else
                                        <tr>
                                            <td><input type="text" name="q_title[]" class="form-control col-lg-2" /></td>
                                            <td><input type="text" name="q_institute[]" class="form-control col-lg-2" /></td>
                                            <td><input type="text" name="q_year[]" class="form-control col-lg-2" /></td>
                                            <td><a class="delParam" data-type='q' href=""><i class="fa fa-minus-circle"></i></a></td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            
                            <hr style="float:left; width:100%;" />
                            <h3>Specialization</h3>

                            <table width="100%" id="specTbl">
                                <thead>
                                    <tr>
                                        <th>Technologies</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($specs as $spec)
                                    <tr style="width:25%; float:left;">
                                        <td><input class="specs" type="checkbox" @if(!empty($cvSpecs) && !empty($cvSpecs[str_replace(" ","_",$spec)])) checked="checked" @endif data-key="{{{str_replace(' ','_',$spec)}}}" name="specs[{{str_replace(' ','_',$spec)}}]"/> {{{$spec}}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <br/><br/>
                            <input type="hidden" name="candidate_id" value="{{{$candidate->candidate_id}}}"/>
                            <input type="hidden" name="user_type" value="{{{$user->user_type}}}"/>
                            <input type="submit" class="btn btn-info pull-right" value="Save Resume"/>

                        </section>
                    </div>
                    {{Form::close()}}
                </div>
            </section>
            <section class="panel">
                <div class="panel-body">
                    <h3>Others</h3>
                    {{Form::open(array('id'=>'resumeEditFrm','url'=>'resume/update-others','enctype'=>'multipart/form-data','class'=>'cmxform form-horizontal','method'=>'post'))}}
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Source Code Link</label>
                        <div class="col-lg-5">
                            <input type="text" class="form-control" name="source_code" placeholder="http://demoproject.com" value="{{{$candidate->source_code or ''}}}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Academic Certificate</label>
                        <div class="controls col-md-2">
                            @if(!empty($candidate->academic_certificate) && File::exists('data/academic_certificate/'.$candidate->academic_certificate))
                                <a href="{{url('/')}}/data/academic_certificate/{{$candidate->academic_certificate}}" target="_blank">{{$candidate->academic_certificate}}</a>
                            @endif
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <span class="btn btn-white btn-file">
                                            <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Browse file</span>
                                            <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                            <input type="file" class="default" name="photo_name"/>
                                            </span>
                                <span class="fileupload-preview" style="margin-left:5px;"></span>
                                <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <input type="hidden" name="candidate_id" value="{{{$candidate->candidate_id}}}"/>
                            <input type="submit" class="btn btn-primary pull-right" value="Save Links"/>
                        </div>
                    </div>
                    {{Form::close()}}
                </div>
            </section>
        </div>
    </div>
</section>
<script src="{{$theme}}assets/jquery-steps-master/build/jquery.steps.js"></script>
<script src="{{$theme}}js/custom/Validate.js"></script>
<script src="{{$theme}}js/custom/cvmanagement.js"></script>
<script type="text/javascript">
    $(function() {
        $(".startDate,.endDate").datepicker();
    })
</script>
<script type="text/javascript">
$(function(){   
            $("input[name='dob'],input[name='last_date'],input[name='created_at']").datepicker({
                changeMonth: true,
                dateFormat:"yy-mm-dd",
                changeYear: true
            });
        });
</script>
@stop