@extends('templates.bucket.bucket')

@section('wrapper')  
<section class="wrapper">
    <!-- page start-->
    <h3>{{{$msg or ''}}} </h3>
    
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Employer profile
                    <span class="tools pull-right">
                        <form method="GET" action="{{url('profile/edit/'.$users->id)}}" accept-charset="UTF-8">  
                            <button class="btn btn-sm btn-info panel-btn" style="margin-top:-5px;" type="submit">
                                <i class="fa fa-pencil"></i> Edit
                            </button>
                        </form>
                    
                    </span>
                </header>
                <div class="panel-body">
                    <div class="position-center" style="width:100%;">
                    
                    <div class="col-lg-4" style="padding-left:0;">
                    <div class="form-group">                            
                            <div class="controls col-md-12" style="padding-left:0;">
                                        <div class="fileupload-new thumbnail popo-imgr">
                                    @if(!empty($users->Employer->photo_name) && file_exists('data/employer/'.$users->Employer->photo_name))
                                    <img alt="{{{$users->Employer->photo_name or ''}}}" src="{{{url('/')}}}/data/employer/{{{$users->Employer->photo_name}}}"/>
                                    @else
                                    <img alt="thumbnail" src="{{$theme}}images/placeholder.gif"/>
                                    @endif
                                </div>
                                <!-- <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <span class="btn btn-white btn-file">
                                        <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Browse file</span>
                                        <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                        <input type="file" class="default" name="emp_photo"/>
                                    </span>
                                    <span class="fileupload-preview" style="margin-left:5px;"></span>
                                    <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-8" style="padding:0;">
                        <input type="hidden" name="id" value="{{{$users->id}}}" />
                        <input type="hidden" name="user_id" value="{{{$users->user_id}}}" />
                        <input type="hidden" name="username" value="{{{$users->username}}}" />
                        <div class="form-group po-pro-box">
                            <label for="title">Title : </label>
                            <span>{{{$users->Employer->title}}}</span>
                        </div>
                        <div class="form-group po-pro-box">
                            <label for="address">Address : </label>
                        <span>{{{$users->Employer->address}}}</span>
                        </div>
                        <div class="form-group po-pro-box">
                            <label for="Phone">Phone : </label>
                            <span>{{{$users->Employer->phone}}}</span>
                        </div>
                        <div class="form-group po-pro-box">
                            <label for="email">Email : </label>
                            <span>{{{$users->Employer->email}}}</span>
                        </div>
                        <div class="form-group po-pro-box">
                            <label for="website">Website : </label>
                            <span>{{{$users->Employer->website}}}</span>
                        </div>

                        <div class="form-group po-pro-box">
                            <label for="Contact">Contact person : </label>
                            <span>{{{$users->Employer->contact_person}}}</span>
                        </div>
                        <div class="form-group po-pro-box">
                            <label for="Contact">Contact person phone : </label>
                            <span>{{{$users->Employer->contact_person_phone}}}</span>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-lg-12" style="padding:10px 0 0 0;">
                               <!--  <button type="submit" class="btn btn-info">Update</button> -->
                                <a href="{{url('dashboard')}}" class="btn btn-danger">Back</a>
                            </div>
                        </div>
                        
</div>
                        
                       <!-- <div class="form-group">
                            <div class="col-lg-12">
                               <button type="submit" class="btn btn-info">Update</button>
                                <a href="{{url('dashboard')}}" class="btn btn-danger">Back</a>
                            </div>
                        </div>-->

                    </div>

                </div>
            </section>

        </div>

    </div>         

    <!-- page end-->
</section>
@stop