@extends('templates.bucket.bucket')

@section('wrapper')		




<section class=" wrapper">
    {{{Helpers::showMessage()}}}
    <div class="row">
        <h1 class="title">Find Jobs</h1>
    </div>
    
    <div class="row_x">
        
        
        @foreach($skills as $skill)
        <div class="col-lg-4">
            <section class="panel_x" style="border:1px solid #e6e6e6;border-top:solid 3px #BCD74A;">
                <div class="panel-body_x" style="text-align:left;">
                    <?php $skill_name=base64_encode($skill->skill_name); ?>
                    <a style="float:left; margin-right:8px; padding:2px; border:solid 1px #CCC;" href="{{{url('candidate/jobs')}}}/{{{$skill_name}}}">
                        @if(!empty($skill->skill_photo) && file_exists('data/skill/'.$skill->skill_photo))
                        <img height='60' alt="thumbnail" alt="{{{$skill->skill_photo or ''}}}" src="{{{url('/')}}}/data/skill/{{{$skill->skill_photo}}}"/>
                        @else
                        <img height='60' alt="thumbnail" alt="thumbnail" src="{{$theme}}images/placeholder.gif"/>
                        @endif
                    </a>
                    <p align="justify" style="font-size:13px; line-height:normal;">
                        {{$skill->skill_description}}
                    </p>

                    <div class="a_clearfix"></div>
                </div>
            </section>
        </div>
    
        @endforeach
        
        
    </div>


</section>
            



@stop