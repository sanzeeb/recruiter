@extends('templates.bucket.bucket')

@section('wrapper')
<section class="wrapper">
    <!-- page start-->
    {{Helpers::showMessage()}}
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    Inbox
                         <span class="tools pull-right"></span>
                </header>
                <div class="panel-body cmxform form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-lg-3">Subject :</label>
                        <div class="col-lg-6">
                            <input type="text" readonly class="form-control" value="{{{$inbox->subject}}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Message :</label>
                        <div class="col-lg-6">
                            <div>{{$inbox->body}}</div>
                        </div>
                    </div>
                    <?php $agrAr=  explode(":", $inbox->in_type);
                    
                   if(count($agrAr)==2&&$user->user_type=="Candidate"){
                    ?>
                    @if($agrAr[0]=="agreement")
                    <div class="form-group">
                        <label class="control-label col-lg-3">&nbsp;</label>
                        <div class="col-lg-6">
                            <div><a class="btn btn-info" href="{{{url('agreement/agreementDtl/')}}}/{{$agrAr[1]}}"> Go to agreement details</a></div>
                        </div>
                    </div>
                    @endif
                   <?php } ?>
                </div>
            </section>
        </div>
    </div>
</section>
@stop