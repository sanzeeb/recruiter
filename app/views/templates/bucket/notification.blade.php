@extends('templates.bucket.bucket')

@section('wrapper')  
<section class="wrapper">
        <!-- page start-->
    <h3>{{{$msg or ''}}} </h3>
     
    
    <div class="row">
        <div class="col-lg-12">
                        <section class="panel">

                            <header class="panel-heading">
                                Notifications
                            </header>

                            <div class="panel-body">

                                <div class="col-lg-12">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Subject</th>
                                                <th>Sender</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(count($notifications))
                                                @foreach($notifications as $notification)
                                                    <tr>
                                                        @if($notification->read_status)
                                                            <td>{{$notification->subject}}</td>
                                                        @else
                                                            <td><strong>{{$notification->subject}}</strong></td>
                                                        @endif
                                                        <?php 
                                                            $username = '';
                                                            if($notification->User['user_type'] == 'Employer')
                                                            {
                                                               $employer = Employer::find($notification->User['user_id']); 
                                                               $username = $employer->title;

                                                            }else if($notification->User['user_type'] == 'Candidate'){

                                                                $candidate = Candidate::find($notification->User['user_id']); 
                                                                $username  = $candidate->firstname;

                                                            }else{

                                                                $username = $notification->User['username'];
                                                            } 
                                                        ?>
                                                        <td>{{$username or ''}}</td>
                                                        <td><a class="btn btn-sm btn-send" href="{{url('inbox/index/'.$notification->inbox_id)}}">View Details</a></td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>

                            </div>

                        </section>
        </div>
    </div>

</section>
@stop