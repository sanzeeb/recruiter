@extends('templates.bucket.bucket')

@section('wrapper')  
<section class="wrapper">
        <!-- page start-->

    {{Helpers::showMessage()}}
    <div class="row">
        <div class="col-lg-12">

                <section class="panel">
                    <header class="panel-heading">
                        Payment till now
                    </header>
                    <div class="panel-body">
                        <table class="table  table-hover general-table">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Amount</th>
                                    <th>Date</th>
                                     <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($payments))
                                    @foreach($payments as $payment)
                                        @if(count($payment->AgreementAcc))
                                        <tr>
                                                                                        <td>{{{$payment->AgreementAcc->agreement_title}}}</td>
                                            <td>{{{$payment->amount}}}</td>
                                            
                                            <td>{{{Helpers::dateTimeFormat('j F, Y',$payment->created_at)}}}</td>
                                            
                                            
                                            <td>{{{$payment->status}}}</td>
                                            <td>
                                                @if($user->user_type=="Admin")
                                                @if($payment->status == 'Pending')
                                                <a href="#" data-id="{{{$payment->payment_id}}}" class="btn btn-success btn-xs paid" >Paid</a>|
                                                
                                                <a href="#" data-id="{{{$payment->payment_id}}}" class="btn btn-danger btn-xs cancel" >Cancel</a>
                                                @else
                                                
                                                
                                                <a href="{{url('agreement/details/'.$payment->agreement_id)}}" class="btn btn-xs btn-info">View Agreement</a>
                                                @endif
                                            @endif
                                            </td>
                                        </tr>
                                        @endif
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
     <div class="modal fade in" id="myModal-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Payment cancel reason form </h4>
                </div>
                <div class="modal-body">
                    <div class="msg"></div>
                    <input type='hidden' id='payment_id' name='payment_id' />
                    <form role="form" id='agreement-form' class="form-horizontal">


                        <div class="form-group">
                            <label class="control-label col-md-3">Description</label>
                            <div class="col-md-5">
                                <textarea rows="7" class='form-control' id='disdescription' name='disdescription'></textarea>

                            </div>

                            <button  class="btn btn-success add-time cancel-confirm" type="button">Disagree confirm</button>
                        </div>


                    </form>

                </div>

            </div>
        </div>
    </div>

</section>
            <script type="text/javascript" src="{{$theme}}js/custom/agreement.js"></script>
@stop