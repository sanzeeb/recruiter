@extends('templates.bucket.bucket')

@section('wrapper')  
<section class="wrapper">
        <!-- page start-->

    {{Helpers::showMessage()}}
    <div class="row">
        <div class="col-lg-12">
                <section class="panel">
                        <header class="panel-heading">
                           Pay Now

                        </header>
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <div class="col-lg-6">
                                     <table class="table  table-hover general-table">

                                         <tr>
                                             <td>Title</td>
                                             <td>{{{$agreement->agreement_title}}}</td>
                                         </tr>
                                         <tr>
                                             <td>Description</td>
                                             <td>{{{$agreement->description}}}</td>
                                         </tr>
                                         <tr>
                                             <td>Candidate</td>
                                             <td>{{{$agreement->ReceiverUserAcc->Candidate->firstname}}}</td>
                                         </tr>
                                         <tr>
                                             <td>Amount</td>
                                             <td>{{{$agreement->amount}}}</td>
                                         </tr>
                                     </table>
                                </div>

                                <div class="col-lg-3">
                                    {{Form::Open(array('url'=>'payment/approve','method'=>'post','class'=>'cmxform form-horizontal'))}}
                                    <div class="form-group">
                                        <label class="control-label col-lg-6">Amount To Pay:</label>
                                        <div class="col-lg-6">
                                            <input type="number" name="amountToPay" value="{{($agreement->amount * 0.2)}}" class="form-control" required/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-6 col-lg-offset-6">
                                            <input type="hidden" value="{{{$agreement->agreement_id}}}" name="agreement_id"/>
                                            <input type="submit" title="Checkout with PayPal" class="btn btn-warning" value="Pay"/>
                                        </div>
                                    </div>
                                    {{Form::Close()}}
                                </div>
                            </div>
                        </div>
                </section>

                <section class="panel">
                    <header class="panel-heading">
                        Payment till now
                    </header>
                    <div class="panel-body">
                        <table class="table  table-hover general-table">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Amount</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($payments))
                                    @foreach($payments as $payment)
                                        <tr>
                                            <td>{{{$agreement->title}}}</td>
                                            <td>{{{$payment->amount}}}</td>
                                            <td>{{{Helpers::dateTimeFormat('j F, Y',$payment->created_at)}}}</td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
    </div>
</section>
@stop