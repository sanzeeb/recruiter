@extends('templates.bucket.bucket')

@section('wrapper')
<section class="wrapper">
    <!-- page start-->


    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Payment Success

                </header>
                <div class="panel-body">

                    <div class="col-lg-6">
                        <h3>Your payment successfully debited to escrow</h3>
                        <table class="table  table-hover general-table">

                            <tr>
                                <td>Title</td>
                                <td>{{{$agreement->title}}}</td>
                            </tr>
                            <tr>
                                <td>Description</td>
                                <td>{{{$agreement->description}}}</td>
                            </tr>
                            <tr>
                                <td>Amount Paid</td>
                                <td>{{{$amount}}}</td>
                            </tr>

                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
</section>
@stop