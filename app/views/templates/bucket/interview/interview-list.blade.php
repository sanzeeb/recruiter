@extends('templates.bucket.bucket')

@section('wrapper')

<section class="wrapper">
    <!-- page start-->
{{Helpers::showMessage()}}
    <div id="msg">
    </div>
    <div class="row">
        <div class="col-md-12">

            <section class="panel">
                <header class="panel-heading">
                    Search interviewer 
                    <span class="tools pull-right">
                        <a class="fa fa-chevron-down" href="javascript:;"></a>
                        <a class="fa fa-cog" href="javascript:;"></a>
                        <a class="fa fa-times" href="javascript:;"></a>
                    </span>
                </header>
                <div class="panel-body">
                    <h3>{{{$msg or ''}}} </h3>
                    {{ Form::open(array('id'=>'searchCandidateFrm','class'=>'form-horizontal','method'=>'post')) }}

                    <div class="form-group">
                        <label class="control-label col-lg-3">Year of experience</label>
                        <div class="col-lg-1">
                            <input type="number" class="form-control spinner" name="fromYear" value="2"/>
                        </div>
                        <div class="col-sm-1 text-center" style="width:20px;">
                            ---
                        </div>
                        <div class="col-lg-1">
                            <input type="number" class="form-control spinner" name="toYear" value="4"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Match with my schedule</label>
                        <div class="col-lg-3">
                            <input type="checkbox" name="checkMySchedule" value="1"/>
                        </div>
                    </div>

                    <div class="form-group checkSchedule hidden" >
                        <label class="col-lg-3 col-sm-2 control-label">Select Schedule </label>

                        <div class="col-lg-5">
                            <select name="schedule_title" class="schedule_title form-control" required style="width: 240px" disabled="disabled" id="source">
                                <option value="">Select schedule</option>
                                @foreach($titles as $schedule)

                                <option value="{{{$schedule->avail_id}}}">{{{$schedule->title}}}</option>

                                @endforeach

                            </select>
                        </div>
                    </div>

                    <div class="form-group checkSchedule hidden">
                        <label class="control-label col-md-3">Select your available date</label>

                        <div class="col-md-2 col-xs-11">
                            <input class="form-control form-control-inline input-medium default-date-picker"
                                   name="available_date" id="available_date" size="16" type="text" required disabled="disabled" value=""/>
                            <span class="help-block">Select date</span>
                        </div>
                    </div>


                    <div class="form-group checkSchedule hidden">
                        <label class="control-label col-md-3">Select your from time</label>

                        <div class="col-md-2">
                            <div class="input-group bootstrap-timepicker">
                                <input readonly="readonly" id="from_time" name="availtime" type="text"
                                       class="form-control timepicker-default">


                            </div>

                        </div>
                    </div>
                    <div class="form-group checkSchedule hidden">
                        <label class="control-label col-md-3">Select your to time</label>

                        <div class="col-md-2">
                            <div class="input-group bootstrap-timepicker">
                                <input readonly="readonly" id="to_time" name="availtimeto" type="text"
                                       class="form-control timepicker-default">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-3 col-lg-offset-3">
                            <button class="btn btn-success add-time" type="submit">Search</button>
                        </div>
                    </div>

                    {{Form::close() }}
                </div>
            </section>

        </div>


    </div>


    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    Available time
                    <span class="tools pull-right">

                    </span>
                </header>
                <div class="panel-body">
                    {{Form::open(array('id'=>'saveRecruiterRequestFrm','class'=>'cmxform form-horizontal','method'=>'post'))}}
                        @if($user->user_type == 'Admin')
                        <div class="form-group">
                            <label class="control-label col-lg-3">Select Recruiter</label>
                            <div class="col-lg-3">
                                <select class="form-control-select2" name="recruiter" style="width:250px;">
                                    <option value=""></option>
                                    @if(count($recruiters))
                                        @foreach($recruiters as $recruiter)
                                            <option value="{{{$recruiter->emp_id}}}">{{{$recruiter->title}}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        @elseif($user->user_type == 'Employer')
                        <div class="form-group">
                            <label class="control-label col-lg-3 hidden">Select Recruiter</label>
                            <div class="col-lg-3">
                                <select class="form-control-select2 hidden" name="recruiter"  style="width:250px;">
                                    <option value="{{$recruiter->emp_id}}">{{$recruiter->title}}</option>

                                </select>
                            </div>
                        </div>
                        @endif
                        <div class="form-group">
                            <label class="control-label col-lg-3">Job list</label>
                            
                            <div class="col-lg-3">
                                <select name="joblist" class="form-control-select2" style="width:250px;">
                                    <option value=""></option>
                                    @if(count($jobs))
                                        @foreach($jobs as $job)
                                            <option value="{{{$job->jobs_id}}}">{{{$job->job_title}}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            
                            
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-3">Purpose (optional)</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="purpose"/>
                            </div>
                        </div>
                        <div class="form-group">
                            
                            <div class="col-lg-3 col-lg-offset-3">
                                <button class="btn btn-info panel-btn" type="submit" name="approve">Approve</button>
                            </div>
                        </div>
                    <table id='candidateTable' class="table  table-hover general-table">
                        <thead>
                        <tr>

                            <th> Actions</th>
                            <th>Created at</th>

                            <th class="hidden-phone">Title</th>
                            <th class="hidden-phone">Year exp</th>
                            
                            <th> Available date</th>
                            <th> Time</th>


                        </tr>
                        </thead>
                        <tbody>

                        

                        </tbody>
                    </table>
                    <div class="row-fluid">
                        <div class="span6">
                            <div id="hidden-table-info_info" class="dataTables_info"></div>
                        </div>
                        <div class="span6">
                            <div class="dataTables_paginate paging_bootstrap pagination"></div>
                        </div>
                    </div>
                    {{Form::close()}}
                </div>
            </section>
        </div>
    </div>

    <!-- page end-->
</section>


<!--<div class="modal fade in" id="myModal-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Schedule Edit form </h4>
            </div>
            <div class="modal-body">
                <div class="msg"></div>
                <form role="form" class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-md-3">Title</label>

                        <div class="col-md-4 col-xs-11">
                            <input type="hidden" name="etitle_id" id="etitle_id"/>
                            <input type="text" name="etitle" placeholder="Enter title" id="etitle" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Select Available Date</label>

                        <div class="col-md-4 col-xs-11">
                            <input class="form-control form-control-inline input-medium default-date-picker"
                                   name="eavailable_date" id="eavailable_date" size="16" type="text" value=""/>
                            <span class="help-block">Select date</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Select from time</label>

                        <div class="col-md-5">
                            <div class="input-group bootstrap-timepicker">
                                <input id="eavailtime" name="eavailtime" type="text"
                                       class="form-control timepicker-default">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" style="margin-right: 13px; " type="button"><i
                                            class="fa fa-clock-o"></i></button>



                                </span>


                            </div>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Select to time</label>

                        <div class="col-md-5">
                            <div class="input-group bootstrap-timepicker">
                                <input id="eavailtimeto" name="eavailtimeto" type="text"
                                       class="form-control timepicker-default">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" style="margin-right: 13px; " type="button"><i
                                            class="fa fa-clock-o"></i></button>



                                </span>


                            </div>


                        </div>

                        <button onclick="updateSchedule()" class="btn btn-success add-time" type="button">Update
                        </button>
                    </div>


                </form>

            </div>

        </div>
    </div>
</div>-->

<script src="{{ $theme }}js/custom/schedule.js"></script>
<script src="{{ $theme }}js/custom/CustomMessage.js"></script>
<script src="{{ $theme }}js/custom/candidate.js"></script>
@stop