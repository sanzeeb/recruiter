	<div class="col-lg-12">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Replies</h4>
      </div>
      <div class="modal-body" style="background:#fff;">
        @if(count($replies))
    		@foreach($replies as $reply)
    			 @if($user->user_type != "Admin")
	    			 	@if($reply->status == "Approved")
	                    <?php $userObj = User::where('user_id',$reply->replier_id)->where('user_type',$reply->replier_type)->first();
	                    $replier = '';
	                    
	                        if($userObj->user_type == 'Candidate')
	                        {
	                           $candidate = Candidate::find($userObj->user_id);
	                            $replier = $candidate->firstname;
	                        }else{
	                            $emp = Employer::find($userObj->user_id);
	                            $replier = $emp->title;
	                        } 
	                        ?>
	                    @endif
                        @if($reply->status != "Pending")
                        <i class="fa fa-user"></i>&nbsp;{{$replier or 'Admin'}} <small><em>{{Helpers::dateTimeFormat('j F, Y',$reply->created_at)}}</em></small>
                        
                        <p align="justify">{{$reply->reply}}</p>
                        
                        @else
                          Reply waiting for review
                        @endif
                        @if($reply->status == "Canceled")
                            {{$reply->inquery_reason}} - by admin
                        @endif
                    @endif
                    <br class="clearfix"/>
    		@endforeach
    	@endif
      </div>
      <div class="modal-footer" style="background:#fff;margin-top:0px;">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        
      </div>
    
    </div>	
	