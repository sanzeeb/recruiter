@extends('templates.bucket.bucket')

@section('wrapper')
<style type="text/css">
#searchBtn{float:right; top:120px; right:145px;}
</style>
<section class="wrapper" ng-app="Interview">
    <!-- page start-->
    {{Helpers::showMessage()}}
    <div class="row" ng-controller="InterviewCtrl">
        <div class="col-sm-12">

            <section class="panel">
                <header class="panel-heading">
                    Interview List
                         <span class="tools pull-right"></span>
                </header>
                <div class="panel-body cmxform form-horizontal">
                    
                        @if(count($interviews))
                            <button id="searchBtn" ng-click="search()" class="btn btn-xs btn-info">Search By Date</button>
                            <table ng-table="tableParams" show-filter="true" class="table ng-table-responsive">
                                <tr ng-repeat="interview in $data | filter:searchInterview" >
                                   

                                    <td align="center" data-title="'Sl'">
                                        @{{($index+1)}}
                                    </td>

                                    
                                    <td align="center" data-title="'Candidate'" sortable="'candidate.firstname'" filter="{'candidate.firstname':'text'}">
                                        <a target="_blank" href="{{url('resume/display-resume')}}/@{{$interview.Candidate.cv_tbl_id}}">@{{interview.candidate.firstname}}</a>
                                    </td>
                                    <td align="center" data-title="'Employer'" sortable="employer.title" filter="{'employer.title':'text'}">@{{interview.employer.title}}</td>                                    
                                    <td align="center" data-title="'Job Title'" sortable="job.job_title" filter="{'job.job_title':'text'}">@{{interview.job.job_title}}</td>
                                    <td align="center" data-title="'Status'"  sortable="'id'" filter="{ 'status': 'select' }" filter-data="statuses($column)">
                                         @if($user->user_type == "Candidate")
                                          <span ng-if="interview.status == 'Approved'">Recevied</span>
                                          <span ng-if="interview.status != 'Approved'">@{{interview.status}}</span>
                                          @else
                                           <span ng-if="interview.status == 'Approved'">Delivered</span>
                                          <span ng-if="interview.status != 'Approved'">@{{interview.status}}</span>
                                          @endif
                                    </td>
                                    <td align="center" data-title="'Send date'" sortable="'created_at'" filter="{'created_at':'text'}">@{{interview.created_at}}</td>

                                    <td data-title="'Actions'" width="211">
                                        <div ng-if="(interview.status == 'Declined') || (interview.status == 'Replied') || (interview.status == 'Replied by admin') || (interview.status == 'Canceled')">

                                            @if($user->user_type == "Employer")
                                                <a data-toggle="modal" href="{{url('interview/view-replies')}}/@{{interview.interview_id}}" data-target="#modal" style="margin-right:5px; margin-bottom:5px;" class="btn btn-warning btn-xs pull-left">Interview Details</a>
                                            @endif

                                            @if($user->user_type == 'Admin' || $user->user_type == 'Candidate')
                                                <a target="_blank" href="{{url('interview/details')}}/@{{interview.interview_id}}" style="margin-right:5px; margin-bottom:5px;" class="btn btn-warning btn-xs pull-left" >Interview Details</a>
                                            @endif

                                            @if($user->user_type == "Employer")

                                                <form  method="post" action="{{url('interview/create')}}/@{{interview.candidate_id}}" style="float:left; width:auto;">
                                                            <input type="hidden" name="job_id" value="@{{interview.job_id}}"/>
                                                            <button class="btn btn-info btn-xs pull-left" type="submit">Re-interview</button>
                                                </form>

                                            @endif

                                        </div>

                                        <div ng-if="(interview.status != 'Declined') && (interview.status != 'Replied') && (interview.status != 'Replied by admin') && (interview.status != 'Canceled')">
                                            <a target="_blank" href="{{url('interview/details')}}/@{{interview.interview_id}}" style="margin-right:5px; margin-bottom:5px;" class="btn btn-warning btn-xs pull-left" >Interview Details</a>

                                            @if($user->user_type == 'Employer')
                                                <a ng-if="interview.status == 'Completed'" target="_blank" href="{{url('interview/offer')}}/@{{interview.interview_id}}" class="btn btn-success btn-xs pull-left" >Offer Now</a>
                                            @endif

                                        </div>
                                        
                                    </td>


                                </tr> 
                            </table>
                        
                        @else
                        <div style="width:100%;padding:35px 0; text-align:center;"><img src="{{$theme}}images/no-interview.jpg" /></div>
                        @endif
                </div>
            </section>

        </div>
    </div>
</section>
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    </div>
  </div>
</div>

<script type="text/javascript">

$(function(){   
            $("input[name='last_date'],input[name='created_at']").datepicker({
                changeMonth: true,
                dateFormat:"yy-mm-dd",
				yearRange: '-15:+15',
                changeYear: true,
                onSelect: function() {
                    $("#searchBtn").addClass("po-show");
                }
            });
        });
   
        var app = angular.module('Interview', ['ngTable']).controller('InterviewCtrl', function($scope, $http, $filter, $q, NgTableParams) {

         var data = {{$interviews}};
         
         $scope.search = function(){
            
            var searchDate = angular.element('input[name="created_at"]').val();

           

                $http({
                        url : BASE + 'interview/get_interview_by_date',
                        method: "POST",
                        data: {'date':searchDate}
                    }).success(function(response,status){
                        data = response;
                        angular.element('#searchBtn').removeClass('po-show');
                        $scope.tableParams.reload();
                    });
            
        
            
        }


        $scope.tableParams = new NgTableParams({
            page: 1,            // show first page
            count: 10           // count per page
        }, {
            total: data.length, // length of data
            getData: function($defer, params) {
                // use build-in angular filter
                var orderedData = params.sorting() ?
                        $filter('orderBy')(data, params.orderBy()) :
                        data;
                orderedData = params.filter() ?
                        $filter('filter')(orderedData, params.filter()) :
                        orderedData;

                params.total(orderedData.length); // set total for recalc pagination
                $defer.resolve($scope.users = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        });

        var inArray = Array.prototype.indexOf ?
                function (val, arr) {
                    return arr.indexOf(val)
                } :
                function (val, arr) {
                    var i = arr.length;
                    while (i--) {
                        if (arr[i] === val) return i;
                    }
                    return -1
                };
            $scope.statuses = function(column) {
                var def = $q.defer(),
                    arr = [],
                    statuses = [];
                angular.forEach(data, function(item){
                  
                   /*var status = (item.status == 'Approved') ? 'Delivered' : item.status; */
                   <?php if($user->user_type == "Candidate"){ ?> 
                        if (inArray(item.status, arr) === -1) {
                            arr.push(item.status);
                            statuses.push({
                             'id': item.status,
                             'title':  ((item.status == 'Approved') ? 'Recevied' : item.status)
                            });
                        }
                    <?php }else{  ?> 
                        if (inArray(item.status, arr) === -1) {
                            arr.push(item.status);
                            statuses.push({
                             'id': item.status,
                             'title':  ((item.status == 'Approved') ? 'Delivered' : item.status)
                            });
                        }
                    <?php } ?>
                });
                def.resolve(statuses);
                return def;
            };
        });
</script>     
@stop