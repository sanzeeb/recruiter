@extends('templates.bucket.bucket')

@section('wrapper')
<section class="wrapper">
    <!-- page start-->
    {{Helpers::showMessage()}}
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">Interview Details
                    @if($user->user_type=='Employer')
                    <span class="tools pull-right">
                        <form action="{{url('interview/lists')}}">
                            <button class="btn btn-info" style="padding:4px 12px;font-size:13px;margin-top:4px;" type="submit">Back</button>
                        </form>
                    </span>
                    @endif
                </header>
                <div class="panel-body">
                    @if($user->user_type == 'Admin')
                    <form method="post" id="approveInterviewFrm" class="form-horizontal bucket-form">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Company Name :</b></label>
                            <div class="col-sm-6">
                                <p>{{$interview->Employer->title}}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Candidate Name :</b></label>
                            <div class="col-sm-6">
                                <p><a href="{{url('resume/display-resume/'.$interview->Candidate->cv_tbl_id)}}">{{$interview->Candidate->firstname}}</a></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Title :</b></label>
                            <div class="col-sm-6">
                                <p>{{$interview->Job->job_title}}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Description :</b></label>
                            <div class="col-sm-6">
                                <p>{{$interview->topics}}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Type :</b></label>
                            <div class="col-sm-6">
                                <p>{{$interview->type}}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Salary :</b></label>
                            <div class="col-sm-6">
                                <p>{{$interview->salary}}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Interview Date :</b></label>
                            <div class="col-sm-6">
                                 @if($interview->status == Interview::$Pending)
                                    <input class="form-control" type="text" name="interview_date" value="{{$interview->interview_date}}" required/>
                                 @else
                                    <p>{{$interview->interview_date}}</p>
                                 @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Interview Time :</b></label>
                            <div class="col-sm-6">
                                @if($interview->status == Interview::$Pending)
                                    <input class="form-control" type="text" name="interview_time" value="{{$interview->interview_time}}" required/>
                                 @else
                                    <p>{{$interview->interview_time}}</p>
                                 @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Interview Duration :</b></label>
                            <div class="col-sm-6">
                                @if($interview->status == Interview::$Pending)
                                    <input class="form-control" type="text" name="interview_duration" value="{{$interview->interview_duration}}" required/>
                                @else
                                    <p>{{$interview->interview_duration}}</p>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Message :</b></label>
                            <div class="col-sm-6">
                                 {{str_replace("\n","<br/>",$interview->message)}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Status :</b></label>
                            <div class="col-sm-6">
                                {{$interview->status}}
                            </div>
                        </div>
                        @if($interview->status == Interview::$Pending)
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Replay :</b></label>
                            <div class="col-sm-6">
                                <textarea id="comment" class="form-control " name="reply" required=""></textarea>
                            </div>
                            <div class="col-lg-6 col-lg-offset-3" style="margin-top:4px;">
                                 <button id="replyBtn" class="btn btn-warning" type="button">Replay</button>
                            </div>
                        </div>
                        @endif
                        <div class="form-group">
                            <div class="col-lg-offset-3 col-lg-6">
                                <input type="hidden" name="id" value="{{$interview->interview_id}}"/>
                                @if($interview->status == Interview::$Pending)
                                <button id="approveBtn" type="button" class="btn btn-success">Approve</button>
                                @endif
                                @if($interview->status == "Accepted Pending")
                                    <button id="approveAcceptBtn" type="button" class="btn btn-success">Approve</button>
                                @endif
                                @if(in_array($interview->status,array("Accepted","Replied")))
                                    <button id="completedBtn" type="button" class="btn btn-success">Completed</button>
                                @endif  
                            
                                @if($interview->status == Interview::$Pending)
                                <button data-toggle="modal" href="#myModalCancelInterview" class="btn btn-danger">Cancel</button>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                           
                            <div class="col-sm-6">
                                <?php $interviewReplies =  $interview->InterviewReplies; $replySchedule = '';  $pendingComment =''?>
                                @if(count($interviewReplies))
                                @foreach($interviewReplies as $ir)

                                <?php
                                if($user->user_type != "Admin"){
                                    $userObj = User::where('user_id',$ir->replier_id)->where('user_type',$ir->replier_type)->first();
                                    $replier = '';
                                    if($userObj->user_type == 'Candidate')
                                    {
                                        $candidate = Candidate::find($userObj->user_id);
                                        $replier = $candidate->firstname;
                                    }else{
                                        $emp = Employer::find($userObj->user_id);
                                        $replier = $emp->title;
                                    }
                                }
                                ?>
                                <i class="fa fa-user"></i>&nbsp;{{$replier or 'Admin'}} <small><em>{{Helpers::dateTimeFormat('j F, Y',$ir->created_at)}}</em></small>
                                <p align="justify"><?php 
                                    if(preg_match('/^{/',$ir->reply))
                                        $reply = json_decode($ir->reply);
                                    else
                                        $reply = $ir->reply;
                                    $replySchedule = $reply;
                                ?>
                                @if(is_object($reply))
                                    {{'Available Date: '.$reply->date. '<br/> Avaiable time: '.$reply->time}}
                                @else
                                    {{$reply}}
                                @endif
                                </p>
                                @if($ir->status == "Canceled")
                                    {{$ir->inquery_reason}} - by admin
                                @endif
                                @if($ir->status == "Pending")
                                    <?php $pendingComment = $ir->id; ?>
                                        @if($interview->status != 'Declined')
                                        <a data-toggle="modal" href="#myModalApproveTimeInterview" class="btn btn-success" >Approve</a>
                                        @else

                                        <a  id="approveReply" href="{{$ir->id}}" class="btn btn-success" >Approve</a>
                                        @endif
                                        <a data-toggle="modal" href="#cancelReplyFrm" class="btn btn-danger" >Cancel</a>
                                @endif
                                <span style="height:1px; float:left; width:100%; margin:10px 0;background:#eff2f7;"></span>
                                @endforeach
                                @endif

                            </div>
                        </div>
                    </form>
                    <div class="modal fade in" id="myModalApproveTimeInterview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title">Approve Schedule</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="msg"></div>
                                        <input type='hidden' id='dis_agreement_id' name='dis_agreement_id' />
                                        <form role="form" id='reasonFrm' action="{{url('interview/accept-interview-schedule')}}" class="form-horizontal" method="POST">
                                            <?php 

                                                if(is_object($replySchedule))
                                                    $reply = $replySchedule;
                                                else
                                                    $reply = $replySchedule;
                                            ?>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Date</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" value="{{$reply->date or ''}}" name="approve_date"/>

                                                </div>
                                                
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Time</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" value="{{$reply->time or ''}}" name="approve_time"/>
                                                </div>
                                                
                                            </div>
                                            <div class="form-group">
                                                <input type="hidden" name="ir_id" value="<?php echo $pendingComment; ?>"/>
                                                <div class="col-md-3 col-md-offset-3">
                                                    <button  class="btn btn-success add-time sent_disagree" type="submit">Approve</button>
                                                </div>
                                            </div>

                                        </form>

                                    </div>

                                </div>
                            </div>
                        </div>
                    <div class="modal fade in" id="myModalCancelInterview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title">Canceling Reason</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="msg"></div>
                                        <input type='hidden' id='dis_agreement_id' name='dis_agreement_id' />
                                        <form role="form" id='reasonFrm' action="{{url('dashboard/cancel-interview')}}" class="form-horizontal" method="POST">


                                            <div class="form-group">
                                                <label class="control-label col-md-3">Give a reason</label>
                                                <div class="col-md-9">
                                                    <textarea rows="7" style="resize:none;" class='form-control' id='disdescription' name='comment'></textarea>

                                                </div>
                                                
                                            </div>
                                            <div class="form-group">
                                               

                                                <input type="hidden" name="interview_id" value="<?php echo $interview->interview_id; ?>"/>
                                                <div class="col-md-3 col-md-offset-3">
                                                    <button  class="btn btn-success add-time sent_disagree" type="submit">Send</button>
                                                </div>
                                            </div>

                                        </form>

                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="modal fade in" id="cancelReplyFrm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title">Canceling Reason</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="msg"></div>
                                        <input type='hidden' id='dis_agreement_id' name='dis_agreement_id' />
                                        <form role="form" id='reasonFrm' action="{{url('inquiry/cancel-reply')}}" class="form-horizontal" method="POST">


                                            <div class="form-group">
                                                <label class="control-label col-md-3">Give a reason</label>
                                                <div class="col-md-9">
                                                    <textarea rows="7" style="resize:none;" class='form-control' id='disdescription' name='comment'></textarea>

                                                </div>
                                                
                                            </div>
                                            <div class="form-group">
                                               

                                                <input type="hidden" name="ir_id" value="<?php echo $pendingComment; ?>"/>
                                                <div class="col-md-3 col-md-offset-3">
                                                    <button  class="btn btn-success add-time sent_disagree" type="submit">Send</button>
                                                </div>
                                            </div>

                                        </form>

                                    </div>

                                </div>
                            </div>
                        </div>
                    @endif
                    @if($user->user_type == 'Employer')
                    <form method="post"  @if($interview->status == Interview::$Completed) action="{{url('interview/offer/'.$interview->interview_id)}}" @endif id="approveInterviewFrm" class="form-horizontal bucket-form">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Company Name :</b></label>
                            <div class="col-sm-6">
                                <p>{{$interview->Employer->title}}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Candidate Name :</b></label>
                            <div class="col-sm-6">
                                <p><a href="{{url('resume/display-resume/'.$interview->Candidate->cv_tbl_id)}}">{{$interview->Candidate->firstname}}</a></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Title :</b></label>
                            <div class="col-sm-6">
                                <p>{{$interview->Job->job_title}}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Description :</b></label>
                            <div class="col-sm-6">
                                <p>{{$interview->topics}}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Type :</b></label>
                            <div class="col-sm-6">
                                <p>{{$interview->type}}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Salary :</b></label>
                            <div class="col-sm-6">
                                <p>{{$interview->salary}}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Interview Date :</b></label>
                            <div class="col-sm-6">
                                
                                    <p>{{$interview->interview_date}}</p>
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Interview Time :</b></label>
                            <div class="col-sm-6">
                                
                                    <p>{{$interview->interview_time}}</p>
                               
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Interview Duration :</b></label>
                            <div class="col-sm-6">
                                
                                    <p>{{$interview->interview_duration}}</p>
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Message :</b></label>
                            <div class="col-sm-6">
                                 {{str_replace("\n","<br/>",$interview->message)}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Status :</b></label>
                            <div class="col-sm-6">
                                @if($interview->status == Interview::$Approved)
                                    Delivered
                                @elseif($interview->status == Interview::$Accepted)
                                    Accepted by Candidate
                                @else
                                    {{$interview->status}}
                                @endif
                            </div>
                        </div>
                        

                        <div class="form-group">
                            <div class="col-lg-offset-3 col-lg-6">
                                <input type="hidden" name="id" value="{{$interview->interview_id}}"/>
                                @if(0)
                                @if($interview->status == Interview::$Accepted)
                                    <button id="progressBtn" class="btn btn-warning" type="button">Change To Progressing</button>
                                @endif

                                @if($interview->status == Interview::$Progressing)
                                    <button id="completedBtn" class="btn btn-warning" type="button">Change to Completed</button>
                                @endif
                                @endif
                               
                            </div>
                        </div>
                        @if(0)
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Replay :</b></label>
                            <div class="col-sm-6">
                                <textarea id="comment" class="form-control " name="reply" ></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-3 col-lg-6">

                                <button id="replyBtn" class="btn btn-warning" type="button">Reply</button>

                            </div>
                        </div>
                        @endif
                        <div class="form-group">
                            
                            <div class="col-sm-6">
                                <?php $interviewReplies =  $interview->InterviewReplies; ?>
                                @if(count($interviewReplies))
                                @foreach($interviewReplies as $ir)
                                 @if(in_array($ir->status,array( "Approved","Replied by admin")))
                                <?php
                                if($ir->replier_type != 'Admin'){
                                    $userObj = User::where('user_id',$ir->replier_id)->where('user_type',$ir->replier_type)->first();
                                    $replier = '';
                                    if($userObj->user_type == 'Candidate')
                                    {
                                        $candidate = Candidate::find($userObj->user_id);
                                        $replier = $candidate->firstname;
                                    }else{
                                        $emp = Employer::find($userObj->user_id);
                                        $replier = $emp->title;
                                    }
                                }
                                ?>
                                <i class="fa fa-user"></i>&nbsp;{{$replier or 'Admin'}} <small><em>{{Helpers::dateTimeFormat('j F, Y',$ir->created_at)}}</em></small>
                                <p align="justify"><?php 
                                        $replyJson = '';
                                    if(!empty($interview->candidate_prefered_datetime))
                                        $replyJson = $interview->candidate_prefered_datetime;
                                    else
                                        $replyJson = $ir->reply;

                                    if(preg_match('/^{/',$replyJson))
                                        $reply = json_decode($replyJson);
                                    else
                                        $reply = $ir->reply;
                                ?>
                                @if(is_object($reply))
                                    {{'Available Date: '.$reply->date. '<br/> Avaiable time: '.$reply->time}}
                                @else
                                    {{$reply}}
                                @endif
                                </p>
                                <span style="height:1px; float:left; width:100%; margin:10px 0;background:#eff2f7;"></span>
                                @endif
                                @endforeach
                                @endif

                            </div>
                        </div>
                    </form>
                    @endif

                    @if($user->user_type == 'Candidate')
                    <form method="post" id="approveInterviewFrm" class="form-horizontal bucket-form">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Company Name :</b></label>
                            <div class="col-sm-6">
                                <p>{{$interview->Employer->title}}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Candidate Name :</b></label>
                            <div class="col-sm-6">
                                <p><a href="{{url('resume/display-resume/'.$interview->Candidate->cv_tbl_id)}}">{{$interview->Candidate->firstname}}</a></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Title :</b></label>
                            <div class="col-sm-6">
                                <p>{{$interview->Job->job_title}}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Description :</b></label>
                            <div class="col-sm-6">
                                <p>{{$interview->topics}}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Type :</b></label>
                            <div class="col-sm-6">
                                <p>{{$interview->type}}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Salary :</b></label>
                            <div class="col-sm-6">
                                <p>{{$interview->salary}}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Message :</b></label>
                            <div class="col-sm-6">
                                {{str_replace("\n","<br/>",$interview->message)}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Status :</b></label>
                            <div class="col-sm-6">
                                {{$interview->status}}
                            </div>
                        </div>
                        @if($editPermission)
                            @if($interview->status=="Pending" && $interviewResponse==0)
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><b>Reply :</b></label>
                                <div class="col-sm-6">
                                    <textarea id="comment" class="form-control " name="reply" required=""></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-3 col-lg-6">
                                    <input type="hidden" name="id" value="{{$interview->interview_id}}"/>
                                    <button id="replyBtn" class="btn btn-warning" type="button">Reply</button>
                                </div>
                            </div>
                            @endif
                        @endif
                        <div class="form-group">
                            <label class="control-label col-lg-3"><b>Requested Date</b></label>
                            <div class="col-lg-3">
                                {{$interview->interview_date}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-3"><b>Requested Time</b></label>
                            <div class="col-lg-3">
                                {{$interview->interview_time}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-3"><b>Requested Duration</b></label>
                            <div class="col-lg-3">
                                {{$interview->interview_duration}}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-3 col-lg-6">
                                <input type="hidden" name="id" value="{{$interview->interview_id}}"/>
                                @if($interview->status != Interview::$Canceled && $interview->status != Interview::$Declined)
                                
                                @endif
                                @if($interview->status == Interview::$Approved)
                                 @if($interviewResponse==0)
                                 <button id="acceptBtn" class="btn btn-success" type="button">Accept</button>
                                 <a data-toggle="modal" href="#declineFrm" class="btn btn-danger" >Decline</a>
                                 
                                 @endif
                                @endif

                            </div>
                        </div>
                        @if($interview->status != Interview::$Accepted && $interview->status != Interview::$AcceptedPending && $interview->status != Interview::$Declined)
                        @if($interviewResponse==0)
                        <div class="form-group">
                            <label class="control-label col-lg-3">Your Prefered Date</label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control" name="your_date"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-3">Your Prefered Time</label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control" name="your_time"/>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-lg-offset-3 col-lg-3">
                                <input type="hidden" name="id" value="{{$interview->interview_id}}"/>
                                <button id="replyPreferedTime" class="btn btn-success" type="button">Reply</button>
                            </div>
                        </div>
                        @endif
                        @endif
                        
                        <div class="form-group">
                            
                            <div class="col-sm-6">
                                <?php $interviewReplies =  $interview->InterviewReplies; ?>
                                @if(count($interviewReplies))
                                @foreach($interviewReplies as $ir)

                                <?php
                                $userObj = User::where('user_id',$ir->replier_id)->where('user_type',$ir->replier_type)->first();
                                $replier = '';
                                if($userObj->user_type == 'Candidate')
                                {
                                    $candidate = Candidate::find($userObj->user_id);
                                    $replier = $candidate->firstname;
                                }else{
                                    $emp = Employer::find($userObj->user_id);
                                    $replier = $emp->title;
                                }
                                ?>
                                <i class="fa fa-user"></i>&nbsp;{{$replier}} <small><em>{{Helpers::dateTimeFormat('j F, Y',$ir->created_at)}}</em></small>
                                <p align="justify">
                                <?php 
                                    if(preg_match('/^{/',$ir->reply))
                                        $reply = json_decode($ir->reply);
                                    else
                                        $reply = $ir->reply;

                                ?>
                                @if(is_object($reply))
                                    {{'Available Date: '.$reply->date. '<br/> Avaiable time: '.$reply->time}}
                                @else
                                    {{$reply}}
                                @endif
                                </p>
                                {{$ir->status}}
                                <span style="height:1px; float:left; width:100%; margin:10px 0;background:#eff2f7;"></span>
                                @endforeach
                                @endif

                            </div>
                        </div>
                    </form>
                    <div class="modal fade in" id="declineFrm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title">Decline Reason</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="msg"></div>
                                        <input type='hidden' id='dis_agreement_id' name='dis_agreement_id' />
                                        <form id="declineReasonFrm" role="form" action="{{url('dashboard/decline-interview')}}" class="form-horizontal" method="POST">


                                            <div class="form-group">
                                                <label class="control-label col-md-3">Give a reason</label>
                                                <div class="col-md-9">
                                                    <textarea rows="7" style="resize:none;" class='form-control' id='disdescription' name='comment'></textarea>

                                                </div>
                                                
                                            </div>
                                            <div class="form-group">
                                               

                                                <input type="hidden" name="ir_id" value="<?php echo $interview->interview_id; ?>"/>
                                                <div class="col-md-3 col-md-offset-3">
                                                    <button  class="btn btn-success add-time sent_disagree" type="submit">Send</button>
                                                </div>
                                            </div>

                                        </form>

                                    </div>

                                </div>
                            </div>
                        </div>
                    @endif

                </div>
            </section>
        </div>
    </div>
</section>
<script type="text/javascript" src="{{$theme}}js/custom/interview-details.js"></script>
<script type="text/javascript">

    $(".approveComment").click(function(){

        var obj = $(this);
        $.ajax({
            type:"POST",
            url : BASE + 'interview/approve-reply',
            data:{id:obj.attr('data-id')},
            success:function(e)
            {
                window.location.reload();
            }
        });
       
    });

    $("#declineReasonFrm").submit(function(){
        var comment = $("#declineReasonFrm textarea[name='comment']").val();
        
        var phoneWeburlEmail = /((^[0][1-9]|\d+){6,14}|http:\/\/.+\.([a-z]{3,}|[a-z]{3,}\.[a-z]{2,3})|[a-z]+\.[a-z]{3,}|[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-z]{2,})/

        if(phoneWeburlEmail.test(comment))
        {
            $("#declineReasonFrm textarea[name='comment']").attr('placeholder', 'Sorry! You cannot write any phone no, email or web url').val('');
            return false;
        }  
    });

</script>
@stop