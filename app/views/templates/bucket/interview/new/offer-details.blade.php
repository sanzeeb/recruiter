@extends('templates.bucket.bucket')

@section('wrapper')
    <section class="wrapper">
    <!-- page start-->
    {{Helpers::showMessage()}}
    <div class="row">
    <div class="col-sm-12">
    <section class="panel">
    <header class="panel-heading">Interview Details
        @if($user->user_type=='Employer')
                        <span class="tools pull-right">
                            <form action="{{url('interview/lists')}}">
                                <button class="btn btn-info" style="padding:4px 12px;font-size:13px;margin-top:4px;" type="submit">Back</button>
                            </form>
                        </span>
        @endif
    </header>
    <div class="panel-body">
    @if($user->user_type == 'Admin')
    <form method="post" id="approveInterviewFrm" class="form-horizontal bucket-form">
        <div class="form-group">
            <label class="col-sm-3 control-label"><b>Company Name :</b></label>
            <div class="col-sm-6">
                <p>{{$interview->Employer->title}}</p>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label"><b>Candidate Name :</b></label>
            <div class="col-sm-6">
                <p><a href="{{url('resume/display-resume/'.$interview->Candidate->cv_tbl_id)}}">{{$interview->Candidate->firstname}}</a></p>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label"><b>Title :</b></label>
            <div class="col-sm-6">
                <p>{{$interview->Job->job_title}}</p>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label"><b>Description :</b></label>
            <div class="col-sm-6">
                <p>{{$interview->topics}}</p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"><b>Type :</b></label>
            <div class="col-sm-6">
                <p>{{$interview->type}}</p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"><b>Salary :</b></label>
            <div class="col-sm-6">
                <p>{{$interview->salary}}</p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"><b>Message for Inquery :</b></label>
            <div class="col-sm-6">
                {{$interview->message}}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"><b>Status :</b></label>
            <div class="col-sm-6">
                {{$interview->status}}
            </div>
        </div>
        @if($interview->status != Interview::$Canceled)
        <div class="form-group">
            <label class="col-sm-3 control-label"><b>Replay :</b></label>
            <div class="col-sm-6">
                <textarea id="comment" class="form-control " name="reply" required=""></textarea>
            </div>
        </div>
        @endif
        <div class="form-group">
            <div class="col-lg-offset-3 col-lg-6">
                <input type="hidden" name="id" value="{{$interview->interview_id}}"/>
                @if($interview->status == Interview::$Pending)
                <button id="approveBtn" type="button" class="btn btn-success">Approve</button>
                @endif
                @if($interview->status != Interview::$Canceled)
                <button id="replyBtn" class="btn btn-warning" type="button">Replay</button>
                @endif
                @if($interview->status == Interview::$Pending)
                <button id="cancelBtn" type="button" class="btn btn-danger">Cancel</button>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"><b>Replies :</b></label>
            <div class="col-sm-6">
                <?php $interviewReplies =  $interview->InterviewReplies; ?>
                @if(count($interviewReplies))
                @foreach($interviewReplies as $ir)
                <p align="justify">{{$ir->reply}}</p>
                <?php
                $userObj = User::where('user_id',$ir->replier_id)->where('user_type',$ir->replier_type)->first();
                $replier = '';
                if($userObj->user_type == 'Candidate')
                {
                    $candidate = Candidate::find($userObj->user_id);
                    $replier = $candidate->firstname;
                }else{
                    $emp = Employer::find($userObj->user_id);
                    $replier = $emp->title;
                }
                ?>
                <i class="fa fa-user"></i>&nbsp;{{$replier}}
                <span style="height:1px; float:left; width:100%; margin:10px 0;background:#eff2f7;"></span>
                @endforeach
                @endif

            </div>
        </div>
    </form>
    @endif
    @if($user->user_type == 'Employer')
    <form method="post"  action="{{url('dashboard/offer-interview/')}}" id="approveInterviewFrm" class="form-horizontal bucket-form">
    <div class="form-group">
        <label class="col-sm-3 control-label"><b>Company Name :</b></label>
        <div class="col-sm-6">
            <p>{{$interview->Employer->title}}</p>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label"><b>Candidate Name :</b></label>
        <div class="col-sm-6">
            <input type="hidden" name="candidate_id" value="{{$interview->Candidate->candidate_id}}"/>
            <p><a href="{{url('resume/display-resume/'.$interview->Candidate->cv_tbl_id)}}">{{$interview->Candidate->firstname}}</a></p>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label"><b>Title :</b></label>
        <div class="col-sm-6">
            <input type="hidden" name="title" value="{{$interview->Job->job_title}}"/>
            <p>{{$interview->Job->job_title}}</p>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label"><b>Description :</b></label>
        <div class="col-sm-6">
            <input type="hidden" name="description" value="{{$interview->topics}}"/>
            <p>{{$interview->topics}}</p>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label"><b>Type :</b></label>
        <div class="col-sm-6">
            <p>{{$interview->type}}</p>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label"><b>Salary :</b></label>
        <div class="col-sm-6">
            <input class="form-control sm-input" type="number" name="amount" step="50" value="{{$interview->salary}}"/>

        </div>
    </div>
    <!--<div class="form-group">
        <label class="col-sm-3 control-label"><b>Message for Inquery :</b></label>
        <div class="col-sm-6">
            {{$interview->message}}
        </div>
    </div>-->
    <div class="form-group">
        <label class="col-sm-3 control-label"><b>Interview Status :</b></label>
        <div class="col-sm-6">
            {{$interview->status}}
        </div>
    </div>
    @if($interview->status == Interview::$Completed)
    <div class="form-group">
        <label class="col-sm-3 control-label"><b>Feedback :</b></label>
        <div class="col-sm-6">
            <select name="feedback">
                <option value="">Select Feedback</option>
                @for($i=1; $i<=10; $i++)
                    <option value="{{$i}}">{{$i}}</option>
                @endfor
            </select>
        </div>
    </div>
    @endif
    @if($interview->status == "Completed")
    <div class="form-group">
        <div class="col-lg-offset-3 col-lg-6">
            <input type="hidden" name="id" value="{{$interview->interview_id}}"/>
            <button class="btn btn-warning" type="submit">Confirm Offer</button>
        </div>
    </div>
    @endif

    </form>
    @endif

    @if($user->user_type == 'Candidate')
    <form method="post" id="approveInterviewFrm" class="form-horizontal bucket-form">
        <div class="form-group">
            <label class="col-sm-3 control-label"><b>Company Name :</b></label>
            <div class="col-sm-6">
                <p>{{$interview->Employer->title}}</p>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label"><b>Candidate Name :</b></label>
            <div class="col-sm-6">
                <p><a href="{{url('resume/display-resume/'.$interview->Candidate->cv_tbl_id)}}">{{$interview->Candidate->firstname}}</a></p>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label"><b>Title :</b></label>
            <div class="col-sm-6">
                <p>{{$interview->Job->job_title}}</p>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label"><b>Description :</b></label>
            <div class="col-sm-6">
                <p>{{$interview->topics}}</p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"><b>Type :</b></label>
            <div class="col-sm-6">
                <p>{{$interview->type}}</p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"><b>Salary :</b></label>
            <div class="col-sm-6">
                <p>{{$interview->salary}}</p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"><b>Message for Inquery :</b></label>
            <div class="col-sm-6">
                {{$interview->message}}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"><b>Status :</b></label>
            <div class="col-sm-6">
                {{$interview->status}}
            </div>
        </div>
        @if($interview->status != Interview::$Canceled && $interview->status != Interview::$Declined)
        <div class="form-group">
            <label class="col-sm-3 control-label"><b>Reply :</b></label>
            <div class="col-sm-6">
                <textarea id="comment" class="form-control " name="reply" required=""></textarea>
            </div>
        </div>
        @endif
        <div class="form-group">
            <div class="col-lg-offset-3 col-lg-6">
                <input type="hidden" name="id" value="{{$interview->interview_id}}"/>
                @if($interview->status != Interview::$Canceled && $interview->status != Interview::$Declined)
                <button id="replyBtn" class="btn btn-warning" type="button">Reply</button>
                @endif
                @if($interview->status == Interview::$Approved)
                <button id="acceptBtn" class="btn btn-success" type="button">Accept</button>
                <button id="declinedBtn" class="btn btn-danger" type="button">Declined</button>
                @endif

            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"><b>Replies :</b></label>
            <div class="col-sm-6">
                <?php $interviewReplies =  $interview->InterviewReplies; ?>
                @if(count($interviewReplies))
                @foreach($interviewReplies as $ir)
                <p align="justify">{{$ir->reply}}</p>
                <?php
                $userObj = User::where('user_id',$ir->replier_id)->where('user_type',$ir->replier_type)->first();
                $replier = '';
                if($userObj->user_type == 'Candidate')
                {
                    $candidate = Candidate::find($userObj->user_id);
                    $replier = $candidate->firstname;
                }else{
                    $emp = Employer::find($userObj->user_id);
                    $replier = $emp->title;
                }
                ?>
                <i class="fa fa-user"></i>&nbsp;{{$replier}}
                <span style="height:1px; float:left; width:100%; margin:10px 0;background:#eff2f7;"></span>
                @endforeach
                @endif

            </div>
        </div>
    </form>
    @endif

    </div>
    </section>
    </div>
    </div>
    </section>
    <script type="text/javascript" src="{{$theme}}js/custom/interview-details.js"></script>
@stop