@extends('templates.bucket.bucket')

@section('wrapper')

<section class="wrapper">
    <!-- page start-->
    {{Helpers::showMessage()}}
    <div id="msg"></div>

         <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
 <span class="tools pull-right">
                             Candidate requested for interview
                             <?php $id=Request::segment(3);  ?>
       {{Form::open(array('url'=>'interview/candidate-req-pdf','target'=>'_blank','class'=>'form-horizontal'))}}
                 
       <input type="hidden" name="emp_id" value="<?php echo $id; ?>"/>
         <input type="submit" class="btn  btn-success" value="Requested pdf"/>
                        
                               {{Form::close()}}
                               
                         </span>
                        <span class="tools pull-left">
                                                 Candidate confirmed for interview
                        {{Form::open(array('url'=>'interview/candidate-con-pdf','target'=>'_blank','class'=>'form-horizontal'))}}
                 
       <input type="hidden" name="emp_id" value="<?php echo $id; ?>"/>
         <input type="submit" class="btn   btn-success" value="Confirmed pdf"/>
                        
                               {{Form::close()}}
                        </span>
                    </header>
                    <div class="panel-body">
                        {{Form::open(array('url'=>'interview/send-notification','class'=>'cmxform form-horizontal'))}}
                   <div class="form-group">
                        <div class="col-lg-9 col-lg-offset-9">
                            <hr/>
                        <input type="submit" class="btn  btn-info" value="Send Notification"/>
                        
                        </div>
                       
                    </div>
                        <div class="form-group">
                        <table class="table  table-hover general-table">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Speciality</th>
                                <th>Interview time</th>
                                <th>View </th>
                                
                                

                            </tr>
                            </thead>
                            <tbody>
                                
                                 @if(count($requests))
                                    @foreach($requests as $request)
                        
                            <tr>
                                <td>
                                        @if($request->Candidate->photo)
                                        <img width="80" height="80" src="{{url('/').'/data/profile/'.$request->Candidate->photo}}"/>
                                        @else
                                        <img width="80" height="80" src="{{$theme}}images/FB-profile-avatar.jpg"/>
                                        @endif
                                        <?php echo $name = $request->Candidate->firstname .' '.$request->Candidate->lastname; ?>
                                       
                                </td>
                                <td>
                                    
                                    {{$request->Candidate->phone}}
                                    
                                </td>
                                <td>{{(!empty($request->Candidate->email))? $request->Candidate->email : $request->Candidate->alternative_email}}</td>
                                <td>
                                        <?php $specialization = $request->Candidate->CvTable->CvSpecialization; ?>
                                        @if(count($specialization))
                                        <ul>
                                            @foreach($specialization as $spec)
                                            <li>{{{$spec->key}}}</li>
                                            @endforeach
                                        </ul>
                                        @endif
                                        
                                    
                                </td>
                                <td class="col-lg-6">
                                    
                                    <div class="col-lg-5">
                                    <?php $availableTimes = $request->Candidate->UserAcc->Schedules; ?>
                                    
                                    <p>Candidate's Schedule</p>
                                    <input type="hidden" name="candidate_id[{{$request->aprov_id}}]" value="{{$request->Candidate->UserAcc->id}}"/>
                                    @if(count($availableTimes))
                                        <ul>
                                            @foreach($availableTimes as $time)
                                            <li><input disabled type="checkbox" name="time1[{{$request->aprov_id}}]" value="{{$time->available_date.' '.$time->from_time.'-'.$time->to_time}}"/> {{$time->available_date.' '.$time->from_time.'-'.$time->to_time}}</li>
                                            @endforeach
                                        </ul>
                                     <input type="text" class="form-control" name="time[{{$request->aprov_id}}]"/>
                                    @else
                                        <input type="text" class="form-control" name="time[{{$request->aprov_id}}]"/>
                                    @endif
                                </div>
                                
                                <div class="col-lg-5">
                                    
                                    <?php $availTimes = $employer->UserEAcc->Schedules; ?>
                                    
                                    <p>Recruiter's Schedule</p>
                                    <input type="hidden" name="recruiter_id[{{$request->aprov_id}}]" value="{{$employer->UserEAcc->id}}"/>
                                    @if(count($availTimes))
                                        <ul>
                                            @foreach($availTimes as $time)
                                            <li><input disabled  type="checkbox" name="recruiter_time1[{{$request->aprov_id}}]" value="{{$time->available_date.' '.$time->from_time.'-'.$time->to_time}}"/> {{$time->available_date.' '.$time->from_time.'-'.$time->to_time}}</li>
                                            @endforeach
                                        </ul>
                                    <input type="text" class="form-control" name="recruiter_time[{{$request->aprov_id}}]"/>
                                    @else
                                        <input type="text" class="form-control" name="recruiter_time[{{$request->aprov_id}}]"/>
                                    @endif
                                </div>
                                </td>
                                
                                <td><a target="_blank" href="{{url('interview/details/')}}" title="view"><i class="fa fa-eye"></i></a></td>

                            </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                        </div>
                        {{Form::close()}}
                    </div>
                </section>
            </div>
        </div>
        
   
</section>
@stop