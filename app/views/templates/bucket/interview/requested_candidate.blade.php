@extends('templates.bucket.bucket')

@section('wrapper') 
<section class="wrapper">
        <!-- page start-->
{{Helpers::showMessage()}}
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        Candidate requested for interview
                         <span class="tools pull-right">

                            
                         </span>
                    </header>
                    <div class="panel-body">
                       
                        <table class="table  table-hover general-table">
                            <thead>
                            <tr>
                                <th>Created at</th>
                                <th>Company Logo</th>
                                <th>Company name</th>
                                <th>Company phone</th>
                                <th>Company email</th>
                                <th>Status</th>
                                <th>View Details</th>
                                
                                
<!--                                <th> Job seeker email</th>
                                <th> Job seeker phone</th>-->
                                

                            </tr>
                            </thead>
                            <tbody>
                                
                                @foreach($requesteds as $requested)
                                
                                <?php $emp =  Employer::find($requested->emp_id); ?>
                                <?php //$can= Candidate::find($requested->candidate_id); ?>
                            <tr>
                                <td>{{{Helpers::dateTimeFormat("F j, Y",$requested->created_at)}}} </td>
                                <td>
                                    @if(!empty($emp->photo_name))
                                    <img height="50" width="150" alt="logo" src="{{url('/').'/data/employer/'.$emp->photo_name}}"/>
                                    @else
                                    <img height="50" width="150" alt="logo" src="{{$theme.'images/avatar.jpg'}}"/>
                                    @endif
                                </td>
                                <td><a href="#">{{{$emp->title}}}</a></td>
                                <td>{{{$emp->phone}}}</td>
                                <td>{{{$emp->email}}}</td>
                                <td>
                                    @if($requested->status)
                                    <span class="label label-success">Notification sent</span>
                                    @else
                                    <span class="label label-default">Pending</span>
                                    @endif
                                </td>
                                <td><a target="_blank" href="{{url('interview/details/'.$requested->id)}}" title="view"><i class="fa fa-eye"></i></a></td>

                            </tr>
                                    @endforeach

                            </tbody>
                        </table>
                        <div class="row-fluid"><div class="span6"><div id="hidden-table-info_info" class="dataTables_info">{{$requesteds->links()}}</div></div><div class="span6"><div class="dataTables_paginate paging_bootstrap pagination"></div></div></div>
                    </div>
                </section>
            </div>
        </div>
        
        <!-- page end-->
        </section>

@stop