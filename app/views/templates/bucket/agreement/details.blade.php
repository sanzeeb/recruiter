@extends('templates.bucket.bucket')

@section('wrapper')  
<section class="wrapper">
        <!-- page start-->

    {{Helpers::showMessage()}}
    <div class="row">
        <div class="col-lg-12">
                <section class="panel">
                        <header class="panel-heading">
                           Agreement Details

                        </header>
                        <div class="panel-body">

                            <?php $receiverUSerAcc = User::find($agreement->receiver_id); ?>


                            <div @if($user->user_type == 'Employer' || $user->user_type=='Candidate') class="col-lg-6" @else class="col-lg-12" @endif>

                                     <table class="table  table-hover general-table">

                                         <tr>
                                             <td>Title</td>
                                             <td>{{{$agreement->agreement_title}}}</td>
                                         </tr>
                                         <tr>
                                             <td>Description</td>
                                             <td>{{{$agreement->description}}}</td>
                                         </tr>
                                         <tr>
                                             <td>Candidate</td>
                                             <td>{{{$agreement->ReceiverUserAcc->Candidate->firstname}}}</td>
                                         </tr>
                                         <tr>
                                             <td>Amount</td>
                                             <td>{{{$agreement->amount}}}</td>
                                         </tr>
                                     </table>

                            </div>
                            @if($user->user_type == 'Employer')
                                <div class="col-lg-6">
                                <?php
                                    if($agreement->rating != '' || $agreement->rating != 0)
                                    {
                                        echo Theme::make('share.rating',array('agreement'=>$agreement));
                                    }else{

                                        echo Theme::make('share.rating-form',array('agreement'=>$agreement));
                                    }
                                ?>
                                </div>
                            @elseif($user->user_type == 'Candidate')
                                <div class="col-lg-6">
                                    {{Theme::make('share.rating',array('agreement'=>$agreement))}}
                                </div>
                            @endif
                        </div>
                </section>

                <section class="panel">
                    <header class="panel-heading">
                        Payment till now
                    </header>
                    <div class="panel-body">
                        <table class="table  table-hover general-table">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Amount</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($payments))
                                    @foreach($payments as $payment)
                                        <tr>
                                            <td>{{{$agreement->agreement_title}}}</td>
                                            <td>{{{$payment->amount}}}</td>
                                            
                                            <td>{{{Helpers::dateTimeFormat('j F, Y',$payment->created_at)}}}</td>
                                            <td>
                                                @if($user->user_type=="Admin")
                                                @if($payment->status == 'Pending')
                                                <a href="#" data-id="{{{$payment->payment_id}}}" class="btn btn-success btn-sm paid" >Paid</a>|
                                                
                                                <a href="#" data-id="{{{$payment->payment_id}}}" class="btn btn-danger btn-sm cancel" >Cancel</a>
                                                @endif
                                                
                                                @else
                                                {{{$payment->status}}}
                                            @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
    </div>
</section>
@if($user->user_type == 'Employer')
 @if($agreement->rating == '' || $agreement->rating == 0)
<script type="text/javascript" src="{{$theme}}js/custom/agreement.js"></script>
@endif
@endif
@stop