
 <div class="col-lg-12" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Agree </h4>
                </div>
                <div class="modal-body" style="background:#fff;">
                    <div class="msg"></div>
                    <input type='hidden' id='dis_agreement_id' name='dis_agreement_id' />
                    <form role="form" method="POST" action="{{url('agreement/update-agreement')}}" class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-md-3">Charge Amount</label>
                                <div class="col-lg-3">
                                    <input type="text" name="charge" value="{{$agreement->amount}}"/>
                                </div>
                            </div>
                        
                            <div class="form-group">
                                <label class="control-label col-md-3">Description</label>
                                <div class="col-md-9">
                                    <textarea rows="7" class='form-control' id='disdescription' name='disdescription' style="resize:none;"></textarea>

                                </div>

                                <div class="col-lg-3 col-lg-offset-3" style="margin-top:5px;">
                                    <input type="hidden" name="agreement_id" value="{{$agreement->agreement_id}}"/>
                                    <button  class="btn btn-success add-time" type="submit">Approve</button>
                                </div>
                            </div>
                      

                    </form>

                </div>
</div>
      