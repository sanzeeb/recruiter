@extends('templates.bucket.bucket')

@section('wrapper')
<section class="wrapper">
    {{{Helpers::showMessage()}}}
    <!--mini statistics start-->
    <div class="row"> 
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Agreement details </h4>
            </div>
            <div class="modal-body">
                <div class="msg"></div>
                
                <input type='hidden' value="{{{$agreements->agreement_id}}}" id='agreement_id' name='agreement_id' />
                <form role="form" id='agreement-form' class="form-horizontal">

                    <div class="form-group">
                        <label class="control-label col-md-3">Recruiter</label>
                        <div class="col-md-7 col-xs-11">
                            <input readonly="readonly" value="{{{$agreements->SenderUserAcc->Employer->title or ''}}}" type="text" id="eemployer" name="eemployer"  class="form-control">
                        </div>
                    </div> 

                    <div class="form-group">
                        <label class="control-label col-md-3">Candidate</label>
                        <div class="col-md-8 col-xs-11">
                            <input readonly="readonly" type="text" value="{{{$agreements->ReceiverUserAcc->Candidate->firstname or ''}}}"  name="ecandidate" id="ecandidate"  class="form-control">
                        </div>
                    </div> 

                    <div class="form-group">
                        <label class="control-label col-md-3">Title</label>
                        <div class="col-md-8 col-xs-11">
                            <input type="text" value="{{{$agreements->agreement_title}}}" readonly="readonly" name="etitle" placeholder="Enter title" id="etitle" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Amount</label>
                        <div class="col-md-4 col-xs-11">
                            <input type="text" readonly="readonly" value="{{{$agreements->amount}}}"  name="eamount" placeholder="Enter amount" id="eamount" class="form-control">

                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-3">Description</label>
                        <div class="col-md-9">
                            <textarea rows="7" readonly="readonly"  class='form-control' id='edescription' name='edescription'>
{{{$agreements->description}}}

                            </textarea>

                        </div>

                    </div>
                     @if($agreements->status=="Confirmed")
                    <div class="form-group">
                        <label class="control-label col-md-3">Actions:</label>
                        <div class="col-md-9">
                           
                                <a href="#" data-id='{{{$agreements->agreement_id}}}' class='btn btn-sm btn-success agree'>Accept</a> |
                           
                                <a  data-toggle="modal" href="#myModal-1"  data-id='{{{$agreements->agreement_id}}}' class='btn btn-sm btn-danger disagree'>Decline</a>
                         
                        </div>

                    </div>
                    @else
                    <div class="form-group">
                        <label class="control-label col-md-3">Status</label>
                        <div class="col-md-9">
                            <strong>{{$agreements->status}}</strong>
                        </div>
                    </div>
                    @endif
                </form>

            </div>

        </div>
    </div>
    <div class="modal fade in" id="myModal-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Disagree reason form </h4>
                </div>
                <div class="modal-body">
                    <div class="msg"></div>
                    <input type='hidden' id='dis_agreement_id' name='dis_agreement_id' />
                    <form role="form" id='agreement-form' class="form-horizontal">


                        <div class="form-group">
                            <label class="control-label col-md-3">Description</label>
                            <div class="col-md-5">
                                <textarea rows="7" class='form-control' id='disdescription' name='disdescription'></textarea>

                            </div>

                            <button  class="btn btn-success add-time sent_disagree" type="button">Disagree confirm</button>
                        </div>


                    </form>

                </div>

            </div>
        </div>
    </div>
    <script type="text/javascript" src="{{$theme}}js/custom/agreement.js"></script>
</section>
@stop