@extends('templates.bucket.bucket')

@section('wrapper')  
<section class="wrapper">
    <!-- page start-->
{{Helpers::showMessage()}}

    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    My Agreements

                </header>
                <div class="panel-body">
                    @if(count($lists))
                    <table class="table  table-hover general-table">
                        <thead>
                            <tr>

                                <th>Sl</th>
                                <th class="hidden-phone">Title</th>
                                @if($user->user_type=="Employer")
                                <th> Candidate</th>
                                @endif
                                
                                <th> Amount</th>
                                <th> Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($lists))
                            @foreach($lists as $i=> $list)
                            <tr>
                                <td>{{($i+1)}}</td>
                                <td>{{{$list->agreement_title}}}</td>
                                @if($user->user_type=="Employer")
                                <td>{{{$list->ReceiverUserAcc->Candidate->firstname or ''}}}</td>
                                @endif
                                  
                                <td>{{{$list->amount}}}</td>
                                <td>{{{$list->status}}}</td>
                                <td>
                                    @if($user->user_type=="Employer")
                                        @if($list->status=="Accepted")
                                        <a class="btn btn-sm btn-info" href="{{{url('payment/paynow/'.$list->agreement_id)}}}">Pay Now</a>

                                        @endif
                                        <a class="btn btn-sm btn-send" href="{{{url('agreement/details/'.$list->agreement_id)}}}">Details</a>
                                        @elseif($user->user_type=="Candidate")
                                        <a href="{{{url('agreement/agreementDtl/'.$list->agreement_id)}}}" class="btn btn-sm btn-info">View</a>
                                        
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                    @else
                        <div style="width:100%;padding:35px 0; text-align:center;"><img src="{{$theme}}images/no-job-offer.jpg" /></div>
                    @endif

                </div>
                <div class="modal fade in" id="myModal-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title">Disagree reason form </h4>
                            </div>
                            <div class="modal-body">
                                <div class="msg"></div>
                                <input type='hidden' id='dis_agreement_id' name='dis_agreement_id' />
                                <form role="form" id='agreement-form' class="form-horizontal">


                                    <div class="form-group">
                                        <label class="control-label col-md-3">Description</label>
                                        <div class="col-md-5">
                                            <textarea rows="7" class='form-control' id='disdescription' name='disdescription'></textarea>

                                        </div>

                                        <button  class="btn btn-success add-time sent_disagree" type="button">Disagree confirm</button>
                                    </div>


                                </form>

                            </div>

                        </div>
                    </div>
                </div>
                <script type="text/javascript" src="{{$theme}}js/custom/agreement.js"></script>
            </section>
        </div>
    </div>
</section>

@stop