@extends('templates.bucket.bucket')


@section('wrapper')  
<section class="wrapper">
    <!-- page start-->


    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    All Agreements

                </header>
                <div class="panel-body">
                    <table class="table  table-hover general-table">
                        <thead>
                            <tr>
                                <th class="hidden-phone">Title</th>
                                <th> Recruiter</th>
                                <th> Candidate</th>
                                <th> Amount</th>
                                <th> Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($lists))
                            @foreach($lists as $list)
                            <tr>
                                <td>{{{$list->title}}}</td>
                                <td>{{{$list->SenderUserAcc->Employer->title or ''}}}</td>

                                <td>{{{$list->ReceiverUserAcc->Candidate->firstname or ''}}}</td>
                                <td>{{{$list->amount}}}</td>
                                <td>{{{$list->status}}}</td>
                                <td>
                                    <a data-toggle="modal" href="{{url('agreement/details/'.$list->agreement_id)}}"  data-id='{{{$list->agreement_id}}}' class='btn btn-sm btn-default view-agree'>View</a>
                                    @if($list->status!="Accepted"&&$list->status!="Confirmed" && $list->status!="Canceled")
                                    <a data-toggle="modal" href="{{url('agreement/agree-frm/'.$list->agreement_id)}}" data-target="#agree" class='btn btn-sm btn-success'>Agree</a>|
                                    @endif
                                    @if($list->status!="Canceled"&&$list->status!="Accepted")

                                    <a  data-toggle="modal" href="#myModal-1"  data-id='{{{$list->agreement_id}}}' class='btn btn-sm btn-warning disagree'>Deny</a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>

                </div>



            </section>
        </div>
    </div>

    <div class="modal fade in" id="agree" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
        <div class="modal-dialog">
            <div class="modal-content">

            </div>
        </div>
    </div>
    
    <div class="modal fade in" id="myModal-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Disagree reason form </h4>
                </div>
                <div class="modal-body">
                    <div class="msg"></div>
                    <input type='hidden' id='dis_agreement_id' name='dis_agreement_id' />
                    <form role="form" id='agreement-form' class="form-horizontal">


                        <div class="form-group">
                            <label class="control-label col-md-3">Description</label>
                            <div class="col-md-5">
                                <textarea rows="7" class='form-control' id='disdescription' name='disdescription'></textarea>

                            </div>

                            <button  class="btn btn-success add-time sent_disagree" type="button">Disagree confirm</button>
                        </div>


                    </form>

                </div>

            </div>
        </div>
    </div>


    <div class="modal fade in" id="myModal-2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Agreement details </h4>
                </div>
                <div class="modal-body">
                    <div class="msg"></div>
                    <input type='hidden' id='agreement_id' name='agreement_id' />
                    <form role="form" id='agreement-form' class="form-horizontal">

                        <div class="form-group">
                            <label class="control-label col-md-3">Recruiter</label>
                            <div class="col-md-7 col-xs-11">
                                <input readonly="readonly" type="text" id="eemployer" name="eemployer"  class="form-control">
                            </div>
                        </div> 

                        <div class="form-group">
                            <label class="control-label col-md-3">Candidate</label>
                            <div class="col-md-8 col-xs-11">
                                <input readonly="readonly" type="text" name="ecandidate" id="ecandidate"  class="form-control">
                            </div>
                        </div> 

                        <div class="form-group">
                            <label class="control-label col-md-3">Title</label>
                            <div class="col-md-8 col-xs-11">
                                <input type="text" readonly="readonly" name="etitle" placeholder="Enter title" id="etitle" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Amount</label>
                            <div class="col-md-4 col-xs-11">
                                <input type="text" readonly="readonly"  name="eamount" placeholder="Enter amount" id="eamount" class="form-control">

                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3">Description</label>
                            <div class="col-md-9">
                                <textarea rows="7" readonly="readonly"  class='form-control' id='edescription' name='edescription'></textarea>

                            </div>

                        </div>



                    </form>

                </div>

            </div>
        </div>
    </div> 

</section>

<script type="text/javascript" src="{{$theme}}js/custom/cvmanagement.js"></script>
<script type="text/javascript" src="{{$theme}}js/custom/agreement.js"></script>
@stop

