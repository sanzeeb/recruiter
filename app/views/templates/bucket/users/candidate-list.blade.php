@extends('templates.bucket.bucket')

@section('wrapper') 
<section class="wrapper" ng-app="User">
    <!-- page start-->
    {{Helpers::showMessage()}}
    <div class="row" ng-controller="UserCtrl">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                 
                        Candidate List
                   
                   
                    <span class="tools pull-right">

                        {{ Form::open(array('url'=>'resume/create-resume','method'=>'get')) }}
                        <button class="btn btn-primary panel-btn" type="submit"><i class="fa fa-plus-circle"></i> Add New</button>
                        {{ Form::close() }}

                    </span>
                </header>
                <div class="panel-body">
                    <form action="{{Request::url()}}" class="font-ser" method="get">
                       <!--  <label>Search :</label>
                        <label><input name="key" placeholder="name, username or email" type="text" /></label>

                        <input name="" class="srbtn" type="submit" value="search" /> -->
                    </form>
                    @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                    <!-- <div class="form-group">
                        <label class="control-label col-lg-3">Search</label>
                        <div class="col-lg-3">
                            <input type="text" ng-model="searchUser" class="form-control"/>
                        </div>
                    </div> -->
                    <table ng-table="tableParams" show-filter="true" class="table ng-table-responsive">
                            <tr ng-repeat="user in $data" >
                                <td data-title="'Created At'" sortable="'created_at'">@{{(user.created_at)}}</td>
                                
                                <td data-title="'Name'" sortable="'title'" filter="{'firstname':'text'}">@{{user.firstname}}</td>
                                <td data-title="'Username'" sortable="'username'" filter="{'username':'text'}">@{{user.username}}</td>
                                <td data-title="'Email'" sortable="'email'" filter="{'email':'text'}">@{{user.email}}</td>
                                <td data-title="'Candidate ID'" sortable="'user_id'" filter="{'user_id':'text'}">@{{user.user_id}}</td>
                                <td data-title="'Actions'">
                                    <a  href="{{{url('profile/index')}}}/@{{user.cv_tbl_id}}">Edit</a> 
                                    <!-- <a onclick="deleteEmp(@{{user.id}})" href="javascript:void(0)">Delete</a> -->
                                </td>
                            </tr>
                     </table>  
                                 
                    <!-- <div class="row-fluid"><div class="span6"><div id="hidden-table-info_info" class="dataTables_info"></div></div><div class="span6"><div class="dataTables_paginate paging_bootstrap pagination"></div></div></div> -->
                </div>
            </section>
        </div>
    </div>

    <!-- page end-->
</section>

<div class="modal fade in" id="myModal-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">User</h4>
            </div>
            <div class="modal-body">

                <form role="form" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-lg-2 col-sm-2 control-label" for="inputEmail1">Email</label>
                        <div class="col-lg-10">
                            <input type="email" placeholder="Email" id="inputEmail4" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 col-sm-2 control-label" for="inputPassword1">Password</label>
                        <div class="col-lg-10">
                            <input type="password" placeholder="Password" id="inputPassword4" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox"> Remember me
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button class="btn btn-default" type="submit">Sign in</button>
                        </div>
                    </div>
                </form>

            </div>

        </div>
    </div>
</div>
<script src="{{ $theme }}js/custom/emp.js"></script>
<script type="text/javascript">
   
        var app = angular.module('User', ['ngTable']).controller('UserCtrl', function($scope, $filter, $q, NgTableParams) {

         var data = {{json_encode($users)}};
         console.log(data);
        $scope.tableParams = new NgTableParams({
            page: 1,            // show first page
            count: 50           // count per page
         }, {
            total: data.length, // length of data
            getData: function($defer, params) {
                // use build-in angular filter
                var orderedData = params.sorting() ?
                        $filter('orderBy')(data, params.orderBy()) :
                        data;
                orderedData = params.filter() ?
                        $filter('filter')(orderedData, params.filter()) :
                        orderedData;

                params.total(orderedData.length); // set total for recalc pagination
                $defer.resolve($scope.users = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        });

        var inArray = Array.prototype.indexOf ?
                function (val, arr) {
                    return arr.indexOf(val)
                } :
                function (val, arr) {
                    var i = arr.length;
                    while (i--) {
                        if (arr[i] === val) return i;
                    }
                    return -1
                };
            
        });
</script>    
@stop