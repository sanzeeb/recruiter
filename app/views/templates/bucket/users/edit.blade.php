@extends('templates.bucket.bucket')

@section('wrapper')  
<section class="wrapper">
        <!-- page start-->
<h6>{{ HTML::ul($errors->all()) }}</h6>


     {{ Form::open(array('url'=>'users/edit-user','class'=>'wpcf7-form contact_form', 'method'=>'post')) }}   
    <div class="row">
    <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            User basic edit form
                            
                        </header>
                        <div class="panel-body">
                            <div class="position-center">

                                <input type="hidden" name="id" value="{{{$users->id}}}" />
                                @if(0)
                                <div class="form-group">
                                    
                                     <label for="FirstName">First name</label>
                                     
                                       <input type="text" value="{{{$users->Candidate->firstname}}}" name="firstname" id="LastName" data-val-required="The User name field is required." data-val="true" class="form-control">
                                </div>
                                <div class="form-group">
                                     <label for="LastName">Last name</label>
                                       <input type="text" value="{{{$users->Candidate->lastname}}}" name="lastname" id="FirstName" data-val-required="The User name field is required." data-val="true" class="form-control">
                                </div>
                              
                                <div class="form-group">
                                    
                                    <input tabindex="3" type="radio" checked="checked"  name="demo-radio">
                                    <label>Female </label>
                                    <input tabindex="3" type="radio"   name="demo-radio">
                                    <label>Male </label>
                                                
                                </div>
                                @endif
                                <div class="form-group">
                                     <label for="UserName">User name</label>
                                     <input type="text" value="{{{$users->username}}}" name="username" id="UserName" data-val-required="The User name field is required." data-val="true" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" value="{{{$users->user_email}}}" name="email" id="Email" data-val-required="The User name field is required." data-val="true" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="role_id">User role</label>
                                    <div>
                                    <select name="role_id" class="form-control">
                                        @if(count($roles))
                                            @foreach($roles as $role)
                                                
                                                <option  @if($role->role_id == $users->role_id) selected="selected" @endif value="{{$role->role_id}}">{{$role->role_name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    </div>
                                </div>
                               
                                <button type="submit" class="btn btn-info">Submit</button>
                            
                            </div>

                        </div>
                    </section>

            </div>

</div>          {{ Form::close() }}

        <!-- page end-->
        </section>
@stop