@extends('templates.bucket.bucket')

@section('wrapper')  
<section class="wrapper">
        <!-- page start-->
<h3>{{{$msg or ''}}} </h3>
     {{ Form::open(array('url'=>'users/change-password','id'=>'changePassFrm','class'=>'wpcf7-form contact_form', 'autocomplete'=>'off', 'method'=>'post')) }}   
    <div class="row">
    <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            Change password from 
                            
                        </header>
                        <div class="panel-body">
                            <div class="position-center">
                                <div class="form-group">
                                    
                                    <input type="hidden" value='{{{Session::get("id")}}}' name="user_id" />
                                      <label for="OldPassword">Old Password</label>
                                     <input type="password" name="Oldpassword" id="OldPassword" data-val-required="Old Password field is required." data-val-length-min="6" data-val-length-max="100" data-val-length="The Password must be at least 6 characters long." data-val="true" class="form-control">
                                </div>
                                <div class="form-group">
                                      <label for="Password">New Password</label>
                                     <input type="password" name="password" id="Password" data-val-required="The Password field is required." data-val-length-min="6" data-val-length-max="100" data-val-length="The Password must be at least 6 characters long." data-val="true" class="form-control">
                                </div>
                                
                               <div class="form-group">
                                       <label for="ConfirmPassword">Confirm password</label>
                <input type="password" name="re_password" id="ConfirmPassword" data-val-equalto-other="*.Password" data-val-equalto="The password and confirmation password do not match." data-val="true" class="form-control">
                                </div>
                                <span>Strength : <strong class="passStrength"></strong></span>
                                <br/><br/>
                                <button type="submit" class="btn btn-info">Change</button>
                            
                            </div>

                        </div>
                    </section>

            </div>

</div>          {{ Form::close() }}
        <!-- page end-->
        </section>
        <script type="text/javascript">
          var weekPass = false;
    var password = '';
    
    $("input[name='password']").keyup(function(){
       
       password = $(this).val();
       var pattern = /(\w|\W){8,}/;
       if(pattern.test(password)){
            
            if(password.match(/@/) && password.match(/#/))
                $(".passStrength").html('Strong').css('color','green');
            else if(password.match(/@/))
                $(".passStrength").html('Medium').css('color','green');
            else
                $(".passStrength").html('Medium').css('color','green');
            weekPass = false;
       }else{
            weekPass = true;
            $(".passStrength").html('Week').css('color','red');
       }
       
    });

    $("#changePassFrm").submit(function(){
        if(weekPass)
            return false;

        
        var re_password = $("input[name='re_password']").val();
        
        if(password != re_password)
        {    $(".passStrength").html('Not Matched').css('color','red');
            return false;
        }else{
            $(".passStrength").html('');
        }
        
    });
        </script>
@stop