@extends('templates.bucket.bucket')

@section('wrapper')
<section class="wrapper">
    <!-- page start-->

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    User Role
                        <span class="tools pull-right">

                        </span>
                </header>
                <div class="panel-body">
                    {{Form::open(array('url'=>'user/save-user-role', 'class'=>'cmxform form-horizontal'))}}
                        <div class="form-group">
                            <label class="control-label col-lg-3">User Type</label>
                            <div class="col-lg-3">
                                <select class="form-control" name="user_type" required>
                                    <option value="">Select</option>
                                    <option value="Admin">Admin</option>
                                    <option value="Employer">Employer</option>
                                    <option value="Candidate">Candidate</option>
                                </select>

                            </div>

                        </div>
                    {{Form::close()}}
                </div>
            </section>
            <section class="panel">
                <header class="panel-heading">
                    Module Permission
                        <span class="tools pull-right">

                        </span>
                </header>
                <div class="panel-body">
                    <table class="permissionList display table table-bordered table-striped" id="dynamic-table">
                        <thead>
                            <tr>
                                <th>Module Name</th>
                                <th>Read</th>
                                <th>Create</th>
                                <th>Update</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($modules))
                                @foreach($modules as $module)
                                   <!-- <tr>
                                        <td><input type="hidden" name="module_id[{{{$module->module_id}}}]" value="{{{$module->module_id}}}"/>{{{ucfirst($module->module)}}}</td>
                                        <td><input type="checkbox" class="permissionChk" data-field="read" name="read[{{{$module->module_id}}}]" value="{{{$module->module_id}}}"/></td>
                                        <td><input type="checkbox" class="permissionChk" data-field="create" name="create[{{{$module->module_id}}}]" value="{{{$module->module_id}}}"/></td>
                                        <td><input type="checkbox" class="permissionChk" data-field="update" name="update[{{{$module->module_id}}}]" value="{{{$module->module_id}}}"/></td>
                                        <td><input type="checkbox" class="permissionChk" data-field="delete" name="delete[{{{$module->module_id}}}]" value="{{{$module->module_id}}}"/></td>
                                    </tr>-->
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>
</section>
<script type="text/javascript" src="{{$theme}}js/custom/user-role.js"></script>
@stop