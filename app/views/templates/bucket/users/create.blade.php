@extends('templates.bucket.bucket')

@section('wrapper')  
<section class="wrapper">
        <!-- page start-->
<h3>{{{$msg or ''}}} </h3>
     {{ Form::open(array('url'=>'users/createUser','class'=>'wpcf7-form contact_form', 'method'=>'post')) }}   
    <div class="row">
    <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            User create form
                            
                        </header>
                        <div class="panel-body">
                            <div class="position-center">

                                <div class="form-group">
                                     <label for="FirstName">First name</label>
                                       <input type="text" value="" name="firstname" id="LastName" data-val-required="The User name field is required." data-val="true" class="form-control">
                                </div>
                                <div class="form-group">
                                     <label for="LastName">Last name</label>
                                       <input type="text" value="" name="lastname" id="FirstName" data-val-required="The User name field is required." data-val="true" class="form-control">
                                </div>
                                <div class="form-group">
                                     <label for="email">Email</label>
                                       <input type="email" value="" name="email" id="Email" data-val-required="Email field is required." data-val="true" class="form-control">
                                </div>
                               
                                <div class="form-group">
                                    <label for="user_type">User Type</label>
                                    <div>
                                        <select name="user_type" class="form-control">
                                            <option value="Admin">Admin</option>
                                            <option value="Candidate">Candidate</option>
                                            <option value="Employer">Employer</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="role_id">User Role</label>
                                    <div>
                                    <select name="role_id" class="form-control">
                                        @if(count($roles))
                                            @foreach($roles as $role)
                                                <option value="{{$role->role_id}}">{{$role->role_name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label >Gender</label><br/>
                                                    <input tabindex="3" type="radio"  name="gender">
                                                    <label>Male </label>
                                                    <input tabindex="3" type="radio"  name="gender">
                                                    <label>Female </label>
                                                
                                            </div>
                                
                                
                                <div class="form-group">
                                     <label for="UserName">User name</label>
                                       <input type="text" value="" name="username" id="UserName" data-val-required="The User name field is required." data-val="true" class="form-control">
                                </div>
                                
                                <div class="form-group">
                                      <label for="Password">Password</label>
                                     <input type="password" name="password" id="Password" data-val-required="The Password field is required." data-val-length-min="6" data-val-length-max="100" data-val-length="The Password must be at least 6 characters long." data-val="true" class="form-control">
                                </div>
                                
                               <div class="form-group">
                                       <label for="ConfirmPassword">Confirm password</label>
                <input type="password" name="re_password" id="ConfirmPassword" data-val-equalto-other="*.Password" data-val-equalto="The password and confirmation password do not match." data-val="true" class="form-control">
                                </div>

                                    
                                    
                                <button type="submit" class="btn btn-info">Submit</button>
                            
                            </div>

                        </div>
                    </section>

            </div>

</div>          {{ Form::close() }}
        <!-- page end-->
        </section>
@stop