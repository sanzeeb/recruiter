@extends('templates.bucket.bucket')

@section('wrapper') 
<section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-sm-12">
                 <div class="form-group">
        <label class="control-label col-md-3">&nbsp;</label>
        <div class="controls col-md-2">
            <div class="fileupload-new thumbnail">
                @if(!empty($candidate->photo) && file_exists('data/profile/'.$candidate->photo))
                <img alt="{{{$candidate->photo or ''}}}" src="{{{url('/')}}}/data/profile/{{{$candidate->photo}}}"/>
                @else
                <img alt="thumbnail" src="{{$theme}}images/placeholder.gif"/>
                @endif
            </div>
            <div class="fileupload fileupload-new" data-provides="fileupload">
                             {{{$candidate->firstname or ''}}}&nbsp;            
                             {{{$candidate->lastname or ''}}}               
            </div>
        </div>
    </div>
                
            </div>
        </div>
                <div class="row">
                    
                    
                    
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        Give a feedback on skills
                        
                    </header>
                    <div class="panel-body">
                        @if (Session::has('message'))
                <div class="alert alert-info">{{ Session::get('message') }}</div>
                        @endif
                        <table class="table  table-hover general-table">
                            <thead>
                            <tr>
                                <th> Skill name</th>
                                <th>Rating</th>

                            </tr>
                            </thead>
                            <tbody>
                                
                                @foreach($speacs as $row)
                                
                            <tr>
                                
                                <td>{{{$row->key}}}</td>
                                <td>
                                    <h3>Rating</h3>
        <div class="starRate">
            <ul>
                @for($i=5; $i>=1; $i--)
                <li>
                    @if($i<=$row->rating)
                    <a class="selected" data-id="{{$row->cv_specialization_id}}" href="{{(5-$i)+1}}"><span>Give {{($i)}} Star</span></a>
                    @else
                    <a  data-id="{{$row->cv_specialization_id}}" href="{{(5-$i)+1}}"><span>Give {{{$i}}} Star</span></a>
                    @endif
                </li>
                @endfor
            </ul>
        </div>
                                </td>


                                

                                
                            </tr>
                                    @endforeach

                            </tbody>
                        </table>
                    </div>
                </section>
            </div>
        </div>
        
        <!-- page end-->
        </section>

<script src="{{ $theme }}js/custom/feedback.js"></script>
  
@stop