{{Form::open(array('id'=>'clientRatingFrm','class'=>'cmxform form-horizontal'))}}

<div class="form-group">
    <div class="col-lg-6">
        <h3>Rating</h3>
        <div class="starRate">
            <ul>
                @for($i=1; $i<=5; $i++)
                <li>
                    <a href="{{(5-$i)+1}}"><span>Give {{(5-$i)+1}} Star</span></a>
                </li>
                @endfor
            </ul>
        </div>
    </div>
</div>
<div class="form-group">

    <div class="col-lg-6">
        <h3>Comment:</h3>
        <textarea class="form-control" name="comment" style="width:490px; resize:none;"></textarea>
    </div>
</div>
<div class="form-group">
    <div class="col-lg-3">
        <input type="hidden" name="agreement_id" value="{{{$agreement->agreement_id}}}" />
        <input class="btn btn-send" value="Submit" type="submit"/>
    </div>
</div>
{{Form::Close()}}

