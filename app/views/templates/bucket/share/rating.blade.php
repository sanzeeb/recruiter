<div class="cmxform form-horizontal">
<div class="form-group">
    <div class="col-lg-6">
        <h3>Rating</h3>
        <div class="starRate">
            <ul>
                @for($i=5; $i>=1; $i--)

                <li>
                    <a @if($i<=$agreement->rating) class="selected" @endif href="{{(5-$i)+1}}"></a>
                </li>
                @endfor
            </ul>
        </div>
    </div>
</div>

<div class="form-group">

    <div class="col-lg-6">
        <h3>Comment:</h3>
        @if($agreement->rating_comment != '')
        <p>
           {{{$agreement->rating_comment}}}
        </p>
        @endif
    </div>
</div>

<div class="form-group">
    <div class="col-lg-3">
        <input type="hidden" name="agreement_id" value="{{{$agreement->agreement_id}}}" />

    </div>
</div>
</div>
