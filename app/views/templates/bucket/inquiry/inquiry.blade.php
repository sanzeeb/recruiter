@extends('templates.bucket.bucket')

@section('wrapper')
<section class="wrapper">
    <!-- page start-->
    {{Helpers::showMessage()}}
    <div class="row">
        <div class="col-sm-12">

            <section class="panel">
                <header class="panel-heading">
                    inquiry Form
                         <span class="tools pull-right">

                         </span>
                </header>
                <div class="panel-body cmxform form-horizontal">

                    <div class=" form">
                        <form action="#" method="get" id="commentForm" class="cmxform form-horizontal " novalidate="novalidate">
                            <div class="form-group ">
                                <label class="control-label col-lg-3" for="cname">Company Name :</label>
                                <div class="col-lg-6">
                                    <input type="text" required="" minlength="2" name="name" id="cname" class=" form-control" disabled="" placeholder="carbon51.com">
                                </div>

                            </div>
                            <div class="form-group ">
                                <label class="control-label col-lg-3" for="cemail">Candidate Name :</label>
                                <div class="col-lg-6">
                                    <input type="email" required="" name="email" id="cemail" class="form-control" disabled="" placeholder="MD.Johir Uddin">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="control-label col-lg-3" for="curl">Title :</label>
                                <div class="col-lg-6">
                                    <select class="form-control m-bot15">
                                        <option>Web Design</option>
                                        <option>Web Development</option>
                                        <option>UI Design</option>
                                        <option>PHP</option>
                                        <option>JVA</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="control-label col-lg-3" for="ccomment">Description :</label>
                                <div class="col-lg-6">
                                    <textarea required="" name="comment" id="ccomment" class="form-control " placeholder="A web designer develops and creates websites and associated applications. Web designers work in a variety of industries and often as independent contractors. Education in web design and related fields is necessary for those interested in this career." ></textarea>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="control-label col-lg-3" for="ccomment">Message for inquiry :</label>
                                <div class="col-lg-6">
                                    <textarea required="" name="comment" id="ccomment" class="form-control "></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-3 col-lg-6">
                                   <button class="btn btn-info" type="button">Send</button>
                                   <!-- <button type="submit" class="btn btn-success">Approve</button>-->
                                    <button class="btn btn-warning" type="submit">Replay</button>
                                    <button type="submit" class="btn btn-danger">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </section>

            <section class="panel">
                <header class="panel-heading">Replay Form </header>
                <div class="panel-body">
                    <form method="get" class="form-horizontal bucket-form">

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Company Name :</b></label>
                            <div class="col-sm-6">
                               <p>carbon51.com</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Candidate Name :</b></label>
                            <div class="col-sm-6">
                                <p>MD. Johir Uddin</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Title :</b></label>
                            <div class="col-sm-6">
                                <p>Web Design</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Description :</b></label>
                            <div class="col-sm-6">
                                <p>A web designer develops and creates websites and associated applications. Web designers work in a variety of industries and often as independent contractors. Education in web design and related fields is necessary for those interested in this career.</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Message for inquiry :</b></label>
                            <div class="col-sm-6">
                                <textarea id="ccomment" class="form-control " name="comment" required="" placeholder="A web designer develops and creates websites and associated applications. Web designers work in a variety of industries and often as independent contractors. Education in web design and related fields is necessary for those interested in this career. A web designer develops and creates websites and associated applications. Web designers work in a variety of industries and often as independent contractors. Education in web design and related fields is necessary for those interested in this career."></textarea>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Replay :</b></label>
                            <div class="col-sm-6">
                                <textarea id="ccomment" class="form-control " name="comment" required=""></textarea>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Replay :</b></label>
                            <div class="col-sm-6">
                                <p align="justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                <i class="fa fa-user"></i>&nbsp;MD.Tanvir
                                <span style="height:1px; float:left; width:100%; margin:10px 0;background:#eff2f7;"></span>
                                <p align="justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                <i class="fa fa-user"></i>&nbsp;MD.Tanvir
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-offset-3 col-lg-6">
                              <button class="btn btn-info" type="button">Send</button>
                                <button type="submit" class="btn btn-success">Approve</button>
                                <button class="btn btn-warning" type="submit">Replay</button>
                                <button type="submit" class="btn btn-danger">Cancel</button>
                            </div>
                        </div>

                    </form>
                </div>
            </section>
            
            <section class="panel">
                <header class="panel-heading">Replay Form </header>
                <div class="panel-body">
                    <form method="get" class="form-horizontal bucket-form">

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Company Name :</b></label>
                            <div class="col-sm-6">
                               <p>carbon51.com</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Candidate Name :</b></label>
                            <div class="col-sm-6">
                                <p>MD. Johir Uddin</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Title :</b></label>
                            <div class="col-sm-6">
                                <p>Web Design</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Description :</b></label>
                            <div class="col-sm-6">
                                <p>A web designer develops and creates websites and associated applications. Web designers work in a variety of industries and often as independent contractors. Education in web design and related fields is necessary for those interested in this career.</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Message for inquiry :</b></label>
                            <div class="col-sm-6">
                                <textarea id="ccomment" class="form-control " name="comment" required="" placeholder="A web designer develops and creates websites and associated applications. Web designers work in a variety of industries and often as independent contractors. Education in web design and related fields is necessary for those interested in this career. A web designer develops and creates websites and associated applications. Web designers work in a variety of industries and often as independent contractors. Education in web design and related fields is necessary for those interested in this career."></textarea>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Replay :</b></label>
                            <div class="col-sm-6">
                                <textarea id="ccomment" class="form-control " name="comment" required=""></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-offset-3 col-lg-6">
<!--                              <button class="btn btn-info" type="button">Send</button>
                                <button type="submit" class="btn btn-success">Approve</button>-->
                                <button class="btn btn-warning" type="submit">Replay</button>
                                <button type="submit" class="btn btn-danger">Cancel</button>
                            </div>
                        </div>

                    </form>
                </div>
            </section>


            <section class="panel">
                <header class="panel-heading">Test Project
                         <span class="tools pull-right"></span>
                </header>
                <div class="panel-body cmxform form-horizontal">

                    <div class=" form">
                        <form action="#" method="get" id="commentForm" class="cmxform form-horizontal " novalidate="novalidate">
                            <div class="form-group ">
                                <label class="control-label col-lg-3" for="cname">Company Name :</label>
                                <div class="col-lg-6">
                                    <input type="text" required="" minlength="2" name="name" id="cname" class=" form-control" disabled="" placeholder="Disabled input here...">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="control-label col-lg-3" for="cemail">Candidate Name :</label>
                                <div class="col-lg-6">
                                    <input type="email" required="" name="email" id="cemail" class="form-control " >
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="control-label col-lg-3" for="curl">Title :</label>
                                <div class="col-lg-6">
                                    <input type="email" required="" name="email" id="cemail" class="form-control ">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="control-label col-lg-3" for="ccomment">Description :</label>
                                <div class="col-lg-6">
                                    <textarea required="" name="comment" id="ccomment" class="form-control "></textarea>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="control-label col-lg-3" for="ccomment">File input :</label>
                                <div class="col-lg-6">
                                    <input id="exampleInputFile" type="file">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-3 col-lg-6">
                                    <button type="submit" class="btn btn-success">Approve</button>
                                    <button class="btn btn-warning" type="submit">Replay</button>
                                    <button type="submit" class="btn btn-danger">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </section>


<section class="panel">
                    <header class="panel-heading" style="display:inline-block; width:100%;">
                        <div class="text-center pull-left">
                            <ul class="pagination" style="margin:0;">
                                <li><a href="#">&laquo;</a></li>
                                <li><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#">&raquo;</a></li>
                            </ul>
                        </div>
                        <span class="tools pull-right">
								<button class="btn btn-info" style="padding:4px 12px;font-size:13px;margin-top:4px;" type="button">Create Inquiry</button>
                         </span>
                    </header>
                    <div class="panel-body">
                        <section id="unseen">
                            <table class="table table-bordered table-striped table-condensed">
                                <thead>
                                <tr>
                                    <th>Candidate Name</th>
                                    <th>Title</th>
                                    <th class="numeric">Status</th>
                                    <th class="numeric">Send date</th>
                                    <th width="180">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>AAC</td>
                                    <td>AUSTRALIAN AGRICULTURAL COMPANY LIMITED.</td>
                                    <td class="numeric">Status</td>
                                    <td class="numeric">04-08-1990</td>
                                    <td class="numeric">
                         <button class="btn btn-default btn-sm pull-left" type="button">View Details</button><button class="btn btn-danger btn-sm pull-right" type="button">Delete</button></td>
                                </tr>
                              <tr>
                                    <td>AAC</td>
                                    <td>AUSTRALIAN AGRICULTURAL COMPANY LIMITED.</td>
                                    <td class="numeric">Status</td>
                                    <td class="numeric">04-08-1990</td>
                                    <td class="numeric">
                         <button class="btn btn-default btn-sm pull-left" type="button">View Details</button><button class="btn btn-danger btn-sm pull-right" type="button">Delete</button></td>
                                </tr>
                                <tr>
                                    <td>AAC</td>
                                    <td>AUSTRALIAN AGRICULTURAL COMPANY LIMITED.</td>
                                    <td class="numeric">Status</td>
                                    <td class="numeric">04-08-1990</td>
                                    <td class="numeric">
                         <button class="btn btn-default btn-sm pull-left" type="button">View Details</button><button class="btn btn-danger btn-sm pull-right" type="button">Delete</button></td>
                                </tr>
                                <tr>
                                    <td>AAC</td>
                                    <td>AUSTRALIAN AGRICULTURAL COMPANY LIMITED.</td>
                                    <td class="numeric">Status</td>
                                    <td class="numeric">04-08-1990</td>
                                    <td class="numeric">
                         <button class="btn btn-default btn-sm pull-left" type="button">View Details</button><button class="btn btn-danger btn-sm pull-right" type="button">Delete</button></td>
                                </tr>
                                <tr>
                                    <td>AAC</td>
                                    <td>AUSTRALIAN AGRICULTURAL COMPANY LIMITED.</td>
                                    <td class="numeric">Status</td>
                                    <td class="numeric">04-08-1990</td>
                                    <td class="numeric">
                         <button class="btn btn-default btn-sm pull-left" type="button">View Details</button><button class="btn btn-danger btn-sm pull-right" type="button">Delete</button></td>
                                </tr>
                                <tr>
                                    <td>AAC</td>
                                    <td>AUSTRALIAN AGRICULTURAL COMPANY LIMITED.</td>
                                    <td class="numeric">Status</td>
                                    <td class="numeric">04-08-1990</td>
                                    <td class="numeric">
                         <button class="btn btn-default btn-sm pull-left" type="button">View Details</button><button class="btn btn-danger btn-sm pull-right" type="button">Delete</button></td>
                                </tr>
                                <tr>
                                <td colspan="5"><div>
                                    <ul class="pagination pagination-sm" style="margin:0;">
                                <li><a href="#">&laquo;</a></li>
                                <li><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#">&raquo;</a></li>
                                    </ul>
                                </div></td>
                                </tr>
                                </tbody>
                            </table>
                        </section>
                    </div>
                </section>


        </div>
    </div>
</section>
@stop