@extends('templates.bucket.bucket')

@section('wrapper')
<script>
    var jobs = {{$jobs}};
    var job = "{{$job or ''}}"
</script>
<section class="wrapper">
    <!-- page start-->
    {{Helpers::showMessage()}}
    <div class="row">
        <div class="col-sm-12">

            <section class="panel">
                <header class="panel-heading">
                    Inquiry Form
                         <span class="tools pull-right">

                         </span>
                </header>
                <div class="panel-body cmxform form-horizontal">

                    <div class=" form">
                        <form action="{{url('inquiry/send-inquiry')}}" id="inqueryFrm" method="post" class="cmxform form-horizontal " novalidate="novalidate">
                            <div class="form-group">
                                <label class="control-label col-lg-3" for="cname">Company Name : <span class="required">*</span></label>
                                <div class="col-lg-6">
                                    <input type="text" required="" minlength="2" name="cname" id="cname" class=" form-control" disabled="" value="{{$employer->title}}">
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-3" for="cemail">Candidate Name : <span class="required">*</span></label>
                                <div class="col-lg-6">
                                    <input type="hidden" name="cid" value="{{$candidate->candidate_id}}">
                                    <input type="text" required="" name="candiate_name" class="form-control" disabled="" value="{{$candidate->firstname}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-3" for="curl">Title : <span class="required">*</span></label>
                                <div class="col-lg-6">
                                    <?php $jobs = json_decode($jobs); ?>
                                    @if(!empty($jobs))
                                    <select class="form-control m-bot15" name="joblist">
                                    </select>
                                    @else
                                        <p>You have to create job before taking any inquiry</p>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-3" for="ccomment">Description : <span class="required">*</span></label>
                                <div class="col-lg-6">
                                    <textarea required name="job_description" id="job_description" class="form-control" ></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-3" for="ccomment">Message for inquiry : <span class="required">*</span></label>
                                <div class="col-lg-6">
                                    <textarea required name="inquiry" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-3" for="source_code">Submit Source Code</label>

                                <div class="col-lg-6"><input type="checkbox" name="source_code"/></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-3" for="source_code">Submit Academic Certificate</label>

                                <div class="col-lg-6"><input type="checkbox" name="academic_certificate"/></div>
                            </div>
                            @if(!empty($jobs))
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <button class="btn btn-info" type="submit">Send</button>
                                        <!-- <button type="submit" class="btn btn-success">Approve</button>-->
                                        <!--<button class="btn btn-warning" type="submit">Replay</button>-->

                                        @if(!empty($job))
                                        <?php 
                                            $redirectUrl = '';
                                            $segment4 = Request::segment(4);
                                            $segment5 = Request::segment(5);
                                            if(($segment4 == 1 && $segment5 == 1) || ($segment4 == 1 && $segment5 == 2))
                                                $redirectUrl = 'inquiry/lists';
                                            else if(($segment4 == 2 && $segment5 == 1) || ($segment4 == 2 && $segment5 == 2))
                                                $redirectUrl = 'interview/lists';
                                        ?>
                                        <a class="btn btn-danger" href="{{url($redirectUrl)}}">Cancel</a>
                                        @else
                                        <a class="btn btn-danger" href="{{url('resume/display-resume/'.$candidate->candidate_id)}}">Cancel</a>
                                        @endif
                                        
                                    </div>
                                </div>
                            @else
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <h3 class="text-danger">You have to create job before taking any inquiry</h3>
                                        <a href="{{url('job/create')}}" class="btn btn-info">Create Job Now</a>
                                    </div>
                                </div>
                            @endif

                        </form>
                    </div>

                </div>
            </section>
        </div>
    </div>
</section>
<script src="{{$theme}}js/custom/inquery.js"></script>

<script type="text/javascript">
    $("#inqueryFrm").submit(function(){
        var comment = $("textarea[name='inquiry']").val();
        
        var phoneWeburlEmail = /((^[0][1-9]|\d+){6,14}|http:\/\/.+\.([a-z]{3,}|[a-z]{3,}\.[a-z]{2,3})|[a-z]+\.[a-z]{3,}|[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-z]{2,})/

        if(phoneWeburlEmail.test(comment))
        {
            $("textarea[name='inquiry']").attr('placeholder', 'Sorry! You cannot write any phone no, email or web url').val('');
            return false;
        }  
       
    });

    
</script>
@stop