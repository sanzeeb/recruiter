@extends('templates.bucket.bucket')

@section('wrapper')
<section class="wrapper">
    <!-- page start-->
    {{Helpers::showMessage()}}
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">inquiry Details
                    @if($user->user_type=='Employer')
                    <span class="tools pull-right">
                        <form action="{{url('inquiry/lists')}}">
                            <button class="btn btn-info" style="padding:4px 12px;font-size:13px;margin-top:-4px;" type="submit">Back</button>
                        </form>
                    </span>
                    @endif
                </header>
                <div class="panel-body">
                    @if($user->user_type == 'Admin')
                        <form method="post" id="approveInqueryFrm" action="{{url('dashboard/approve-inquery')}}" class="form-horizontal bucket-form">

                            <div class="form-group">
                                <label class="col-sm-3 control-label"><b>Company Name :</b></label>
                                <div class="col-sm-6">
                                    <p>{{$inquiry->Employer->title}}</p>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label"><b>Candidate Name :</b></label>
                                <div class="col-sm-6">
                                    <p><a href="{{url('resume/display-resume/'.$inquiry->Candidate->cv_tbl_id)}}">{{$inquiry->Candidate->firstname}}</a></p>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label"><b>Title :</b></label>
                                <div class="col-sm-6">
                                    <p>{{$inquiry->Job->job_title or 'Job not found'}}</p>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label"><b>Description :</b></label>
                                <div class="col-sm-6">
                                    <p><?php echo str_replace("\n","<br/><br/>",$inquiry->description); ?></p>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-3 control-label"><b>Message for inquiry :</b></label>
                                <div class="col-sm-6">
                                    {{$inquiry->message}}
                                </div>
                            </div>
                            @if($inquiry->ask_source_code)
                            <div class="form-group">
                                <div class="col-sm-6 col-lg-offset-3"><b>Asked for Source Code</b></div>

                            </div>
                            @endif

                            @if($inquiry->ask_academic_certificate)
                            <div class="form-group">
                                <div class="col-sm-6  col-lg-offset-3"><b>Asked for Academic Certificate</b></div>
                            </div>
                            @endif

                            @if(in_array($user->user_type,array('Admin','Employer')))
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><b>Status :</b></label>
                                <div class="col-sm-6">
                                    @if($inquiry->status == inquiry::$Approved)
                                        Delivered
                                    @else
                                        {{$inquiry->status}}
                                    @endif
                                </div>
                            </div>
                            @endif

                           @if($inquiry->status == inquiry::$Pending)
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><b>Reply :</b></label>
                                <div class="col-sm-6">
                                    <textarea id="ccomment" class="form-control " name="reply" required=""></textarea>
                                </div>
                                <div class="col-lg-6 col-lg-offset-3" style="margin-top:4px;">
                                    <button id="replyBtn" class="btn btn-warning" type="button">Reply</button>
                                </div>
                            </div>
                            @endif
                            <div class="form-group">
                                <div class="col-lg-offset-3 col-lg-6">
                                    <input type="hidden" name="id" value="{{$inquiry->id}}"/>
                                    @if($inquiry->status == inquiry::$Pending)
                                    <button id="approveBtn" type="button" class="btn btn-success">Approve</button>
                                    @endif
                                   
                                    @if($inquiry->status == inquiry::$Pending)
                                    <button data-toggle="modal" href="#myModalCancelInquiry" type="button" class="btn btn-danger">Cancel</button>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <?php $inqueryResponse =  $inquiry->InqueryResponse; $pendingComment=''; ?>
                                @if(count($inqueryResponse))
                                <label class="col-sm-3 control-label"><b>Replies :</b></label>
                                <div class="col-sm-6">
                                    
                                        @foreach($inqueryResponse as $ir)

                                            <?php
                                                if($ir->replier_type != "Admin"){
                                                $userObj = User::where('user_id',$ir->replier_id)->where('user_type',$ir->replier_type)->first();
                                                $replier = '';
                                                    if($userObj->user_type == 'Candidate')
                                                    {
                                                       $candidate = Candidate::find($userObj->user_id);
                                                        $replier = $candidate->firstname;
                                                    }else{
                                                        $emp = Employer::find($userObj->user_id);
                                                        $replier = $emp->title;
                                                    }
                                                }
                                            ?>
                                            <i class="fa fa-user"></i>&nbsp;{{$replier or 'Admin'}} <small><em>{{Helpers::dateTimeFormat('j F, Y',$ir->created_at)}}</em></small>
                                            <p align="justify">{{$ir->reply}}</p>
                                            @if($ir->status == "Canceled")
                                                {{$ir->inquery_reason}} - by {{$ir->cancel_by or 'admin'}}
                                            @endif
                                            @if($ir->status == "Pending")
                                                <?php $pendingComment = $ir->id; ?>
                                                    <input type="button" data-id="{{$ir->id}}" class="approveComment btn btn-success" value="Approve"/>

                                                    <a data-toggle="modal" href="#myModal-1" class="btn btn-danger" >Cancel</a>
                                            @endif
                                            <span style="height:1px; float:left; width:100%; margin:10px 0;background:#eff2f7;">

                                            </span>
                                        @endforeach
                                    
                                </div>
                                @endif
                            </div>

                        </form>
                        <div class="modal fade in" id="myModalCancelInquiry" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title">Canceling Reason</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="msg"></div>
                                        <input type='hidden' id='dis_agreement_id' name='dis_agreement_id' />
                                        <form role="form" id='reasonFrm' action="{{url('dashboard/cancel-inquiry')}}" class="form-horizontal" method="POST">


                                            <div class="form-group">
                                                <label class="control-label col-md-3">Give a reason</label>
                                                <div class="col-md-9">
                                                    <textarea rows="7" style="resize:none;" class='form-control' id='disdescription' name='comment'></textarea>

                                                </div>
                                                
                                            </div>
                                            <div class="form-group">
                                               

                                                <input type="hidden" name="ir_id" value="<?php echo $inquiry->id; ?>"/>
                                                <div class="col-md-3 col-md-offset-3">
                                                    <button  class="btn btn-success add-time sent_disagree" type="submit">Send</button>
                                                </div>
                                            </div>

                                        </form>

                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="modal fade in" id="myModal-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title">Canceling Reason</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="msg"></div>
                                        <input type='hidden' id='dis_agreement_id' name='dis_agreement_id' />
                                        <form role="form" id='reasonFrm' action="{{url('inquiry/cancel-reply')}}" class="form-horizontal" method="POST">


                                            <div class="form-group">
                                                <label class="control-label col-md-3">Give a reason</label>
                                                <div class="col-md-9">
                                                    <textarea rows="7" style="resize:none;" class='form-control' id='disdescription' name='comment'></textarea>

                                                </div>
                                                
                                            </div>
                                            <div class="form-group">
                                               

                                                <input type="hidden" name="ir_id" value="<?php echo $pendingComment; ?>"/>
                                                <div class="col-md-3 col-md-offset-3">
                                                    <button  class="btn btn-success add-time sent_disagree" type="submit">Send</button>
                                                </div>
                                            </div>

                                        </form>

                                    </div>

                                </div>
                            </div>
                        </div>
                    @endif
                    @if($user->user_type == 'Employer')
                    <form method="post" id="approveInqueryFrm" class="form-horizontal bucket-form">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Company Name :</b></label>
                            <div class="col-sm-6">
                                <p>{{$inquiry->Employer->title}}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Candidate Name :</b></label>
                            <div class="col-sm-6">
                                <p>{{$inquiry->Candidate->firstname}}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Title :</b></label>
                            <div class="col-sm-6">
                                <p>{{$inquiry->Job->job_title}}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Description :</b></label>
                            <div class="col-sm-6">
                                <p><?php echo str_replace("\n","<br/><br/>",$inquiry->description); ?></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Message for inquiry :</b></label>
                            <div class="col-sm-6">
                                {{$inquiry->message}}
                            </div>
                        </div>
                        @if($inquiry->ask_source_code)
                        <div class="form-group">
                            <div class="col-sm-6 col-lg-offset-3"><b>Asked for Source Code</b></div>

                        </div>
                        @endif

                        @if($inquiry->ask_academic_certificate)
                        <div class="form-group">
                            <div class="col-sm-6  col-lg-offset-3"><b>Asked for Academic Certificate</b></div>
                        </div>
                        @endif
                        @if($editPermission )
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><b>Reply :</b></label>
                                <div class="col-sm-6">
                                    <textarea id="comment" class="form-control " name="reply" required=""></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-3 col-lg-6">
                                    <input type="hidden" name="id" value="{{$inquiry->id}}"/>
                                    <button id="replyBtn" class="btn btn-warning" type="button">Reply</button>
                                </div>
                            </div>
                        @endif
                        <div class="form-group">
                         <?php $inqueryResponse =  $inquiry->InqueryResponse; ?>
                         <?php $inqueryRepliedAdminResponse =  $inquiry->InqueryRepliedAdminResponse; ?>
                            @if(count($inqueryResponse))
                            <label class="col-sm-3 control-label"><b>Replies :</b></label>
                            <div class="col-sm-6">
                               
                                @foreach($inqueryResponse as $ir)
                                    @if($ir->status == "Approved")
                                        <?php
                                       
                                        
                                        $userObj = User::where('user_id',$ir->replier_id)->where('user_type',$ir->replier_type)->first();
                                        $replier = '';
                                        if($userObj->user_type == 'Candidate')
                                        {
                                            $candidate = Candidate::find($userObj->user_id);
                                            $replier = $candidate->firstname;
                                        }else{
                                            $emp = Employer::find($userObj->user_id);
                                            $replier = $emp->title;
                                        }
                                        ?>
                                        <i class="fa fa-user"></i>&nbsp;{{$replier}} <small><em>{{Helpers::dateTimeFormat('j F, Y',$ir->created_at)}}</em></small>
                                        <p align="justify">{{$ir->reply}}</p>
                                        <span style="height:1px; float:left; width:100%; margin:10px 0;background:#eff2f7;"></span>
                                    
                                    @endif
                                @endforeach
                                @if(count($inqueryRepliedAdminResponse))
                            
                          
                               
                                    @foreach($inqueryRepliedAdminResponse as $ir)
                                        
                                            
                                            <i class="fa fa-user"></i>&nbsp;{{$replier or 'Admin'}} <small><em>{{Helpers::dateTimeFormat('j F, Y',$ir->created_at)}}</em></small>
                                            <p align="justify">{{$ir->reply}}</p>
                                            <span style="height:1px; float:left; width:100%; margin:10px 0;background:#eff2f7;"></span>
                                        
                                       
                                    @endforeach
                                
                           
                                @endif
                               
                            </div>
                             @endif
                             
                        </div>
                    </form>
                    @endif
                    @if($user->user_type == 'Candidate')

                    <form method="post" id="approveInqueryFrm" class="form-horizontal bucket-form">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Company Name :</b></label>
                            <div class="col-sm-6">
                                <p>{{$inquiry->Employer->title}}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Candidate Name :</b></label>
                            <div class="col-sm-6">
                                <p>{{$inquiry->Candidate->firstname}}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Title :</b></label>
                            <div class="col-sm-6">
                                <p>{{$inquiry->Job->job_title}}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Description :</b></label>
                            <div class="col-sm-6">
                                <p><?php echo str_replace("\n","<br/><br/>",$inquiry->description); ?></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Message for inquiry :</b></label>
                            <div class="col-sm-6">
                                {{$inquiry->message}}
                            </div>
                        </div>
                        @if($inquiry->ask_source_code)
                        <div class="form-group">
                            <div class="col-sm-6 col-lg-offset-3"><b>Asked for Source Code</b></div>

                        </div>
                        @endif

                        @if($inquiry->ask_academic_certificate)
                        <div class="form-group">
                            <div class="col-sm-6  col-lg-offset-3"><b>Asked for Academic Certificate</b></div>
                        </div>
                        @endif
                        @if($editPermission)
                            @if($inquiryResponse == 0)
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><b>Reply :</b></label>
                                    <div class="col-sm-6">
                                        <textarea id="comment" class="form-control " name="reply" required=""></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <input type="hidden" name="id" value="{{$inquiry->id}}"/>
                                        <button id="replyBtn" class="btn btn-warning" type="button">Reply</button>
                                        <a class="btn btn-danger" href="{{url('inquiry/lists')}}">Cancel</a>
                                    </div>
                                </div>
                            @endif
                        @endif
                        <div class="form-group">
                            <?php $inqueryResponse =  $inquiry->InqueryResponse; ?>
                            @if(count($inqueryResponse))
                            <label class="col-sm-3 control-label"><b>Replies :</b></label>
                            <div class="col-sm-6">
                                
                                @foreach($inqueryResponse as $ir)

                                <?php
                                $userObj = User::where('user_id',$ir->replier_id)->where('user_type',$ir->replier_type)->first();
                                $replier = '';
                                if($userObj->user_type == 'Candidate')
                                {
                                    $candidate = Candidate::find($userObj->user_id);
                                    $replier = $candidate->firstname;
                                }else{
                                    $emp = Employer::find($userObj->user_id);
                                    $replier = $emp->title;
                                }
                                ?>
                                <i class="fa fa-user"></i>&nbsp;{{$replier}} <small><em>{{Helpers::dateTimeFormat('j F, Y',$ir->created_at)}}</em></small>
                                <p align="justify">{{$ir->reply}}</p>
                                @if($ir->status == "Canceled")
                                    {{$ir->inquery_reason}} - by {{$ir->cancel_by or 'admin'}}
                                @endif
                                <span style="height:1px; float:left; width:100%; margin:10px 0;background:#eff2f7;"></span>
                                @endforeach
                                

                            </div>
                            @endif
                        </div>
                    </form>

                    @endif
                </div>
                
            </section>
        </div>
    </div>
</section>
<script type="text/javascript" src="{{$theme}}js/custom/inquery-details.js"></script>
<script type="text/javascript">

    $(".approveComment").click(function(){
        var obj = $(this);
        $.ajax({
            type:"POST",
            url : BASE + 'inquiry/approve-reply',
            data:{id:obj.attr('data-id')},
            success:function(e)
            {
                window.location.reload();
            }
        });
       
    });

</script>
@stop