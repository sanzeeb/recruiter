@extends('templates.bucket.bucket')

@section('wrapper')
<style type="text/css">
#searchBtn{float:right; top:120px; right:185px;}
</style>
<section class="wrapper"  ng-app="Inquiry">
    <!-- page start-->
    {{Helpers::showMessage()}}
    <div class="row" ng-controller="InquiryCtrl">
        <div class="col-sm-12">

            <section class="panel">
                <header class="panel-heading">
                    Inquiry list
                         <span class="tools pull-right">

                         </span>
                </header>
                <div class="panel-body cmxform form-horizontal">
                   
                   
                        @if(count($inqueries))
                            <button id="searchBtn" ng-click="search()" class=" btn btn-xs btn-info">Search By Date</button>
                            <table ng-table="tableParams" show-filter="true" class="table ng-table-responsive">
                                <tr ng-repeat="inquiry in $data">
                        
                                    <td align="center" data-title="'Sl'" >@{{($index+1)}}</td>
                                    <td align="center" data-title="'Title'" sortable="'job_title'" filter="{'job.job_title':'text'}">@{{inquiry.job.job_title}}</td>
                                    @if($user->user_type == "Employer")
                                    <td align="center" data-title="'Candidate'" sortable="'candidate.firstname'" filter="{'candidate.firstname':'text'}">@{{inquiry.candidate.firstname}}</td>
                                    @elseif($user->user_type == "Candidate")
                                    <td align="center" data-title="'Employer'" sortable="'employer.title'" filter="{'employer.title':'text'}">@{{inquiry.employer.title}}</td>
                                    @else
                                        <td align="center" data-title="'Candidate'" filter="{'candidate.firstname':'text'}">@{{inquiry.candidate.firstname}}</td>
                                        <td align="center" data-title="'Employer'" filter="{'employer.title':'text'}">@{{inquiry.employer.title}}</td>
                                    @endif
                                    <td align="center" data-title="'Status'"  sortable="'id'" filter="{ 'status': 'select' }" filter-data="statuses($column)">
                                        @if($user->user_type == "Candidate")
                                        <span ng-if="inquiry.status == 'Approved'">Received</span>
                                        <span ng-if="inquiry.status != 'Approved'">@{{inquiry.status}}</span>
                                        @else
                                        <span ng-if="inquiry.status == 'Approved'">Delivered</span>
                                        <span ng-if="inquiry.status != 'Approved'">@{{inquiry.status}}</span>
                                        @endif
                                    </td>
                                    <td align="center" data-title="'Send Date'" sortable="'created_at'" filter="{'created_at':'text'}">@{{inquiry.created_at}}</td>
                                    
                                    <td data-title="'Actions'" width="251">
                                  
                                         @if($user->user_type == "Employer")

                                        <a  target="_blank" href="{{url('inquiry/details/')}}/@{{inquiry.id}}" style="margin-bottom:5px;" class="btn btn-warning btn-xs pull-left" >Inquiry details</a>

                                      <form ng-if="(inquiry.status == 'Canceled') || (inquiry.status == 'Replied by admin') || (inquiry.status == 'Replied')" method="post" action="{{url('inquiry/create')}}/@{{inquiry.candidate_id}}/1/1" style="float:left;margin:0px 5px;">                          
                                       <input type="hidden" name="job_id" value="@{{inquiry.job_id}}"/>
                                       <button type="submit" title="Inquery" style="margin-bottom:5px;" class="btn btn-success btn-xs">Re-Inquiry</button>
                                      </form>

                                        <form ng-if="(inquiry.status=='Replied') || (inquiry.status == 'Canceled') && (inquiry.status == 'Replied by admin')" method="post" action="{{url('interview/create')}}/@{{inquiry.candidate_id}}/1/2">
                                            <input type="hidden" name="job_id" value="@{{inquiry.job_id}}"/>
                                            <button type="submit" title="Interview" style="margin-bottom:5px;" class="btn btn-info btn-xs pull-left">Interview</button>
                                        </form>

                                    @else
                                        <a target="_blank" href="{{url('inquiry/details/')}}/@{{inquiry.id}}" style="margin-bottom:5px;" class="btn btn-warning btn-xs pull-left" >Inquiry details</a>
                                    @endif
                                                
                                    </td>
                                </tr> 
                            </table>
                        @else
                            <!-- No inquiry found -->
                            <div style="width:100%;padding:35px 0; text-align:center;"><img src="{{$theme}}images/no-inquiry.jpg" /></div>

                        @endif
                        
                   
                </div>
            </section>
        </div>
    </div>
</section>
<script type="text/javascript">
var app;
$(function(){   
		   
		  
            $("input[name='last_date'],input[name='created_at']").datepicker({
                changeMonth: true,
                dateFormat:"yy-mm-dd",
				yearRange: '-15:+15',
                changeYear: true,
				onSelect: function() {
					$("#searchBtn").addClass("po-show");
				}
            });
			
			
        });
   
        app = angular.module('Inquiry', ['ngTable']).controller('InquiryCtrl', function($scope,$http, $filter, $q, NgTableParams) {

         
         var data = {{$inqueries}};

         $scope.search = function(){
            var searchDate = angular.element('input[name="created_at"]').val();

           

                $http({
                        url : BASE + 'inquiry/get_inquiry_by_date',
                        method: "POST",
                        data: {'date':searchDate}
                    }).success(function(response,status){
                        data = response;
                        
                        angular.element('#searchBtn').removeClass('po-show');
                        $scope.tableParams.reload();
                    });
            
        
            
        }

        $scope.tableParams = new NgTableParams({
            page: 1,            // show first page
            count: 10           // count per page
        }, {
            total: data.length, // length of data
            getData: function($defer, params) {
                // use build-in angular filter
                var orderedData = params.sorting() ?
                        $filter('orderBy')(data, params.orderBy()) :
                        data;
                orderedData = params.filter() ?
                        $filter('filter')(orderedData, params.filter()) :
                        orderedData;

                params.total(orderedData.length); // set total for recalc pagination
                $defer.resolve($scope.users = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        });

        var inArray = Array.prototype.indexOf ?
                function (val, arr) {
                    return arr.indexOf(val)
                } :
                function (val, arr) {
                    var i = arr.length;
                    while (i--) {
                        if (arr[i] === val) return i;
                    }
                    return -1
                };
            $scope.statuses = function(column) {
                var def = $q.defer(),
                    arr = [],
                    statuses = [];
                angular.forEach(data, function(item){
                  
                   /*var status = (item.status == 'Approved') ? 'Delivered' : item.status; */
                   <?php if($user->user_type == "Candidate"){ ?> 
                        if (inArray(item.status, arr) === -1) {
                            arr.push(item.status);
                            statuses.push({
                             'id': item.status,
                             'title':  ((item.status == 'Approved') ? 'Received' : item.status)
                            });
                        }
                    <?php }else{  ?> 
                        if (inArray(item.status, arr) === -1) {
                        arr.push(item.status);
                        statuses.push({
                         'id': item.status,
                         'title':  ((item.status == 'Approved') ? 'Delivered' : item.status)
                        });
                    }
                    <?php } ?>
                });
                def.resolve(statuses);
                return def;
            };
        });
</script>
@stop