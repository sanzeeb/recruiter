@extends('templates.bucket.bucket')



@section('wrapper')	
<section class="wrapper" ng-app="relatedJob" ng-controller="relatedJobCtrl">
		


						<div class="col-md-7">

								<h1 class="page-title">{{{$about->title or ''}}} </h1>

								<p>

								{{{$about->content or ''}}}

                                </p>

						</div>


<div class="col-lg-12">
<header class="panel-heading">Find Jobs<h6>Skill: {{$skill}}</h6></header>             
<div class="panel-body panel">
                        @if(count($jobs))
                        <table  ng-table="tableParams" show-filter="true" class="table ng-table-responsive">
                            <tr ng-repeat="job in $data" >
                                <td align="center" data-title="'Sl'">@{{($index+1)}}</td>
                                <td align="center" data-title="'company name'" filter="{'title':'text'}">@{{(job.title)}}</td>
                                <td align="center" data-title="'Job Title'" filter="{'job_title':'text'}">@{{(job.job_title)}}</td>
                                <td align="center" data-title="'Skill name'" filter="{'skill_name':'text'}">@{{(job.skill_name)}}</td>
                                <td align="center" data-title="'Job Vacancies'" filter="{'job_vacancies':'text'}">@{{job.job_vacancies}}</td>
                                <td align="center" data-title="'Salary Range'" filter="{'salary_range':'text'}">@{{job.salary_range}} USD</td>
                                <td align="center" data-title="'Post Date'" filter="{'created_at':'text'}">@{{job.created_at}}</td>
                                <td align="center" data-title="'Last Application Date'" filter="{'last_date':'text'}">@{{job.last_date}}</td>
                                <td data-title="'Action'">
                                    <div ng-if="jobApplied(job.candidates,user_id) == true">
                                        <a href="{{url('job/view')}}/@{{job.jobs_id}}/{{Helpers::EncodeDecode('view')}}"  style="margin-bottom:5px;" class="btn btn-info btn-xs right">View Details</a>
                                        <a class="btn btn-danger btn-xs" style="margin-bottom:5px;">Applied</a>
                                    </div>
                                    <div ng-if="(jobApplied(job.candidates,user_id) != true)">
                                        <a href="{{url('job/view')}}/@{{job.jobs_id}}/{{Helpers::EncodeDecode('view')}}"  style="margin-bottom:5px;" class="btn btn-info btn-xs right">View Details</a>
                                        <a href="{{url('job/view')}}/@{{job.jobs_id}}"  style="margin-bottom:5px;" class="btn btn-danger btn-xs">Apply</a>
                                    </div>

                                </td>
                            </tr>
                        </table>
                        @else
                            <!-- here will be image for no job available -->
                            <div style="width:100%;padding:35px 0; text-align:center;"><img src="{{$theme}}images/no-jobs2.png" /></div>
                        @endif

	</div>				

</div>


</section>
<script type="text/javascript">

$(function(){   
            $("input[name='last_date'],input[name='created_at']").datepicker({
                changeMonth: true,
                dateFormat:"yy-mm-dd",
				yearRange: '-15:+15',
                changeYear: true
            });
        });
   
        var app = angular.module('relatedJob', ['ngTable']).controller('relatedJobCtrl', function($scope, $filter, $q, NgTableParams) {

        $scope.user_id = {{$user->user_id}};
        var data = {{json_encode($jobs)}};
       
        
        $scope.jobApplied = function(candidate_id,user_id){
           if(candidate_id == null)
                return false;
           var candidates = candidate_id.split(',')
           for(c in candidates)
           {
                c_id = candidates[c].trim();
                if(c_id == user_id)
                    return true;
           }
           return false;
        }

        $scope.tableParams = new NgTableParams({
            page: 1,            // show first page
            count: 10           // count per page
        }, {
            total: data.length, // length of data
            getData: function($defer, params) {
                // use build-in angular filter
                var orderedData = params.sorting() ?
                        $filter('orderBy')(data, params.orderBy()) :
                        data;

                orderedData = params.filter() ?
                        $filter('filter')(orderedData, params.filter()) :
                        orderedData;

                params.total(orderedData.length); // set total for recalc pagination
                $defer.resolve($scope.users = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        });

        var inArray = Array.prototype.indexOf ?
                function (val, arr) {
                    return arr.indexOf(val)
                } :
                function (val, arr) {
                    var i = arr.length;
                    while (i--) {
                        if (arr[i] === val) return i;
                    }
                    return -1
                };
            $scope.categories = function(column) {
                var def = $q.defer(),
                    arr = [],
                    categories = [];
                angular.forEach(data, function(item){
                  
                    if (inArray(item.job_category, arr) === -1) {
                        arr.push(item.job_category);
                        categories.push({
                         'id': item.job_category,
                         'title': item.job_category
                        });
                    }
                });
                def.resolve(categories);
                return def;
            };
        });
</script>
@stop

