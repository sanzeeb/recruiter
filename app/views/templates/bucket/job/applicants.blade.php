@extends('templates.bucket.bucket')

@section('wrapper')

<section class="wrapper">
    <!-- page start-->
    {{Helpers::showMessage()}}
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <div class="msg"></div>
                <header class="panel-heading" style="padding-left:24px;">
                   Applicant lists
                </header>
                <div class="panel-body">
                        <?php $jobCandidates = $jobs->JobCandidate; ?>
                        @if(count($jobCandidates))
                        <h5 style="padding-left:10px;text-transform:uppercase;color:#06C;">Applied Candidates</h5>
                        <table class="table  table-hover general-table">
                            <thead>
                            <tr>
                                <th>Candidate Name</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($jobCandidates as $jC)
                            <?php $candidate = $jC->Candidate; ?>
                                <tr>
                                    <td><a target="_blank" href="{{url('resume/display-resume/'.$candidate->cv_tbl_id.'/'.Helpers::EncodeDecode($jobs->jobs_id))}}">{{$candidate->firstname}}</a></td>
                                    <td>
                                        <a target="_blank" href="{{url('job/replies/'.$jC->job_id.'/'.$candidate->candidate_id)}}" class="btn btn-info">Applicants Message</a>
                                        <form action="{{url('job/remove-application')}}" class="inline-block" method="post">
                                            <input type="hidden" name="cid" value="{{$candidate->candidate_id}}"/>
                                            <input type="hidden" name="job_id" value="{{$jC->job_id}}"/>
                                            <button class="btn btn-danger">Remove</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @endif
                </div>
            </section>
        </div>
    </div>
</section>
@stop