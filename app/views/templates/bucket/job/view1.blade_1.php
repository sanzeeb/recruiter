@extends('templates.bucket.bucket')

@section('wrapper')

<section class="wrapper">
    <!-- page start-->
    {{Helpers::showMessage()}}
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <div class="msg"></div>
                <header class="panel-heading">
                   JOB Details
                    <span class="tools pull-right">
                    {{ Form::open(array('url'=>'job/edit/'.$jobs->jobs_id,'method'=>'get')) }}
                        <button class="btn btn-info panel-btn" type="submit"><i class="fa fa-pencil"></i> Edit</button>
                    {{ Form::close() }}
                </header>

                <div class="panel-body form-horizontal" >

                    <div class="form-group">
                        <label class="control-label col-lg-3">Job Title :</label>
                        <div class="col-lg-3">
                            <input type="text" class="form-control" disabled value="{{{$jobs->job_title or ''}}}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Job Category :</label>
                        <div class="col-lg-3">
                                @if(count($skills))
                                @foreach($skills as $skill)
                                    @if($skill->skill_id == $jobs->skill_id)
                            <input type="text" class="form-control" disabled value="{{{$skill->skill_name}}}"/>

                                    @endif
                                @endforeach
                                @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">No. of Vacancies :</label>
                        <div class="col-lg-1">
                            <input type="text" class="form-control" disabled value="{{{$jobs->job_vacancies or ''}}}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Job Nature</label>
                        <div class="col-lg-3">
                                @foreach($job_natures as $k=> $nature)
                                    @if($k == $jobs->job_nature)
                                        <input type="text" class="form-control" disabled value="{{{$nature}}}"/>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-lg-12">

                        <table width="100%">
                            <thead>
                            <tr>
                                <th style="width:800px;">Responsibilities</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $responsibility = (!empty($jobs->job_responsibility))? json_decode($jobs->job_responsibility) : ''; ?>
                                @if(count($responsibility))
                                    @foreach($responsibility as $res)
                                        <tr>
                                                <td>{{{$res or ''}}}</td>

                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="form-group col-lg-12">

                        <table width="100%" >
                            <thead>
                            <tr>
                                <th style="width:800px;">Educational Requirements</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $educational = (!empty($jobs->eduactional_requirement))? json_decode($jobs->eduactional_requirement) : ''; ?>
                                @if(count($educational))
                                    @foreach($educational as $edu)
                                        <tr>
                                            <td>{{{$edu or ''}}}</td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="form-group col-lg-12">

                        <table width="100%" >
                            <thead>
                            <tr>
                                <th style="width:800px;">Experience Requirements</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php $experience = (!empty($jobs->experience_requirment))? json_decode($jobs->experience_requirment) : ''; ?>
                            @if(count($experience))
                                @foreach($experience as $exp)
                                    <tr>
                                        <td>{{{$exp or ''}}}</td>

                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="form-group col-lg-12">

                        <table width="100%">
                            <thead>
                            <tr>
                                <th style="width:800px;">Additional Requirements</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php $extras = (!empty($jobs->extra_job_req))? json_decode($jobs->extra_job_req) : ''; ?>
                            @if(count($extras))
                                @foreach($extras as $extra)
                                    <tr>
                                        <td>{{{$extra or ''}}}</td>

                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-3">Other Benefit :</label>
                        <div class="col-lg-3">
                            <input type="text" class="form-control" disabled value="{{{$jobs->other_benefit or ''}}}"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-3">Salary Range :</label>
                        <div class="col-lg-3">
                            <input type="text" class="form-control" disabled value="{{{$jobs->salary_range or ''}}}"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-3">Job Location :</label>
                        <div class="col-lg-3">
                            <input type="text" class="form-control" disabled value="{{{$jobs->job_location or ''}}}"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-3">Job Source :</label>
                        <div class="col-lg-3">
                            <input type="text" class="form-control" disabled value="{{{$jobs->job_source or ''}}}"/>
                        </div>
                    </div>



                </div>
            </section>
        </div>
    </div>
</section>






    <section class="wrapper">
        <!-- page start-->

    <div class="row">
	<div class="col-lg-12">
	<div class="col-lg-12 tite2"> <h4 style="float:left;"><b><u>Category : Accounting/Finance </u></b></h4><i class="fa fa-file-pdf-o fa-2x" style="float:right; margin-top:7px;"></i> </div>
    	<div class="col-lg-12 jobb" style="border: 2px solid white;"><br>
		
    		<h5><b>No. of Vacancies :</b><span> 3 </span> </h5><br>
			<h5><b>Job Description / Responsibility :</b></h5><br>
			<ul>
				<li> To look after the total accounts related activities </li>
				
			</ul>
			
			<h5><b>Job Nature :</b><span> Full time </span></h5><br>
			<h5><b>Educational Requirements :</b><span>  </span></h5>
			<ul>
				<li> B.Com./ M.Com. (Accounting) </li>
				<li> CA-CC</li>
				
			</ul>
			
			<h5><b>Experience Requirements:</b><span> </span></h5>
			<ul>
				<li> Minimum 3 year(s) </li>
				<li> The applicants should have experience in the following area(s) : Accounts, Audit, Cash Management, Finance, Internal Audit, Tax (VAT/ Customs Duty/ Income Tax)</li>
			</ul><br>
			
			<h5><b>Additional Job Requirements :</b><span> </span></h5>
			<ul>
				<li> Age 28 to 40 year(s)  </li>
				<li> Only males are allowed to apply. </li>
				<li> Hard working, self-driven and able to perform under pressure; </li>
			</ul><br>
			
			<h5><b>Salary Range :</b><span> Negotiable </span></h5><br>
			<h5><b>Other Benefits:</b><span> As per company policy  </span></h5><br>
			<h5><b>Job Location  :</b><span>Dhaka </span></h5><br>
			
        </div>
	</div>
       
    </div>


        <!-- page end-->
        </section>
    
	
<script type="text/javascript" src="{{$theme}}js/custom/job_management.js"></script>
@stop
