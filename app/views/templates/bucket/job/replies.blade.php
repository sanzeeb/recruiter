@extends('templates.bucket.bucket')

@section('wrapper')

    <section class="wrapper">
        <!-- page start-->

        <!-- page start-->
    {{Helpers::showMessage()}}
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <div class="msg"></div>
                <header class="panel-heading">
                   {{{$jobs->job_title or ''}}}
                    <span class="tools pull-right">

                </header>
            
            <div class="col-lg-12 jobb" style="border: 2px solid white; background:#fff;">
		
    		<h5 style="padding:10px 0px 20px; border-bottom:solid 1px #ededed;"><b>Candidate :</b><span> {{{$candidate->firstname or ''}}} </span> </h5>

            @if(0)
                <form id="replyApplicantFrm" action="{{url('job/emp-reply')}}" method="post" class="form-horizontal">



                <div class="form-group">
                    <label class="col-sm-3 control-label"><b>Replay :</b></label>
                    <div class="col-sm-6">
                        <textarea id="comment" class="form-control " name="reply" required=""></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-3 col-sm-offset-3">
                        <input type="hidden" name="job_id" value="{{$jobs->jobs_id}}"/>
                        <input type="hidden" name="cid" value="{{$candidate->candidate_id}}"/>
                        <button type="submit" class="btn btn-warning">Reply</button>
                    </div>
                </div>

                </form>
            @endif

                <div class="form-group">
                    <label class="col-sm-3 control-label" style="padding-left:0;"><b>Replies :</b></label>
                    <div class="col-sm-6">

                        <?php $interviewReplies =  $replies; ?>
                        @if(count($interviewReplies))
                        @foreach($interviewReplies as $ir)

                        <?php
                        $userObj = User::where('user_id',$ir->replier_id)->where('user_type',$ir->replier_type)->first();
                        $replier = '';
                        if($userObj->user_type == 'Candidate')
                        {
                            $candidate = Candidate::find($userObj->user_id);
                            $replier = $candidate->firstname;
                        }else{
                            $emp = Employer::find($userObj->user_id);
                            $replier = $emp->title;
                        }
                        ?>
                        <i class="fa fa-user"></i>&nbsp;{{$replier}} <small><em>{{Helpers::dateTimeFormat('j F, Y',$ir->created_at)}}</em></small>
                        <p align="justify">{{$ir->reply}}</p>
                        <span style="height:1px; float:left; width:100%; margin:10px 0;background:#eff2f7;"></span>
                        @endforeach
                        @endif

                    </div>
                </div>
			
        </div>





	</div>
       
    </div>


        <!-- page end-->
        </section>
    
	
<script type="text/javascript" src="{{$theme}}js/custom/job_management.js"></script>
<script type="text/javascript">
    $(function(){
        $('a.summary').click(function(){
            var obj = $(this);
            obj.parent().next().slideDown().children('a.details').show();
            obj.parent().hide();
            return false;
        });

        $('a.details').click(function(){
            var obj = $(this);
            obj.parent().slideUp('fast').prev().slideDown();
            return false;
        });

        $("#replyApplicantFrm").submit(function(){
            var comment = $("textarea[name='reply']").val();
            
            var phoneWeburlEmail = /((^[0][1-9]|\d+){6,14}|http:\/\/.+\.([a-z]{3,}|[a-z]{3,}\.[a-z]{2,3})|[a-z]+\.[a-z]{3,}|[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-z]{2,})/

            if(phoneWeburlEmail.test(comment))
            {

                $("textarea[name='reply']").attr('placeholder', 'Sorry! You cannot write any phone no, email or web url').val('');
                return false;
            }  
           
        });

    });
</script>
@stop
