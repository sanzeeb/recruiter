@extends('templates.bucket.bucket')

@section('wrapper')

    <section class="wrapper">
        <!-- page start-->

        <!-- page start-->
    {{Helpers::showMessage()}}
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <div class="msg"></div>
                <header class="panel-heading" style="font-weight:400; font-size:16px;" >
                   {{{$jobs->job_title or ''}}}
                    <span class="tools pull-right">
                    @if($user->user_type != 'Candidate')
                    {{ Form::open(array('url'=>'job/edit/'.$jobs->jobs_id,'method'=>'get')) }}
                        <button class="btn btn-sm btn-info panel-btn" style="margin-top:-5px;" type="submit"><i class="fa fa-pencil"></i> Edit</button>
                    {{ Form::close() }}
                    @endif
                </header>
            
            <div class="col-lg-12 jobb" style="border: 2px solid white; background:#fff;">
		
    		<h5 style="padding:10px 0px 20px; border-bottom:solid 1px #ededed;"><b>No. of Vacancies : </b><span> {{{$jobs->job_vacancies or ''}}} </span> </h5>
			<h5 style="padding:10px 0px 0px;"><b>Job Description / Responsibility :</b></h5>
			<ul style="padding:10px 0px 20px 20px; border-bottom:solid 1px #ededed;">
                            <?php $responsibility = (!empty($jobs->job_responsibility))? json_decode($jobs->job_responsibility) : ''; ?>
                                @if(count($responsibility))
                                    @foreach($responsibility as $res)
                                        				<li> {{{$res or ''}}} </li>
                                                
                                        
                                    @endforeach
                                @endif

				
			</ul>
			
			<h5 style="padding:10px 0px 20px; border-bottom:solid 1px #ededed;"><b>Job Nature : </b><span>  @foreach($job_natures as $k=> $nature)
                                    @if($k == $jobs->job_nature)
                                       {{{$nature}}}
                                    @endif
                                @endforeach </span></h5>
			<h5><b>Educational Requirements : </b><span>  </span></h5>
			<ul style="padding:10px 0px 20px 20px; border-bottom:solid 1px #ededed;">
				<?php $educational = (!empty($jobs->eduactional_requirement))? json_decode($jobs->eduactional_requirement) : ''; ?>
                                @if(count($educational))
                                    @foreach($educational as $edu)
                                        <li>
                                            {{{$edu or ''}}}
                                        </li>
                                    @endforeach
                                @endif
				
			</ul>
			
			<h5><b>Experience Requirements : </b><span> </span></h5>
			<ul style="padding:10px 0px 20px 20px; border-bottom:solid 1px #ededed;">                            
			<?php $experience = (!empty($jobs->experience_requirment))? json_decode($jobs->experience_requirment) : ''; ?>
                            @if(count($experience))
                                @foreach($experience as $exp)                                   
                                        <li>{{{$exp or ''}}}</li>
                                @endforeach
                            @endif
                        </ul>
                        
                     
			
			<h5 style="padding:10px 0px 20px; border-bottom:solid 1px #ededed;"><b>Additional Job Requirements :</b><span></span></h5>
			<ul>
			<?php $extras = (!empty($jobs->extra_job_req))? json_decode($jobs->extra_job_req) : ''; ?>
                            @if(count($extras))
                                @foreach($extras as $extra)
                                <li>{{{$extra or ''}}}</li>
                                @endforeach
                            @endif
                        
                        </ul>

			
			<h5 style="padding:10px 0px 20px; border-bottom:solid 1px #ededed;"><b>Salary Range : </b><span> {{{$jobs->salary_range or ''}}} USD</span></h5>
			<h5 style="padding:10px 0px 20px; border-bottom:solid 1px #ededed;"><b>Other Benefits : </b><span> {{{$jobs->other_benefit or ''}}} </span></h5>
			<h5 style="padding:10px 0px 20px; border-bottom:solid 1px #ededed;"><b>Job Location  : </b><span>{{{$jobs->job_location or ''}}} </span></h5>
            <h5 style="padding:10px 0px 20px; border-bottom:solid 1px #ededed;"><b>Job Source  : </b><span>{{{$jobs->job_source or ''}}}</span></h5>
			
        </div>

        <div class="col-lg-12" style="background: none repeat scroll 0 0 #fff; border: 2px solid white; padding-bottom:20px;text-align:right;">
            
            @if($user->user_type != "Candidate")
               
                <a class="btn btn-success" href="{{url('job/lists')}}">Back</a>
            @endif
        </div>

        @if($user->user_type == 'Candidate')
            @if($applicable == 1)
                <h4 style="margin-left:10px; padding-bottom:10px; color:green;">Application Submitted</h4>
                <form action="{{url('job/candidate-reply')}}" method="post" class="form-horizontal">


                    <?php 
                      $jobCandidateReply =  JobCandidateReply::where('job_id',$jobs->jobs_id)->where('replier_type','Candidate')->where('replier_id',$user->user_id)->count();
                    ?>

                    @if(!$jobCandidateReply)
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Replay : </b></label>
                            <div class="col-sm-6">
                                <textarea id="comment" class="form-control " name="reply" required=""></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3 col-sm-offset-3">
                                <input type="hidden" name="job_id" value="{{$jobs->jobs_id}}"/>
                                <input type="hidden" name="cid" value="{{$user->user_id}}"/>
                                <button type="submit" class="btn btn-warning">Reply</button>
                            </div>
                        </div>
                    @endif
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><b>Applicants : </b></label>
                        <div class="col-sm-6">

                            <?php $interviewReplies =  $replies; ?>
                            @if(count($interviewReplies))
                            @foreach($interviewReplies as $ir)

                            <?php
                            $userObj = User::where('user_id',$ir->replier_id)->where('user_type',$ir->replier_type)->first();
                            $replier = '';
                            if($userObj->user_type == 'Candidate')
                            {
                                $candidate = Candidate::find($userObj->user_id);
                                $replier = $candidate->firstname;
                            }else{
                                $emp = Employer::find($userObj->user_id);
                                $replier = $emp->title;
                            }
                            ?>
                            <i class="fa fa-user"></i>&nbsp;{{$replier}} <small><em>{{Helpers::dateTimeFormat('j F, Y',$ir->created_at)}}</em></small>
                            <p align="justify">{{$ir->reply}}</p>
                            <span style="height:1px; float:left; width:100%; margin:10px 0;background:#eff2f7;"></span>
                            @endforeach
                            @endif

                        </div>
                    </div>
                    <div class="col-lg-12">

                        @if($user->user_type == "Candidate")
                            <a class="btn btn-success" href="{{url('candidate/related-jobs')}}">Back</a>
                        @else
                            <a class="btn btn-success" href="{{url('job/lists')}}">Back</a>
                        @endif
                    </div>
                </form>


            @else
                <div class="col-lg-12">
                        
                    @if($user->user_type == "Candidate")
                        <a class="btn btn-success" href="{{url('candidate/related-jobs')}}">Back</a>
                    @else
                        <a class="btn btn-success" href="{{url('job/lists')}}">Back</a>
                    @endif
                </div>  
                @if($viewType !='view')
                <form id="applyJob" action="{{url('job/apply')}}" method="post">
                    <input type="hidden" name="jobid" value="{{$jobs->jobs_id}}"/>
                    <div class="form-group">
                        <div class="col-lg-offset-5">
                            <a href="#myModal-1" data-toggle="modal" class="btn btn-primary">Apply</a>
                        </div>
                        <br/>
                    </div>

                    <div class="modal fade in" id="myModal-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title">Give Comment </h4>
                                </div>
                                <div class="modal-body form-horizontal">
                                    <div class="msg"></div>




                                        <div class="form-group">
                                            <label class="control-label col-md-3">comment</label>
                                            <div class="col-md-5">
                                                <textarea rows="3" style="resize:none;" name="comment" class='form-control'></textarea>

                                            </div>


                                        </div>

                                        <div class="form-group">
                                            <div class="col-lg-3 col-lg-offset-3">
                                                <button  class="btn btn-success add-time" type="submit">Submit</button>
                                            </div>
                                        </div>


                                </div>

                            </div>
                        </div>
                    </div>
                </form>
                @endif
            @endif
        @else
                <?php $jobCandidates = $jobs->JobCandidate; ?>
                
        @endif


	</div>
       
    </div>


        <!-- page end-->
        </section>
    
	
<script type="text/javascript" src="{{$theme}}js/custom/job_management.js"></script>
<script type="text/javascript">
    $(function(){
        $('a.summary').click(function(){
            var obj = $(this);
            obj.parent().next().slideDown().children('a.details').show();
            obj.parent().hide();
            return false;
        });

        $('a.details').click(function(){
            var obj = $(this);
            obj.parent().slideUp('fast').prev().slideDown();
            return false;
        });
    });

    var phone = /(^[0][1-9]|\d+){6,14}/
    var weburl = /http:\/\/.+\.([a-z]{3,}|[a-z]{3,}\.[a-z]{2,3})/
    var email = /[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-z]{2,}/


    $("#applyJob").submit(function(){
        var comment = $("textarea[name='comment']").val();
        
        var phoneWeburlEmail = /((^[0][1-9]|\d+){6,14}|http:\/\/.+\.([a-z]{3,}|[a-z]{3,}\.[a-z]{2,3})|[a-z]+\.[a-z]{3,}|[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-z]{2,})/

        if(phoneWeburlEmail.test(comment))
        {

            $("textarea").attr('placeholder', 'Sorry! You cannot comment any phone no, email or web url').val('');
            return false;
        }  
       

        
    });


</script>
@stop
