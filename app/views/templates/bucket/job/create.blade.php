@extends('templates.bucket.bucket')

@section('wrapper')

<section class="wrapper">
    <!-- page start-->
    {{Helpers::showMessage()}}
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <div class="msg"></div>
                <header class="panel-heading">
                    CREATE JOB
                </header>
                <div class="panel-body">
                    {{Form::open(array('url'=>'job/save-job', 'id'=>'saveJobFrm', 'class'=>'cmxform form-horizontal'))}}
                        <div class="form-group">
                            <label class="control-label col-lg-3">Job Title : <span class="required">*</span></label>
                            <div class="col-lg-4">
                                <input type="text" class="form-control" name="job_title" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-3">Job Category : <span class="required">*</span></label>
                            <div class="col-lg-4">
                                <select class="form-control-select2" style="width:100%;" name="job_category" required>
                                    <option value="">Select</option>
                                    @if(count($skills))
                                        @foreach($skills as $skill)
                                            <option value="{{{$skill->skill_id}}}">{{{$skill->skill_name}}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-3">No. of Vacancies : <span class="required">*</span></label>
                            <div class="col-lg-1">
                                <input type="text" class="form-control" name="job_vacancies" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-3">Job Nature <span class="required">*</span></label>
                            <div class="col-lg-3">
                                <select class="form-control" name="job_nature" required>
                                    @foreach($job_natures as $k=> $nature)
                                        <option value="{{{$k}}}">{{{$nature}}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-lg-12">
                            <a class="btn btn-primary newAddBtn col-lg-2" style="margin-left:75px;" id="addResponsibilityParam" href="#"><i class="fa fa-plus-circle"> Add More</i></a>
                            <table class="col-lg-7" id="ResponsibilityParamTbl" style="margin-left:23px;">
                                <thead>
                                <tr>
                                    <th style="width:400px;">Responsibilities <span class="required">*</span></th>
                                    <th><i class="fa fa-trash-o"></i></th>
                                </tr>
                                </thead>
                                <tbody>

                                    <td>
                                        <input type="text" name="responsibility[]" class="form-control col-lg-2" required/>
                                    </td>

                                </tbody>
                            </table>
                        </div>

                        <div class="form-group col-lg-12">
                            <a class="btn btn-primary newAddBtn col-lg-2" style="margin-left:75px;" id="addEducationalParam" href="#"><i class="fa fa-plus-circle"> Add More</i></a>
                            <table class="col-lg-7" id="EducationalParamTbl" style="margin-left:23px;">
                                <thead>
                                <tr>
                                    <th style="width:400px;">Educational Requirements <span class="required">*</span></th>
                                    <th><i class="fa fa-trash-o"></i></th>
                                </tr>
                                </thead>
                                <tbody>
                                    <td>
                                        <input type="text" name="educational[]" class="form-control col-lg-2" required/>
                                    </td>
                                </tbody>
                            </table>
                        </div>

                        <div class="form-group col-lg-12">
                            <a class="btn btn-primary newAddBtn col-lg-2" style="margin-left:75px;" id="addExperienceParam" href="#"><i class="fa fa-plus-circle"> Add More</i></a>
                            <table class="col-lg-7" id="ExperienceParamTbl" style="margin-left:23px;">
                                <thead>
                                <tr>
                                    <th style="width:400px;">Experience Requirements <span class="required">*</span></th>
                                    <th><i class="fa fa-trash-o"></i></th>
                                </tr>
                                </thead>
                                <tbody>
                                    <td>
                                        <input type="text" name="experience[]" class="form-control col-lg-2" required/>
                                    </td>
                                </tbody>
                            </table>
                        </div>

                        <div class="form-group col-lg-12">
                            <a class="btn btn-primary newAddBtn col-lg-2" style="margin-left:75px;" id="addAdditionalParam" href="#"><i class="fa fa-plus-circle"> Add More</i></a>
                            <table class="col-lg-7" id="AdditionalParamTbl" style="margin-left:23px;">
                                <thead>
                                <tr>
                                    <th style="width:400px;">Additional Requirements </th>
                                    <th><i class="fa fa-trash-o"></i></th>
                                </tr>
                                </thead>
                                <tbody>
                                    <td>
                                        <input type="text" name="additional[]" class="form-control col-lg-2"/>
                                    </td>
                                </tbody>
                            </table>
                        </div>


                    <div class="form-group">
                        <label class="control-label col-lg-3">Salary Range : <span class="required">*</span></label>
                        <div class="col-lg-4">
                            <input type="number" class="form-control number"  name="salary_range" required/> <small>(Only number, Salary range has to be given into USD)</small>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-3">Other Benefit :</label>
                        <div class="col-lg-4">
                            <textarea class="form-control" name="other_benefit"></textarea>
                        </div>
                    </div>

                        <div class="form-group">
                            <label class="control-label col-lg-3">Job Location : <span class="required">*</span></label>
                            <div class="col-lg-4">
                                <input type="text" class="form-control" name="job_location" required/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-3">Job Source :</label>
                            <div class="col-lg-4">
                                <input type="text" class="form-control" name="job_source" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-3">Last Application Date : <span class="required">*</span></label>
                            <div class="col-lg-4">
                                <input type="text" class="form-control" name="last_date" required />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-3 col-lg-offset-3">
                                <input type="submit" value="Save" class="btn btn-primary" />
                            </div>
                        </div>
                    {{Form::close()}}
                </div>
            </section>
        </div>
    </div>
</section>
<script type="text/javascript">    
	$(function(){   
            $("input[name='last_date'],input[name='created_at']").datepicker({
                changeMonth: true, 
                yearRange: '-15:+15',               
                changeYear: true
            });
        });	
</script>
<script type="text/javascript" src="{{$theme}}js/custom/job_management.js"></script>

@stop
