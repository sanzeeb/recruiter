@extends('templates.bucket.bucket')

@section('wrapper')

<section class="wrapper">
    <!-- page start-->
    {{Helpers::showMessage()}}
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <div class="msg"></div>
                <header class="panel-heading">
                    Edit JOB @if($applied)<strong style="color:tomato;float:right;font-weight:400;"> Candidate applied to this job, so it is not editable you can only change last apply date</strong> @endif
                </header>
                <div class="panel-body">
                    {{Form::open(array('url'=>'job/update-job', 'id'=>'saveJobFrm', 'class'=>'cmxform form-horizontal'))}}

                    <div class="form-group">
                        <label class="control-label col-lg-3" style="padding-top:0px;">Job Title :</label>
                        <div class="col-lg-4" style="padding-left:0;">
                            @if($applied)
                                {{$jobs->job_title}}
                            @else
                            <input type="text" class="form-control" name="job_title" value="{{{$jobs->job_title or ''}}}" required/>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3" style="padding-top:0px;">Job Category :</label>
                        <div class="col-lg-4" style="padding-left:0;">
                             @if($applied)
                                @if(count($skills))
                                @foreach($skills as $skill)
                                     @if($skill->skill_id == $jobs->skill_id)
                                     {{{$skill->skill_name}}}
                                     @endif
                                @endforeach
                                @endif
                             @else
                            <select class="form-control-select2" style="width:100%;" name="job_category" required>
                                <option value="" >Select</option>
                                @if(count($skills))
                                    @foreach($skills as $skill)
                                        @if($skill->skill_id == $jobs->skill_id)
                                            <option selected="selected" value="{{{$skill->skill_id}}}">{{{$skill->skill_name}}}</option>
                                        @else
                                            <option value="{{{$skill->skill_id}}}">{{{$skill->skill_name}}}</option>
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3" style="padding-top:0px;">No. of Vacancies :</label>
                        <div class="col-lg-1" style="padding-left:0;">
                            @if($applied)
                                {{{$jobs->job_vacancies}}}
                            @else
                            <input type="text" class="form-control" name="job_vacancies" value="{{{$jobs->job_vacancies or ''}}}" required/>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3" style="padding-top:0px;">Job Nature :</label>
                        <div class="col-lg-3" style="padding-left:0;">
                            @if($applied)
                                @foreach($job_natures as $k=> $nature)
                                    @if($k == $jobs->job_nature)
                                        {{{$nature}}}
                                    @endif
                                @endforeach
                            @else
                                <select class="form-control" name="job_nature" required>
                                    @foreach($job_natures as $k=> $nature)
                                        @if($k == $jobs->job_nature)
                                            <option selected="selected" value="{{{$k}}}">{{{$nature}}}</option>
                                        @else
                                            <option value="{{{$k}}}">{{{$nature}}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            @endif
                        </div>
                    </div>
                    <div class="form-group col-lg-12">
                        @if(!$applied)
                        <a class="btn btn-primary newAddBtn col-lg-2" style="margin-left:75px;" id="addResponsibilityParam" href="#"><i class="fa fa-plus-circle"> Add More</i></a>
                        @endif
                        <table class="col-lg-12" id="ResponsibilityParamTbl">
                            <thead class="col-lg-3">
                            <tr style="float: right;">
                                <th class="control-label">Responsibilities :</th>
                                @if(!$applied)<th><i class="fa fa-trash-o"></i></th>@endif
                            </tr>
                            </thead>
                            <tbody class="col-lg-7" style="padding-left:0;">
                            <?php $responsibility = (!empty($jobs->job_responsibility))? json_decode($jobs->job_responsibility) : ''; ?>

                                @if(count($responsibility))
                                    @foreach($responsibility as $res)
                                        <tr>
                                                <td class="control-label">
                                                    @if($applied)
                                                        {{$res or ''}}
                                                    @else
                                                        <input type="text" class="form-control col-lg-2" name="responsibility[]" value="{{{$res or ''}}}"/>
                                                    @endif
                                                </td>
                                                @if(!$applied)
                                                <td>
                                                    
                                                    <a class="delParam" href="#"><i class="fa fa-minus-circle"></i></a>
                                                    
                                                </td>
                                                @endif
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="form-group col-lg-12">
                        @if(!$applied)<a class="btn btn-primary newAddBtn col-lg-2" style="margin-left:75px;"  id="addEducationalParam" href="#"><i class="fa fa-plus-circle"> Add More</i></a>@endif
                        <table class="col-lg-12" id="EducationalParamTbl">
                            <thead class="col-lg-3">
                            <tr style="float: right;">
                                <th class="control-label">Educational Requirements :</th>
                                 @if(!$applied)<th><i class="fa fa-trash-o"></i></th>@endif
                            </tr>
                            </thead>
                            <tbody class="col-lg-7" style="padding-left:0;">
                            <?php $educational = (!empty($jobs->eduactional_requirement))? json_decode($jobs->eduactional_requirement) : ''; ?>
                            @if(count($educational))
                                @foreach($educational as $edu)
                                    <tr>
                                       <td class="control-label">
                                            @if($applied)
                                                {{$edu or ''}}
                                            @else
                                            <input type="text" class="form-control col-lg-2" name="educational[]" value="{{{$edu or ''}}}"/>
                                            @endif
                                        </td>
                                        @if(!$applied)
                                        <td><a class="delParam" href="#"><i class="fa fa-minus-circle"></i></a></td>
                                        @endif
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="form-group col-lg-12">
                        @if(!$applied)<a class="btn btn-primary newAddBtn col-lg-2" style="margin-left:75px;" id="addExperienceParam" href="#"><i class="fa fa-plus-circle"> Add More</i></a>@endif
                        <table class="col-lg-12" id="ExperienceParamTbl">
                            <thead class="col-lg-3">
                            <tr style="float: right;">
                                <th class="control-label">Experience Requirements :</th>
                                @if(!$applied)<th><i class="fa fa-trash-o"></i></th>@endif
                            </tr>
                            </thead>
                            <tbody class="col-lg-7" style="padding-left:0;">
                            <?php $experience = (!empty($jobs->experience_requirment))? json_decode($jobs->experience_requirment) : ''; ?>
                            @if(count($experience))
                                @foreach($experience as $exp)
                                    <tr>
                                        <td class="control-label">
                                            @if($applied)
                                                {{$exp or ''}}
                                            @else
                                            <input type="text" class="form-control col-lg-2" name="experience[]" value="{{{$exp or ''}}}"/>
                                            @endif
                                        </td>
                                        @if(!$applied)
                                        <td><a class="delParam" href="#"><i class="fa fa-minus-circle"></i></a></td>@endif
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="form-group col-lg-12">
                        @if(!$applied)<a class="btn btn-primary newAddBtn col-lg-2" style="margin-left:75px;" id="addAdditionalParam" href="#"><i class="fa fa-plus-circle"> Add More</i></a>@endif
                        <table class="col-lg-12" id="AdditionalParamTbl">
                            <thead class="col-lg-3">
                            <tr style="float: right;">
                                <th class="control-label">Additional Requirements :</th>
                                @if(!$applied)<th><i class="fa fa-trash-o"></i></th>@endif
                            </tr>
                            </thead>
                            <tbody class="col-lg-7" style="padding-left:0;">
                            <?php $extras = (!empty($jobs->extra_job_req))? json_decode($jobs->extra_job_req) : ''; ?>
                            @if(count($extras))
                                @foreach($extras as $extra)
                                    <tr>
                                        <td class="control-label">
                                            @if($applied)
                                                {{$extra}}
                                            @else
                                                <input type="text" class="form-control col-lg-2" name="additional[]" value="{{{$extra}}}"/>
                                            @endif
                                        </td>
                                        @if(!$applied)
                                        <td><a class="delParam" href="#"><i class="fa fa-minus-circle"></i></a></td>
                                        @endif
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-3" style="padding-top:0px;">Salary Range :</label>
                        <div class="col-lg-4" style="padding-left:0;">
                            @if($applied)
                                {{$jobs->salary_range or ''}} USD
                            @else
                                <input type="number" class="form-control number" name="salary_range" value="{{{$jobs->salary_range or ''}}}" required/> <small>(Only number, Salary range has to be given into USD)</small>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-3" style="padding-top:0px;">Other Benefit :</label>
                        <div class="col-lg-4" style="padding-left:0;">
                            @if($applied)
                                {{$jobs->other_benefit}}
                            @else
                                <textarea class="form-control" name="other_benefit">{{{$jobs->other_benefit or ''}}}</textarea>
                            @endif
                        </div>
                    </div>



                    <div class="form-group">
                        <label class="control-label col-lg-3" style="padding-top:0px;">Job Location :</label>
                        <div class="col-lg-4" style="padding-left:0;">
                            @if($applied)
                                {{{$jobs->job_location}}}
                            @else
                                <input type="text" class="form-control" name="job_location" value="{{{$jobs->job_location or ''}}}" required/>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-3" style="padding-top:0px;">Job Source :</label>
                        <div class="col-lg-4" style="padding-left:0;">
                            @if($applied)
                                {{{$jobs->job_source}}}
                            @else
                            <input type="text" class="form-control" name="job_source" value="{{{$jobs->job_source or ''}}}" />
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-3" style="padding-top:0px;">Last Application Date : <span class="required">*</span></label>
                        <div class="col-lg-4" style="padding-left:0;">
                            <input type="text" class="form-control" name="last_date" value="{{Helpers::dateTimeFormat('m/d/Y',$jobs->last_date)}}" required />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-4 col-lg-offset-3" style="padding-left:0;">
                            <input type="hidden" name="job_id" value="{{{$jobs->jobs_id}}}"/>
                            <input type="submit" value="Save" class="btn btn-primary" />
                            <a class="btn btn-danger" href="{{url('job/lists')}}">Cancel</a>
                        </div>
                    </div>
                    {{Form::close()}}
                </div>
            </section>
        </div>
    </div>
</section>
<script type="text/javascript">    
	$(function(){   
            $("input[name='last_date'],input[name='created_at']").datepicker({
                changeMonth: true,
				yearRange: '-15:+15',
                changeYear: true
            });
        });	
</script>

<script type="text/javascript" src="{{$theme}}js/custom/job_management.js"></script>
@stop
