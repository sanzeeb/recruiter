@extends('templates.bucket.bucket')

@section('wrapper')
<style type="text/css">
    #searchBtn{float:right; top:125px; right:30px;}
</style>
<section class="wrapper" ng-app="main">
    <!-- page start-->
    {{Helpers::showMessage()}}
    <div class="row" ng-controller="DemoCtrl">
        <div class="col-lg-12">
            <section class="panel">
                <div class="msg"></div>
                <header class="panel-heading" style="font-weight:400; font-size:16px;">
                    Job Lists
                    <span class="tools pull-right">

                            {{ Form::open(array('url'=>'job/create','method'=>'get')) }}
                                <button class="btn btn-primary panel-btn" style="margin-top:-5px;" type="submit"><i class="fa fa-plus-circle"></i> Add New</button>
                            {{ Form::close() }}
                         </span>
                </header>
                <div class="panel-body">
                    @if(count($jobs))
                    <button id="searchBtn" ng-click="search()" class="btn btn-xs btn-info">Search By Date</button>
                    <table ng-table="tableParams" show-filter="true" class="table ng-table-responsive">
                    <tr ng-repeat="job in $data" >
                        
                        <td width="10" align="center" data-title="'Sl'" >@{{($index+1)}}</td>
                        <td align="center" width="350" data-title="'Title'" sortable="'job_title'" filter="{'job_title':'text'}">@{{job.job_title}}</td>
                        <td align="center" width="140" data-title="'Job Category'" filter="{ 'job_category': 'select' }" filter-data="categories($column)">@{{job.job_category}}</td>
                        <td align="center" width="80" data-title="'Vacancies'" sortable="'job_vacancies'" filter="{'job_vacancies':'text'}">@{{job.job_vacancies}}</td>
                        <td align="center" width="100" data-title="'Job Applied'" sortable="'job_applied'" filter="{'job_applied':'text'}">@{{job.job_applied}}</td>
                        <td align="center" width="100" data-title="'Last Date'" sortable="'last_date'"  filter="{'last_date':'text'}">@{{job.last_date}}</td>
                        <td width="80" data-title="'Actions'">
                            <a target="_blank" href="{{url('job/edit')}}/@{{job.jobs_id}}"class="grid-action-link job_edit"><i class="fa fa-pencil" title="Edit"></i></a>
                            <a target="_blank" href="{{url('job/view/')}}/@{{job.jobs_id}}" class="grid-action-link job_view"><i class="fa fa-eye" title="View"></i></a>
                            <a  href="@{{job.jobs_id}}" class="grid-action-link job_del"><i class="fa fa-trash-o" title="Delete"></i></a>
                            <a  href="{{url('job/applicants/')}}/@{{job.jobs_id}}" class="grid-action-link"><i class="fa fa-user" title="Applicants"></i></a>
                        </td>
                    </tr>
                </table>
                @else
                    <!-- here will be image for no job available -->
                    <div style="width:100%;padding:35px 0; text-align:center;"><img src="{{$theme}}images/no-jobs.png" /></div>
                    
                @endif
                </div>
            </section>
        </div>
    </div>
</section>
<script type="text/javascript" src="{{$theme}}js/custom/job_management.js"></script>
<script type="text/javascript">
 var app;
 
        $(function(){   
            $("input[name='last_date'],input[name='created_at']").datepicker({
                changeMonth: true,
                dateFormat:"yy-mm-dd",
				yearRange: '-15:+15',
                changeYear: true,
                onSelect: function() {
                    $("#searchBtn").addClass("po-show");
                }
            });
        });

   
        app = angular.module('main', ['ngTable']).controller('DemoCtrl', function($scope, $http, $filter, $q, NgTableParams) {

       

        var data = {{json_encode($jobs)}};
        

        $scope.search = function(){
            var searchDate = angular.element('input[name="last_date"]').val();

            $http({
                    url : BASE + 'job/get_job_by_date',
                    method: "POST",
                    data: {'date':searchDate}
                }).success(function(response,status){
                    data = response;
                    angular.element('#searchBtn').removeClass('po-show');
                    
                    $scope.tableParams.reload();
                });
            
        }

        $scope.tableParams = new NgTableParams({
            page: 1,            // show first page
            count: 10           // count per page
        }, {
            total: data.length, // length of data
            getData: function($defer, params) {
                // use build-in angular filter
                var orderedData = params.sorting() ?
                        $filter('orderBy')(data, params.orderBy()) :
                        data;

                orderedData = params.filter() ?
                        $filter('filter')(orderedData, params.filter()) :
                        orderedData;

                params.total(orderedData.length); // set total for recalc pagination
                $defer.resolve($scope.users = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        });

        $scope.checkId = function()
        {

            console.log(angular.element(this));
        };

        var inArray = Array.prototype.indexOf ?
                function (val, arr) {
                    return arr.indexOf(val)
                } :
                function (val, arr) {
                    var i = arr.length;
                    while (i--) {
                        if (arr[i] === val) return i;
                    }
                    return -1
                };
            $scope.categories = function(column) {
                var def = $q.defer(),
                    arr = [],
                    categories = [];
                angular.forEach(data, function(item){
                  
                    if (inArray(item.job_category, arr) === -1) {
                        arr.push(item.job_category);
                        categories.push({
                         'id': item.job_category,
                         'title': item.job_category
                        });
                    }
                });
                def.resolve(categories);
                return def;
            };
        });
</script>
@stop