@extends('templates.bucket.bucket')

@section('wrapper')
<section class="wrapper">
{{{Helpers::showMessage()}}}
<!--mini statistics start-->
<div class="row">

<div class="col-sm-12">
<section class="panel">
     <div class="panel-body">
         <?php $rank=1; ?>
         @foreach($expertises as $j=> $expert)
         <?php $candidate = Candidate::where('cv_tbl_id', $expert->cv_tbl_id)->first(); ?>
     	<div class="top-engi-box">
                    <span>
                        <div class="left">
                            <span><b>Name : </b>                                
                             <a href="">{{$candidate->firstname}}</a>
                            </span>
                            <span><b>Rank : </b> <strong>{{$rank}} <?php $rank++; ?></strong></span><span><b>Age : </b> <strong>{{Candidate::getAge($candidate->dob)}}</strong></span>
                            <span><b>Year of exp :</b> <strong>: {{$expert->year_of_exp}}</strong></span>
                        </div>
                            <div class="mid"><p>A Computer Science and Engineering graduate have more than four years? of experience in related field. Currently working as a Software Engineer in an IT based organization and core responsibility is ensuring the quality of coding user interface design and development projects using technologies such as HTML5, CSS3, html,  CSS, JavaScript library (jQuery), PHP (CakePHP), Codeigniter, Yii, Zend, XML, Joomla, Wordpress, MySQL. Previously worked as a web developer where there was an involvement in Interface design, documentation, implementation and support functions, generated projects more than ten. Also has knowledge on Joomla structure and coding.</p></div>
                            <div class="right">
                                <p><a class="push_button" href="">Favorite</a></p>
                                <p><a class="push_button2" href="">Inquery</a></p>
                                <p><a class="push_button3" href="">Interview</a></p>
                                <p><a class="push_button4" href="">Source Code</a></p>
                                <p><a class="push_button5" href="">Test Project</a></p>
                                <p><a class="push_button6" href="">Academic Paper</a></p>
                            </div>
                    </span>
                </div>
         @endforeach

     </div>
   </section>
</div>

     <table class="searchResult display table table-bordered table-striped" id="dynamic-table">
                        <thead>
                            <tr>

                                <th class="hidden-phone">Photo</th>
                                <th>Details</th>
                                <th>Rank</th>
                                <th class="hidden-phone">Action</th>

                            </tr>
                        </thead>
                        <tbody>
                        <?php $rank=1; ?>
                @foreach($expertises as $j=> $expert)


<tr>

<td class="">
 @if(!empty($expert->photo) && file_exists('data/profile/'.$expert->photo))
<img class="photo" alt="{{{$expert->photo or ''}}}" src="{{{url('/')}}}/data/profile/{{{$expert->photo}}}">
@else
<img  class="photo" alt="thumbnail" src="{{$theme}}images/placeholder.gif"/>
@endif
</td>
<td class=""><span class="name">
 <?php $candidate = Candidate::where('cv_tbl_id', $expert->cv_tbl_id)->first(); ?>
                       
<strong>{{$candidate->firstname }}</strong></span><div class="starRate"><span class="dsg"><?php
                                $title = CvJob::where('cv_tbl_id', $expert->cv_tbl_id)->first();
                                
                                ?>{{{$title->designation or ''}}} </span>
    
    <?php                          $agreements=  Agreements::where("receiver_id",$expert->id)
                                ->where("status","Accepted")->where("rating",'!=',"")->get();
    
    
     $countAgr=  count($agreements);
                        $rating=0;
                        
                foreach ($agreements as $agr){
                    $rating+=$agr->rating;
                        }
                        
                        if($countAgr>0&& $rating!=0){
                            $rating=ceil($rating/$countAgr);
                        }

                    
 ?>
    Feedback:<ul>
        <?php for($j=5;$j>=1;$j--){
                                if($j<=$rating){ ?>
<li><a class="selected" href="#"><span></span></a></li>
        <?php  }else { ?>
        <li><a  href="#"><span></span></a></li>
        <?php              } }?>
</ul><br/>

<?php   
     $skills = CvSpecialization::where('cv_tbl_id', $expert->cv_tbl_id)->get();
              
          $countSkill=  count($skills);
                        $djrating=0;
                        
     foreach($skills as $skill){
         
         if($skill->rating!=0){
            $djrating+=$skill->rating;
         }else{
           $djrating+=0;
         }

                                                 
     }  
    // echo "t:".$djrating;
      //    echo "coun:".$countSkill;
     
     if($countSkill>0){
        $djrating=ceil($djrating/$countSkill);
     }
     
     
?>
    DJIT:<ul>
        <?php for($j=5;$j>=1;$j--){ 
                         
 if($j<=$djrating){ ?>
<li><a class="selected" href="#"><span></span></a></li>
        <?php  }else { ?>
        <li><a  href="#"><span></span></a></li>
        <?php              } }?>
        
</ul>
</div><strong class="skills">Skills</strong>
<ul class="p-skills">
   <?php
                              //  echo implode(",",$skills);
                              $skillar=array();
                              
                              if(count($skills)>0){
                              foreach($skills as $skill){
                            //  array_push($skillar,$skill->key);
                              if($skill->key=="microcontroller_programming"){
                              ?>
                              <li>micro. programming</li>
                              <?php }
                              
                              else{
                                ?>
                                <li>{{$skill->key}}</li>
                                <?php } 
                              }
                                }?>
</ul>
</td>
<td class="">
    <span class="rank">Rank : ({{$rank}}) <?php $rank++; ?></span>
    <span class="yearofexp">Exp. year:{{$expert->year_of_exp or ''}}</span>

<a target="_blank" style="margin-top:3px;" class="btn btn-send" href="{{{url('feedback/index')}}}/{{$expert->cv_tbl_id}}">Feedback</a>'
</td>
<td class=" sorting_1">
    <a target="_blank" style="margin-right:5px;" class="btn btn-primary view-icon"  href="{{{url('resume/display-resume/')}}}/{{$expert->cv_tbl_id}}">View</a>
    <a data-toggle="modal" style="margin-top:3px;" href="#myModal-1" data-id="{{$expert->id}}" class="btn btn-success offer offer-now">Offer now</a>
    <form target="_blank" method="post" action="{{{url('resume/pdf/')}}}" class="col-sm-12"  style="margin-top:3px;padding:0px;" >
        <input type="hidden" name="cv_tbl_id" value="{{$expert->cv_tbl_id}}">
        <button type="submit" title="PDF Download" class="btn btn-primary col-lg-12" style="margin-right:5px;">
            <i class="fa fa-download"></i>
        </button>
    </form>
</td>
</tr>
@endforeach
                    </tbody>
            </table>


    
</div>

    <div class="modal fade in" id="myModal-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Agreement form </h4>
                </div>
                <div class="modal-body">
                    <div class="msg"></div>
                    <form role="form" id='agreement-form' class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-md-3">Title</label>
                            <div class="col-md-4 col-xs-11">
                                <input type="hidden" name="candidate_id" id="candidate_id"  />
                                <input type="text" name="etitle" placeholder="Enter title" id="etitle" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Amount</label>
                            <div class="col-md-4 col-xs-11">
                                <input type="text" name="eamount" placeholder="Enter amount" id="eamount" class="form-control">

                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3">Description</label>
                            <div class="col-md-5">
                                <textarea rows="7" class='form-control' id='edescription' name='edescription'></textarea>

                            </div>

                            <button  class="btn btn-success add-time sent-agrrement" type="button">Confirm</button>
                        </div>


                    </form>

                </div>

            </div>
        </div>
    </div>

</section>

<!--dynamic table-->
<script type="text/javascript" language="javascript" src="{{$theme}}assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="{{$theme}}assets/data-tables/DT_bootstrap.js"></script>

<!--dynamic table initialization -->
<script src="{{$theme}}js/dynamic_table/dynamic_table_init.js"></script>
<script type="text/javascript" src="{{$theme}}js/custom/cvmanagement.js"></script>


@stop