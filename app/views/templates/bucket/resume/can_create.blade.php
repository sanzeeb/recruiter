@extends('templates.bucket.bucket')

@section('wrapper')

<section class="wrapper">
    <!-- page start-->
    {{Helpers::showMessage()}}
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <div class="msg"></div>
                <header class="panel-heading">
                    CREATE RESUME
                </header>
                <div class="panel-body">
                    {{Form::open(array('id'=>'form-2','url'=>'resume/candidate-resume-save','class'=>'cmxform form-horizontal','enctype'=>'multipart/form-data','method'=>'post'))}}
                    <div id="wizard">

                        <h2>Personal Detail</h2>

                        <section>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Name <span class="text-danger">*</span></label>
                                <div class="col-lg-5">
                                    <input type='hidden' value='{{{$candidate->candidate_id}}}' name='candidate_id' />
                                    <input type="text" name="name" value="{{{$candidate->firstname or ''}}}&nbsp;{{{$candidate->lastname or ''}}} " class="form-control" title="Name" required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-3">Total Year of Experience <span class="text-danger">*</span></label>
                                <div class="col-lg-2">
                                    <input type="text" name="year_of_exp" title="Total Work Experience" class="form-control" required/>
                                </div>
                            </div>
                            <hr/>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Father's Name <span class="text-danger">*</span></label>
                                <div class="col-lg-5">
                                    <input type="text" name="father_name" class="form-control" title="Father's Name" required/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Mother's Name <span class="text-danger">*</span></label>
                                <div class="col-lg-5">
                                    <input type="text" name="mother_name" class="form-control" title="Mother's Name" required/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Date of Birth <span class="text-danger">*</span></label>
                                <div class="col-lg-3">
                                    <input type="text" name="dob" class="form-control" title="Date of Birth" required/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Gender <span class="text-danger">*</span></label>
                                <div class="col-lg-8">
                                    <select name="gender" class="form-control-select2" required>
                                        @foreach($genders as $g)
                                        @if($candidate->gender == $g)
                                        <option selected="selected" value="{{$g}}">{{$g}}</option>
                                        @else
                                        <option value="{{$g}}">{{$g}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Marital Status </label>
                                <div class="col-lg-8">
                                    <select name="marital_status" class="form-control-select2">
                                        @foreach($marital_status as $mt)
                                        <option value="{{$mt}}">{{$mt}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>



                            <div class="form-group">
                                <label class="col-lg-3 control-label">Nationality</label>
                                <div class="col-lg-3">
                                    <input type="text" name="nationality" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Religion</label>
                                <div class="col-lg-3">
                                    <input type="text" name="religion" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Present Address</label>
                                <div class="col-lg-8">
                                    <input type="text" name="present_address" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Permanent Address</label>
                                <div class="col-lg-8">
                                    <input type="text" name="permanent_address" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Home Phone</label>
                                <div class="col-lg-3">
                                    <input type="text" name="home_phone" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Mobile <span class="text-danger">*</span></label>
                                <div class="col-lg-3">
                                    <input type="text" name="mobile" class="form-control" title="Mobile number"  required/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Email</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control" value="{{{$candidate->email or ''}}}" name="email" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Alternate Email</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control" name="alternate_email" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Photo</label>
                                <div class="controls col-md-2">
                                    <div class="fileupload-new thumbnail">
                                        <img alt="" src="{{$theme}}images/placeholder.gif"/>

                                    </div>
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <span class="btn btn-white btn-file">
                                            <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Browse file</span>
                                            <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                            <input type="file" class="default" name="photo"/>
                                        </span>
                                        <span class="fileupload-preview" style="margin-left:5px;"></span>
                                        <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <h2>Education Info</h2>
                        <section>

                            <h4>Additional Info</h4>
                            <div class="form-group col-lg-12">
                                <a class="btn btn-primary newAddBtn" id="addEduParam" href="#"><i class="fa fa-plus-circle"> Add More</i></a>
                                <table width="100%" id="eduParamTbl">
                                    <thead>
                                        <tr>
                                            <th>Exam Title</th>
                                            <th>Major</th>
                                            <th>Institution</th>
                                            <th>Result</th>
                                            <th>Passing Year</th>
                                            <th><i class="fa fa-trash-o"></i></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <tr>
                                            <td><input type="text" name="title[]" class="form-control col-lg-2" value=""/></td>
                                            <td><input type="text" name="major[]" class="form-control col-lg-2" value=""/></td>
                                            <td><input type="text" name="institute[]" class="form-control col-lg-2" value=""/></td>
                                            <td><input type="text" name="result[]" class="form-control col-lg-2" value=""/></td>
                                            <td><input type="text" name="passing_year[]" class="form-control col-lg-2" value=""/></td>
                                            <td><a class="delParam" href="#"><i class="fa fa-minus-circle"></i></a></td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>

                        </section>

                        <h2>Work Experience</h2>
                        <section>

                            <h4>Work Info</h4>
                            <div class="form-group col-lg-12">

                                <a class="btn btn-primary newAddBtn" id="addWorkParam" href="#"><i class="fa fa-plus-circle"> Add More</i></a>
                                <table width="100%" id="workParamTbl">
                                    <thead>
                                        <tr>
                                            <th>Job Title</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Company Name</th>
                                            <th>Designation</th>
                                            <th>Department</th>
                                            <th><i class="fa fa-trash-o"></i></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <tr>
                                            <td><input type="text" name="job_title[]" class="form-control col-lg-2" value=""/></td>
                                            <td><input type="text" name="start_dt[]" class="form-control col-lg-2" value=""/></td>
                                            <td><input type="text" name="end_dt[]" class="form-control col-lg-2" value=""/></td>
                                            <td><input type="text" name="company[]" class="form-control col-lg-2" value=""/></td>
                                            <td><input type="text" name="designation[]" class="form-control col-lg-2" value=""/></td>
                                            <td><input type="text" name="department[]"class="form-control col-lg-2" value=""/></td>
                                            <td><a class="delParam" href="#"><i class="fa fa-minus-circle"></i></a></td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>

                        </section>

                        <h2>Others</h2>
                        <section>
                            <h4>Professional Qualification</h4>
                            <div class="form-group col-lg-12">
                                <a class="btn btn-primary newAddBtn" id="addPqParam" href="#"><i class="fa fa-plus-circle"> Add More</i></a>
                                <table width="100%" id="pqParamTbl">
                                    <thead>
                                        <tr>
                                            <th>Qualification Title</th>
                                            <th>Institution</th>
                                            <th>Finished Year</th>
                                            <th><i class="fa fa-trash-o"></i></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <tr>
                                            <td><input type="text" name="q_title[]" class="form-control col-lg-2" value=""/></td>
                                            <td><input type="text" name="q_institute[]" class="form-control col-lg-2" value=""/></td>
                                            <td><input type="text" name="q_year[]" class="form-control col-lg-2" value=""/></td>
                                            <td><a class="delParam" href="#"><i class="fa fa-minus-circle"></i></a></td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>

                            <h4>Specialization</h4>

                            <table width="100%" id="specTbl">
                                <thead>
                                    <tr>
                                        <th>Technologies</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($specs as $spec)
                                    <tr>
                                        <td><input type="checkbox" name="specs[{{str_replace(' ','_',$spec)}}]"/> {{{$spec}}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <br/><br/>
                            <input type="submit" class="btn btn-primary" value="Save"/>

                        </section>
                    </div>
                    {{Form::close()}}
                </div>
            </section>
        </div>
    </div>
</section>
<script src="{{$theme}}assets/jquery-steps-master/build/jquery.steps.js"></script>
<script src="{{$theme}}js/custom/Validate.js"></script>
<script src="{{$theme}}js/custom/cvmanagement.js"></script>
<script type="text/javascript">
    $(function() {});
</script>
<script type="text/javascript">    
	$(function(){   
            $("input[name='dob'],input[name='last_date'],input[name='created_at']").datepicker({
                changeMonth: true,
                dateFormat:"yy-mm-dd",
                changeYear: true
            });
        });
	
</script>
@stop