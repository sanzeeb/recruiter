@extends('templates.bucket.bucket')

@section('wrapper')





<section class="wrapper">
    <!-- page start-->

    <div class="row">
        <div class="col-lg-12">
        {{Helpers::showMessage()}}
        <section class="panel">

           <header class="panel-heading"  style="display:inline-block; width:100%;">
                           <form target="_blank" method="post" action="{{url('resume/pdf')}}" style="float:left;">
                            <input type="hidden" name="cv_tbl_id" value="{{$candidate->cv_tbl_id}}"/><button type="submit" title="PDF Download" class="btn btn-primary" style="margin-right:5px;"><i class="fa fa-download"></i> Download Resume</button>
                           </form>
                            @if($user->user_type != "Candidate")
                               @if($user->user_type != 'Admin')
                                   <form target="_blank" method="post" action="{{url('resume/add-to-favorite')}}" style="float:left; width:140px; margin-right:5px;">
                                       <input type="hidden" name="cid" value="{{$candidate->candidate_id}}"/>
                                       <input type="hidden" name="cv_tbl_id" value="{{$candidate->cv_tbl_id}}"/><button type="submit" title="Add to favorite" class="push_button11" style="margin-right:5px;">Favourite</button>
                                   </form>

                                   <form target="_blank" method="post" action="{{url('inquiry/create/'.$candidate->candidate_id)}}" style="float:left; width:140px; margin-right:5px;">
                                       <input type="hidden" name="cv_tbl_id" value="{{$candidate->cv_tbl_id}}"/>
                                       <input type="hidden" name="job_id" value="{{$job->jobs_id or ''}}"/>
                                       <button type="submit" title="Inquery" class="push_button22" style="margin-right:5px;">Inquiry</button>
                                   </form>
                                   <form target="_blank" method="post" action="{{url('interview/create/'.$candidate->candidate_id)}}" style="float:left; width:140px; margin-right:5px;">
                                       <input type="hidden" name="cv_tbl_id" value="{{$candidate->cv_tbl_id}}"/>
                                       <input type="hidden" name="job_id" value="{{$job->jobs_id or ''}}"/>
                                       <button type="submit" title="Interview" class="push_button33" style="margin-right:5px;">Interview</button>
                                   </form>
                                    <form target="_blank" method="post" action="{{url('tproject/create/'.$candidate->candidate_id)}}" style="float:left;margin-right:5px;">
                                       <input type="hidden" name="cv_tbl_id" value="{{$candidate->cv_tbl_id}}"/>
                                       <input type="hidden" name="job_id" value="{{$job->jobs_id or ''}}"/>
                                       <button type="submit" title="Test Project" class="push_button55" style="margin-right:5px;">Test Project</button>
                                   </form>
                               @endif
                           @if(!empty($candidate->source_code))
                                <?php 
                                        $sourceLink = '';
                                        
                                        if(preg_match('/http/',$candidate->source_code))
                                            $souceLink  = $candidate->source_code;
                                        else
                                            $sourceLink = 'http://'.$candidate->source_code; 
                                    ?>
                                <a href="{{url($sourceLink)}}" target="_blank" class="push_button44" style="float:left; width:140px; margin-right:5px;">Source Code</a>

                           @endif
                           @if(!empty($candidate->academic_certificate) && File::exists('data/academic_certificate/'.$candidate->academic_certificate))
                           <a target="_blank" href="{{url('data/academic_certificate/'.$candidate->academic_certificate)}}" class="push_button66" style="float:left; width:180px; margin-right:5px;">Academic Certificate</a>
                           @endif
                           <!--<form target="_blank" method="post" action="#" style="float:left;margin-right:5px;">
                               <input type="hidden" name="cv_tbl_id" value="{{$candidate->cv_tbl_id}}"/><button type="submit" title="Source Code" class="push_button44" style="margin-right:5px;">Source Code</button>
                           </form>
                           <form target="_blank" method="post" action="#" style="float:left;margin-right:5px;">
                               <input type="hidden" name="cv_tbl_id" value="{{$candidate->cv_tbl_id}}"/><button type="submit" title="Academic Paper" class="push_button66" style="margin-right:5px;">Academic Paper</button>
                           </form>-->
                           @endif
            </header>
                        
            <div class="col-lg-12" style="background-color:#FFF; padding:0;" >

                <div class="col-lg-12" style="padding:0;">
                    <div class="col-lg-8">
                        <h3 class="h3">{{{$cvInfo->name or ''}}}</h3>
                        
                        @if($user->user_type!="Employer")
                        <address>Address: {{{$candidate->current_address or ''}}} <br>
                            Home Phone: {{{$candidate->phone or ''}}}<br>
                            <!--Office Phone :058148544554 <br>-->
                            <?php $user = User::where('user_id',$candidate->candidate_id)->where('user_type','Candidate')->first(); ?>
                            Mobile : {{{$candidate->mobile or ''}}}<br>
                            e-mail :{{{$candidate->email or ''}}}</address>
                            @endif
                    </div>
                    <div class="col-lg-4" style="margin-top: 8px; float: left; padding-right: 14px; margin-bottom: 0px;">
                    
                        @if(!empty($candidate->photo))

                        <img class="sobi"  alt="{{{$candidate->photo or ''}}}" src="{{JOB_DOMAIN.'data/profile/'.$candidate->photo}}"/>
                @else
                    <img src="{{{$theme}}}images/FB-profile-avatar.jpg" style="padding:1px;border:solid 1px #e5e5e5;"  alt="salina" class="sobi" />
                    
                @endif  
                    
                    
                    </div>
                </div>

                @if(!empty($candidate->csummary))
                <div class="col-lg-12" style="padding:0;">
                    <div class="col-lg-12 tite"><h4><strong>Summery :</strong></h4></div>
                    <p style="padding:0 20px 0 15px;text-align: justify; ">{{{$candidate->csummary}}}</p>
                </div>
                @endif

                <div class="col-lg-12" style="padding:0;">
                    <div class="col-lg-12 tite"><h4><strong>Employment History :</strong></h4></div>
                    <h5 style="padding-left:15px;"><b>Total Year of Experience :</b><span> {{{$cvInfo->year_of_exp or '' }}} Year(s) </span> </h5>
<?php $i=1; ?>
                    @if(count($cvJob))
                    @foreach($cvJob as $cJ)
                    
                    <h5 style="padding-left:15px;"><span><?php echo $i;?>.</span>Company Name :{{{$cJ->company_name or ''}}}( {{{$cJ->start or ''}}} - {{{$cJ->end or ''}}})</h5>
                    <h5 style="padding-left:15px;">{{{$cJ->designation or ''}}}</h5> 
                    <p style="padding-left:15px;">Department : {{{$cJ->department or ''}}}</p>
                    <?php $i++; ?>
                    @endforeach
                    @endif

                </div>
                <div class="col-lg-12" style="padding:0;">
                    <div class="col-lg-12 tite"> <h4><strong>Academic Qualification :</strong></h4></div>

                    <div class="table-responsive col-lg-12" style="padding:0;">
                        <table class="table table-striped mb30">
                            <thead>
          
                       <tr>
                <th>Exam Title</th>
                <th>Major</th>
                <th>Institution</th>
                <th>Result</th>
                <th>Passing Year</th>
                
            </tr>
                         
                            </thead>
                            <tbody>
                  @if(count($cvEdu))
                @foreach($cvEdu as $cE)
                <tr>
                        <td><label class=" col-lg-0 control-label">{{{$cE->cv_edu_title or ''}}}</label></td>
                        <td><label class=" col-lg-0 control-label">{{{$cE->major or ''}}}</label></td>
                        <td><label class=" col-lg-0 control-label">{{{$cE->institution or ''}}}</label></td>
                        <td><label class=" col-lg-0 control-label">{{{$cE->result or ''}}}</label></td>
                        <td><label class=" col-lg-0 control-label">{{{$cE->passing_year or ''}}}</label></td>
                </tr>
                    
                    
                    @endforeach
            @endif
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="col-lg-12" style="padding:0;">
                    <div class="col-lg-12 tite"> <h4><strong>Professional Qualification :</strong></h4></div>
                    <br>
                    <div class="table-responsive col-lg-12" style="padding:0;">
                      
   
        <table width="100%" class="table table-striped mb30">
            <thead>
            <tr>
                <th>Qualification Title</th>
                <th>Institution</th>
                <th>Finished Year</th>
                
            </tr>
            </thead>
            <tbody>
                            @if(count($cvQ))
                @foreach($cvQ as $cq)
                
                         <tr>
                        <td><label class=" col-lg-0 control-label">{{{$cq->cv_q_title or ''}}}</label></td>
                        <td><label class=" col-lg-0 control-label">{{{$cq->institution or ''}}}</label></td>
                        <td><label class=" col-lg-0 control-label">{{{$cq->finished_year or ''}}}</label></td>
                       
                    </tr>
                    
                    @endforeach
            @endif
                                        </tbody>
        </table>
    
	
                    </div>
                </div>

                    <div class="col-lg-12" style="padding:0;">

                         <div class="col-lg-12 tite"> <h4><strong>Speacialization :</strong></h4></div>
                         <br>
                         <div class="table-responsive col-lg-12" style="padding:0;">
                            <table width="100%" class="table table-striped mb30">
                                <thead>
                                    <tr>
                                        <th>Technologies</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($cvSpecs as $spec)
                                    <?php $specName = strtolower($spec->key); ?>
                                    <tr style="width:25%; float:left;">
                                        <td style="width:100%; float:left;"><input class="specs" disabled type="checkbox" @if(!empty($cvSpecs) && !empty($cvSpecs[str_replace(" ","_",$specName)])) checked="checked" @endif data-key="{{{str_replace(' ','_',$specName)}}}" name="specs[{{str_replace(' ','_',$specName)}}]"/> {{{$specName}}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                         </div>
                    </div>

                    <div class="col-lg-12" style="padding:0;" >
                        <div class="col-lg-12 tite"> <h4><strong>Personal Details :</strong></h4></div>
                        <br>
                        <table style="margin-bottom:10px;">
                        <tbody style="padding-left:15px; float:left;">
                                <tr>
                                    <td>Father name	</td>	<td>: {{{$candidate->father_name or '' }}}</td>
                                </tr>
                                <tr>
                                    <td>Mother name</td>	<td>: {{{$candidate->mother_name or '' }}}</td>
                                </tr>
                                <tr>
                                    <td>Date of birth</td>	<td>: {{{$candidate->dob or '' }}}</td>
                                </tr>
                                <tr>
                                    <td>Gender</td>	<td>: {{$candidate->gender}}</td>
                                </tr>
                                <tr>
                                    <td>Marital status</td>	<td>: {{$candidate->marital_status }}</td>
                                </tr>
                                <tr>
                                    <td>Religion</td>	<td>: {{{$candidate->religion or ''}}}</td>
                                </tr>
                                <tr>
                                    <td>Nationality</td>	<td>: {{{$candidate->nationality or ''}}}</td>
                                </tr>
                                <tr>
                                    <td>Permanent address</td>	<td>: {{{$candidate->parmanent_address or ''}}}</td>
                                </tr>
                                <tr>
                                    <td>Current location</td>	<td>: {{{$candidate->current_address or ''}}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            
            
           </section> 
            
        </div>

    </div>


    <!-- page end-->
</section>

@stop