@extends('templates.bucket.bucket')

@section('wrapper')

<section class="wrapper">
<!-- page start-->

<div class="row">
<div class="col-md-12">
    <section class="panel">
        <div class="panel-body profile-information">
            <div class="col-md-3">
                <div class="profile-pic text-center">
                    <img src="images/cezar-admissions-profile.png" alt=""/>
                </div>
            </div>
            <div class="col-md-8">
                <div class="profile-desk">
                    <h1>David Rojormillan</h1>
                    <span class="text-muted">Software Engineer</span>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean porttitor vestibulum imperdiet. Ut auctor accumsan erat, a vulputate metus tristique non. Aliquam aliquam vel orci quis sagittis.
                    </p>
                    <a href="#" class="btn btn-primary">View Profile</a>
                </div>
            </div>

        </div>
    </section>
</div>
<div class="col-md-12">
<section class="panel">
<header class="panel-heading tab-bg-dark-navy-blue">
    <ul class="nav nav-tabs nav-justified ">
        <li class="active">
            <a data-toggle="tab" href="#overview">
                Personal Detail
            </a>
        </li>
        <li>
            <a data-toggle="tab" href="#job-history">
                Career and Application Information
            </a>
        </li>
        <li>
            <a data-toggle="tab" href="#contacts" class="contact-map">
                Preferred Job
            </a>
        </li>

    </ul>
</header>
<div class="panel-body">
<div class="tab-content tasi-tab">
<div id="overview" class="tab-pane active">
    <div class="row">

        <div class="col-md-12">
            <table width="100%" cellspacing="1" cellpadding="0" border="0" style="margin-bottom: 1px;">
                <tbody>


                <tr>
                    <td width="163" height="29" bgcolor="" align="right" class="ducclebels">Name :&nbsp;&nbsp;&nbsp;</td>
                    <td width="358" bgcolor="" class="duccvalues2"><input type="text" name="name" value="" id="name" style="width:265px"></td>
                </tr>

                <tr>
                    <td height="29" bgcolor="" align="right" class="ducclebels">Father's Name :&nbsp;&nbsp;&nbsp;</td>
                    <td bgcolor="" class="duccvalues2"> <input type="text" name="fa_name" value="" id="fa_name" style="width:265px"></td>
                </tr>

                <tr>
                    <td height="29" bgcolor="" align="right" class="ducclebels">Mother's Name :&nbsp;&nbsp;&nbsp;</td>
                    <td bgcolor="" class="duccvalues2"><input type="text" name="ma_name" value="" id="ma_name" style="width:265px"> </td>
                </tr>

                <tr>
                    <td height="29" bgcolor="" align="right" class="ducclebels">Date of Birth :&nbsp;&nbsp;&nbsp;</td>
                    <td bgcolor="" class="duccvalues2"><input type="text" class="hasDatepicker" name="birth" value="" id="birth" style="width:265px"> </td>
                </tr>

                <tr>
                    <td height="29" bgcolor="" align="right" class="ducclebels">Gender :&nbsp;&nbsp;&nbsp;</td>
                    <td bgcolor="" class="duccvalues2"><input type="radio" name="gender" value="male" id="gender" > Male
                        <input type="radio" name="gender" value="female" id="gender"> Female</td>
                </tr>

                <tr>
                    <td height="29" bgcolor="" align="right" class="ducclebels">Marital Status :&nbsp;&nbsp;&nbsp; </td>
                    <td bgcolor="" class="duccvalues2"><input type="radio" name="marit_s" value="married" id="marit_s" > Single
                        <input type="radio" name="marit_s" value="unmarried" id="marit_s"> Married </td>
                </tr>

                <tr>
                    <td height="29" bgcolor="" align="right" class="ducclebels">Nationality :&nbsp;&nbsp;&nbsp; </td>
                    <td bgcolor="" class="duccvalues2"><input type="text" name="nationality" value="" id="nationality" style="width:265px"></td>
                </tr>

                <tr>
                    <td height="29" bgcolor="" align="right" class="ducclebels">Religion :&nbsp;&nbsp;&nbsp; </td>
                    <td bgcolor="" class="duccvalues2"><input type="text" name="religion" value="" id="religion" style="width:265px"></td>
                </tr>

                <tr>
                    <td height="29" bgcolor="" align="right" class="ducclebels">Present Address :&nbsp;&nbsp;&nbsp; </td>
                    <td bgcolor="" class="duccvalues2"><input type="text" name="present_add" value="" id="present_add" style="width:265px"></td>
                </tr>

                <tr>
                    <td nowrap="nowrap" height="29" bgcolor="" align="right" class="ducclebels">Permanent Address :&nbsp;&nbsp;&nbsp;</td>
                    <td bgcolor="" class="duccvalues2"><input type="text" name="permanent_add" value="" id="permanent_add" style="width:265px"></td>
                </tr>

                <!--            <tr>
                                <td class="ducclebels" align="right" bgcolor="" height="29">Current Location : </td>
                                <td class="duccvalues2" bgcolor=""> <input type="text" name="c_location" value=""  id="c_location" /></td>
                            </tr>-->

                <tr>
                    <td height="29" bgcolor="" align="right" class="ducclebels">Home Phone :&nbsp;&nbsp;&nbsp; </td>
                    <td bgcolor="" class="duccvalues2"><input type="text" name="home_phone" value="" id="home_phone" style="width:265px"></td>
                </tr>

                <tr>
                    <td height="29" bgcolor="" align="right" class="ducclebels">Mobile : &nbsp;&nbsp;&nbsp;</td>
                    <td bgcolor="" class="duccvalues2"><input type="text" name="mobile_phone" value="" id="mobile_phone" style="width:265px"></td>
                </tr>

                <tr>
                    <td height="29" bgcolor="" align="right" class="ducclebels">Office Phone :&nbsp;&nbsp;&nbsp;</td>
                    <td bgcolor="" class="duccvalues2"><input type="text" name="office_phone" value="" id="office_phone" style="width:265px"></td>
                </tr>

                <tr>
                    <td height="29" bgcolor="" align="right" class="ducclebels">Email :&nbsp;&nbsp;&nbsp; </td>
                    <td bgcolor="" class="duccvalues2"><input type="text" name="email" id="email" value="" style="width:265px">
                    </td>
                </tr>

                <tr>
                    <td height="29" bgcolor="" align="right" class="ducclebels">Alternate Email :&nbsp;&nbsp;&nbsp; </td>
                    <td bgcolor="" class="duccvalues2"><input type="text" name="a_email" value="" id="a_email" style="width:265px"></td>
                </tr>


                </tbody>
            </table>
        </div>
    </div>
</div>
<div id="job-history" class="tab-pane ">
    <div class="row">
        <div class="col-md-12">
            <table width="100%" cellspacing="0" cellpadding="0" border="0" style="margin-bottom: 1px;">
                <tbody>




                <!--/* <tr><td>&nbsp;</td></tr>*/-->

                <tr>
                    <td height="29" bgcolor="" align="right" class="ducclebels">Year of Experience :&nbsp;&nbsp;&nbsp;</td>
                    <td bgcolor="" class="duccvalues2"><input type="text" name="y_exp" id="y_exp" value="" style="width:265px"></td>
                </tr>
                <tr>
                    <td height="29" bgcolor="" align="right" class="ducclebels">Present Salary :&nbsp;&nbsp;&nbsp;</td>
                    <td bgcolor="" class="duccvalues2"><input type="text" name="y_exp" id="y_exp" value="" style="width:265px"></td>
                </tr>

                <tr>
                    <td height="29" bgcolor="" align="right" class="ducclebels">Expected Salary :&nbsp;&nbsp;&nbsp;</td>
                    <td bgcolor="" class="duccvalues2"><input type="text" name="e_salary" id="e_salary" value="" style="width:265px"></td>
                </tr>

                <tr>
                    <td height="29" bgcolor="" align="right" class="ducclebels">Looking For : &nbsp;&nbsp;&nbsp;</td>
                    <td bgcolor="" class="duccvalues2"><input type="text" name="looking_f" id="looking_f" value="" style="width:265px"></td>
                </tr>

                <tr>
                    <td height="29" bgcolor="" align="right" class="ducclebels">Available For :&nbsp;&nbsp;&nbsp; </td>
                    <td bgcolor="" class="duccvalues2"><input type="text" name="available_for" id="available_for" value="" style="width:265px"></td>
                </tr>

                <tr>
                    <td width="163" height="29" bgcolor="" align="right" class="ducclebels">Objective :&nbsp;&nbsp;&nbsp;</td>
                    <td width="358" bgcolor="" class="duccvalues2">

                        <textarea name="objective" id="objective" cols="30" rows="3">
                        </textarea>
                    </td>
                </tr>


                </tbody>
            </table>
        </div>
    </div>
</div>
<div id="contacts" class="tab-pane ">
    <div class="row">
        <table width="100%" cellspacing="1" cellpadding="0" border="0" style="margin-bottom: 1px;">
            <tbody><tr>
                <td height="22" bgcolor="#dadce1" colspan="2" class="duccFormTitle" style="padding-left: 11px; padding-right: 1px;">
                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                        <tbody><tr>
                            <td class="duccFormTitle">Preferred Job Category </td>
                            <td width="46"><i class="fa fa-pencil-square-o fa-2x" style="margin-top: 4px;"></td>
                        </tr>
                        </tbody></table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>

            <tr>
                <td width="163" height="29" bgcolor="" align="right" class="ducclebels">Preferred Job Category:&nbsp;&nbsp;&nbsp; </td>
                <td width="358" bgcolor="" class="duccvalues2">
                    <select name="job_category" id="job_category" class="job_category" style="height: 21px;width:265px;" onchange="change_cu(this)" >
                        <option value="">-------Select-----------</option>
                        <option value="2">Accounting/Finance</option>
                        <option value="43">IT &amp; telecommunication</option>
                        <option value="60">Advertisement/Event Mgt.</option>
                        <option value="55">Bank/Insurance/Leasing</option>
                        <option value="56">Marketing/Sales</option>
                        <option value="57">Commercial/Supply Chain</option>
                        <option value="58">Customer Support/Call Center</option>
                        <option value="59">Education/Training</option>
                        <option value="61">Engineer/Architects</option>
                        <option value="62">Medical/Pharmaceuticals</option>
                        <option value="63">Garments/Textile</option>
                        <option value="65">NGO/Development</option>
                        <option value="66">HR/Org. Development</option>
                        <option value="67">Research/Consultency</option>
                        <option value="68">Gen Mgt/Admin</option>
                        <option value="69">Secretary/Receptionist</option>
                        <option value="71">Design/Creative</option>
                        <option value="72">Data Entry/Operator/BPO</option>
                        <option value="73">Others</option>
                    </select>


                </td>
            </tr>

            <tr>
                <td>&nbsp;
                </td>
            </tr>
            </tbody>
        </table>

        <table width="100%" cellspacing="1" cellpadding="0" border="0" style="margin-bottom: 1px;">
            <tbody><tr>
                <td height="22" bgcolor="#dadce1" colspan="2" class="duccFormTitle" style="padding-left: 11px; padding-right: 1px;">
                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                        <tbody><tr>
                            <td class="duccFormTitle">Preferred Job Location(s)</td>
                            <td width="46"><i class="fa fa-pencil-square-o fa-2x" style="margin-top: 4px;"></td>
                        </tr>
                        </tbody></table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>

            <tr>
                <td width="163" height="29" bgcolor="" align="right" class="ducclebels">Inside Bangladesh:&nbsp;&nbsp;&nbsp;</td>
                <td width="358" bgcolor="" class="duccvalues2">
                    <select name="job_inside" id="job_inside" class="job_inside" style="height: 21px; width:265px;" onchange="change_cu(this)" >
                        <option value="">-------Select-----------</option>
                        <option value="Dhaka">Dhaka</option>
                        <option value="Rajshahi">Rajshahi</option>
                        <option value="Khulna">Khulna</option>
                    </select>
                </td>
            </tr>

            <tr>
                <td width="163" height="29" bgcolor=""  align="right" class="ducclebels">Outside Bangladesh:&nbsp;&nbsp;&nbsp;</td>
                <td width="358" bgcolor="" class="duccvalues2">
                    <select name="job_outside" id="job_outside" class="job_outside" style="height: 21px;width:265px;" onchange="change_cu(this)" >
                        <option value="">-------Select-----------</option>
                        <option value="India">India</option>
                        <option value="China">China</option>

                    </select>
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>


            </tbody>
        </table>

        <table width="100%" cellspacing="1" cellpadding="0" border="0" style="margin-bottom: 1px;">
            <tbody><tr>
                <td height="22" bgcolor="#dadce1" colspan="2" class="duccFormTitle" style="padding-left: 11px; padding-right: 1px;">
                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                        <tbody><tr>
                            <td class="duccFormTitle">Preferred Organization Type(s) </td>
                            <td width="46"><i class="fa fa-pencil-square-o fa-2x" style="margin-top: 4px;"></td>
                        </tr>
                        </tbody></table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>

            <tr>
                <td width="340" height="29" bgcolor="" class="ducclebels" align="right" class="ducclebels">Organization Type:&nbsp;&nbsp;&nbsp;</td>
                <td bgcolor="" class="duccvalues2">
                    <select name="org_type" id="org_type" class="org_type" style="height: 21px;width:265px;" onchange="change_cu(this)">
                        <option value="">-------Select-----------</option>
                        <option value="public">Public</option>
                        <option value="private">Private</option>

                    </select>

                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            </tbody>
        </table>


        <table width="100%" cellspacing="1" cellpadding="0" border="0" style="margin-bottom: 1px;">
            <tbody><tr>
                <td height="22" bgcolor="#dadce1" colspan="2" class="duccFormTitle" style="padding-left: 11px; padding-right: 1px;">
                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                        <tbody><tr>
                            <td class="duccFormTitle">Other Relevant Information</td>
                            <td width="46"><i class="fa fa-pencil-square-o fa-2x" style="margin-top: 4px;"></td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>

            <tr>
                <td width="163" height="29" bgcolor="" align="right" class="ducclebels">Career Summary :&nbsp;&nbsp;&nbsp;</td>
                <td width="358" bgcolor="" class="duccvalues2"><textarea name="career_sum" cols="40" class="career_sum" style="width:265px"></textarea></td>

            </tr>

            <tr>
                <td>&nbsp;
                </td>
            </tr>



            <tr>
                <td height="29" bgcolor="" align="right" class="ducclebels">Special Qualification :&nbsp;&nbsp;&nbsp;</td>
                <td bgcolor="" class="duccvalues2"><textarea name="special_qua" cols="40" class="special_qua" style="width:265px"></textarea></td>
            </tr>

            <tr>
                <td>&nbsp;
                </td>
            </tr>


            <tr>
                <td height="29" bgcolor="" align="right" class="ducclebels">Keyword :&nbsp;&nbsp;&nbsp;</td>
                <td bgcolor="" class="duccvalues2"><input type="text" name="keyword" id="keyword" value="" style="width:265px"></td>
            </tr>

            <tr>
                <td>&nbsp;
                </td>
            </tr>


            <tr>
                <td height="4" bgcolor="#dadce1" align="center" colspan="2">
                    <input type="submit" name="Submit" id="Submit" class="BDJButton4" value="Edit Information" >
                </td>
            </tr>

            </tbody>
        </table>
    </div>
</div>

</div>
</div>
</section>
</div>
</div>
<!-- page end-->
</section>

@stop