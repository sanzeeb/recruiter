@extends('templates.bucket.bucket')

@section('wrapper')

<section class="wrapper">
    <!-- page start-->
    {{Helpers::showMessage()}}
    <div id="msg"></div>
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <div class="msg"></div>
                <header class="panel-heading">
                    Search  Cv/Resume
                </header>
                <div class="panel-body">
                    <div class="row">
                        {{Form::open(array('url'=>'resume/search-resume','id'=>'searchResumeFrm','method'=>'post','class'=>'cmxform form-horizontal'))}}
                        <div class="form-group">
                            <label class="control-label col-lg-3">Year of Experience</label>
                            <div class="col-lg-2">
                                <select name="fromYear" class="form-control-select2" style="width:100px;">
                                    <option value="0">Select</option>
                                    @for($i=1; $i<=20; $i++)
                                    <option value="{{{$i}}}">{{{$i}}}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="col-sm-1">
                                To
                            </div>
                            <div class="col-lg-3">
                                <select name="toYear" class="form-control-select2" style="width:100px;">
                                    <option value="0">Select</option>
                                    @for($i=1; $i<=20; $i++)
                                    <option value="{{{$i}}}">{{{$i}}}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-3">Specialization</label>
                            <div class="col-lg-6">
                                <select name="spec" class="form-control-select2" style="width:200px;">
                                    <option value="0">Select</option>
                                    @foreach($specs as $spec)
                                    <option value="{{{str_replace(' ','_',$spec)}}}">{{{ucfirst($spec)}}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-3 col-lg-offset-3">
                                <input type="submit" class="btn btn-primary" value="Search"/>
                            </div>
                        </div>
                        {{Form::close()}}
                    </div>
                </div>
            </section>
            <section class="panel">
                <header class="panel-heading"> Search Result </header>
                <div class="panel-body">   
                    {{Form::open(array('id'=>'sendRequestFrm','url'=>'interview/invite-for-interview','class'=>'cmxform form-horizontal','method'=>'post'))}}
                    @if($user->user_type=="Admin")
                    <div class="form-group">
                        <label class="control-label col-lg-3">Recruiter</label>
                        <div class="col-lg-3">
                            <select class="form-control-select2" name="recruiter" required style="width:250px;">
                                <option value="">Select</option>
                                @if(count($recruiters))
                                @foreach($recruiters as $recruiter)
                                <option value="{{{$recruiter->emp_id}}}">{{{$recruiter->title}}}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Job list</label>

                        <div class="col-lg-3">
                            <select name="joblist" class="form-control-select2" style="width:250px;">
                                <option value=""></option>
                               
                            </select>
                        </div>


                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Purpose</label>
                        <div class="col-lg-6">
                            <input type="text" name="purpose" class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-3 col-lg-offset-3">
                            <input type="submit" class="btn btn-info" value="Send Request"/>
                        </div>
                    </div>
                    @else
                    <div class="form-group">
                        <label class="control-label col-lg-3">Job list</label>

                        <div class="col-lg-3">
                            <select name="joblist" class="form-control-select2" style="width:250px;">
                                <option value=""></option>
                                @if(count($jobs))
                                @foreach($jobs as $job)
                                <option value="{{{$job->jobs_id}}}">{{{$job->job_title}}}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>


                    </div>
                    <select name="recruiter" class="hidden">
                        <option value="value="{{$user->user_id}}">{{$user->user_id}}</option>
                    </select>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Purpose</label>
                        <div class="col-lg-6">
                            <input type="text" name="purpose" class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-3 col-lg-offset-3">
                            <input type="submit" class="btn btn-info" value="Send Request"/>
                        </div>
                    </div>
                    @endif
                    <div class="row">
                    <table class="searchResult display table table-bordered table-striped" id="dynamic-table">
                        <thead>
                            <tr>
                                <th>Checkbox</th>
                                <th class="hidden-phone">Photo</th>
                                <th>Details</th>
                                <th>Rank</th>
                                <th class="hidden-phone">Action</th>

                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                    </div>


                    {{Form::close()}}
                </div>
            </section>
        </div>
    </div>
    <div class="modal fade in" id="myModal-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Agreement form </h4>
                </div>
                <div class="modal-body">
                    <div class="msg"></div>
                    <form role="form" id='agreement-form' class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-md-3">Title</label>
                            <div class="col-md-4 col-xs-11">
                                <input type="hidden" name="candidate_id" id="candidate_id"  />
                                <input type="text" name="etitle" placeholder="Enter title" id="etitle" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Amount</label>
                            <div class="col-md-4 col-xs-11">
                                <input type="text" name="eamount" placeholder="Enter amount" id="eamount" class="form-control">

                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3">Description</label>
                            <div class="col-md-5">
                                <textarea rows="7" class='form-control' id='edescription' name='edescription'></textarea>

                            </div>

                            <button  class="btn btn-success add-time sent-agrrement" type="button">Confirm</button>
                        </div>


                    </form>

                </div>

            </div>
        </div>
    </div>
</section>
<script type="text/javascript" src="{{$theme}}js/custom/CustomMessage.js"></script>
<script type="text/javascript" src="{{$theme}}js/custom/cvmanagement.js"></script>


<!--dynamic table-->
<script type="text/javascript" language="javascript" src="{{$theme}}assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="{{$theme}}assets/data-tables/DT_bootstrap.js"></script>

<!--dynamic table initialization -->
<script src="{{$theme}}js/dynamic_table/dynamic_table_init.js"></script>

@stop