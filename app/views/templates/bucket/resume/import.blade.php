@extends('templates.bucket.bucket')

@section('wrapper')

<section class="wrapper">
    <!-- page start-->


    {{Helpers::showMessage()}}
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Import Data from here

                </header>
                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-12">
				

                            {{Form::open(array('url'=>'resume/import','method'=>'post','enctype'=>'multipart/form-data','id'=>'cvImportForm','class'=>'cmxform form-horizontal'))}}

                                        <!--<div class="form-group">
                                            <label class="control-label col-lg-3">Name <small class="text-danger">(*)</small></label>
                                            <div class="col-lg-3">
                                                <select id="e1" class="populate " style="width:170px; text-align:center;" >
                                                        <option value="AK">Alaska</option>
                                                        <option value="HI">Hawaii</option>
                                                        <option value="CA">California</option>
                                                        <option value="NV">Nevada</option>
                                                        <option value="OR">Oregon</option>
                                                        <option value="WA">Washington</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-lg-3">Keyword<small class="text-danger">(*)</small></label>
                                            <div class="col-lg-3">
                                                <input type="text" name="keyword" class="form-control" value="" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-lg-3">Email<small class="text-danger">(*)</small></label>
                                            <div class="col-lg-3">
                                                <input type="text" name="keyword" class="form-control" value="" />
                                            </div>
                                        </div>-->
					<div class="form-group">
						<div class="col-lg-3 col-lg-offset-3"><strong>File format should be in (.xls)</strong></div>
					</div>
                                        <div class="form-group">
                                            <label class="control-label col-lg-3">Browse File<small class="text-danger">(*)</small></label>
                                            <div class="col-lg-3">
                                                <input type="file" name="cvfile" required/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-3 col-lg-offset-3">
                                                <input type="submit" name="submit" class="btn btn-primary" value="Upload File" />
                                            </div>
                                        </div>

                               {{Form::close()}}


                        </div>

                    </div>
                </div>
            </section>

        </div>
    </div>

</section>



@stop
