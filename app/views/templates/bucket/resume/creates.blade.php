@extends('templates.bucket.bucket')

@section('wrapper')
<script type="text/javascript">
    /*
            $("input[name='dob']").datepicker({
                
                changeMonth: true,
                changeYear: true
            });*/
</script>
<section class="wrapper" ng-app="">
    <!-- page start-->
{{Helpers::showMessage()}}
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <div class="msg"></div>
                <header class="panel-heading">
                    CREATE RESUME
                </header>
                <div class="panel-body">
                    {{Form::open(array('id'=>'form-2','url'=>'resume/save-resume','class'=>'cmxform form-horizontal','autocomplete'=>'off','enctype'=>'multipart/form-data','method'=>'post'))}}
                    <div id="wizard">

                        <h2>Personal Detail</h2>

                        <section>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Name <span class="text-danger">*</span></label>
                                    <div class="col-lg-5">
                                        <input type="text" name="name" class="form-control" title="Name" required/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Username <span class="text-danger">*</span></label>
                                    <div class="col-lg-5">
                                        <input type="text" name="username" class="form-control" title="Name" required/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Password <span class="text-danger">*</span></label>
                                    <div class="col-lg-5">
                                        <input type="password" name="pass" class="form-control" title="Name" required/>
                                    </div>
                                    <span>Strength : <strong class="passStrength"></strong></span>
                                </div>
                                
                                <!-- <div class="form-group">
                                    <label class="col-lg-3 control-label">Confirm Password <span class="text-danger">*</span></label>
                                    <div class="col-lg-5">
                                        <input type="text" name="confirm_pass" class="form-control" title="Name" required/>
                                    </div>
                                </div> -->

                                <div class="form-group">
                                    <label class="control-label col-lg-3">Total Year of Experience <span class="text-danger">*</span></label>
                                    <div class="col-lg-2">
                                        <input type="text" name="year_of_exp" title="Total Work Experience" class="form-control" required/>
                                    </div>
                                </div>
                                <hr/>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Father's Name <span class="text-danger">*</span></label>
                                    <div class="col-lg-5">
                                        <input type="text" name="father_name" class="form-control" title="Father's Name" required/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Mother's Name <span class="text-danger">*</span></label>
                                    <div class="col-lg-5">
                                        <input type="text" name="mother_name" class="form-control" title="Mother's Name" required/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Date of Birth <span class="text-danger">*</span></label>
                                    <div class="col-lg-3">
                                        <input type="text" name="dob" class="form-control" title="Date of Birth" required/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Gender <span class="text-danger">*</span></label>
                                    <div class="col-lg-8">
                                        <select name="gender" class="form-control-select2" required>
                                            @foreach($genders as $g)
                                                <option value="{{$g}}">{{$g}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Marital Status </label>
                                    <div class="col-lg-8">
                                        <select name="marital_status" class="form-control-select2">
                                            @foreach($marital_status as $mt)
                                                <option value="{{$mt}}">{{$mt}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>



                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Nationality</label>
                                    <div class="col-lg-3">
                                        <input type="text" name="nationality" class="form-control" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Religion</label>
                                    <div class="col-lg-3">
                                        <input type="text" name="religion" class="form-control" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Present Address</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="present_address" class="form-control" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Permanent Address</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="permanent_address" class="form-control" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Home Phone</label>
                                    <div class="col-lg-3">
                                        <input type="text" name="home_phone" class="form-control" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Mobile <span class="text-danger">*</span></label>
                                    <div class="col-lg-3">
                                        <input type="text" name="mobile" class="form-control" title="Mobile number"  required/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Email <span class="text-danger">*</span></label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control" name="email" title="E-mail" required/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Alternate Email</label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control" name="alternate_email" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Photo</label>
                                    <div class="controls col-md-2">
                                        <div class="fileupload-new thumbnail">
                                            <img alt="" src="{{$theme}}images/placeholder.gif"/>

                                        </div>
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <span class="btn btn-white btn-file">
                                            <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Browse file</span>
                                            <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                            <input type="file" class="default" name="photo"/>
                                            </span>
                                            <span class="fileupload-preview" style="margin-left:5px;"></span>
                                            <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                        </div>
                                    </div>
                                </div>
                        </section>

                        <h2>Education Info</h2>
                        <section>

                            <h4>Additional Info</h4>
                            <div class="form-group col-lg-12">
                                <a class="btn btn-primary newAddBtn" id="addEduParam" href="#"><i class="fa fa-plus-circle"> Add More</i></a>
                                <table width="100%" id="eduParamTbl">
                                    <thead>
                                    <tr>
                                        <th>Exam Title</th>
                                        <th>Major</th>
                                        <th>Institution</th>
                                        <th>Result</th>
                                        <th>Passing Year</th>
                                        <th><i class="fa fa-trash-o"></i></th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <tr>
                                        <td><input type="text" name="title[]" class="form-control col-lg-2" value=""/></td>
                                        <td><input type="text" name="major[]" class="form-control col-lg-2" value=""/></td>
                                        <td><input type="text" name="institute[]" class="form-control col-lg-2" value=""/></td>
                                        <td><input type="text" name="result[]" class="form-control col-lg-2" value=""/></td>
                                        <td><input type="text" name="passing_year[]" class="form-control col-lg-2" value=""/></td>
                                        <td><a class="delParam" href="#"><i class="fa fa-minus-circle"></i></a></td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>

                        </section>

                        <h2>Work Experience</h2>
                        <section>

                            <h4>Work Info</h4>
                            <div class="form-group col-lg-12">

                                <a class="btn btn-primary newAddBtn" id="addWorkParam" href="#"><i class="fa fa-plus-circle"> Add More</i></a>
                                <table width="100%" id="workParamTbl">
                                    <thead>
                                    <tr>
                                        <th>Job Title</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Company Name</th>
                                        <th>Designation</th>
                                        <th>Department</th>
                                        <th><i class="fa fa-trash-o"></i></th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <tr>
                                        <td><input type="text" name="job_title[]" class="form-control col-lg-2" value=""/></td>
                                        <td><input type="text" name="start_dt[]" class="form-control col-lg-2" value=""/></td>
                                        <td><input type="text" name="end_dt[]" class="form-control col-lg-2" value=""/></td>
                                        <td><input type="text" name="company[]" class="form-control col-lg-2" value=""/></td>
                                        <td><input type="text" name="designation[]" class="form-control col-lg-2" value=""/></td>
                                        <td><input type="text" name="department[]"class="form-control col-lg-2" value=""/></td>
                                        <td><a class="delParam" href="#"><i class="fa fa-minus-circle"></i></a></td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>

                        </section>

                        <h2>Others</h2>
                        <section>
                            <h4>Professional Qualification</h4>
                            <div class="form-group col-lg-12">
                                <a class="btn btn-primary newAddBtn" id="addPqParam" href="#"><i class="fa fa-plus-circle"> Add More</i></a>
                                <table width="100%" id="pqParamTbl">
                                    <thead>
                                    <tr>
                                        <th>Qualification Title</th>
                                        <th>Institution</th>
                                        <th>Finished Year</th>
                                        <th><i class="fa fa-trash-o"></i></th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <tr>
                                        <td><input type="text" name="q_title[]" class="form-control col-lg-2" value=""/></td>
                                        <td><input type="text" name="q_institute[]" class="form-control col-lg-2" value=""/></td>
                                        <td><input type="text" name="q_year[]" class="form-control col-lg-2" value=""/></td>
                                        <td><a class="delParam" href="#"><i class="fa fa-minus-circle"></i></a></td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>

                            <h4>Specialization</h4>

                            <table width="100%" id="specTbl">
                                <thead>
                                <tr>
                                    <th>Technologies</th>

                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($specs as $spec)
                                    <tr>
                                        <td><input type="checkbox" name="specs[{{str_replace(' ','_',$spec)}}]"/> {{{$spec}}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <br/><br/>
                             <input type="submit" class="btn btn-primary" value="Save"/>

                        </section>
                    </div>
                    {{Form::close()}}
                </div>
            </section>
        </div>
    </div>
</section>
<script src="{{$theme}}assets/jquery-steps-master/build/jquery.steps.js"></script>
<script src="{{$theme}}js/custom/Validate.js"></script>
<script src="{{$theme}}js/custom/cvmanagement.js"></script>

<script type="text/javascript">
    var usernameCheck = 0;
    var weekPass = false;
    var password=''
$(function(){   

            


           
            $("input[name='pass']").keyup(function(){
               var userName = $('input[name="username"]').val();
               password  = $(this).val();
               var pattern = /(\w|\W){8,}/;
               if(pattern.test(password)){
                    if(password.match(/@/) && password.match(/#/))
                        $(".passStrength").html('Strong').css('color','green');
                    else if(password.match(/@/))
                        $(".passStrength").html('Medium').css('color','green');
                    else
                        $(".passStrength").html('Medium').css('color','green');
                    weekPass = false;
               }else if(pattern.test(userName))
               {
                    weekPass = true;
                    $(".passStrength").html('Week').css('color','red');

               }else{
                    weekPass = true;
                    $(".passStrength").html('Week').css('color','red');
               }
               
            });

            $("input[name='username']").blur(function(){
                
                var obj = $(this);
                
                $.ajax({
                    type:"POST",
                    url : "{{url('users/check-username-exist')}}",
                    data: {'username':obj.val()},
                    success:function(e)
                    {
                        if(e == 1)
                         {
                             usernameCheck = 1;
                             obj.next().remove();
                             obj.after('<p class="text-danger">Not Available</p>');

                         }else{

                             obj.next().remove();
                             obj.after('<p class="text-success">Available</p>');
                         }  

                    }
                });


            });

            $("#form-2").submit(function(){

                if(usernameCheck == 1)
                 {
                    bootbox.alert('Username not avaiable');
                    return false;
                 }  

                if(weekPass)
                {
                    bootbox.alert('Password stength should be at least medium');
                    return false;
                }

            });
        });
</script>
@stop