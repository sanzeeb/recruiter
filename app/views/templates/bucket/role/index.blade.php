@extends('templates.bucket.bucket')

@section('wrapper')

<section class="wrapper">
    <!-- page start-->
    {{Helpers::showMessage()}}
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <div class="msg"></div>
                
                <header class="panel-heading">
                    User roles
                </header>
                
                <div class="panel-body">
                    <form action="{{url('roles/create')}}" method="POST">
                        <label class="col-lg-3">Role Name</label>
                        <div class="col-lg-3">
                            <input type="text" name="role_name" class="form-control"/>
                        </div>
                        <div class="col-lg-3">
                            <input class="btn btn-primary" type="submit" value="Save"/>
                        </div>
                    </form>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Role Name</th>
                                <th>Number of Users</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            @if(count($roles))
                                @foreach($roles as $role)
                                    <tr>
                                        <td>{{$role->role_name}}</td>
                                        <td>{{0}}</td>
                                        <td>
                                            @if($role->editable)
                                                <a href="{{url('roles/permission/'.$role->role_id)}}"><i class="icon-pencil"></i> Edit</a>
                                            @else
                                                Not editable
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>

                    </table>
                </div>

            </section>
        </div>
    </div>

</section>
@stop