@extends('templates.bucket.bucket')

@section('wrapper')

<section class="wrapper">
    <!-- page start-->
    {{Helpers::showMessage()}}
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <div class="msg"></div>
                
                <header class="panel-heading">
                    Set module permission for <strong>{{$role->role_name}}</strong> user role
                </header>
                
                <div class="panel-body">
                    <form action="{{url('roles/update-permission')}}" method="post">
                       
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Module Name</th>
                                <th>
                                    View
                                </th>
                                <th>
                                    Edit
                                </th>
                                <th>
                                    Delete
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($modules))
                                @foreach($modules as $module)
                                <tr>
                                    <td>{{$module}}</td>
                                    <td>
                                        <input @if(isset($permissions[$module]['action']->view)) checked="checked" @endif type="checkbox" name="permission[{{$module}}][view]" value="{{$role->role_id}}"/>
                                    </td>
                                    <td>
                                        <input @if(isset($permissions[$module]['action']->edit)) checked="checked" @endif type="checkbox" name="permission[{{$module}}][edit]" value="{{$role->role_id}}"/>
                                    </td>
                                    <td>
                                        <input @if(isset($permissions[$module]['action']->delete)) checked="checked" @endif type="checkbox" name="permission[{{$module}}][delete]" value="{{$role->role_id}}"/>
                                    </td>
                                </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                    <input type="hidden" name="role" value="{{$role->role_id}}"/>
                    <input type="submit" class="btn btn-primary" value="Save Permission"/>
                    </form>
                </div>
            </section>
        </div>
    </div>
</section>
@stop