<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="images/favicon.html">

    <title>Registration</title>

    <!--Core CSS -->
    <link href="{{$theme}}bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{$theme}}css/bootstrap-reset.css" rel="stylesheet">
    <link href="{{$theme}}assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="{{$theme}}css/style.css" rel="stylesheet">
    <link href="{{$theme}}css/style-responsive.css" rel="stylesheet" />

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="{{$theme}}js/ie8/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

  <body class="login-body">

    <div class="container">

        <h3>{{{$msg or ''}}} </h3>
      {{ Form::open(array('url'=>'home/registration','class'=>'form-signin', 'method'=>'post')) }}
        <h2 class="form-signin-heading">Free sign up  now</h2>
     <div class="login-wrap">
            <p>Enter your personal details below</p>
            <input type="text" name="firstname" autofocus="" placeholder="Frist Name" class="form-control">
            <input type="text" name="lastname" autofocus="" placeholder="Last Name" class="form-control">
            <input type="text" autofocus="" name="email" placeholder="Email" class="form-control">
            <div class="radios">
                <label for="radio-01" class="label_radio col-lg-6 col-sm-6">
                    <input type="radio"  checked="" value="1" id="radio-01" name="gender"> Male
                </label>
                <label for="radio-02" class="label_radio col-lg-6 col-sm-6">
                    <input type="radio" value="1" id="radio-02" name="gender"> Female
                </label>
            </div>

            <p> Enter your account details below</p>
            <input type="text" autofocus="" name="username" placeholder="User Name" class="form-control">
            <input type="password" placeholder="Password" name="password" class="form-control">
            <input type="password" placeholder="Re-type Password" name="re_password" class="form-control">
            <label class="checkbox">
                <input type="checkbox" name="agreement" value="agree this condition"> I agree to the Terms of Service and Privacy Policy
            </label>
            <button type="submit" class="btn btn-lg btn-login btn-block">Submit</button>

            <div class="registration">
                Already Registered.
                <a href="login.html" class="">
                    Login
                </a>
            </div>

        </div>
          <!-- Modal -->
          <!-- modal -->
      {{ Form::close() }}
        <!-- </form>-->

       </div>



       <!-- Placed js at the end of the document so the pages load faster -->

    <!--Core js-->
    <script src="{{$theme}}js/lib/jquery.js"></script>
    <script src="{{$theme}}bs3/js/bootstrap.min.js"></script>

  </body>

</html>
