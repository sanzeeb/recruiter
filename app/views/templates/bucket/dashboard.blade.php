@extends('templates.bucket.bucket')

@section('wrapper')
<section class="wrapper">
{{{Helpers::showMessage()}}}
<!--mini statistics start-->
<div class="row">
    <h1 class="title">Engineers Database</h1>
</div>
<!--mini statistics end-->

<div class="row_x">
            @foreach($skills as $skill)
                
                    <!--<div class="col-md-3">
                        <div class="mini-stat clearfix">
                            <div class="mini-stat-info">
                                <a href="{{{url('dashboard/top-engineer')}}}/{{{base64_encode($skill->skill_name)}}}">
                                    @if(!empty($skill->skill_photo) && file_exists('data/skill/'.$skill->skill_photo))
                                    <img height='60' alt="{{{$skill->skill_photo or ''}}}" src="{{{url('/')}}}/data/skill/{{{$skill->skill_photo}}}"/>
                                    @else
                                    <img height='60' alt="thumbnail" src="{{$theme}}images/placeholder.gif"/>
                                    @endif
                                </a>
                                  <a style="font-size:15px;" href="{{{url('dashboard/top-engineer')}}}/{{{base64_encode($skill->skill_name)}}}">
                                    @if($skill->skill_name=="microcontroller_programming")
                                micro programming
                                @else
                                {{{$skill->skill_name}}}
                                @endif
                                
                                </a></div>
                        </div>
                    </div>-->
                    <div class="col-lg-4">
                        <section class="panel_x" style="border:1px solid #e6e6e6;border-top:solid 3px #BCD74A;">
                            <div class="panel-body_x" style="text-align:left;">
                                <?php $skill_name=base64_encode($skill->skill_name); ?>
                                <a style="float:left; margin-right:8px; padding:2px; border:solid 1px #CCC;" href="{{{url('dashboard/top-engineer')}}}/{{{$skill_name}}}">
                                    @if(!empty($skill->skill_photo) && file_exists('data/skill/'.$skill->skill_photo))
                                    <img height='60' alt="thumbnail" alt="{{{$skill->skill_photo or ''}}}" src="{{{url('/')}}}/data/skill/{{{$skill->skill_photo}}}"/>
                                    @else
                                    <img height='60' alt="thumbnail" alt="thumbnail" src="{{$theme}}images/placeholder.gif"/>
                                    @endif
                                </a>
                                <p align="justify" style="font-size:13px; line-height:normal;">
                                    {{$skill->skill_description}}
                                </p>

                                <!--<a href="{{{url('recruiter/top-engineers')}}}/{{{$skill_name}}}">
                                @if($skill->skill_name=="microcontroller_programming")
                                    micro programming
                                @else
                                    {{{$skill->skill_name}}}
                                @endif
                                </a>-->
                                <div class="a_clearfix"></div>
                            </div>
                        </section>
                    </div>
                    @endforeach
    
        
        </div>
        <!--<div class="row">
            <table class="display table table-bordered table-striped" id="dynamic-table">
                <thead>
                    <tr>
                        <td colspan="4"><input id="checkDuplicateData" type="button" class="btn btn-primary" value="Check Redundant entry"/></td>
                    </tr>
                    <tr>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>E-mail</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>-->

</section>
<!--<script type="text/javascript" src="{{$theme}}js/custom/pull-redundant-entry.js"></script>-->
@stop