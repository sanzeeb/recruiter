<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="images/favicon.png">
    <title>{{{$user->user_type}}}</title>
    <!--Core CSS -->
    <link href="{{ $theme }}bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ $theme }}css/select2.css" rel="stylesheet">
    <link href="{{ $theme }}assets/jquery-ui/jquery-ui-1.10.1.custom.min.css" rel="stylesheet">
    <link href="{{ $theme }}css/bootstrap-reset.css" rel="stylesheet">
    <link href="{{ $theme }}assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="{{ $theme }}assets/jvector-map/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <link href="{{ $theme }}css/clndr.css" rel="stylesheet">
   <link href="{{ $theme }}marque/marquecss.css" rel="stylesheet">
    <!-- icheckbox -->
    <link href="{{ $theme }}assets/iCheck-master/skins/square/square.css" rel="stylesheet">
    <link href="{{ $theme }}assets/iCheck-master/skins/square/red.css" rel="stylesheet">
    <link href="{{ $theme }}assets/iCheck-master/skins/square/green.css" rel="stylesheet">
    <link href="{{ $theme }}assets/iCheck-master/skins/square/blue.css" rel="stylesheet">
    <link href="{{ $theme }}assets/iCheck-master/skins/square/yellow.css" rel="stylesheet">
    <link href="{{ $theme }}assets/iCheck-master/skins/square/purple.css" rel="stylesheet">

    <!--clock css-->
    <link href="{{ $theme }}assets/css3clock/css/style.css" rel="stylesheet">


    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="{{ $theme }}assets/morris-chart/morris.css">
    <!-- Custom styles for this template -->
    <link href="{{ $theme }}css/style.css" rel="stylesheet">
    <link href="{{ $theme}}css/style-responsive.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="{{$theme}}assets/bootstrap-timepicker/compiled/timepicker.css"/>
    <link rel="stylesheet" type="text/css" href="{{$theme}}assets/bootstrap-datepicker/css/datepicker.css"/>
    <link rel="stylesheet" href="{{$theme}}assets/jquery-steps-master/demo/css/jquery.steps.css">
    <script src="{{$theme}}js/lib/jquery-1.8.3.min.js"></script>
    <link href="{{ $theme }}assets/data-tables/DT_bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{$theme}}assets/select2-master/select2.css"/>
    <link rel="stylesheet" type="text/css" href="{{$theme}}assets/bootstrap-fileupload/bootstrap-fileupload.css"/>
    <link rel="stylesheet" type="text/css" href="{{$theme}}js/angular/ng-table.min.css"/>
    
    <link rel="stylesheet" type="text/css" href="{{$theme}}css/trumbowyg.css"/>

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="{{$theme}}js/ie8/ie8-responsive-file-warning.js"></script><![endif]-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
<script type="text/javascript" src="{{$theme}}assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="{{$theme}}assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="{{$theme}}assets/select2-master/select2.js"></script>
    <script type="text/javascript" src="{{$theme}}js/angular/angular.min.js"></script>
    <script type="text/javascript" src="{{$theme}}js/angular/ng-table.min.js"></script>
    <script type="text/javascript" src="{{$theme}}js/custom/bootbox.min.js"></script>
    
    <script src="{{ $theme }}js/trumbowyg.js"></script>
    
    
    <link href="{{ $theme }}css/custom.css" rel="stylesheet"/>
    <script typep="text/javascript">
        var BASE = "{{{url('/').'/'}}}";
    </script>
   
</head>
<body>


<section id="container">
<!--header start-->
<header class="header fixed-top clearfix">
<!--logo start-->
<div class="brand">

    
@if($user->user_type=="Candidate")
       
       <?php     $candidate=Candidate::find($user->user_id); ?>
    <a href="{{{url('resume/display-resume')}}}/{{$candidate->cv_tbl_id}}" class="logo">
        <?php $vendor_name = Option::getData('vendor_name'); ?>
        @if(!empty($vendor_name))
        {{Option::getData('vendor_name')}}
        @else
        <img src="{{$theme }}images/logo.png" alt="">
        @endif
    </a>
    @else
    <a href="{{{url('dashboard')}}}" class="logo">
        <?php $vendor_name = Option::getData('vendor_name'); ?>
        @if(!empty($vendor_name))
        {{Option::getData('vendor_name')}}
        @else
        <img src="{{$theme }}images/logo.png" alt="">
        @endif
    </a>
    @endif

    <div class="sidebar-toggle-box">
        <div class="fa fa-bars"></div>
    </div>
</div>
<!--logo end-->

<div class="nav notify-row" id="top_menu">
    <!--  notification start -->
    <ul class="nav top-menu">
        <!-- settings start -->
        <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <i class="fa fa-tasks"></i>
               <span class="badge bg-success">{{count($messages)}}</span>

            </a>
            <ul class="dropdown-menu extended tasks-bar">
                
                @if(count($messages))
                    @foreach($messages as $message)
                        <li>
                            <a href="{{url('inbox/index/'.$message->inbox_id)}}">
                                <div class="task-info clearfix">
                                    <div class="desc pull-left">
                                        <h5>{{$message->subject}}</h5>

                                        <p>{{Helpers::dateTimeFormat('j F, Y',$message->created_at)}}</p>
                                    </div>
                                         
                                </div>
                            </a>
                        </li>
                    @endforeach
                @endif
               <li>
                    <p class=""><a href="{{url('notification/all')}}">See All</a></p>
                </li>
            </ul>
        </li>
        
        @if($user->user_type!="Candidate")
                <li id="header_inbox_bar" class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <i class="fa fa-envelope-o"></i>
                <span class="badge bg-important">{{count($comments)}}</span>
            </a>
            <ul class="dropdown-menu extended inbox">
                <li>
                   <p class="red">You have {{count($comments)}} Message</p>
                  
                </li>
                
                @foreach($comments as $comment)
                <li>
                    <a href="{{url('message/details/'.$comment->discussion_id)}}/{{$comment->comment_id}}">
                                <div class="task-info clearfix">
                                    <div class="desc pull-left">
                                        <h5>{{$comment->message}}</h5>

                                        <p>{{Helpers::dateTimeFormat('j F, Y',$comment->created_at)}}</p>
                                    </div>
                                         
                                </div>
                            </a>
                </li>
                @endforeach
            </ul>
        </li>
        @endif
     

        <!-- settings end -->
       
    </ul>
    <!--  notification end -->
</div>

<div class="nav notify-row" style="float:left;text-align:left;width:59%;text-indent:0%;padding-left:6%;">
@if(($user->user_type == "Employer") && ($jobCount == 0))
    
    <marquee behavior="alternate" style="font-size:12px; text-transform:uppercase; font-weight:700; color:#C00;">" You need to post at least one Job to communicate with Candidate "</marquee>
@endif
</div>
<div class="top-nav clearfix">
    <!--search & user info start-->
    <ul class="nav pull-right top-menu">
        <!--<li>
            <input type="text" class="form-control search" placeholder=" Search">
        </li>-->
        <!-- user login dropdown start-->
        <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                
                <span class="username">{{ isset($user)? $user->username : '' }}</span>
                
                <b class="caret"></b>
            </a>
            
            <ul class="dropdown-menu extended logout">
                @if($user->user_type=="Candidate")
                <?php $candidate = Candidate::find($user->user_id); ?>
                <li><a href="{{{url('profile')}}}/index/{{$candidate->cv_tbl_id}}"><i class=" fa fa-suitcase"></i>Profile</a></li>
                @elseif($user->user_type=="Employer")
                <li><a href="{{{url('profile')}}}/index/{{{$user->id}}}"><i class=" fa fa-suitcase"></i>Profile</a></li>
                @endif
                <li><a href="{{{url('users/change-password')}}}"><i class="fa fa-cog"></i> Settings</a></li>
                <li><a href="{{ url('logout') }}"><i class="fa fa-key"></i> Log Out</a></li>
            </ul>
        </li>
        <!-- user login dropdown end -->
        <!--<li>
            <div class="toggle-right-box">
                <div class="fa fa-bars"></div>
            </div>
        </li>-->
    </ul>
    <!--search & user info end-->
</div>
</header>
<!--header end-->
<!--sidebar start-->
<aside>
    <div id="sidebar" class="nav-collapse">
        <!-- sidebar menu start-->
        <div class="leftside-navigation">
            {{ Menu::getMenu() }}
        </div>
        <!-- sidebar menu end-->
    </div>
</aside>
<!--sidebar end-->
<!--main content start-->
<section id="main-content">

    @yield('wrapper')
</section>
<!--main content end-->

<!--right sidebar end-->
</section>
<!--end container -->
<!-- Placed js at the end of the document so the pages load faster -->
<!--Core js-->

<script src="{{ $theme }}assets/jquery-ui/jquery-ui-1.10.1.custom.min.js"></script>
<script src="{{ $theme }}bs3/js/bootstrap.min.js"></script>
<script src="{{ $theme }}js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script>
<script src="{{ $theme }}js/scrollTo/jquery.scrollTo.min.js"></script>
<script src="{{ $theme }}assets/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="{{ $theme }}js/nicescroll/jquery.nicescroll.js"></script>
<!--[if lte IE 8]>
<script language="javascript" type="text/javascript" src="js/flot-chart/excanvas.min.js"></script><![endif]-->
<script src="{{ $theme }}assets/skycons/skycons.js"></script>
<script src="{{ $theme }}assets/jquery.scrollTo/jquery.scrollTo.js"></script>
<script src="{{ $theme }}js/jquery.easing.min.js"></script>
<script src="{{ $theme }}assets/calendar/clndr.js"></script>
<script src="{{ $theme }}js/underscore-min.js"></script>
<script src="{{ $theme }}assets/calendar/moment-2.2.1.js"></script>
<script src="{{ $theme }}js/calendar/evnt.calendar.init.js"></script>
<script src="{{ $theme }}assets/jvector-map/jquery-jvectormap-1.2.2.min.js"></script>
<script src="{{ $theme }}assets/jvector-map/jquery-jvectormap-us-lcc-en.js"></script>
<!--<script src="{{ $theme }}assets/gauge/gauge.js"></script>-->
<!--clock init-->
<script src="{{ $theme }}assets/css3clock/js/script.js"></script>
<!--Easy Pie Chart-->
<script src="{{ $theme }}assets/easypiechart/jquery.easypiechart.js"></script>
<!--Sparkline Chart-->
<script src="{{ $theme }}assets/sparkline/jquery.sparkline.js"></script>
<!--Morris Chart-->
<!--<script src="{{ $theme }}assets/morris-chart/morris.js"></script>-->
<script src="{{ $theme }}assets/morris-chart/raphael-min.js"></script>

<script type="text/javascript" src="{{$theme}}assets/bootstrap-fileupload/bootstrap-fileupload.js"></script>
<!--jQuery Flot Chart-->
<!--<script src="{{ $theme }}assets/flot-chart/jquery.flot.js"></script>
<script src="{{ $theme }}assets/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="{{ $theme }}assets/flot-chart/jquery.flot.resize.js"></script>
<script src="{{ $theme }}assets/flot-chart/jquery.flot.pie.resize.js"></script>
<script src="{{ $theme }}assets/flot-chart/jquery.flot.animator.min.js"></script>
<script src="{{ $theme }}assets/flot-chart/jquery.flot.growraf.js"></script>-->
<script src="{{ $theme }}js/dashboard.js"></script>
<script src="{{ $theme }}js/custom-select/jquery.customSelect.min.js"></script>

<script src="{{$theme}}js/select2/select-init.js"></script>
<script src="{{ $theme }}js/scripts.js"></script>
<script type="text/javascript" src="{{$theme}}js/jquery-validate/jquery.validate.min.js"></script>
<script src="{{$theme}}js/jquery-validate/validation-init.js"></script>




</body>
</html>
