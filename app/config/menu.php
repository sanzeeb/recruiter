<?php
/**
 * Created by PhpStorm.
 * User: Himel
 * Date: 2/18/14
 * Time: 5:40 PM
 */

return array(
    'Candidate' => array(
            
            'Related Jobs' => array(
                'route'  => 'candidate/related-jobs',
                'access' => 'Candidate',
                'icon'   => 'fa-magnet'
            ),
            'Inquiry' => array(
            'route'  => 'inquiry/lists',
            'access' => 'Employer | Admin | Candidate',
            'icon'   => 'fa-question'
             ),
            'Interview' => array(
                'route' => 'interview/lists',
                'access' => 'Admin | Candidate | Employer',
                'child' => array(
                    /* 'Search schedule' => array('route' => 'interview/manage-interview','access'=>'Admin'),
                     'Requested candidate' => array('route' => 'interview/requested-interviewers','access'=>'Admin')*/
                ),
                'icon' => 'fa-user'
            ),
            'Offers' => array(
                'route'  => 'agreement/lists',
                'access' => 'Employer | Candidate',
                'icon'   => 'fa-tasks'
            ),
            'Test Project' => array(
              'route'  => 'tproject/lists',
              'access' => 'Employer | Candidate | Admin',
              'icon'   => 'fa-code-fork'
            ),
     ),
     'Employer' => array(
        'Dashboard' => array(
            'route' => 'dashboard',
            'icon'  => 'fa-dashboard',
            'access' => 'Admin | Employer'
        ),

        'Job posting' => array(
            'route' => 'job/lists',
            'access' => 'Admin | Employer',
            'icon' => 'fa-users'
        ),
        'Inquiry' => array(
            'route'  => 'inquiry/lists',
            'access' => 'Employer | Admin | Candidate',
            'icon'   => 'fa-question'
        ),

        'Favorites' => array(
            'route'   => 'employer/favorites',
            'access'  => 'Employer',
            'icon'    => 'fa-heart'
        ),

        'Interview' => array(
            'route' => 'interview/lists',
            'access' => 'Admin | Candidate | Employer',
            'child' => array(
                /* 'Search schedule' => array('route' => 'interview/manage-interview','access'=>'Admin'),
                 'Requested candidate' => array('route' => 'interview/requested-interviewers','access'=>'Admin')*/
            ),
            'icon' => 'fa-user'
        ),
        'Test Project' => array(
          'route'  => 'tproject/lists',
          'access' => 'Employer | Candidate | Admin',
          'icon'   => 'fa-code-fork'
        ),
        'Offers' => array(
            'route'  => 'agreement/lists',
            'access' => 'Employer | Candidate',
            'icon'   => 'fa-tasks'
        ),
       /* 'Manage schedule' => array(
            'route' => 'schedule/create-schedule',
            'access' => 'Admin  | Candidate | Employer',
            'icon' => 'fa-user'
        ),*/
        /*'Payments'=>array(
          'route'  => 'agreement/lists',
          'access' => 'Employer',
          'icon'   => 'fa-dollar'
        ),*/
     ),
    'Admin' => array(
        'Dashboard' => array(
            'route' => 'dashboard',
            'icon'  => 'fa-dashboard',
            'access' => 'Admin | Employer'
        ),


       /* 'Search candidate' => array(
            'route' => 'resume/bank-resume',
            'access' => '',
            
            'icon' => 'fa-user'
        ),

        'Schedule search' => array(
            'route' => 'interview/manage-interview',
            'access' => '',
            
            'icon' => 'fa-user'
        ),*/
        'Inquiry' => array(
            'route'  => 'inquiry/lists',
            'access' => 'Employer | Admin | Candidate',
            'icon'   => 'fa-question'
        ),
/*
        'Favorites' => array(
            'route'   => 'employer/favorites',
            'access'  => 'Admin | Candidate | Employer',
            'icon'    => 'fa-heart'
        ),
*/
        'Interview' => array(
            'route' => 'interview/lists',
            'access' => 'Admin | Candidate | Employer',
            'child' => array(
                /* 'Search schedule' => array('route' => 'interview/manage-interview','access'=>'Admin'),
                 'Requested candidate' => array('route' => 'interview/requested-interviewers','access'=>'Admin')*/
            ),
            'icon' => 'fa-user'
        ),

        /*'Related Jobs' => array(
            'route'  => 'candidate/related-jobs',
            'access' => 'Admin | Candidate | Employer',
            'icon'   => 'fa-magnet'
        ),*/

        /*'Offers' => array(
            'route'  => 'agreement/lists',
            'access' => 'Admin | Employer | Candidate',
            'icon'   => 'fa-tasks'
        ),*/



        'Message' => array(
            'route'  => 'message/lists',
            'access' => '',
            'icon'   => 'fa-tasks'
        ),

        'Manage agreements' => array(
            'route'  => 'agreement/agreement-list',
            'access' => 'Admin',
            'icon'   => 'fa-tasks'
        ),

        'Finance' => array(
            'route'  => 'payment/payment-approve',
            'access' => 'Admin',
            'icon'   => 'fa-dollar'
        ),
        

        'Test Project' => array(
          'route'  => 'tproject/lists',
          'access' => 'Employer | Candidate | Admin',
          'icon'   => 'fa-code-fork'
        ),
        
        /*'Manage schedule' => array(
            'route' => 'schedule/create-schedule',
            'access' => 'Admin  | Candidate | Employer',
            'icon' => 'fa-user'
        ),*/
        
        'Manage skills' => array(
            'route' => 'skills/create-skill',
            'access' => 'Admin',
            'icon' => 'fa-star'
        ),

        'Payments'=>array(
          'route'  => 'payment/index',
          'access' => 'Employer',
          'icon'   => 'fa-dollar'
        ),
        
        'Job posting' => array(
            'route' => 'job/lists',
            'access' => 'Admin | Employer',
            'icon' => 'fa-users'
        ),
        
        'Resume manage' => array(
            'route' => 'resume',
            'access' => 'Admin',
            'child' => array(
                'parent' => 'resume',
                'menus'   => array(         
                    'Import resume' => array('route' => 'resume/import','access'=>'Admin'),
                    'Create resume' => array('route' => 'resume/create-resume','access'=>'Admin'),
                    'Search resume' => array('route' => 'resume/bank-resume','access'=>'Admin')
                )
            ),
            'icon' => 'fa-file'
        ),

        /*'Employer' => array(
            'route' => 'employer',
            'access' => 'Admin',
            'child' => array(
                'Employer list' => array('route' => 'employer/list-employer','access'=>'Admin')
            ),
            'icon' => 'fa-user'
        ),*/
       

        'User' => array(
            'route' => 'users',
            'access' => 'Admin',
            'child' => array(
                'parent' => 'users',
                'menus'  => array(
                    'Employer list' => array('route' => 'users/view-employer-user','access'=>'Admin'),
                    'Candidate list' => array('route' => 'users/view-user','access'=>'Admin'),
                /*'Permission' => array('route' => 'users/permission','access'=>'Admin'),*/
                    'Admin users' => array('route' => 'users/index','access'=>'Admin'),
                    'User role' => array('route' => 'roles/lists','access'=>'Admin')
                )
            ),
            'icon' => 'fa-user'
        ),
        
        'Job Seeker Page' => array(
            'route' => 'jpage',
            'access' => 'Admin',
            'child' => array(
                'parent' => 'page',
                'menus'  => array(
                    'Job seeker about us' => array('route' => 'jpage/job-about-us','access'=>'Admin'),
                    'Daffodil Group' => array('route' => 'jpage/daffodil-group','access'=>'Admin'),
                )

            ),
            'icon' => 'fa-edit'
        ),
        'Recruiter Page' => array(
            'route' => 'page',
            'access' => 'Admin',
            'child' => array(
                'parent' => 'page',
                'menus'  => array(
                    'Reqcruiter about us' => array('route' => 'page/req-about-us','access'=>'Admin'),
                    'Our Service' => array('route' => 'page/our-service','access'=>'Admin'),
                    'Our Mission' => array('route' => 'page/our-mission','access'=>'Admin'),
                    'Why DJIT' => array('route' => 'page/why-djit','access'=>'Admin'),
                    'Home Country' => array('route' => 'page/home-country','access'=>'Admin'),
                    'Remote Programmer' => array('route' => 'page/remote-programmer','access'=>'Admin'),
                    'Part Time Programmer' => array('route' => 'page/part-time-programmer','access'=>'Admin'),
                    'Client say' => array('route' => 'page/client-say','access'=>'Admin'),
                    'Specialists' => array('route' => 'page/specialists','access'=>'Admin'),
                    'We\'re Trusted' => array('route' => 'page/trusted','access'=>'Admin'),
                    'Our Team' => array('route' => 'page/our-team','access'=>'Admin'),
                    'Social link' => array('route' => 'page/social-links','access'=>'Admin'),
                    'Gallery manage' => array('route' => 'page/gallery','access'=>'Admin'),
                )
            ),
            'icon' => 'fa-edit'
        ),

    )
);