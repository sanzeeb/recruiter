<?php
/**
 * Created by PhpStorm.
 * User: Himel
 * Date: 2/18/14
 * Time: 5:46 PM
 */

class Menu {


    private static function getUserType()
    {
        $user = Authenticate::check();
        return $user->user_type;
    }
    
    private static function getUserId()
    {
        $user = Authenticate::check();
        return $user->user_id;
    }
    public static function getRole()
    {
        $user = Authenticate::check();
        return $user->role_id;
    }
    private static function getId()
    {
        $user = Authenticate::check();
        return $user->id;
    }

    protected static function getConfig()
    {
       return require_once 'app/config/menu.php';
    }

    public static function getMenu($data = array())
    {

        $uri = self::getUri();
        $optionAttendance = Option::getData('attendance_default');

        $config = self::getConfig();
        if(!empty($config))
        {
            if(count($data)>0)
            {
                // sub menu here
                $url = trim(substr($_SERVER['REQUEST_URI'],1,strlen($_SERVER['REQUEST_URI'])));
                
                    echo '<ul class="sub">';
                

                foreach($data['menus'] as $menu => $option)
                {


                    if(!empty($option['access']))  // check that user have access to sub-route
                    {
                        $routeAccess = strtolower($option['access']);
                        $userType = strtolower(self::getUserType());
                        if(!preg_match('/'.$userType.'/',$routeAccess))
                            continue;
                    }else{
                        continue;
                    }

                    $route = explode("/",$option['route']);
                    
                   /* Helpers::debug($uri[1]);die();*/
                    echo '<li ';
                    if(isset($uri[1]) && ($uri[1] == $route[1]))
                        echo 'class="active"';
                    if(preg_match('/'.$route[1].'/', $url))
                        echo 'class="active"';
                    echo '>';

                    if(isset($option['child']) && (count($option['child'])>0)){
                        echo '<a href="javascript:;">'.$menu.'</a>';
                        self::getMenu($option['child']);
                    }else{
                        if(strtolower($optionAttendance) != 'automatic'){
                            if(preg_match('/automatic/',$option['route']))
                                continue;
                            echo '<a href="'.url($option['route']).'">'.$menu.'</a>';
                        }else{
                            echo '<a href="'.url($option['route']).'">'.$menu.'</a>';
                        }

                    }
                    echo '</li>';
                }
                echo '</ul>';

            }else{

                echo '<ul class="sidebar-menu" id="nav-accordion">';
                
                $user_typ=  Menu::getUserType();
                
                
               
                
                //}
                $url = trim(substr($_SERVER['REQUEST_URI'],1,strlen($_SERVER['REQUEST_URI'])));
                if($user_typ=="Candidate"){
                    
                $user_id=  Menu::getUserId();    
                
                $candidate=  Candidate::find($user_id);
                
                    if($candidate['cv_tbl_id']=="0"){
                        echo '<li class=" dcjq-parent-li">
                        <a href="resume/can-create-resume">
                        <i class="fa fa-user"></i>
                        <span>Create resume</span>
                        <span class="dcjq-icon"></span>
                        </a></li>';
                    }else{
                        echo "<li class='dcjq-parent-li'>";
                        
                        $css = ($url == 'candidate/dashboard')? "class='active'" : '';
                            
                        echo "<a {$css} href='".url('candidate/dashboard')."'>
                        <i class='fa fa-user'></i>
                        <span>Dashboard</span>
                        </a></li>";
                        
                        echo "<li class='dcjq-parent-li'>";
                        
                        $css = ($url == 'profile/index/'.$candidate->cv_tbl_id)? "class='active'" : '';
                            
                        echo "<a {$css} href='".url('profile/index/'.$candidate->cv_tbl_id)."'>
                        <i class='fa fa-user'></i>
                        <span>Edit resume</span>
                        </a></li>";
                        $css = ($url == 'resume/display-resume/'.$candidate->cv_tbl_id)? "class='active'" : '';
                        echo "<li class='dcjq-parent-li'>
                        <a {$css} href='".url('resume/display-resume/'.$candidate->cv_tbl_id)."'>
                        <i class='fa fa-user'></i>
                        <span>Profile</span>
                        </a></li>";
                            

                    }
                }
                
                if($user_typ=="Employer"){
                    
                $id=  Menu::getId(); 
                
                //$candidate=  Candidate::find($user_id);
                
                // if($candidate['cv_tbl_id']=="0"){
                $css = ($url == 'profile/index/'.$id)? "class='active'" : '';    
                echo "<li class='dcjq-parent-li'>
                    <a {$css} href='".url('profile/index/'.$id)."'>
                    <i class='fa fa-user'></i>
                    <span>Profile</span>
                   
                    </a></li>";
                
                }
                
                $role = self::getRole();
                foreach($config[$user_typ] as $menu => $option)
                {
                    $permission = Permission::getPermission($role,$menu,'view');
                    
                    if($role != 1)
                    {
                        if($menu != "Dashboard")
                        {
                            if(!$permission)
                                continue;
                        }    
                            
                    }
                    
                        if(!empty($option['access']))  // check that user have access to route
                        {
                            $routeAccess = strtolower($option['access']);
                            $userType = strtolower(self::getUserType());
                            if(!preg_match('/'.$userType.'/',$routeAccess))
                                continue;
                        }else{
                            continue;
                        }

                        if(isset($option['child']) && (count($option['child'])>0)){
                            echo '<li class="sub-menu dcjq-parent-li"><a ';
                            $url = trim(substr($_SERVER['REQUEST_URI'],1,strlen($_SERVER['REQUEST_URI'])));
                            if(preg_match('/^'.$option['route'].'/',$url))
                                echo 'class="active"';
                            echo ' href="javascript:;"><i class="fa '.$option['icon'].'"></i><span>'.$menu.'</span></a>';
                            self::getMenu($option['child']);

                        }else{
                            echo '<li><a ';
                            $uri = trim(substr($_SERVER['REQUEST_URI'],1,strlen($_SERVER['REQUEST_URI'])));
                            if($uri == $option['route'])
                                echo 'class="active"';
                            echo ' href="'.url($option['route']).'"><i class="fa '.$option['icon'].'"></i><span>'.$menu.'</span></a>';
                        }

                        echo '</li>';
                   
                }
                
                
                echo '</ul>';
            }
        }
    }

    public static function setUri($data)
    {
        Session::put('Uri',$data);
    }

    public static function getUri()
    {
        $uri = Session::get('Uri');
        if($uri != '')
        {
            return explode('/',$uri);
        }
        return 0;
    }
}
