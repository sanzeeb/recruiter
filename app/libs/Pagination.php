<?php

class Pagination{

	private $limit;
	private $offset;
	private $count;
	private $pages;

	function __construct($count,$limit,$offset)
	{
		$this->limit = $limit;
		$this->offset = $offset;
		$this->count = $count;
		$this->pages = ceil($this->count/$this->limit);
	}

	public function generateLinks()
	{
		$li = '';
		$current = Request::get('page');
		$links = ceil($this->count/$this->limit);
		$queryString = preg_replace("/&page=[0-9]/", "", $_SERVER['QUERY_STRING']);

		if($current>1)
			$li.='<li><a href="'.Request::url().'?'.$queryString.'&page=1">First</a></li>';
		for($i=1; $i<=$links; $i++)
		{
			if($links>10)
			{
				if(($i > ($current+5)) || ($i<($current-5)))
					continue;
			}

			

			if($i == $current)
				$li.='<li class="active"><span>'.$i.'</span></li>';
			else
				$li.='<li><a href="'.Request::url().'?'.$queryString.'&page='.$i.'">'.$i.'</a></li>';

			
		}
		if($current<$links)
			$li.='<li><a href="'.Request::url().'?'.$queryString.'&page='.$links.'">Last</a></li>';
		return $li;
	}
	
}