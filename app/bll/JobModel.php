<?php
/**
 * Created by PhpStorm.
 * User: Himel
 * Date: 6/9/14
 * Time: 5:13 PM
 */

class JobModel {

    public static function saveJob($userSession)
    {
        if(Request::isMethod('post'))
        {
            $job_title = Input::get('job_title');
            $job_vacancies = Input::get('job_vacancies');
            $salary_range = Input::get('salary_range');
            $other_benefite = Input::get('other_benefit');
            $job_location = Input::get('job_location');
            $job_source = Input::get('job_source');
            $job_nature = Input::get('job_nature');
            $skillId = Input::get('job_category');

            $ld = explode("/",Input::get('last_date'));
            $last_date = Helpers::dateTimeFormat('Y-m-d',$ld[2].'-'.$ld[0].'-'.$ld[1]);

            $responsibility = json_encode(Input::get('responsibility'));
            $educational = json_encode(Input::get('educational'));
            $experience = json_encode(Input::get('experience'));
            $additional = json_encode(Input::get('additional'));

            $jobs = new Jobs(); //::firstOrNew(array('job_title'=>$job_title,'skill_id'=>$skillId));
            if(!$jobs->exists)
            {
                $jobs->job_title = $job_title;
                $jobs->skill_id = $skillId;
                $jobs->job_vacancies = $job_vacancies;
                $jobs->salary_range = $salary_range;
                $jobs->other_benefit = $other_benefite;
                $jobs->job_location = $job_location;
                $jobs->job_source = $job_source;
                $jobs->job_nature = $job_nature;
                $jobs->job_responsibility = $responsibility;
                $jobs->eduactional_requirement = $educational;
                $jobs->experience_requirment = $experience;
                $jobs->extra_job_req = $additional;
                $jobs->created_by = (!empty($userSession->user_id))? $userSession->user_id : 0;
                $jobs->last_date = $last_date;
                $jobs->save();
                Helpers::addMessage(200, ' Job posted successfully');
                return 1;
            }else{
                Helpers::addMessage(400,' Job Already Posted by this title');
                return 0;
            }


        }else{
            return 0;
        }
    }

    public static function updateJob($userSession)
    {
        if(Request::isMethod('post'))
        {
            $job_title = Input::get('job_title');
            $job_id = Input::get('job_id');
            $job_vacancies = Input::get('job_vacancies');
            $salary_range = Input::get('salary_range');
            $other_benefit = Input::get('other_benefit');
            $job_location = Input::get('job_location');
            $job_source = Input::get('job_source');
            $job_nature = Input::get('job_nature');
            $skillId = Input::get('job_category');

            $responsibility = json_encode(Input::get('responsibility'));
            $educational = json_encode(Input::get('educational'));
            $experience = json_encode(Input::get('experience'));
            $additional = json_encode(Input::get('additional'));
            

            $ld = explode("/",Input::get('last_date'));
            $last_date = Helpers::dateTimeFormat('Y-m-d',$ld[2].'-'.$ld[0].'-'.$ld[1]);

            $jobs = Jobs::find($job_id);
            if(count($jobs))
            {
                $last_date_timestamp  = strtotime($jobs->last_date);
                $new_last_date_timestamp = strtotime($last_date);
                if($last_date_timestamp > $new_last_date_timestamp)
                    return 2;

                $updateData = array();
                if(!empty($job_title))
                    $updateData['job_title'] = $job_title;

                if(!empty($skillId))
                    $updateData['skill_id'] = $skillId;

                if(!empty($job_vacancies))
                    $updateData['job_vacancies'] = $job_vacancies;

                if(!empty($salary_range))
                    $updateData['salary_range'] = $salary_range;

                if(!empty($other_benefit))
                    $updateData['other_benefit'] = $other_benefit;

                if(!empty($job_location))
                    $updateData['job_location'] = $job_location;

                if(!empty($job_source))
                    $updateData['job_source'] = $job_source;

                if(!empty($job_nature))
                    $updateData['job_nature'] = $job_nature;

                if(!empty($job_responsibility))
                    $updateData['job_responsibility'] = $responsibility;

                if(!empty($eduactional_requirement))
                    $updateData['eduactional_requirement'] = $educational;

                if(!empty($experience_requirment))
                    $updateData['experience_requirment'] = $experience_requirment;

                if(!empty($experience_requirment))
                    $updateData['experience_requirment'] = $experience;

                if(!empty($extra_job_req))
                    $updateData['extra_job_req'] = $additional;

                if(!empty($last_date))
                    $updateData['last_date'] = $last_date;
                  
                Jobs::where('jobs_id',$jobs->jobs_id)->update($updateData);


                Helpers::addMessage(200, ' Job post updated');
                return 1;
            }else{
                Helpers::addMessage(400,' Job pos not found by id:'.$job_id);
                return 0;
            }

        }else{
            return 0;
        }
    }

    public static function getJobs($user,$limit=0)
    {
        $jobsCollection = array();
        $userId = (!empty($user->user_id))? $user->user_id : 0;
        if(!empty($limit))
        {
           
            if($userId> 0)
                return Jobs::where('created_by',$userId)->where('deleted_at','=','')->get();
            else
                return Jobs::get();
        }
        else{

            if($userId> 0)
             {
                $jobs = Jobs::where('created_by',$userId)->where('deleted_at','=','')->get();
                foreach($jobs as $i=> $job)
                {
                    $jobsCollection[$i]['jobs_id'] = $job->jobs_id;
                    $jobsCollection[$i]['job_title'] = $job->job_title;
                    $jobsCollection[$i]['job_category'] = $job->Skill->skill_name;
                    $jobsCollection[$i]['job_vacancies'] = $job->job_vacancies;
                    $jobsCollection[$i]['job_applied'] = count($job->JobCandidate);
                    $jobsCollection[$i]['job_salary_range'] = $job->salary_range;
                    $jobsCollection[$i]['last_date'] = $job->last_date;

                }
                return $jobsCollection;
             }  
            else
                {
                    $jobs = Jobs::all();
                    foreach($jobs as $i=> $job)
                    {
                        $jobsCollection[$i]['jobs_id'] = $job->jobs_id;
                        $jobsCollection[$i]['job_title'] = $job->job_title;
                        $jobsCollection[$i]['job_category'] = $job->Skill->skill_name;
                        $jobsCollection[$i]['job_vacancies'] = $job->job_vacancies;
                        $jobsCollection[$i]['job_applied'] = count($job->JobCandidate);
                        $jobsCollection[$i]['job_salary_range'] = $job->salary_range;
                        $jobsCollection[$i]['last_date'] = $job->last_date;

                    }
                    return $jobsCollection;
                } 
        }
    }

    public static function getJob($txt)
    {
        $results = Jobs::where('job_title','like',$txt.'%')->get();
        if(!empty($results))
        {
            foreach($results as $i=> $result)
            {
                $results[$i]['skill'] = $result->Skill;
            }
        }
        return $results;
    }

    public static function getJobsByRecruiter($id)
    {
        return Jobs::where('created_by',$id)->get();
    }

} 