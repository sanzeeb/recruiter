<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Email {

    private static $from = "info@carbon51.com";

    public static function sendMail($user,$data) {

        

// use Mail::send function to send email passing the data and using the $user variable in the closure
        Mail::send('emails.registration',
                $data, function($message) use ($user) {
            
                    $message->from(self::$from, 'DJIT');
                    
                    $message->to($user['email'], $user['name'])->subject('Welcome to DJIT!');
                    
                });
    }

     public static function sendCancelMail($user,$data) {

        Mail::send('emails.cancel',
                $data, function($message) use ($user,$data) {
            
                    $message->from(self::$from, 'DJIT');
                    
                    $message->to($user['email'], $user['name'])->subject('Cancel '.$data['cancel']);
                    
                });
    }

    public static function sendResetPasswordUrl($user,$data)
    {
        Mail::send('emails.auth.reminder',
            $data, function($message) use ($user) {

                $message->from(self::$from, 'Site Admin');

                $message->to($user->user_email, $user->username)->subject('Reset password request from DJIT!');

            });
    }

    public static function passwordChanged($user,$data)
    {
        Mail::send('emails.password-changed',
            $data, function($message) use ($user) {

                $message->from(self::$from, 'Site Admin');

                $message->to($user->user_email, $user->username)->subject('Your password chaged successfully');

            });
    }

}

?>
