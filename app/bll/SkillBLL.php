<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class SkillBLL {

    public static function skillCreate() {

        $skill_name = Input::get("skill_name");
        $skill_nam = Home::clean($skill_name);
        $skill_description = trim(Input::get("skill_description"));
        $display_order = Input::get("display_order");

        if ($skill_name != "" && $skill_description != "" && $display_order != "") {

            $skill = Skills::firstOrNew(array('skill_name' => $skill_name));
            if (!$skill->exists) {
                // save candiadte

                $skill->skill_name = $skill_name;
                $skill->display_order = $display_order;
                $skill->skill_description = $skill_description;

                if (Input::hasFile('skill_photo')) {

                    $ext = Input::file('skill_photo')->getClientOriginalExtension();
                    $photoName = Input::file('skill_photo')->getClientOriginalName();

                    $path = 'data/skill/';
                    //$photoName = time().'_'.str_replace(" ","_",$photoName);
                    $photoName = $skill_nam . '.' . $ext;
                    if (in_array(strtolower($ext), array('jpg', 'png', 'gif'))) {

                        Input::file('skill_photo')->move($path, $photoName);
                    }
                }
                $skill->skill_photo = $photoName;
                $skill->save();

                $msg = "Skill save successfully";
            } else {
                $msg = "Skill already exist ";
            }
        } else {
            $msg = "Please enter all required field";
        }
        return $msg;
    }

    public static function getSkills($limit) {

        if ($limit == "")
            $skills = Skills::all();
        else
            $skills = Skills::paginate($limit);

        return $skills;
    }

    public static function skillEdit() {

        $id = Input::get("id");
        $description = Input::get("description");
        $order = Input::get("order");
        $name = Input::get("name");

        // store
        //$skill = User::find($id);
        //Helpers::debug($user);
        //die();
        $skill = array(
            'skill_name' => $name,
            'skill_description' => $description,
            'display_order' => $order,
        );
        Skills::where('skill_id', $id)->update($skill);

        return 1;
    }

    public static function skillDisplayUpdate()
    {
        $id = Input::get("id");
        $status = Input::get("status");
        $skill = array(
            'status' => $status
        );
        Skills::where('skill_id', $id)->update($skill);
        return 1;

    }

}

?>
