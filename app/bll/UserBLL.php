<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class UserBLL {

    public static function userChangePassword() {

        $password = Input::get("password");
        $oldpassword = Input::get("Oldpassword");
        $re_password = Input::get("re_password");
        $user_id = Input::get("user_id");
        $msg = "";
        if ($password != "" && $oldpassword != "" && $re_password != "") {

            $user = User::firstOrNew(array('id' => $user_id));


            if ($user->exists) {
                if ($user['password'] != md5($oldpassword)) {
                    $msg = "Your old password is not correct";
                } else {

                    $userdt = array(
                        'password' => md5($password)
                    );

                    User::where('id', $user_id)->update($userdt);

                    $msg = "Changed successfully";
                }
            } else {
                $msg = "OPPS! Something wrong";
            }
        } else {
            $msg = "Please enter all required field";
        }

        return $msg;
    }

    public static function userRegister() {

        $firstname = Input::get("firstname");

        $lastname = Input::get("lastname");

        $email = Input::get("email");
        //$city = Input::get("city");

        $gender = Input::get("gender");

        $username = strtolower(trim(Input::get("username")));

        $password = Input::get("password");

        $re_password = Input::get("re_password");

        $user_type  = Input::get('user_type');

        $role_id    = Input::get('role_id');

        $created_by = Session::get('id');
       // Helpers::debug($user_type);die();

        if ($firstname != "" && $lastname != "" && $email != "" && $gender != "" && $username != "" && $password != "" && $re_password != "") {


            if ($password!=$re_password) {
                 $msg = "5";
                 
            } else {
                $user = User::firstOrNew(array('username' => $username));
                if (!$user->exists) {
                    // save candiadte

                    switch($user_type)
                    {
                        case 'Admin':
                            $user->password = md5($password);
                            $user->user_id = null;
                            $user->user_email = $email;
                            $user->username   = $username;
                            $user->role_id     = $role_id;
                            $user->user_status = 1;
                            $user->user_type   = $user_type;
                            $user->created_by = $created_by;
                            $user->save();
                            $msg = "1";
                        break;
                        case 'Employer':
                            $employer = Employer::firstOrNew(array('email' => $email));
                            if (!$employer->exists) {
                               
                                $employer->title = $firstname .' '. $lastname;
                                $employer->save();

                                /*$userdt = array(
                                    'email' => $email,
                                    'name' => $firstname . " " . $lastname
                                );*/

                                if ($employer->emp_id) {
                                    $user->password = md5($password);
                                    $user->user_id = $employer->emp_id;
                                    $user->user_email = $email;
                                    $user->username = $username;
                                    $user->user_status = 1;
                                    $user->role_id     = $role_id;
                                    $user->user_type = $user_type;
                                    $user->created_by = $created_by;
                                    $user->save();

                                   // Email::sendMail($userdt, $data);
                                }
                                $msg = "1";
                            }else {
                                //email exist
                                $msg = "2";
                            }
                        break;
                        case 'Candidate':
                            $name = $firstname. ' '. $lastname;
                            $cvTable = CvTable::firstOrNew(array('name'=>$name,'email'=>$email,'gender'=>$gender));
                            $cvTable->save();

                            $candidate = Candidate::firstOrNew(array('email' => $email));
                            if (!$candidate->exists) {
                                $candidate->firstname = $firstname;
                                $candidate->lastname = $lastname;
                                $candidate->email = $email;
                                $candidate->agreement = "agree this condition";
                                $candidate->gender = $gender;
                                $candidate->cv_tbl_id = $cvTable->cv_tbl_id;
                                $candidate->save();

                                /*$userdt = array(
                                    'email' => $email,
                                    'name' => $firstname . " " . $lastname
                                );

                                $data = array(
                                    'detail' => 'Your awesome detail here',
                                    'name' => $userdt['name']
                                );*/
                                if ($candidate->candidate_id) {
                                    $user->password = md5($password);
                                    $user->user_id = $candidate->candidate_id;
                                    $user->user_email = $email;
                                    $user->username = $username;
                                    $user->user_status = 1;
                                    $user->role_id     = $role_id;
                                    $user->user_type = $user_type;
                                    $user->created_by = $created_by;
                                    $user->save();

                                   // Email::sendMail($userdt, $data);
                                }
                                //save
                                $msg = "1";
                            } else {
                                //email exist
                                $msg = "2";
                            }
                        break;
                    }
                    
                } else {
                    //exist User
                    $msg = "3";
                }
            }
        } else {
            $msg = "4";
        }

        return $msg;
    }

    public static function userEdit() {

        //$firstname = Input::get("firstname");
        $id = Input::get("id");
       // $lastname = Input::get("lastname");

        $email = Input::get("email");
        //$city = Input::get("city");

        //$gender = Input::get("gender");

        $username = strtolower(trim(Input::get("username")));

        $role_id = Input::get('role_id');

        // store
        $users = User::find($id);
        if(count($users))
        {
            $users->user_email = $email;
            $users->username = $username;
            $users->role_id = $role_id;
        }
        //Helpers::debug($user);
        //die();
        /*$user = array(
            'username' => $username,
            'user_email' => $email
        );

        $user_id = $users->user_id;

        User::where('id', $id)->update($user);


        $candidate = array(
            'firstname' => $firstname,
            'lastname' => $lastname,
            'gender' => $gender
        );

        Candidate::where('candidate_id', $user_id)->update($candidate);*/
        $users->save();
        // redirect
        //Session::flash('message', 'Successfully updated User!');
        return 1;
    }

}

?>
