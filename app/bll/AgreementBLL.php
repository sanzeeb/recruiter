<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class AgreementBLL {
    /* Agreement sent function */

    public static function sentDisAgreement($sender_id, $user_id, $user_type) {

        $description = Input::get("disdescription");

        $agreement_id = Input::get("dis_agreement_id");

        $agreements = Agreements::where('agreement_id', $agreement_id)->first();
        //Inbox  notification to Employer
        $inbox = new Inbox();
        $inbox->subject = "Agreement Rejected &nbsp;";
        $inbox->sender_id = $sender_id;
        $inbox->receiver_id = $agreements->sender_id;
        $inbox->in_type = "agreement";
        $message = "<h5>Title:" . $agreements->agreement_title . "</h5>";
        $message .= "<h6>Agreement amount:" . $agreements->amount . "</h6>";
        $message .= "<p>Reason:" . $description . "</p>";
        if ($user_type == "Admin") {
            $message .= "<p>Rejected by DJIT</p>";
        } else {
            $cand = Candidate::where("candidate_id", $user_id)->first();
            $message .= "<p>Rejected by :" . $cand->firstname . "</p>";
        }
        $inbox->body = $message;
        $inbox->save();

        if ($user_type == "Admin") {
            AgreementBLL::update("Canceled", $agreement_id);
        } else {
            //Inbox2 forsent notification to admin
            $inbox2 = new Inbox();
            $inbox2->subject = "Agreement Rejected &nbsp;";
            $inbox2->sender_id = $sender_id;

            $inbox2->in_type = "agreement";

            $inbox2->receiver_id = 1;

            $inbox2->body = $message;
            $inbox2->save();
            AgreementBLL::update("Declined", $agreement_id);
        }
        return 1;
    }

    public static function sentAgreement($sender_id, $user_id) {


        $candidate_id = Input::get("candidate_id");
        $title = Input::get("title");
        $amount = Input::get("amount");


        $emp = Employer::find($user_id)->first();
        $cand = Candidate::where("candidate_id", $candidate_id)->first();


        $description = Input::get("description");
        $agre = new Agreements();
        $agre->sender_id = $sender_id;
        $agre->agreement_title = $title;
        $agre->amount = $amount;
        $agre->status = "Pending";
        $agre->description = $description;
        $agre->receiver_id = $cand->UserAcc->id;
        $agre->approver_id = 1;
        $sts = $agre->save();


        $inbox = new Inbox();
        $inbox->subject = "New Agreement sent by $emp->title &nbsp;for &nbsp;" . $cand->firstname;
        $inbox->sender_id = $sender_id;
        $inbox->receiver_id = 1;
        $inbox->in_type = "agreement";

        $message = "<h5>Title:" . $title . "</h5>";
        $message .= "<h6>Agreement amount:" . $amount . "</h6>";
        $message .= "<p>Details:" . $description . "</p>";
        $message .= "<p>Agreement Sent from:" . $emp->title . "</p>";
        $message .= "<p>Visit link:<a href='" . url(ASIAN_DOMAIN.'agreement/agreement-list') . "'>".url(ASIAN_DOMAIN.'agreement/agreement-list')."</a></p>";
        $inbox->body = $message;
        $inbox->save();

        return 1;
    }

    public static function CreateConfrimAgreement($sender_id, $user_id,$data) {


        $candidate_id = $data["candidate_id"];
        $title = $data["title"];
        $amount = $data["amount"];


        $emp = Employer::find($user_id)->first();
        $cand = Candidate::where("candidate_id", $candidate_id)->first();


        $description = $data["description"];
        $agre = new Agreements();
        $agre->sender_id = $sender_id;
        $agre->agreement_title = $data['title'];
        $agre->amount = $data['amount'];
        $agre->status = "Accepted";
        $agre->description = $data['description'];
        $agre->receiver_id = $data['receiver_id'];
        $agre->approver_id = 1;
        $sts = $agre->save();


       /* $inbox = new Inbox();
        $inbox->subject = "New Agreement for &nbsp;" . $cand->firstname . "&nbsp; " . $cand->lastname;
        $inbox->sender_id = $sender_id;
        $inbox->receiver_id = $candidate_id;
        $inbox->in_type = "agreement";

        $message = "<h5>Title:" . $title . "</h5>";
        $message .= "<h6>Agreement amount:" . $amount . "</h6>";
        $message .= "<p>Details:" . $description . "</p>";
        $message .= "<p>Agreement Sent from:" . $emp->title . "</p>";

        $inbox->body = $message;
        $inbox->save();*/

        return 1;
    }

    public static function update($status, $id) {
        $agreement = array(
            'status' => $status
        );
        Agreements::where('agreement_id', $id)->update($agreement);

        return 1;
    }

    public static function paymentPaid($status, $id) {
        $agreement = array(
            'status' => $status
        );
        
        $payment=Payments::where('payment_id', $id)->first();

        //for candidate inbox
                $inbox = new Inbox();
                $inbox->subject = "Payment paid successfully";
                $inbox->sender_id = $payment->sender_id;
                $inbox->receiver_id = $payment->receiver_id;
                $inbox->in_type = "payment";
                $message = "<h5>Title:" .$payment->AgreementAcc->agreement_title. "</h5>";
                $message .= "<h6>Payment amount:" . $payment->amount . "</h6>";
                $message .= "<p>Payment paid by &nbsp; DJIT</p>";
                $inbox->body = $message;
                $inbox->save();

                
                
                //notify to Recruiter  inbox
                $inbox2 = new Inbox();
                $inbox2->subject = "Payment Paid successfully";
               
                $inbox2->sender_id = 1;
                $inbox2->receiver_id = $payment->sender_id;
               
                $inbox2->in_type = "payment";
                $message = "<h5>Title:" .$payment->AgreementAcc->agreement_title. "</h5>";
                $message .= "<h6>Payment amount:" . $payment->amount . "</h6>";
                $message .= "<p>Payment paid by &nbsp; DJIT</p>";
               
                $inbox2->body = $message;
                $inbox2->save();
        
                Payments::where('payment_id', $id)->update($agreement);
        

        return 1;
    }

    public static function cancelPaid($status, $id) {
        $agreement = array(
            'status' => $status
        );
        $payment=Payments::where('payment_id', $id)->first();
        //for candidate inbox
                $inbox = new Inbox();
                $inbox->subject = "Payment Cancel ";
                $inbox->sender_id = $payment->sender_id;
                $inbox->receiver_id = $payment->receiver_id;
                $inbox->in_type = "payment";
                $message = "<h5>Title:" .$payment->AgreementAcc->agreement_title. "</h5>";
                $message .= "<h6>Payment amount:" . $payment->amount . "</h6>";
                $message .= "<p>Reason:Payment cancel by &nbsp; DJIT</p>";
                
                $message .= "<p>Payment cancel by &nbsp; DJIT</p>";
                $inbox->body = $message;
                $inbox->save();
                
                //Helpers::debug($inbox);
               // die();
                
                
                //notify to admin  inbox
                $inbox2 = new Inbox();
                $inbox2->subject = "Payment Cancel ";
               
                $inbox2->sender_id = 1;
                $inbox2->receiver_id = $payment->sender_id;
               
                $inbox2->in_type = "payment";
                $message = "<h5>Title:" .$payment->AgreementAcc->agreement_title. "</h5>";
                $message .= "<h6>Payment amount:" . $payment->amount . "</h6>";
                $message .= "<p>Reason:Payment cancel by &nbsp; DJIT</p>";
                
                $message .= "<p>Payment cancel by &nbsp; DJIT</p>";
                
               
                $inbox2->body = $message;
                $inbox2->save();
        
                Payments::where('payment_id', $id)->update($agreement);
        


        return 1;
    }

    public static function updateAgreement($status, $id, $sender_id, $user_type,$amount) {

        $agreement = array(
            'status' => $status,
        );

        if($status != "Accepted")
            $agreement['amount'] = $amount;

        Agreements::where('agreement_id', $id)->update($agreement);


        $agreements = Agreements::where('agreement_id', $id)->first();
        
        $user = User::where("id", $agreements->receiver_id)->first();
        $userEmp = User::where("id", $agreements->sender_id)->first();

        $cand = Candidate::where("candidate_id", $user->user_id)->first();

        $emp = Employer::where("emp_id", $userEmp->user_id)->first();

        $inbox = new Inbox();


        if ($user_type == "Candidate") {
            $inbox->subject = "New Agreement receive by &nbsp;" . $cand->firstname;

            $inbox->sender_id = $agreements->receiver_id;
            $inbox->receiver_id = $agreements->sender_id;
        } else {
            $inbox->subject = "New Agreement for you &nbsp;" . $cand->firstname;

            $inbox->sender_id = $agreements->sender_id;
            $inbox->receiver_id = $agreements->receiver_id;
        }
        $inbox->in_type = "agreement:" . $id;

        $message = "<h5>Title:" . $agreements->agreement_title . "</h5>";
        $message .= "<h6>Agreement amount:" . $agreements->amount . "</h6>";
        $message .= "<p>Details:" . $agreements->description . "</p>";

        if ($user_type == "Candidate") {
            $message .= "<p>Agreement Sent from:" . $cand->firstname .  "</p>";
        } else {
            $message .= "<p>Agreement Sent from:" . $emp->title . "</p>";
        }
        $inbox->body = $message;
        $inbox->save();

        return 1;
    }

}

?>
