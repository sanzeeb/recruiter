<?php
/**
 * Created by PhpStorm.
 * User: Himel
 * Date: 6/5/14
 * Time: 3:57 PM
 */

class ResumeModel{

    /**
     * @var CvTbl Object
     */
    private $_cvObj;


    public function saveResume(){
        if(Request::isMethod('post'))
        {
            $this->savePersonalData();
            $this->saveEducationInfo();
            $this->saveWorkInfo();
            $this->saveQualificationInfo();
            $this->saveSpecialization();
            Helpers::addMessage(200, ' Resume successfully saved');
            return 1;
        }else{

            Helpers::addMessage(500, ' Bad Request');
            return 0;
        }
    }

    /**
     *  Saving Candidate Information
     */
    public function saveCanResume(){
        if(Request::isMethod('post'))
        {
            $this->updateCanPersonalData();
            $this->saveEducationInfo();
            $this->saveWorkInfo();
            $this->saveQualificationInfo();
            $this->saveSpecialization();
            Helpers::addMessage(200, ' Resume successfully saved');
            return 1;
        }else{

            Helpers::addMessage(500, ' Bad Request');
            return 0;
        }
    }
    
   protected function updateCanPersonalData()
    {
        if(Request::isMethod('post'))
        {
            $name = Input::get('name');
            $year_of_exp = Input::get('year_of_exp');
            $father_name = Input::get('father_name');
            $mother_name = Input::get('mother_name');
            $dob = Helpers::dateTimeFormat("Y-m-d",trim(Input::get('dob')));
            $gender = Input::get('gender');
            $marital_status = Input::get('marital_status');
            $nationality = Input::get('nationality');
            $religion = Input::get('religion');
            $present_address = Input::get('present_address');
            $permanent_address = Input::get('permanent_address');
            $home_phone = Input::get('home_phone');
            $mobile = trim(Input::get('mobile'));

            $email = Input::get('email');
            $alternate_email = Input::get('alternate_email');

            $candidate_id = Input::get('candidate_id');

            $candidate = Candidate::find($candidate_id);
            
            $cvTable = new CvTable();
                $cvTable->name         = trim($name);
                $cvTable->mobile       = trim($mobile);
                $cvTable->email        = trim($email);
                $cvTable->year_of_exp  = $year_of_exp;
                $cvTable->dob          = Helpers::dateTimeFormat("Y-m-d",trim($dob));
                $cvTable->gender       = trim($gender);
                $cvTable->save();

            if(count($candidate))
            {
                $this->_cvObj = $cvTable;
                $updateData = array(
                    'firstname'   => $name,
                    'father_name' => $father_name,
                    'mother_name' => $mother_name,
                    'dob'         => $dob,
                    'gender'      => $gender,
                    'marital_status' => $marital_status,
                    'nationality'  => $nationality,
                    'religion'     => $religion,
                    'current_address' => $present_address,
                    'parmanent_address' => $permanent_address,
                    'phone'  => $home_phone,
                    'mobile' => $mobile,
                    'email'  => $email,
                    'cv_tbl_id'=>$cvTable->cv_tbl_id,
                    'alternative_email' => $alternate_email
                );


                if(Input::hasFile('photo'))
                {

                    $ext = Input::file('photo')->getClientOriginalExtension();
                    $photoName = Input::file('photo')->getClientOriginalName();

                    $path = 'data/profile/';
                    $candidatePhoto = trim($candidate->photo);
                    if(!empty($candidatePhoto) && file_exists($path.$candidatePhoto))
                    {
                        unlink($path.$candidatePhoto);
                    }
                    $photoName = time().'_'.str_replace(" ","_",$photoName);
                    if(in_array(strtolower($ext),array('jpg','png','gif'))){
                        Input::file('photo')->move($path,$photoName);
                    }
                }
                if(!empty($photoName))
                    $updateData['photo'] = $photoName;

                $updated = Candidate::where('candidate_id',$candidate_id)->update($updateData);

                return 1;
            }else{
                return 0;
            }
        }
    }

    
    
    /**
     *  Saving Personal Information
     */
    protected  function savePersonalData()
    {
        if(Request::isMethod('post'))
        {
            $name = Input::get('name');
            $username = Input::get('username');
            $password = Input::get('pass');
            $year_of_exp = Input::get('year_of_exp');
            $father_name = Input::get('father_name');
            $mother_name = Input::get('mother_name');
            $dob = Helpers::dateTimeFormat("Y-m-d",trim(Input::get('dob')));
            $gender = Input::get('gender');
            $marital_status = Input::get('marital_status');
            $nationality = Input::get('nationality');
            $religion = Input::get('religion');
            $present_address = Input::get('present_address');
            $permanent_address = Input::get('permanent_address');
            $home_phone = Input::get('home_phone');
            $mobile = trim(Input::get('mobile'));

            $email = Input::get('email');
            $alternate_email = Input::get('alternate_email');

            $mobileNumber = substr(trim($mobile),0,10);

            if($username == null)
            {
                $username = (preg_match('/^0+/',$mobileNumber))? $mobileNumber : '0'.$mobileNumber;
                $password = md5($username);

            }else{

                $password = md5($password);
            }  

            $user = User::firstOrNew(array('username'=>$username));


            if(!$user->exists)
            {
                $cvTable = new CvTable();
                $cvTable->name         = trim($name);
                $cvTable->mobile       = trim($mobile);
                $cvTable->email        = trim($email);
                $cvTable->year_of_exp  = $year_of_exp;
                $cvTable->dob          = Helpers::dateTimeFormat("Y-m-d",trim($dob));
                $cvTable->gender       = trim($gender);
                $cvTable->save();

                if($cvTable->cv_tbl_id)
                {
                    $this->_cvObj = $cvTable;
                    $candidate = new Candidate();
                    $candidate->firstname = $cvTable->name;
                    $candidate->email = $cvTable->email;
                    $candidate->alternative_email = $alternate_email;
                    $candidate->gender = $gender;
                    $candidate->dob = Helpers::dateTimeFormat("Y-m-d",trim($dob));
                    $candidate->cv_tbl_id = $cvTable->cv_tbl_id;
                    $candidate->father_name = $father_name;
                    $candidate->mother_name = $mother_name;
                    $candidate->marital_status = $marital_status;
                    $candidate->nationality = $nationality;
                    $candidate->religion = $religion;
                    $candidate->current_address = $present_address;
                    $candidate->parmanent_address = $permanent_address;
                    $candidate->phone = $home_phone;
                    $candidate->mobile = $mobile;
                    $candidate->status = 1;
                    $photoName = '';
                    if(Input::hasFile('photo'))
                    {

                        $ext = Input::file('photo')->getClientOriginalExtension();
                        $photoName = Input::file('photo')->getClientOriginalName();

                        $path = 'data/profile/';
                        $photoName = time().'_'.str_replace(" ","_",$photoName);
                        if(in_array(strtolower($ext),array('jpg','png','gif'))){
                            Input::file('photo')->move($path,$photoName);
                        }
                    }
                    $candidate->photo = $photoName;
                    $candidate->save();


                    // saving user
                    $user->password = $password;
                    $user->user_status = 1;
                    $user->user_type = 'Candidate';
                    $user->user_id = $candidate->candidate_id;
                    $user->role_id = 4;
                    $user->save();

                }

            }else{
                Helpers::addMessage(400, ' User already exist by this name');
            }

        }
    }

    /**
     *  Saving Educational Information
     */
    protected function saveEducationInfo()
    {
        if(Request::isMethod('post'))
        {
            $titles = Input::get('title');
            $major = Input::get('major');
            $institute = Input::get('institute');
            $result = Input::get('result');
            $passing_year = Input::get('passing_year');
            if($this->_cvObj->cv_tbl_id)
            {
                if(!empty($titles))
                {
                    foreach($titles as $e_i => $title)
                    {
                        $cvEdu = new CvEdu();
                        $cvEdu->cv_tbl_id = $this->_cvObj->cv_tbl_id;
                        $cvEdu->cv_edu_title = $title;
                        $cvEdu->major       = (!empty($major[$e_i]))? $major[$e_i] : '';
                        $cvEdu->institution = (!empty($institute[$e_i]))? $institute[$e_i] : '';
                        $cvEdu->result = (!empty($result[$e_i]))? $result[$e_i] : '';
                        $cvEdu->passing_year = (!empty($passing_year[$e_i]))? $passing_year[$e_i] : '';
                        if(!empty($cvEdu->cv_edu_title) && (!empty($cvEdu->major)) && (!empty($cvEdu->result)) && (!empty($cvEdu->passing_year)))
                        {
                            $cvEdu->save();
                        }
                        // Helpers::debug($cvEdu);
                    }
                }
            }
        }
    }

    /**
     *  Saving Work Experience
     */
    protected function saveWorkInfo()
    {
        if(Request::isMethod('post'))
        {
            $job_titles = Input::get('job_title');
            $start_dt = Input::get('start_dt');
            $end_dt = Input::get('end_dt');
            $company = Input::get('company');
            $designation = Input::get('designation');
            $department = Input::get('department');

            if($this->_cvObj->cv_tbl_id)
            {
                if(!empty($job_titles))
                {
                    foreach($job_titles as $j_i => $jtitle)
                    {
                        $cvJob = new CvJob();
                        $cvJob->cv_tbl_id = $this->_cvObj->cv_tbl_id;
                        $cvJob->title = $jtitle;
                        $cvJob->start = (!empty($start_dt[$j_i]))? $start_dt[$j_i] : '';
                        $cvJob->end = (!empty($end_dt[$j_i]))? $end_dt[$j_i] : '';
                        $cvJob->company_name = (!empty($company[$j_i]))? $company[$j_i] : '';
                        $cvJob->designation = (!empty($designation[$j_i]))? $designation[$j_i] : '';
                        $cvJob->department = (!empty($department[$j_i]))? $department[$j_i] : '';
                        if(!empty($cvJob->title) && (!empty($cvJob->company_name)))
                        {
                            $cvJob->save();
                        }
                       // Helpers::debug($cvJob);
                    }
                }
            }
        }
    }

    /**
     *  Saving Professional Qualification
     */
    protected function saveQualificationInfo()
    {
        if(Request::isMethod('post'))
        {
            $q_titles = Input::get('q_title');
            $q_institute = Input::get('q_institute');
            $q_year = Input::get('q_year');
            if($this->_cvObj->cv_tbl_id)
            {
                if(!empty($q_titles))
                {
                    foreach($q_titles as $q_i => $qtitle)
                    {
                        $cvQualification = new CvQualification();
                        $cvQualification->cv_tbl_id = $this->_cvObj->cv_tbl_id;
                        $cvQualification->cv_q_title = 	$qtitle;
                        $cvQualification->institution = (!empty($q_institute[$q_i])) ? $q_institute[$q_i] : '';
                        $cvQualification->finished_year = (!empty($q_year[$q_i])) ? $q_year[$q_i] : '';
                        if(!empty($cvQualification->cv_q_title) && (!empty($cvQualification->institution)) && (!empty($cvQualification->finished_year)))
                        {
                            $cvQualification->save();
                        }
                        //Helpers::debug($cvQualification);
                    }
                }
            }
        }
    }

    /**
     *  Saving Specialization
     */
    protected function saveSpecialization()
    {
        if(Request::isMethod('post'))
        {
            $specs = Input::get('specs');
            if($this->_cvObj->cv_tbl_id)
            {
                if(!empty($specs))
                {
                    foreach($specs as $s_i => $spec)
                    {
                        $cvSpec = new CvSpecialization();
                        $cvSpec->cv_tbl_id = $this->_cvObj->cv_tbl_id;
                        $cvSpec->key = $s_i;
                        $cvSpec->value = ($spec == 'on')? 'Yes' : 'No';

                        $cvSpec->save();
                        //Helpers::debug($cvSpec);
                    }
                }
            }
        }

    }

    public function updateResume()
    {
        if(Request::isMethod('post'))
        {
            /*$this->savePersonalData();
            $this->saveEducationInfo();
            $this->saveWorkInfo();
            $this->saveQualificationInfo();
            $this->saveSpecialization();
            Helpers::addMessage(200, ' Resume successfully saved');*/
            $this->updatePersonalData();
            $this->updateEducationInfo();
            $this->updateWorkInfo();
            $this->updateQualificationInfo();
            return Input::get('cv_id');
        }else{

            Helpers::addMessage(500, ' Bad Request');
            return 0;
        }
    }

    public function updateOthers()
    {
        if(Request::isMethod('post'))
        {
            $candidateId = Input::get('candidate_id');
            $sourceCode  = Input::get('source_code');
            $candidate = Candidate::find($candidateId);
            $updateData = array('source_code'=>$sourceCode);
            if(count($candidate))
            {
                if(Input::hasFile('photo_name'))
                {

                    $ext = Input::file('photo_name')->getClientOriginalExtension();
                    $photoName = Input::file('photo_name')->getClientOriginalName();

                    $path = 'data/academic_certificate/';
                    $candidatePhoto = trim($candidate->photo);
                    if(!empty($candidatePhoto) && file_exists($path.$candidatePhoto))
                    {
                        unlink($path.$candidatePhoto);
                    }
                    $photoName = time().'_'.str_replace(" ","_",$photoName);
                    if(in_array(strtolower($ext),array('jpg','png','gif','pdf'))){
                        Input::file('photo_name')->move($path,$photoName);
                    }
                }

                if(!empty($photoName))
                    $updateData['academic_certificate'] = $photoName;

                $updated = Candidate::where('candidate_id',$candidate->candidate_id)->update($updateData);
            }
        }
        return $candidate->cv_tbl_id;

    }

    

    

    protected function updatePersonalData()
    {
        if(Request::isMethod('post'))
        {
            $name = Input::get('name');
            $year_of_exp = Input::get('year_of_exp');
            $father_name = Input::get('father_name');
            $mother_name = Input::get('mother_name');
            $csummary = Input::get('csummary');
            $dob = Helpers::dateTimeFormat("Y-m-d",trim(Input::get('dob')));
            $gender = Input::get('gender');
            $marital_status = Input::get('marital_status');
            $nationality = Input::get('nationality');
            $religion = Input::get('religion');
            $present_address = Input::get('present_address');
            $permanent_address = Input::get('permanent_address');
            $home_phone = Input::get('home_phone');
            $mobile = trim(Input::get('mobile'));

            $email = Input::get('email');
            $alternate_email = Input::get('alternate_email');

            $candidate_id = Input::get('candidate_id');

            $candidate = Candidate::find($candidate_id);
            if(count($candidate))
            {
                $updateData = array(
                    'firstname'   => $name,
                    'father_name' => $father_name,
                    'mother_name' => $mother_name,
                    'csummary' => $csummary,
                    'dob'         => $dob,
                    'gender'      => $gender,
                    'marital_status' => $marital_status,
                    'nationality'  => $nationality,
                    'religion'     => $religion,
                    'current_address' => $present_address,
                    'parmanent_address' => $permanent_address,
                    'phone'  => $home_phone,
                    'mobile' => $mobile,
                    'email'  => $email,
                    'alternative_email' => $alternate_email
                );
                //Helpers::debug($updateData);die();

                if(Input::hasFile('photo'))
                {

                    $ext = Input::file('photo')->getClientOriginalExtension();
                    $photoName = Input::file('photo')->getClientOriginalName();

                    $path = 'data/profile/';
                    $candidatePhoto = trim($candidate->photo);
                    if(!empty($candidatePhoto) && file_exists($path.$candidatePhoto))
                    {
                        unlink($path.$candidatePhoto);
                    }
                    $photoName = time().'_'.str_replace(" ","_",$photoName);
                    if(in_array(strtolower($ext),array('jpg','png','gif'))){
                        Input::file('photo')->move($path,$photoName);
                    }
                }
                if(!empty($photoName))
                    $updateData['photo'] = $photoName;

                $updated = Candidate::where('candidate_id',$candidate_id)->update($updateData);
                $cvTblId = Input::get('cv_id');
                CvTable::where('cv_tbl_id',$cvTblId)->update(array('name'=>$name,'year_of_exp'=>$year_of_exp));


                return 1;
            }else{
                return 0;
            }
        }
    }

    
    
    protected function updateEducationInfo()
    {
        if(Request::isMethod('post'))
        {
            $cvTblId = Input::get('cv_id');
            $titles = Input::get('title');
            $major = Input::get('major');
            $institute = Input::get('institute');
            $result = Input::get('result');
            $passing_year = Input::get('passing_year');

                if(!empty($titles))
                {

                    foreach($titles as $e_i => $title)
                    {



                            $cvEduObj = CvEdu::firstOrNew(array('cv_tbl_id'=>$cvTblId,'cv_edu_id'=>$e_i));
                            if($cvEduObj->exists)
                            {

                                CvEdu::where('cv_edu_id',$cvEduObj->cv_edu_id)->update(array(
                                    'cv_edu_title' => $title,
                                    'major' => (!empty($major[$cvEduObj->cv_edu_id]))? $major[$cvEduObj->cv_edu_id] : '',
                                    'institution' => (!empty($institute[$cvEduObj->cv_edu_id]))? $institute[$cvEduObj->cv_edu_id] : '',
                                    'result' => (!empty($result[$cvEduObj->cv_edu_id]))? $result[$cvEduObj->cv_edu_id] : '',
                                    'passing_year' => (!empty($passing_year[$cvEduObj->cv_edu_id]))? $passing_year[$cvEduObj->cv_edu_id] : '',
                                ));
                            }else{

                                $cvEduObj->cv_tbl_id = $cvTblId;
                                $cvEduObj->cv_edu_title = $title;
                                $cvEduObj->major       = (!empty($major[$e_i]))? $major[$e_i] : '';
                                $cvEduObj->institution = (!empty($institute[$e_i]))? $institute[$e_i] : '';
                                $cvEduObj->result = (!empty($result[$e_i]))? $result[$e_i] : '';
                                $cvEduObj->passing_year = (!empty($passing_year[$e_i]))? $passing_year[$e_i] : '';
                                if(!empty($cvEduObj->cv_edu_title) && (!empty($cvEduObj->major)) && (!empty($cvEduObj->result)) && (!empty($cvEduObj->passing_year)))
                                {
                                    $cvEduObj->save();
                                }
                            }

                        // Helpers::debug($cvEdu);
                    }
                }

        }
    }

    protected function updateWorkInfo()
    {
        if(Request::isMethod('post'))
        {
            $job_titles = Input::get('job_title');
            $start_dt = Input::get('start_dt');
            $end_dt = Input::get('end_dt');
            $company = Input::get('company');
            $designation = Input::get('designation');
            $department = Input::get('department');
            $cvTblId = Input::get('cv_id');

                if(!empty($job_titles))
                {
                    foreach($job_titles as $j_i => $jtitle)
                    {
                        $cvJobObj = CvJob::firstOrNew(array('cv_tbl_id'=>$cvTblId,'cv_jobs_id'=>$j_i));
                        if($cvJobObj->exists)
                        {
                            CvJob::where('cv_jobs_id',$cvJobObj->cv_jobs_id)->update(array(
                                'title' => $jtitle,
                                'start' => (!empty($start_dt[$cvJobObj->cv_jobs_id]))? $start_dt[$cvJobObj->cv_jobs_id] : '',
                                'end' => (!empty($end_dt[$cvJobObj->cv_jobs_id]))? $end_dt[$cvJobObj->cv_jobs_id] : '',
                                'company_name' => (!empty($company[$cvJobObj->cv_jobs_id]))? $company[$cvJobObj->cv_jobs_id] : '',
                                'designation' => (!empty($designation[$cvJobObj->cv_jobs_id]))? $designation[$cvJobObj->cv_jobs_id] : '',
                                'department' => (!empty($department[$cvJobObj->cv_jobs_id]))? $department[$cvJobObj->cv_jobs_id] : '',
                            ));

                        }else{

                            $cvJobObj->cv_tbl_id = $cvTblId;
                            $cvJobObj->title = $jtitle;
                            $cvJobObj->start = (!empty($start_dt[$j_i]))? $start_dt[$j_i] : '';
                            $cvJobObj->end = (!empty($end_dt[$j_i]))? $end_dt[$j_i] : '';
                            $cvJobObj->company_name = (!empty($company[$j_i]))? $company[$j_i] : '';
                            $cvJobObj->designation = (!empty($designation[$j_i]))? $designation[$j_i] : '';
                            $cvJobObj->department = (!empty($department[$j_i]))? $department[$j_i] : '';
                            if(!empty($cvJobObj->title) && (!empty($cvJobObj->company_name)))
                            {
                                $cvJobObj->save();
                            }
                        }

                        // Helpers::debug($cvJob);
                    }
                }

        }
    }

    protected function updateQualificationInfo()
    {
        if(Request::isMethod('post'))
        {
            $q_titles = Input::get('q_title');
            $q_institute = Input::get('q_institute');
            $q_year = Input::get('q_year');
            $cvTblId = Input::get('cv_id');

                if(!empty($q_titles))
                {
                    foreach($q_titles as $q_i => $qtitle)
                    {
                        $cvQualification = CvQualification::firstOrNew(array('cv_tbl_id'=>$cvTblId,'cv_qualification_id'=>$q_i));
                        if($cvQualification->exists)
                        {
                            CvQualification::where('cv_qualification_id',$cvQualification->cv_qualification_id)->update(array(
                                'cv_q_title' => $qtitle,
                                'institution' => (!empty($q_institute[$cvQualification->cv_qualification_id])) ? $q_institute[$cvQualification->cv_qualification_id] : '',
                                'finished_year' => (!empty($q_year[$cvQualification->cv_qualification_id])) ? $q_year[$cvQualification->cv_qualification_id] : ''
                            ));
                        }
                        else
                        {

                            $cvQualification->cv_tbl_id = $cvTblId;
                            $cvQualification->cv_q_title = 	$qtitle;
                            $cvQualification->institution = (!empty($q_institute[$q_i])) ? $q_institute[$q_i] : '';
                            $cvQualification->finished_year = (!empty($q_year[$q_i])) ? $q_year[$q_i] : '';
                            if(!empty($cvQualification->cv_q_title) && (!empty($cvQualification->institution)) && (!empty($cvQualification->finished_year)))
                            {
                                $cvQualification->save();
                            }
                        }


                        //Helpers::debug($cvQualification);
                    }
                }

        }
    }

    public static function updateSpecs()
    {
        if(Request::ajax())
        {
            $key = Input::get('key');
            $cv_tbl_id = Input::get('cv_id');
            $status = Input::get('status');
            if($status == 'add')
            {
                $cvSpec = new CvSpecialization();
                $cvSpec->key = str_replace(" ","_",trim($key));
                $cvSpec->value= "yes";
                $cvSpec->cv_tbl_id = $cv_tbl_id;
                $cvSpec->save();
            }else{

                CvSpecialization::where('cv_tbl_id',$cv_tbl_id)->where('key',str_replace(" ","_",trim($key)))->delete();

            }
        }
    }


    public static function search()
    {

        if(Request::isMethod('post'))
        {
            $cvTbl = new CvTable();
            $fromYear = Input::get("fromYear");
            $toYear   = Input::get("toYear");
            $spec = Input::get("spec");
            $result = $cvTbl->getCv($fromYear,$toYear,$spec);

            return $result;
        }else{
            return 0;
        }

    }

    public function pdfReport()
    {

    }
} 