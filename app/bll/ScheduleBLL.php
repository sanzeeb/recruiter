<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class ScheduleBLL {

    public static function scheduleCreate() {

        $title = Input::get("title");
        $from_time=Input::get("availtime");
        $to_time=Input::get("availtimeto");
        $available_date = Input::get("available_date");
     $av_dt  =Helpers::dateTimeFormat("Y-m-d", $available_date);
        $uid = Session::get('id');
        if ($title != "" && $from_time != "" && $to_time != ""&& $available_date != "" ) {

           $schedule  =  new Schedule();
           
                // save candiadte
$dt_from=  strtotime(Helpers::dateTimeFormat("Y-m-d",$available_date). $from_time);
$dt_to=  strtotime(Helpers::dateTimeFormat("Y-m-d",$available_date). $to_time);

                    $schedule->title = $title;
                    $schedule->from_time = $from_time;
                    $schedule->to_time = $to_time;
                    $schedule->available_date = $av_dt;
                    $schedule->user_id = $uid;
                    $schedule->timestamp_from=$dt_from;
                    $schedule->timestamp_to=$dt_to;
                    $schedule->save();
                    
                  $msg = "Schedule save successfully";
            
             
        } else {
            $msg = "Please enter all required field";
        }
        return $msg;
    }
    
    
    public static function scheduleEdit(){
        
                $id=Input::get("id");
                $available_date=Input::get("available_date");
                $to_time=Input::get("to_time");
                $from_time=Input::get("from_time");
                $title=Input::get("title");
                $av_dt  =Helpers::dateTimeFormat("Y-m-d", $available_date);
                $dt_from=  strtotime(date("Y-m-d"). $from_time);
$dt_to=  strtotime(date("Y-m-d"). $to_time);
			// store
			$schedule = Schedule::find($id);
                        //Helpers::debug($user);
                        //die();
                        $schedule = array(
                            'title' => $title,
                            'from_time' => $from_time,
                            'to_time' => $to_time,
                            'timestamp_from'=>$dt_from,
                            'timestamp_to'=>$dt_to,
                            'available_date'=>$av_dt,
                            
                            
                         );
                         Schedule::where('avail_id', $id)->update($schedule);
                      
			return 1;
                        
		
    

    } 
    

}

?>
