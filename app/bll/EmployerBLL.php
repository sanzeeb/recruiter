<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class EmployerBLL{
    
    
    
    
    
    public static function empEdit() {

        $title = Input::get("title");
        $uid = Input::get("id");
        $user_id = Input::get("user_id");
        $address = Input::get("address");
        $phone = Input::get("phone");

        $email = Input::get("email");
        $username=Input::get("username");
        
        $website = Input::get("website");
      
        $contact_person = Input::get("contact_person");

    
        $contact_person_phone = Input::get("contact_person_phone");
        
        if ($title != "" && $email != "" && $address != "" && $contact_person != "" ) {

            		$user = User::find($uid);
           
                        $user = array(
                            
                            'user_email'=>$email
                         );
                        
                        $employer = User::find($user_id);
           
                      $employer = array(
                            
                            'email'=>$email,
                            'phone'=>$phone,
                            'title'=>$title,
                            'website'=>$website,
                            'address'=>$address,
                            'contact_person'=>$contact_person,
                            'contact_person_phone'=>$contact_person_phone,
                            
                            
                         );  
                        
                    if(Input::hasFile('emp_photo'))
                    {

                        $ext = Input::file('emp_photo')->getClientOriginalExtension();
                        $photoName = Input::file('emp_photo')->getClientOriginalName();

                        $path = 'data/employer/';

                        $photoName =$username.'.'.$ext;
                        if(in_array(strtolower($ext),array('jpg','png','gif'))){
                            
                            Input::file('emp_photo')->move($path,$photoName);
                            
                        }
                        $employer['photo_name']=$photoName;
                    }

                    
                    
                        
                        User::where('id', $uid)->update($user);
                        Employer::where('emp_id', $user_id)->update($employer);
                        
            $msg = "Employer Update successfully";
                
            
        } else {
            $msg = "Please enter all required field";
        }
        return $msg;
    }
    
    public static function empRegister() {

        $title = Input::get("title");

        $address = Input::get("address");
        $phone = Input::get("phone");

        $email = Input::get("email");
        
        $website = Input::get("website");
      
        $contact_person = Input::get("contact_person");

        $username = strtolower(trim(Input::get("username")));

        $contact_person_phone = Input::get("contact_person_phone");
        

        $password = Input::get("password");

        $re_password = Input::get("re_password");
 //Helpers::debug($_POST);
 //die();
        if ($password!=""&& $re_password!=""&&$title != "" && $email != "" && $address != "" && $contact_person != "" && $username != "") {

           
            if ($password!=$re_password) {
                 $msg = "5";
                 
            } else {
            $user = User::firstOrNew(array('username' => $username));
            if (!$user->exists) {
                // save candiadte
                $employer = Employer::firstOrNew(array('email' => $email));
                
                if (!$employer->exists) {
                    $employer->title = $title;
                    $employer->phone = $phone;
                    $employer->email = $email;
                    $employer->website = $website;
                    $employer->address = $address;
                    $employer->contact_person = $contact_person;
                    $employer->contact_person_phone = $contact_person_phone;
                    if(Input::hasFile('emp_photo'))
                    {

                        $ext = Input::file('emp_photo')->getClientOriginalExtension();
                        $photoName =Input::file('emp_photo')->getClientOriginalName();
                        $path = 'data/employer/';
                        //$photoName = time().'_'.str_replace(" ","_",$photoName);
                        $photoName =$username.'.'.$ext;
                        if(in_array(strtolower($ext),array('jpg','png','gif'))){
                            
                            Input::file('emp_photo')->move($path,$photoName);
                            
                        }
                    }
                    $employer->photo_name = $photoName;
                    $employer->save();
                    
                    $userdt = array(
                        'email' => $email,
                        'name' => $title
                    );

                    $data = array(
                        'detail' => 'Your awesome detail here',
                        'name' => $userdt['name']
                    );
                    if ($employer->emp_id) {
                        $user->password = md5($password);
                        $user->user_id = $employer->emp_id;
                        $user->user_email = $email;
                        $user->username = $username;
                        $user->user_status = 0;
                        $user->user_type = "Employer";
                        $user->save();
                        
                        Email::sendMail($userdt,$data);
                    }
                    $msg = "1";
                } else {
                    $msg = "2";
                }
            } else {
                //exist User
                $msg = "3";
            }
        } }else {
            //
            $msg = "4";
        }
        return $msg;
    }
    
}
?>
