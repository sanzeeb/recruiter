<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class RecruiterBLL{
    
    public static function empRegister() {

        $title = Input::get("title");

        $address = Input::get("address");
        $phone = Input::get("phone");

        $email = Input::get("email");
        
        $website = Input::get("website");
      
        $contact_person = Input::get("contact_person");

        $username = strtolower(trim(Input::get("username")));

        $contact_person_phone = Input::get("contact_person_phone");
        $password = Input::get("password");
        $re_password = Input::get("re_password");
        
        $agreement=Input::get("agreement");
        
        if ($title != "" && $email != "" && $address != "" && $contact_person != "" && $username != "" && $agreement!="" && $password!=""&& $re_password!="" && $phone!="") {
        
                if ($password != $re_password) {
                            
                 $msg = "Password dose not match with re password";
                 
            } else {
            $userad = User::firstOrNew(array('username' => $username));
            if (!$userad->exists) {
                // save candiadte
                $employer = Employer::firstOrNew(array('email' => $email));
                

                if (!$employer->exists) {
                    $employer->title = $title;
                    $employer->phone = $phone;
                    $employer->email = $email;
                    $employer->website = $website;
                    $employer->address = $address;
                    $employer->contact_person = $contact_person;
                    $employer->agreement=$agreement;
                    $employer->contact_person_phone = $contact_person_phone;

                    $employer->save();
                    
                    $userdt = array(
                        'email' => $email,
                        'name' => $title
                    );
                    if ($employer->emp_id) {
                        $token = md5($employer->emp_id.$email);
                        $data = array(
                            'detail' => 'Your awesome detail here',
                            'welcome' => 'Asian Engineers',
                            'name' => $userdt['name'],
                            'password'=>$password,
                            'username'=>$username,
                            'link'    => ASIAN_DOMAIN.'/home/confirm-email/'.$token,
                        );
                        $userad->password = md5($password);
                        $userad->user_id = $employer->emp_id;
                        $userad->user_email = $email;
                        $userad->username = $username;
                        $userad->user_status = 0;
                        $userad->deleted_at  = 1;
                        $userad->role_id = 3;
                        $userad->user_type = "Employer";
                        $userad->token = $token;
                        $userad->save();
                        
                        Email::sendMail($userdt,$data);
                    }
                    $msg = "Recruiter account created successfully check email for confirm";
                } else {
                    $msg = "Email already exist ";
                }
            } else {
                //exist User
                $msg = "Recruiter already exist by this user name";
            }
        } 
        }else {
            $msg = "Please enter all required field";
        }
        return $msg;
    }

    public static function getRecruiters()
    {
       return Employer::all();
    }
   
}
?>
