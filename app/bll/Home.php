<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Home {

    public static function gaEdit() {

        $id = Input::get("id");
        $description = Input::get("description");

        $name = Input::get("name");


        $skill = array(
            'title' => $name,
            'description' => $description
        );
        Gallery::where('ga_id', $id)->update($skill);

        return 1;
    }

    public static function clientCom() {

        $ga_id = trim(Input::get("content_description"));

        $title = trim(Input::get("title"));
        $msg = "";
        if ($ga_id != "" && $title != "") {


            $client = new Client();

            $client->description = $ga_id;
            $client->name = $title;
            $photoName = '';
            if (Input::hasFile('photo')) {

                $ext = Input::file('photo')->getClientOriginalExtension();
                $photoName = Input::file('photo')->getClientOriginalName();

                $path = 'data/gallery/';
                $photoName = time() . '_' . str_replace(" ", "_", $photoName);
                if (in_array(strtolower($ext), array('jpg', 'png', 'gif'))) {
                    Input::file('photo')->move($path, $photoName);
                }
            }
            $client->image_name = $photoName;

            $client->save();
            $msg = "Comment addeded  successfully";
        } else {
            $msg = "Please enter all req field";
        }
        return $msg;
    }

    public static function upImages() {

        $ga_id = Input::get("ga_id");

        $msg = "";
        if ($ga_id != "") {


            $image = new Images();

            $image->ga_id = $ga_id;

            $photoName = '';
            if (Input::hasFile('photo')) {

                $ext = Input::file('photo')->getClientOriginalExtension();
                $photoName = Input::file('photo')->getClientOriginalName();

                $path = 'data/gallery/';
                $photoName = time() . '_' . str_replace(" ", "_", $photoName);
                if (in_array(strtolower($ext), array('jpg', 'png', 'gif'))) {
                    Input::file('photo')->move($path, $photoName);
                }
            }
            $image->image_name = $photoName;

            $image->save();
            $msg = "Images uploaded successfully";
        } else {
            $msg = "Please slect gallery";
        }
        return $msg;
    }

    public static function createGallery() {


        $content_description = trim(Input::get("content_description"));
        $title = trim(Input::get("title"));
        $msg = "";
        if ($content_description != "") {
            $gallery = Gallery::firstOrNew(array('title' => $title));

            if (!$gallery->exists) {
                $gallery->description = $content_description;
                $gallery->title = $title;

                $gallery->save();
            } else {
                $msg = "Same album already exist";
            }
        } else {
            $msg = "Please enter title and page content";
        }
        return $msg;
    }

    public static function reqAbUs() {



        $content_description = trim(Input::get("content_description"));
        $title = trim(Input::get("title"));
        $page_id = trim(Input::get("page_id"));
        $msg = "";
        if ($content_description != "") {

            if (Input::hasFile('photo_name')) {

                $ext = Input::file('photo_name')->getClientOriginalExtension();
                $photoName = Input::file('photo_name')->getClientOriginalName();

                $path = 'data/cv/';

                $photoName = 'reqsimg' . '.' . $ext;
                if (in_array(strtolower($ext), array('jpg', 'png', 'gif'))) {

                    Input::file('photo_name')->move($path, $photoName);
                }

                $about = array(
                    'content' => $content_description,
                    'title' => $title,
                    'photo_name' => $photoName
                );



            } else {
                $about = array(
                    'content' => $content_description,
                    'title' => $title,
                );
                About::where('page_id', $page_id)->update($about);

                $msg = "Please select images";
            }
        } else {
            $msg = "Please enter page content";
        }
        return $msg;
    }

    public static function jobAbUs() {

        $content_description = trim(Input::get("content_description"));
        $title = trim(Input::get("title"));
        $page_id = trim(Input::get("page_id"));
        $msg = "";
        if ($content_description != "") {

            if (Input::hasFile('photo_name')) {

                $ext = Input::file('photo_name')->getClientOriginalExtension();
                $photoName = Input::file('photo_name')->getClientOriginalName();

                $path = 'data/cv/';

                $photoName = 'jobsimg' . '.' . $ext;
                if (in_array(strtolower($ext), array('jpg', 'png', 'gif'))) {

                    Input::file('photo_name')->move($path, $photoName);
                }

                $about = array(
                    'content' => $content_description,
                    'title' => $title,
                    'photo_name' => $photoName
                );


                About::where('page_id', $page_id)->update($about);
            } else {


                $msg = "Please select images";
            }
        } else {
            $msg = "Please enter page content";
        }
        return $msg;
    }

    public static function clean($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-]/', '_', $string); // Removes special chars.
    }

    public static function register() {

        $firstname = Input::get("firstname");

        $lastname = Input::get("lastname");

        $email = Input::get("email");
        //$city = Input::get("city");

        $gender = Input::get("gender");

        $username = strtolower(trim(Input::get("username")));

        $password = Input::get("password");

        $re_password = Input::get("re_password");

        $agreement = Input::get("agreement");

        if ($firstname != "" && $lastname != "" && $email != "" && $gender != "" && $username != "") {

            $user = User::firstOrNew(array('username' => $username));
            if (!$user->exists) {
                // save candiadte
                $candiate = Candidate::firstOrNew(array('email' => $email));

                if (!$candiate->exists) {
                    $candiate->firstname = $firstname;
                    $candiate->lastname = $lastname;
                    $candiate->email = $email;
                    $candiate->agreement = $agreement;
                    $candiate->gender = $gender;

                    $candiate->save();

                    $userdt = array(
                        'email' => $email,
                        'name' => $firstname . " " . $lastname
                    );

                    $data = array(
                        'detail' => 'Your awesome detail here',
                        'name' => $user['name']
                    );
                    if ($candiate->candidate_id) {


                        $user->password = md5($password);
                        $user->user_id = $candiate->candidate_id;
                        $user->user_email = $email;
                        $user->username = $username;
                        $user->user_status = 0;
                        $user->user_type = "Employer";
                        $user->save();

                        Email::sendMail($userdt, $data);
                    }
                    $msg = "Candiate save successfully";
                } else {
                    $msg = "Candiate already exist ";
                }
            } else {
                //exist User
                $msg = "User already exist by this user name";
            }
        } else {
            $msg = "Please enter all required field";
        }
        return $msg;
    }

}

?>
