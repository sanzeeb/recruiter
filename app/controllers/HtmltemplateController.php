<?php
/**
 * Created by PhpStorm.
 * User: PEASH
 * Date: 11/5/14
 * Time: 2:33 PM
 */

class HtmltemplateController extends BaseController{

    protected $layout;
    protected $default_route;
    protected $_userSession;
    protected $pageLimit = 20;
    public function __construct() {

        $this->layout = Theme::getLayout();
        $this->default_route = 'dashboard/index';
        $this->_userSession = Authenticate::check();
        $this->_userSession->user_type;

        $inbox = Inbox::where('receiver_id',$this->_userSession->id)->where('read_status',0)->orderBy('inbox_id','desc')->take(5)->get();

        View::share('messages',$inbox);

        $comment = Comment::where('receiver_id', $this->_userSession->id)->where('read_status', 0)->get();
        View::share('comments', $comment);
        View::share('user', $this->_userSession);
        View::share('theme', Theme::getTheme());

    }

    public function inquery(){
        return Theme::make('inquery.inquery',array());
    }

} 