<?php

class RecruiterController extends BaseController {
    /*
      |--------------------------------------------------------------------------
      | Default Home Controller
      |--------------------------------------------------------------------------
      |
      | You may wish to use controllers instead of, or in addition to, Closure
      | based routes. That's great! Here is an example controller method to
      | get you started. To route to this controller, just add the route:
      |
      |	Route::get('/', 'HomeController@login');
      |
     */

    
        protected $layout;

    /**
     * for set default route
     * @var string
     */
    protected $default_route;

    /**
     * for set user session data
     * @var string
     */
    protected $_userSession;

    //protected $pageLimit;

    public function __construct()
    {
        Theme::setTheme('recruit');
        $this->layout = Theme::getLayout();
        $this->default_route = 'activities/index';
        $this->pageLimit = 20;

        $this->_userSession = Authenticate::hasSession();  // check is user logged in
        if(!empty($this->_userSession) && ($this->_userSession->remember_me))
        {
            $expireTime = (60*24*360);
            Config::set('session.lifetime',$expireTime);
        }
        
        if(!empty($this->_userSession->user_id))
            $jobCount = Jobs::where('created_by',$this->_userSession->user_id)->count();
        else
            $jobCount = 0;
        View::share('jobCount',$jobCount);

        View::share('fb', Option::getData('fb'));
        View::share('twitter',Option::getData('twitter'));
        View::share('gplus', Option::getData('gplus'));
        View::share('linkedin',Option::getData('linkedin'));

        /*Helpers::debug($_COOKIE['userId']);
        Helpers::debug($_COOKIE['pass']);*/
    }
    
    public function registerRecruiter() {

     $viewModel = array(
            'theme' => Theme::getTheme()
        );

    if (Request::isMethod("post")) {


            $msg = RecruiterBLL::empRegister();
            $viewModel["msg"] = $msg;
            
        }

        $id=   Request::segment(3);

        $viewModel['cv_id']   =$id;
        $viewModel['gallery'] = Gallery::first();
        $viewModel['userId']  =  (!empty($_COOKIE['userId']))? $_COOKIE['userId'] : '';
        

        return Theme::make('login-registration', $viewModel);
        
        
    }
    
     /*public function topEngineers() {

     $viewModel = array(
            'theme' => Theme::getTheme(),
            'user'=>  Authenticate::hasSession()
        );


    $skill=base64_decode(Request::segment(3));
   
    $keyword = trim(Request::get('name'));
    $age = trim(Request::get('age'));
    $yxp = trim(Request::get('yxp'));
    $cvTbl=new CvTable();
    
   // $expertises=  CvSpecialization::where('key',  $skill)->get();
  
   $expertises=$cvTbl->getBySkill($skill,$keyword,$age,$yxp);
   //Helpers::LastQuery();

    $viewModel['expertises']=$expertises;
    $viewModel['gallery']= Gallery::first();
    $viewModel['skill']= $skill;

    return Theme::make('top_engineers', $viewModel);
     
        
    }*/

    public function topEngineers() {

    $viewModel = array(
        'theme' => Theme::getTheme(),
        'user'=>  $this->_userSession
    );

    $skill=base64_decode(Request::segment(3));

    $keyword = trim(Request::get('name'));
    $age = trim(Request::get('age'));
    $yxp = trim(Request::get('yxp'));
    $cvTbl=new CvTable();
    $searchskills = Request::get('skills');
    
    
   // $expertises=  CvSpecialization::where('key',  $skill)->get();
    $limit = 20; 
    $page = Request::get('page');
    $offset = (!empty($page) && ($page>1))? ($page * 10) : 0;
    $count = 0;
   if(!empty($searchskills))
   {
    $count = $cvTbl->getBySkill($searchskills,$keyword,$age,$yxp,0,0,true);
    $expertises=$cvTbl->getBySkill($searchskills,$keyword,$age,$yxp,$offset,$limit,false);

   }else
   {

    $count = $cvTbl->getBySkill($skill,$keyword,$age,$yxp,0,0,true);
    $expertises=$cvTbl->getBySkill($skill,$keyword,$age,$yxp,$offset,$limit,false);
   
   }

   $pagination = new Pagination($count,$limit,$offset);
   //Helpers::debug($pagination);
   $links = $pagination->generateLinks();

   
   $skills = array();
   $distinctSkills = array();
   foreach($expertises as $exp)
   {
        $cvSpecializations = CvSpecialization::where('cv_tbl_id',$exp->cv_tbl_id)->get();
        $skills[$exp->cv_tbl_id] = $cvSpecializations;

        foreach($cvSpecializations as $spec)
        {
            $distinctSkills[$spec->key] = strtolower(trim($spec->key));
        }
   }

   if(empty($distinctSkills))
   {
        $allSkills = Skills::all();
        foreach($allSkills as $sk)
        {
            $distinctSkills[$sk->skill_name] =strtolower(trim($sk->skill_name));
        }

   }

  
    $viewModel['expertises']=$expertises;
    $viewModel['searchedSkills'] = $searchskills;
    $viewModel['skill']=strtolower(trim($skill));
    $viewModel['skills'] = $skills;
    $viewModel['disSkills'] =$distinctSkills;
    $viewModel['gallery']= Gallery::first();
    $viewModel['links'] = $links;
    return Theme::make('top_engineers_new', $viewModel);
     
        
    }

    
    /*Render home */
    
     public function index() {

     $viewModel = array(
            'theme' => Theme::getTheme()
        );

        $skills =  Skills::where('status',1)->orderBy("display_order","asc")->paginate(20);
        
        $viewModel["skills"]=$skills;
        $viewModel['gallery']= Gallery::first();
        
        
        $data[]=array();

        $viewModel['comments']=Client::all();
         $feed_array = array();
         $url = 'http://blog.asian-engineers.com/?feed=rss2';
        /*$xmlString = simplexml_load_file($url);

         $json = json_encode($xmlString);
         $jsonArray = json_decode($json);*/
         try{
             $feed = new DOMDocument;
             $feed->load($url);


             foreach($feed->getElementsByTagName('item') as $i=> $item){
                 $story_array = array (
                     'title' => $item->getElementsByTagName('title')->item(0)->nodeValue,
                     'desc' => $item->getElementsByTagName('description')->item(0)->nodeValue,
                     'link' => $item->getElementsByTagName('link')->item(0)->nodeValue,
                     'date' => $item->getElementsByTagName('pubDate')->item(0)->nodeValue
                 );

                if($i<3)
                    array_push($feed_array, $story_array);
                else
                    break;
             }
         }catch(Exception $ex)
         {

         }

        $viewModel['homeCountry'] = About::where('slug','home-country')->where('type','recruiter')->first();
        $viewModel['remoteProgrammer'] = About::where('slug','remote-programmer')->where('type','recruiter')->first();
        $viewModel['partTimeProgrammer'] = About::where('slug','part-time-programmer')->where('type','recruiter')->first();
        $viewModel['feeds'] = $feed_array;

        return Theme::make('recruit_home', $viewModel);
        
        
    }
    
    public function galleryDtl() {

     $viewModel = array(
            'theme' => Theme::getTheme()
        );

         $ga_id=   Request::segment(3);
        $skills=  Skills::paginate(27);
        
        $viewModel["skills"]=$skills;
        $viewModel['gallery']= Gallery::first();
        
        $viewModel['galleries']= Gallery::where('ga_id',$ga_id)->first();
         
        $viewModel['comment']=Client::first();
       
        
        return Theme::make('gallery_dtl', $viewModel);
        
        
    }
    
    public function gallery() {

     $viewModel = array(
            'theme' => Theme::getTheme()
        );

        $skills=  Skills::paginate(27);
        $viewModel["skills"]=$skills;
       
        $viewModel['gallery']= Gallery::first();
         $viewModel['galleries']= Gallery::all();
         
        $viewModel['comment']=Client::first();
       
        
        return Theme::make('gallery', $viewModel);
        
        
    }
    
    /*End home */
    
    
     public function about() {

        Theme::setTheme('recruit');
     $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => Authenticate::hasSession()
        );
     
         $about=  About::where('type', 'recruiter')->first();
         $ourMission =  About::where('type', 'recruiter')->where('slug',"our-mission")->first();
         $specialists =  About::where('type', 'recruiter')->where('slug',"specialists")->first();
         $trusted =  About::where('type', 'recruiter')->where('slug',"trusted")->first();
         $ourTeam =  About::where('type', 'recruiter')->where('slug',"our-team")->first();
         $midContent =  About::where('type', 'recruiter')->where('slug',"mid-content")->first();
         $viewModel['about'] = $about;
         $viewModel['ourMission']  = $ourMission;
         $viewModel['specialists'] = $specialists;
         $viewModel['midContent']  = $midContent;
         $viewModel['trusted'] = $trusted;
         $viewModel['ourTeam'] = $ourTeam;
         $viewModel['gallery'] = Gallery::first();
        return Theme::make('about', $viewModel);
        
        
    }
    
    
    
    public function contact() {
        
    Theme::setTheme('recruit');
     $viewModel = array(
            'theme' => Theme::getTheme()
        );
             $viewModel['gallery']= Gallery::first();
        return Theme::make('contact', $viewModel);
        
        
    }
    
    
    /* Render login form */

    public function login() {

        $user = Authenticate::hasSession();

        if ($user) {
            $userExist = User::find($user->id);
            if ($userExist)
                return Redirect::to('dashboard')->withCookie(Cookie::forever('name',$user->username));
        }

        $viewModel = array(
            'theme' => Theme::getTheme()
        );

        
        //Helpers::debug(Theme::getTheme());exit();  
        return Theme::make('login', $viewModel);
    }

    /* Action for authentication */

    public function authenticate() {

        if (Request::isMethod('post')) {

            $userId   = Input::get('userId');
            $password = Input::get('pass');
            $type_log = Input::get('log_type');

            /*$response = Response::make('authenticate');

            $response->withCookie(Cookie::forever('name', $userId));*/

            setcookie('userId',$userId,time()+(60*60*360));

            Session::put('type_log', $type_log);
            try {
                if(Input::has('remember'))
                {

                    $login = Authenticate::login($userId, $password,1);

                }else{
                    $login = Authenticate::login($userId, $password,0);
                }

                if(is_array($login)){

                    
                    if($login['type'] == 'Candidate')
                        return Redirect::to(JOB_DOMAIN.'jobseeker/register-job-seeker');
                    else
                        return Redirect::to(ASIAN_DOMAIN.'recruiter/register-recruiter'); 
                    
                } 

                if ($login == 1) {
                    
                    $cv_tbl_id = Input::get('cv_tbl_id');
                    
                    if($cv_tbl_id!=""){
                          
                        return Redirect::to('resume/display-resume/'.$cv_tbl_id);
                    }
                    return Redirect::to('dashboard');

                } elseif($login == 2){

                    Session::flash('message', 'Your account not verified yet. please confirm your email address to verify your account');
                    return Redirect::to('recruiter/register-recruiter');

                } else {

                    Session::flash('message', 'User and password incorrect');
                    return Redirect::to('recruiter/register-recruiter');

                }
            } catch (DbConnectionException $e) {
                echo $e->getMessage();
                die();
            }
        }
    }

    /* Logout a user and redirect to login page */

    public function logout() {
        if (Authenticate::check()) {

            Session::forget('user');
            return Redirect::to('/');
            exit();
        }
    }

    public function forgetPass()
    {

        $viewModel = array(
            'theme' => Theme::getTheme(),
            'gallery'=> Gallery::first(),
            'Msg' =>''
        );

        if(Request::isMethod('post'))
        {

            $email = Input::get('email');
            $user = User::where('user_type','Employer')->where('user_email',$email)->first();

            if(!empty($email))
            {
                if(count($user) && $user->user_email == $email)
                {
                    $data= array();
                    $data['token'] = $token = sha1($user->username.$user->user_email.time());

                    $expire_time = time()+(60*60*24);
                    $user->reset_token = $token;
                    $user->token_expire = $expire_time;
                    $user->save();
                     $data['user'] = $user;
                     $data['link'] = ((preg_match('/'.$_SERVER['SERVER_NAME'].'/',ASIAN_DOMAIN))? ASIAN_DOMAIN.'recruiter/reset-pass/'.$token : JOB_DOMAIN.'jobseeker/reset-pass/'.$token);
                     // send email
                     Email::sendResetPasswordUrl($user,$data);
                     return Redirect::to('recruiter/sent-reset-link');
                }else{

                    $viewModel['Msg'] = ' Sorry your email not matched';
                }
            }else{
                $viewModel['Msg'] = ' Sorry email required';
            }


        }else{
            $viewModel['Msg'] = '';
        }
        return Theme::make('forget-pass',$viewModel);
    }

    public function sentResetLink()
    {
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'gallery'=> Gallery::first(),
            'Msg' =>''
        );
        return Theme::make('sent-forget-pass-request',$viewModel);
    }

    public function resetPass()
    {
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'gallery'=> Gallery::first(),
            'Msg' =>''
        );
        $token = Request::segment(3);
        $user = User::where('user_type','Employer')->where('reset_token',$token)->first();

            $viewModel['user'] = $user;
            $viewModel['Msg'] = '';
        if((!count($user)))
        {
            $viewModel['Msg'] = ' Sorry token expired. If you need reset your password then sent request again. <a href="'.url('recruiter/forget-pass').'">Click Here</a>';

        }
        return Theme::make('reset-pass',$viewModel);
    }

    public function updatePassword()
    {
        if(Request::isMethod('post'))
        {
            $newPass = Input::get('newpass');
            $confirmPass = Input::get('confirmPass');

            $id = Input::get('id');
            $user = User::find($id);
            if($newPass == $confirmPass)
            {
                $user->password = md5($confirmPass);
                $user->reset_token = '';
                $user->token_expire = '';
                $user->save();
                $data = array('user' => $user);
                Email::passwordChanged($user,$data);
                
                Session::flash('message', 'Your password changed successfully');
                return Redirect::to('recruiter/register-recruiter');

            }else{
                Session::flash('message', 'Sorry! Your password not changed, try again later');
                return Redirect::to('recruiter/reset-pass/'.$user->reset_token)->withInput();
            }
        }else{
            return Redirect::to('/');
        }
    }
	
	
	/***** custom pages ***/
	
	public function ourService()
	{
		$viewModel = array(
		'theme' => Theme::getTheme(),
            'gallery'=> Gallery::first(),
            'Msg' =>'',
			'ourService' => About::where('type','recruiter')->where('slug','our-service')->first()
			);
		return Theme::make('page.ourservice',$viewModel);
	}
	
	public function whyDjit()
	{
		$viewModel = array(
		'theme' => Theme::getTheme(),
            'gallery'=> Gallery::first(),
            'Msg' =>'',
			'whydjit' => About::where('type','recruiter')->where('slug','why-djit')->first()
			);
		return Theme::make('page.oteritrecruiter',$viewModel);

	}
	
	public function specialists()
	{
		$viewModel = array(
		    'theme' => Theme::getTheme(),
            'gallery'=> Gallery::first(),
            'Msg' =>'',
            'specialists' => About::where('type', 'recruiter')->where('slug','specialists')->first()
		);
		return Theme::make('page.specialists',$viewModel);
	}
	
	public function trusted()
	{
		$viewModel = array(
		'theme' => Theme::getTheme(),
            'gallery'=> Gallery::first(),
            'Msg' =>'',
			'trusted' => About::where('type', 'recruiter')->where('slug','trusted')->first()
			);
		return Theme::make('page.trusted',$viewModel);
	}
	
	public function ourTeam()
	{
		$viewModel = array(
		'theme' => Theme::getTheme(),
            'gallery'=> Gallery::first(),
            'Msg' =>'',
			'ourTeam' => About::where('type', 'recruiter')->where('slug','our-team')->first()
			);
		return Theme::make('page.our-team',$viewModel);
	}

    public function homeCountry(){
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'gallery'=> Gallery::first(),
            'Msg' =>'',
            'page' => About::where('type', 'recruiter')->where('slug','home-country')->first()
        );
        return Theme::make('page.page',$viewModel);
    }

    public function remoteProgrammer(){
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'gallery'=> Gallery::first(),
            'Msg' =>'',
            'page' => About::where('type', 'recruiter')->where('slug','remote-programmer')->first()
        );
        return Theme::make('page.page',$viewModel);
    }

    public function partTimeProgrammer()
    {
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'gallery'=> Gallery::first(),
            'Msg' =>'',
            'page' => About::where('type', 'recruiter')->where('slug','part-time-programmer')->first()
        );
        return Theme::make('page.page',$viewModel);
    }

}