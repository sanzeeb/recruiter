<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class PageController extends BaseController {

    protected $layout;

    /**
     * for set default route
     * @var string
     */
    protected $default_route;

    /**
     * for set user session data
     * @var string
     */
    protected $_userSession;

    //protected $pageLimit;

    public function __construct() {

        $this->layout = Theme::getLayout();
        $this->default_route = 'activities/index';
        $this->pageLimit = 20;
        $this->_userSession = Authenticate::check();  // check is user logged in
        if(!empty($this->_userSession) && ($this->_userSession->remember_me))
        {
            $expireTime = (60*24*360);
            Config::set('session.lifetime',$expireTime);
        }
        $inbox = Inbox::where('receiver_id',$this->_userSession->id)->where('read_status',0)->orderBy('inbox_id','desc')->take(5)->get();
        View::share('messages',$inbox);
                             $comment = Comment::where('receiver_id', $this->_userSession->id)->where('read_status', 0)->get();
        View::share('comments', $comment);
    }

    public function clientSay() {

        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession
        );

        if (Request::isMethod("post")) {

            $msg = Home::clientCom();
            $viewModel["msg"] = $msg;
            return Redirect::to('page/client-say');
        }

        $viewModel['comments'] = Client::paginate(20);
        return Theme::make('page.client', $viewModel);
    }

    public function editClientSay()
    {
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user'  => $this->_userSession
        );

        if(Request::isMethod('post'))
        {

            $ga_id = trim(Input::get("content_description"));
            $cid = trim(Input::get("cid"));

            $title = trim(Input::get("title"));
            $msg = "";
            if ($ga_id != "" && $title != "") {


                $client = Client::find($cid);

                $client->description = $ga_id;
                $client->name = $title;
                $photoName = '';
                if (Input::hasFile('photo')) {

                    $ext = Input::file('photo')->getClientOriginalExtension();
                    $photoName = Input::file('photo')->getClientOriginalName();

                    $path = 'data/gallery/';
                    $photoName = time() . '_' . str_replace(" ", "_", $photoName);
                    if (in_array(strtolower($ext), array('jpg', 'png', 'gif'))) {
                        Input::file('photo')->move($path, $photoName);
                    }
                    $client->image_name = $photoName;
                }


                $client->save();
                Helpers::addMessage(200, 'information updated:');
                return Redirect::to('page/client-say');
            }
        }

        $id = Request::segment(3);
        $clientSay = Client::find($id);
        if(count($clientSay))
        {
            $viewModel['client'] = $clientSay;
            return Theme::make('page.client-edit',$viewModel);
        }else{
            Helpers::addMessage(400, 'no record found with that id:'.$id);
            return Redirect::to('dashboard');
        }
    }


    public function deleteClient() {

//        if (Request::ajax()) {
//
//            $permission = Authenticate::getPermission($this->_userSession);
//            if (!$permission->delete) {
//                return "500";
//            }

        $id = Input::get("id");
        $msg = Client::where('cc_id', $id)->delete();

        return $msg;

        //}
    }

    public function getGa() {

        if (Request::ajax()) {


            $id = Input::get("id");

            $skill = Gallery::where('ga_id', '=', $id)->first();

            return $skill;
        }
    }

    public function deleteGa() {

        if (Request::ajax()) {


            $id = Input::get("id");
            $msg = Gallery::where('ga_id', $id)->delete();

            return $msg;
        }
    }

    public function updateGa() {

        if (Request::ajax()) {


            $msg = Home::gaEdit();

            return $msg;
        }
    }

    public function gallery() {

        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession
        );

        if (Request::isMethod("post")) {

            $msg = Home::createGallery();
            $viewModel["msg"] = $msg;
        }

        $viewModel['gallery'] = Gallery::paginate(20);
        $viewModel['galleries'] = Gallery::all();
        return Theme::make('page.gallery', $viewModel);
    }

    public function imagesUp() {

        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession
        );

        if (Request::isMethod("post")) {

            $msg = Home::upImages();
            $viewModel["msg"] = $msg;
        }


        $viewModel['gallery'] = Gallery::paginate(20);
        $viewModel['galleries'] = Gallery::all();
        return Theme::make('page.gallery', $viewModel);
    }

    public function jobAboutUs() {

        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession
        );

        if (Request::isMethod("post")) {

            $msg = Home::jobAbUs();
            $viewModel["msg"] = $msg;
        }
        $viewModel['page'] = About::where('type', 'jobseeker')->first();
        return Theme::make('page.jobaus', $viewModel);
    }

    public function reqAboutUs() {

        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession
        );

        if (Request::isMethod("post")) {

            $msg = Home::reqAbUs();
            $viewModel["msg"] = $msg;
        }
        $viewModel['page'] = About::where('type', 'recruiter')->first();
        return Theme::make('page.reqaus', $viewModel);
    }

    public function ourMission()
    {
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession
        );
        if(Request::isMethod('post'))
        {
            $content = Input::get('content_description');
            $page_id = Input::get('page_id');
            $title = Input::get('title');
            $page = About::where('page_id',$page_id)->first();
            $page->title = $title;
            $page->content = strip_tags($content,"<p><strong>");
            $page->save();
            return Redirect::to('page/our-mission');
        }
        $viewModel['page'] = About::where('type', 'recruiter')->where('slug','our-mission')->first();
        return Theme::make('page.our-mission', $viewModel);
    }

    public function specialists()
    {
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession
        );

        if(Request::isMethod('post'))
        {
            $content = Input::get('content_description');
            $page_id = Input::get('page_id');
            $title = Input::get('title');
            $page = About::where('page_id',$page_id)->first();
            $page->title = $title;
            $page->content = strip_tags($content,"<p><strong>");
            $page->save();
            return Redirect::to('page/specialists');
        }
        $viewModel['page'] = About::where('type', 'recruiter')->where('slug','specialists')->first();
        return Theme::make('page.specialists', $viewModel);
    }

    public function trusted()
    {
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession
        );

        if(Request::isMethod('post'))
        {
            $content = Input::get('content_description');
            $page_id = Input::get('page_id');
            $title = Input::get('title');
            $page = About::where('page_id',$page_id)->first();
            $page->title = $title;
            $page->content = strip_tags($content,"<p><strong>");
            $page->save();
            return Redirect::to('page/trusted');
        }
        $viewModel['page'] = About::where('type', 'recruiter')->where('slug','trusted')->first();
        return Theme::make('page.trusted', $viewModel);
    }

    public function ourTeam()
    {
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession
        );

        if(Request::isMethod('post'))
        {
            $content = Input::get('content_description');
            $page_id = Input::get('page_id');
            $title = Input::get('title');
            $page = About::where('page_id',$page_id)->first();
            $page->title = $title;
            $page->content = strip_tags($content,"<p><strong>");
            $page->save();
            return Redirect::to('page/our-team');
        }
        $viewModel['page'] = About::where('type', 'recruiter')->where('slug','our-team')->first();
        return Theme::make('page.our-team', $viewModel);
    }

    public function daffodilGroup()
    {
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession
        );

        if(Request::isMethod('post'))
        {
            $content = Input::get('content_description');
            $page_id = Input::get('page_id');
            $title = Input::get('title');
            $page = About::where('page_id',$page_id)->first();
            $page->title = $title;
            $page->content = strip_tags($content,"<p><strong><h1><h2><h3><h4><h5><h6><img>");
            $page->save();
            return Redirect::to('page/daffodil-group');
        }
        $viewModel['page'] = About::where('type', 'recruiter')->where('slug','mid-content')->first();
        return Theme::make('page.daffodil-group', $viewModel);
    }

    public function ourService()
    {
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession
        );

        if(Request::isMethod('post'))
        {
            $content = Input::get('content_description');
            $page_id = Input::get('page_id');
            $title = Input::get('title');
            if ($content != "") {

                if (Input::hasFile('photo_name')) {

                    $ext = Input::file('photo_name')->getClientOriginalExtension();
                    $photoName = Input::file('photo_name')->getClientOriginalName();

                    $path = 'public/themes/recruit/image';

                    $photoName = 'reqsimg-ourservice' . '.' . $ext;
                    if (in_array(strtolower($ext), array('jpg', 'png', 'gif'))) {

                        Input::file('photo_name')->move($path, $photoName);
                    }

                    $about = array(
                        'content' => strip_tags($content,"<p><ol><ul><li><div><strong><h1><h2><h3><h4><h5><h6><img>"),
                        'title' => $title,
                        'photo_name' => $photoName
                    );
                    About::where('page_id', $page_id)->update($about);

                } else {
                    $about = array(
                        'content' => strip_tags($content,"<p><ol><ul><li><div><strong><h1><h2><h3><h4><h5><h6><img>"),
                        'title' => $title,
                    );
                    About::where('page_id', $page_id)->update($about);

                }
            }

            return Redirect::to('page/our-service');
        }
        $viewModel['page'] = About::where('type', 'recruiter')->where('slug','our-service')->first();
        return Theme::make('page.our-service', $viewModel);
    }

    public function whyDjit()
    {
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession
        );

        if(Request::isMethod('post'))
        {
            $content = Input::get('content_description');
            $page_id = Input::get('page_id');
            $title   = Input::get('title');
            if ($content != "") {

                if (Input::hasFile('photo_name')) {

                    $ext = Input::file('photo_name')->getClientOriginalExtension();
                    $photoName = Input::file('photo_name')->getClientOriginalName();

                    $path = 'public/themes/recruit/image';

                    $photoName = 'reqsimg-whydjit' . '.' . $ext;
                    if (in_array(strtolower($ext), array('jpg', 'png', 'gif'))) {

                        Input::file('photo_name')->move($path, $photoName);
                    }

                    $about = array(
                        'content' => strip_tags($content,"<p><blockquote><ol><ul><li><div><strong><h1><h2><h3><h4><h5><h6><img>"),
                        'title' => $title,
                        'photo_name' => $photoName
                    );
                    About::where('page_id', $page_id)->update($about);

                } else {
                    $about = array(
                        'content' => strip_tags($content,"<p><blockquote><ol><ul><li><div><strong><h1><h2><h3><h4><h5><h6><img>"),
                        'title' => $title,
                    );
                    About::where('page_id', $page_id)->update($about);

                }
            }

            return Redirect::to('page/why-djit');
        }

        $viewModel['page'] = About::where('type', 'recruiter')->where('slug','why-djit')->first();
        return Theme::make('page.why-djit', $viewModel);
    }

    public function homeCountry()
    {
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession
        );
        if(Request::isMethod('post'))
        {
            $content = Input::get('content_description');
            $page_id = Input::get('page_id');
            $title   = Input::get('title');
            if ($content != "") {
                $about = array(
                    'content' => strip_tags($content,"<p><blockquote><ol><ul><li><div><strong><h1><h2><h3><h4><h5><h6><img>"),
                    'title' => $title,
                );
                About::where('page_id', $page_id)->update($about);
            }
            return Redirect::to('page/home-country');
        }
        $viewModel['page'] = About::where('type', 'recruiter')->where('slug','home-country')->first();
        return Theme::make('page.home-country',$viewModel);
    }

    public function remoteProgrammer()
    {
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession
        );
        if(Request::isMethod('post'))
        {
            $content = Input::get('content_description');
            $page_id = Input::get('page_id');
            $title   = Input::get('title');
            if ($content != "") {
                $about = array(
                    'content' => strip_tags($content,"<p><blockquote><ol><ul><li><div><strong><h1><h2><h3><h4><h5><h6><img>"),
                    'title' => $title,
                );
                About::where('page_id', $page_id)->update($about);
            }
            return Redirect::to('page/remote-programmer');
        }
        $viewModel['page'] = About::where('type', 'recruiter')->where('slug','remote-programmer')->first();
        return Theme::make('page.remote-programmer',$viewModel);
    }

    public function partTimeProgrammer()
    {
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession
        );

        if(Request::isMethod('post'))
        {
            $content = Input::get('content_description');
            $page_id = Input::get('page_id');
            $title   = Input::get('title');
            if ($content != "") {
                $about = array(
                    'content' => strip_tags($content,"<p><blockquote><ol><ul><li><div><strong><h1><h2><h3><h4><h5><h6><img>"),
                    'title' => $title,
                );
                About::where('page_id', $page_id)->update($about);
            }
            return Redirect::to('page/part-time-programmer');
        }

        $viewModel['page'] = About::where('type', 'recruiter')->where('slug','part-time-programmer')->first();
        return Theme::make('page.part-time-programmer',$viewModel);
    }

    public function socialLinks()
    {
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession
        );

        if(Request::isMethod('post'))
        {
            $key = Input::get('key');
            if(count($key))
            {
                foreach($key as $k=>$v)
                {
                    Option::setData($k,$v);
                }
            }

            return Redirect::to('page/social-links');
        }
        $viewModel['fb'] = Option::getData('fb');
        $viewModel['twitter'] = Option::getData('twitter');
        $viewModel['gplus'] = Option::getData('gplus');
        $viewModel['linkedin'] = Option::getData('linkedin');
        return Theme::make('page.social-links', $viewModel);
    }
}

?>
