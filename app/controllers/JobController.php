<?php

class JobController extends BaseController {

    /*
      |--------------------------------------------------------------------------
      | Default Home Controller
      |--------------------------------------------------------------------------
      |
      | You may wish to use controllers instead of, or in addition to, Closure
      | based routes. That's great! Here is an example controller method to
      | get you started. To route to this controller, just add the route:
      |
      |	Route::get('/', 'HomeController@login');
      |
     */

     protected $layout;

    /**
     * for set default route
     * @var string
     */
    protected $default_route;

    /**
     * for set user session data
     * @var string
     */
    protected $_userSession;

    //protected $pageLimit;

    public function __construct()
    {

        $this->layout = Theme::getLayout();
        $this->default_route = 'activities/index';
        $this->pageLimit = 20;
        $this->_userSession = Authenticate::check();  // check is user logged in
        if(!empty($this->_userSession) && ($this->_userSession->remember_me))
        {
            $expireTime = (60*24*360);
            Config::set('session.lifetime',$expireTime);
        }
        $inbox = Inbox::where('receiver_id',$this->_userSession->id)->where('read_status',0)->orderBy('inbox_id','desc')->take(5)->get();
        View::share('messages',$inbox);
                             $comment = Comment::where('receiver_id', $this->_userSession->id)->where('read_status', 0)->get();
        View::share('comments', $comment);

        if(!empty($this->_userSession->user_id))
            $jobCount = Jobs::where('created_by',$this->_userSession->user_id)->count();
        else
            $jobCount = 0;
        View::share('jobCount',$jobCount);

    }



    public function create() {

    $permission = Permission::getPermission($this->_userSession->role_id,"Job posting",'view');
                
    if($this->_userSession->role_id != 1)
    {

       if(!$permission)
       {
            Helpers::addMessage(500, " You don't have permission to access that page");
            return Redirect::to('dashboard');
       }
    }
     
     
     $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession,
            'skills' => SkillBLL::getSkills(""),
            'job_natures' => array('Part-time'=>'Part Time', 'Full-time'=>'Full Time')
        );


        return Theme::make('job.create', $viewModel);


    }

    public function saveJob()
    {
        $permission = Authenticate::getPermission($this->_userSession);
        if(!$permission->create)
        {
            Helpers::addMessage(500, " You don't have permission to Create Job");
            return Redirect::to('dashboard');
        }

        if(Request::isMethod('post'))
        {
            JobModel::saveJob($this->_userSession);
            return Redirect::to('job/lists');
        }else{
            Helpers::addMessage(500, ' Bad Request');

        }
        return Redirect::to('job/lists');
    }

    public function updateJob()
    {
        $permission = Authenticate::getPermission($this->_userSession);
        if(!$permission->update)
        {
            Helpers::addMessage(500, " You don't have permission to update Job information");
            return Redirect::to('dashboard');
        }

        if(Request::isMethod('post'))
        {
            $id = JobModel::updateJob($this->_userSession);
            if($id == 2)
                Helpers::addMessage(400, " Last application date cannot be older than it previously mentioned ");
            return Redirect::to('job/lists');
        }else{
            Helpers::addMessage(500, ' Bad Request');
            return Redirect::to('job/lists');
        }

    }

    public function lists()
    {
        $permission = Permission::getPermission($this->_userSession->role_id,"Job posting",'view');
                
        if($this->_userSession->role_id != 1)
        {

           if(!$permission)
           {
                Helpers::addMessage(500, " You don't have permission to access that page");
                return Redirect::to('dashboard');
           }
        }

        $jobs = JobModel::getJobs($this->_userSession);
       
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user'  => $this->_userSession,
            'jobs'  => $jobs
        );

        return Theme::make('job.list',$viewModel);

    }

    public function edit()
    {
        $permission = Permission::getPermission($this->_userSession->role_id,"Job posting",'edit');
                
        if($this->_userSession->role_id != 1)
        {

           if(!$permission)
           {
                Helpers::addMessage(500, " You don't have permission to access that page");
                return Redirect::to('dashboard');
           }
        }

        $id = Request::segment(3);
        $jobs = Jobs::find($id);
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user'  => $this->_userSession,
            'skills' => SkillBLL::getSkills(""),
        );
        if(count($jobs))
        {
            $viewModel['jobs'] = $jobs;
            $viewModel['applied'] = count($jobs->JobCandidate);
            $viewModel['job_natures'] = array('Part-time'=>'Part Time', 'Full-time'=>'Full Time');
            return Theme::make('job.edit',$viewModel);
        }else{
            Helpers::addMessage(400, ' Record not found for Id:'.$id);
            return Redirect::to('job/lists');
        }

    }

    public function view()
    {
        $permission = Permission::getPermission($this->_userSession->role_id,"Job posting",'view');
                
        if($this->_userSession->role_id != 1)
        {

           if(!$permission)
           {
                Helpers::addMessage(500, " You don't have permission to access that page");
                return Redirect::to('dashboard');
           }
        }

        $id = Request::segment(3);
        $type = Helpers::EncodeDecode(Request::segment(4),false);
        $jobs = Jobs::find($id);
       
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user'  => $this->_userSession,
            'skills' => SkillBLL::getSkills(""),
        );
        if(count($jobs))
        {
            if($this->_userSession->user_type == 'Candidate')
            {
                $viewModel['applicable'] = JobCandidate::where('job_id',$jobs->jobs_id)->where('candidate_id',$this->_userSession->user_id)->count();
                $viewModel['replies'] = JobCandidateReply::where('job_id',$jobs->jobs_id)->where(function($q){
                    $q->where('replier_id',$this->_userSession->user_id)->where('reply_to','=',$this->_userSession->user_id,'or');
                })->get();

            }
            $viewModel['viewType'] = $type;
            $viewModel['jobs'] = $jobs;
            $viewModel['job_natures'] = array('Part-time'=>'Part Time', 'Full-time'=>'Full Time');
            return Theme::make('job.view',$viewModel);
        }else{
            Helpers::addMessage(400, ' Record not found for Id:'.$id);
            return Redirect::to('job/lists');
        }

    }

    public function applicants($jobId)
    {
        $viewModel = array(
            'user' => $this->_userSession,
            'theme' => Theme::getTheme()
        );
        $viewModel['jobs'] = Jobs::find($jobId);
        return Theme::make('job.applicants',$viewModel);
    }

    public function delJob()
    {
        $permission = Permission::getPermission($this->_userSession->role_id,"Job posting",'delete');
                
        if($this->_userSession->role_id != 1)
        {

           if(!$permission)
           {
                Helpers::addMessage(500, " You don't have permission to access that page");
                return Redirect::to('dashboard');
           }
        }

        if(Request::ajax())
        {
            $job_id = Input::get('job_id');

            $job = Jobs::where('jobs_id',$job_id)->first();
            $job->deleted_at = Helpers::dateTimeFormat('Y-m-d',time());
            $job->save();
            Helpers::addMessage(200,'Job deleted successfully');
            return 200;
        }else{
            Helpers::addMessage(500, " Bad Request");
            return 500;
        }
    }

    public function getJob()
    {
        if(Request::ajax())
        {
            $recruiterId = Input::get('recruiter_id');
            return JobModel::getJobsByRecruiter($recruiterId);

        }else{
            Helpers::addMessage(500, " Bad Request");
            return Redirect::to('dashboard');
        }

    }


    public function search() {
        $permission = Authenticate::getPermission($this->_userSession);


       if(Request::ajax())
       {
           if(!$permission->read)
           {
               $msg = " You don't have permission to Search Job";
               Helpers::addMessage(500, $msg);
               return $msg;
           }
            $txt = Input::get('searchTxt');
            return JobModel::getJob($txt);
       }else{
           Helpers::addMessage(500, " Bad Request");
           return Redirect::to('dashboard');
       }

    }

    public function apply()
    {

        if(Request::isMethod('post'))
        {

            $id = Input::get('jobid');
            $job = Jobs::find($id);

            if($this->_userSession->user_type != 'Candidate')
            {
                Helpers::addMessage(400, " Only Candidate can apply");
                return Redirect::to('job/view/'.$id);
            }

            if(count($job))
            {
                $candidate_id = $this->_userSession->user_id;
                $jobCandidate = JobCandidate::firstOrNew(array(
                   'job_id' => $job->jobs_id,
                   'candidate_id' => $candidate_id
                ));

                $jobCandidate->save();
                $comment = Input::get('comment');
                $jobCandidateReply = new JobCandidateReply;
                $jobCandidateReply->job_id = $job->jobs_id;
                $jobCandidateReply->replier_type = 'Candidate';
                $jobCandidateReply->replier_id = $candidate_id;
                $jobCandidateReply->reply = (!empty($comment)) ? $comment : 'I am interested about this job';
                $jobCandidateReply->reply_to = $job->created_by;
                $jobCandidateReply->save();

                Helpers::addMessage(200, ' Your application submitted, Thank you. Delegates from DJIT will contact you later');
                return Redirect::to('job/view/'.$id);

            }else{

                Helpers::addMessage(400, " No job found to apply");
                return Redirect::to('job/view/'.$id);
            }
        }
        else{
            return Redirect::to('dashboard');
        }

    }

    public function removeApplication()
    {
        if(Request::isMethod('post'))
        {
            $cid = Input::get('cid');
            $job_id = Input::get('job_id');
            JobCandidateReply::where(function($query)use($cid){
                $query->where(function($q)use($cid){
                    $q->where('replier_type','Candidate')->where('replier_id',$cid);
                })->where('reply_to','=',$cid,'or');
            })->delete();
            $jobCandidate = JobCandidate::where('candidate_id',$cid)->where('job_id',$job_id)->first();
            if(count($jobCandidate))
                $jobCandidate->delete();


            Helpers::addMessage(200, ' Candidate Removed');
            return Redirect::to('job/view/'.$job_id);
        }
        return Redirect::to('dashboard');

    }

    public function replies()
    {
        $id = Request::segment(3);
        $cid = Request::segment(4);
        $job = Jobs::find($id);

        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user'  => $this->_userSession,
            'skills' => SkillBLL::getSkills(""),
        );

        $candidate = Candidate::find($cid);
        if(count($job))
        {
            /*if($this->_userSession->user_type == 'Candidate')
            {
                $viewModel['applicable'] = JobCandidate::where('job_id',$jobs->jobs_id)->where('candidate_id',$this->_userSession->user_id)->count();
            }*/
            $viewModel['jobs'] = $job;
            $viewModel['candidate'] = $candidate;
            $viewModel['replies'] = JobCandidateReply::where('job_id',$job->jobs_id)->where(function($q) use($candidate){
                $q->where('replier_id',$candidate->candidate_id)->where('reply_to','=',$candidate->candidate_id,'or');
            })->get();
            $viewModel['job_natures'] = array('Part-time'=>'Part Time', 'Full-time'=>'Full Time');
            $viewModel['jobCandidateReply'] = JobCandidateReply::where('reply_to',$cid)->where('job_id',$job->jobs_id)->where('replier_type','Employer')->where('replier_id',$this->_userSession->user_id)->count();
            return Theme::make('job.replies',$viewModel);

        }else{

            Helpers::addMessage(400, ' Record not found for Id:'.$id);
            return Redirect::to('job/lists');
        }
    }

    public function empReply()
    {
        if(Request::isMethod('post'))
        {
            $job_id = Input::get('job_id');
            $c_id   = Input::get('cid');
            $job = Jobs::find($job_id);
            $candidate = Candidate::find($c_id);
            $jobCandidateReply = new JobCandidateReply;
            $jobCandidateReply->job_id = $job->jobs_id;
            $jobCandidateReply->replier_type = 'Employer';
            $jobCandidateReply->replier_id = $this->_userSession->user_id;
            $jobCandidateReply->reply = Input::get('reply');
            $jobCandidateReply->reply_to = $candidate->candidate_id;
            $jobCandidateReply->save();

            Helpers::addMessage(200, 'your message replied.');
            return Redirect::to('job/replies/'.$job->jobs_id.'/'.$candidate->candidate_id);
        }else{
            return Redirect::to('job/lists');
        }
    }

    public function candidateReply()
    {
        if(Request::isMethod('post'))
        {
            $job_id = Input::get('job_id');
            $c_id   = Input::get('cid');
            $job = Jobs::find($job_id);
            $candidate = Candidate::find($c_id);
            $jobCandidateReply = new JobCandidateReply;
            $jobCandidateReply->job_id = $job->jobs_id;
            $jobCandidateReply->replier_type = 'Candidate';
            $jobCandidateReply->replier_id = $this->_userSession->user_id;
            $jobCandidateReply->reply = Input::get('reply');
            $jobCandidateReply->reply_to = $job->created_by;
            $jobCandidateReply->save();

            Helpers::addMessage(200, 'your message replied.');
            return Redirect::to('job/replies/'.$job->jobs_id.'/'.$candidate->candidate_id);
        }else{
            return Redirect::to('job/lists');
        }
    }

    public function get_job_by_date()
    {
        if(Request::isMethod('post'))
        {
            $jobs = Jobs::where('deleted_at','!=',0)->where('created_by',$this->_userSession->user_id)->where('last_date',Input::get('date'))->get();
        
            return $jobs;
        }
        exit();
        
    }


}
