<?php

class ScheduleController extends BaseController {
    /*
      |--------------------------------------------------------------------------
      | Default Home Controller
      |--------------------------------------------------------------------------
      |
      | You may wish to use controllers instead of, or in addition to, Closure
      | based routes. That's great! Here is an example controller method to
      | get you started. To route to this controller, just add the route:
      |
      |	Route::get('/', 'HomeController@login');
      |
     */

    protected $layout;

    /**
     * for set default route
     * @var string
     */
    protected $default_route;

    /**
     * for set user session data
     * @var string
     */
    protected $_userSession;
    protected $pageLimit;

    public function __construct() {

        $this->layout = Theme::getLayout();
        $this->default_route = 'activities/index';
        $this->pageLimit = 20;
        $this->_userSession = Authenticate::check();  // check is user logged in
        if(!empty($this->_userSession) && ($this->_userSession->remember_me))
        {
            $expireTime = (60*24*360);
            Config::set('session.lifetime',$expireTime);
        }
        $inbox = Inbox::where('receiver_id',$this->_userSession->id)->where('read_status',0)->orderBy('inbox_id','desc')->take(5)->get();
        View::share('messages',$inbox);
        
                             $comment = Comment::where('receiver_id', $this->_userSession->id)->where('read_status', 0)->get();
        View::share('comments', $comment);

        if(!empty($this->_userSession->user_id))
            $jobCount = Jobs::where('created_by',$this->_userSession->user_id)->count();
        else
            $jobCount = 0;
        View::share('jobCount',$jobCount);
    }

    public function createSchedule() {

        $permission = Authenticate::getPermission($this->_userSession);
        if (!$permission->create) {
            Helpers::addMessage(500, " You don't have permission to access that page");
            return Redirect::to('dashboard');
        }
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession
        );

        if (Request::isMethod("post")) {

            $msg = ScheduleBLL::scheduleCreate();
            $viewModel["msg"] = $msg;
        }
        $id = Session::get("id");
        $viewModel["schedules"] = Schedule::where('user_id', '=', $id)->paginate(10);

        return Theme::make('schedule.create', $viewModel);
    }

    public function getSchedule() {

        if (Request::ajax()) {

            $permission = Authenticate::getPermission($this->_userSession);
            if (!$permission->read) {
                return "500";
            }

            $id = Input::get("id");

            $schedule = Schedule::where('avail_id', '=', $id)->first();

            return $schedule;
        }
    }

    public function deleteSchedule() {

        if (Request::ajax()) {

            $permission = Authenticate::getPermission($this->_userSession);
            if (!$permission->delete) {
                return "500";
            }
            $id = Input::get("id");
            $msg = Schedule::where('avail_id', $id)->delete();

            return $msg;
        }
    }

    public function updateSchedule() {

        if (Request::ajax()) {

            $permission = Authenticate::getPermission($this->_userSession);
            if (!$permission->update) {
                return "500";
            }
            $msg = ScheduleBLL::scheduleEdit();

            return $msg;
        }
    }

}
