<?php

class DashboardController extends BaseController {

    protected $layout;
    protected $default_route;
    protected $_userSession;
    protected $pageLimit = 20;
    public function __construct() {

        $this->layout = Theme::getLayout();
        $this->default_route = 'dashboard/index';
        $this->_userSession = Authenticate::check();
        $this->_userSession->user_type;
        if(!empty($this->_userSession) && ($this->_userSession->remember_me))
        {
           $expireTime = (60*24*360);
           Config::set('session.lifetime',$expireTime);
        }
        $inbox = Inbox::where('receiver_id',$this->_userSession->id)->where('read_status',0)->orderBy('inbox_id','DESC')->take(5)->get();
        
        View::share('messages',$inbox);
        
        $comment = Comment::where('receiver_id', $this->_userSession->id)->where('read_status', 0)->get();
        View::share('comments', $comment);
        if(!empty($this->_userSession->user_id))
            $jobCount = Jobs::where('created_by',$this->_userSession->user_id)->count();
        else
            $jobCount = 0;
        View::share('jobCount',$jobCount);

        
        
    }

    
    public function topEngineer() {

     $viewModel = array(
            'theme' => Theme::getTheme(),
            'user'=>  $this->_userSession
        );

    $skill=base64_decode(Request::segment(3));

    $keyword = trim(Request::get('name'));
    $age = trim(Request::get('age'));
    $yxp = trim(Request::get('yxp'));
    $cvTbl=new CvTable();
    $searchskills = Request::get('skills');
    
    
   // $expertises=  CvSpecialization::where('key',  $skill)->get();
    $limit = 20; 
    $page = Request::get('page');
    $offset = (!empty($page) && ($page>1))? ($page * 10) : 0;
    $count = 0;
   if(!empty($searchskills))
   {
    $count = $cvTbl->getBySkill($searchskills,$keyword,$age,$yxp,0,0,true);
    $expertises=$cvTbl->getBySkill($searchskills,$keyword,$age,$yxp,$offset,$limit,false);

   }else
   {

    $count = $cvTbl->getBySkill($skill,$keyword,$age,$yxp,0,0,true);
    $expertises=$cvTbl->getBySkill($skill,$keyword,$age,$yxp,$offset,$limit,false);
   
   }

   $pagination = new Pagination($count,$limit,$offset);
   //Helpers::debug($pagination);
   $links = $pagination->generateLinks();

   
   $skills = array();
   $distinctSkills = array();
   foreach($expertises as $exp)
   {
        $cvSpecializations = CvSpecialization::where('cv_tbl_id',$exp->cv_tbl_id)->get();
        $skills[$exp->cv_tbl_id] = $cvSpecializations;

        foreach($cvSpecializations as $spec)
        {
            $distinctSkills[$spec->key] = strtolower(trim($spec->key));
        }
   }

   if(empty($distinctSkills))
   {
        $allSkills = Skills::all();
        foreach($allSkills as $sk)
        {
            $distinctSkills[$sk->skill_name] =strtolower(trim($sk->skill_name));
        }

   }

  //Helpers::debug($expertises);
  // die();
    $viewModel['expertises']=$expertises;
    $viewModel['searchedSkills'] = $searchskills;
    $viewModel['skill']=strtolower(trim($skill));
    $viewModel['skills'] = $skills;
    $viewModel['disSkills'] =$distinctSkills;
    $viewModel['gallery']= Gallery::first();
    $viewModel['links'] = $links;
    return Theme::make('top_engineers_new', $viewModel);
     
        
    }

    public function index() {


        $viewModel = array(
            'theme'    => Theme::getTheme(),
            'user'     => $this->_userSession,
        );
     
        if($this->_userSession->user_type=="Candidate"){
        
            $candidate=Candidate::find($this->_userSession->user_id);
            return Redirect::to("candidate/dashboard");
        
        }
        
        $skills=  Skills::where('status',1)->orderBy("display_order","asc")->paginate($this->pageLimit);
        
        $viewModel["skills"]=$skills;
        $candidates=User::where('user_type', 'Candidate')->get();
        $employers=User::where('user_type', 'Employer')->get();
        $jobs=  Jobs::all();
        $resumes=  CvTable::all();
        
        $viewModel['candidates']=count($candidates);
        $viewModel['employers']=count($employers);
        

        $viewModel['jobs']=count($jobs);
        $viewModel['resumes']=count($resumes);
        
        return Theme::make('dashboard',$viewModel);
    }

    public function confirm() {

        $viewModel = array(
            'theme'    => Theme::getTheme(),
            'user'     => $this->_userSession,
            
        );
        return Theme::make('dashboard',$viewModel);
    }

    public function approveInquery()
    {
        if(Request::ajax()){

            $id = Input::get('id');
            $inquery = Inquiry::find($id);
            if(count($inquery))
            {
                $inquery->status = "Approved";
                $inquery->save();
                Helpers::addMessage(200, 'Inquery Approved');
            }else{

                Helpers::addMessage(400, " No inquiry found");
            }
            return 1;
        }else{

            return Redirect::to('dashboard');

        }
    }

    public function cancelInquiry()
    {
        if(Request::isMethod('post')){

            $id = Input::get('ir_id');
            $comment = trim(Input::get('comment'));

            $inquiry = Inquiry::find($id);
            if(count($inquiry))
            {
                if($comment == "")
                {
                    Helpers::addMessage(400, "Sorry! you have to give a reason");

                    return Redirect::to('inquiry/details/'.$id);
                }

                $inquiry->status = "Canceled";
                $inquiry->save();
                $inquiryResponse = new InqueryResponse();
                $inquiryResponse->inquery_id = $inquiry->id;
                $inquiryResponse->replier_type = 'Admin';
                $inquiryResponse->replier_id = $this->_userSession->id;
                $inquiryResponse->reply = $comment;
                $inquiryResponse->status = 'Replied by admin';
                $inquiryResponse->save();
                Helpers::addMessage(200, 'Inquiry Canceled');

            }else{

                Helpers::addMessage(400, " No inquiry found");
            }
            return Redirect::to('inquiry/details/'.$id);
        }else{

            return Redirect::to('dashboard');

        }
    }

    public function replyInquery()
    {
        if(Request::isMethod("post"))
        {
            $id = Input::get('id');
            $replyText = Input::get('reply');
            if(preg_match('/((^[0][1-9]|\d+){6,14}|http:\/\/.+\.([a-z]{3,}|[a-z]{3,}\.[a-z]{2,3})|[a-z]+\.[a-z]{3,}|[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-z]{2,})/', $replyText))
            {
                Helpers::addMessage(400,"Sorry! You cannot write any phone no, email or web url");
                return 2;
            }  
            $inquery = Inquiry::find($id);
            if(count($inquery))
            {
                /*$inquery->status = "Replied";
                $inquery->save();*/
                $inqueryResponse = new InqueryResponse();
                $inqueryResponse->inquery_id = $inquery->id;
                if($this->_userSession->user_type == 'Employer')
                {
                    $inqueryResponse->replier_id = $this->_userSession->user_id;
                    $inqueryResponse->replier_type = "Employer";
                    $inqueryResponse->reply = $replyText;
                    $inqueryResponse->status = "Pending";
                    $inqueryResponse->save();
                }
                elseif($this->_userSession->user_type == 'Admin')
                {
                    $inqueryResponse->replier_id = $this->_userSession->user_id;
                    $inqueryResponse->replier_type = "Admin";
                    $inqueryResponse->reply = $replyText;
                    $inqueryResponse->status = "Replied by admin";
                    $inqueryResponse->save();
                    $inquery->status = 'Replied by admin';
                    $inquery->save();

                }else
                {
                    $inqueryResponse->replier_id = $inquery->candidate_id;
                    $inqueryResponse->replier_type = "Candidate";
                    $inqueryResponse->reply = $replyText;
                    $inqueryResponse->status = "Pending";
                    $inqueryResponse->save();
                }
                 Helpers::addMessage(200, " You reply sent successfully");

            }else{

                Helpers::addMessage(400, " No inquiry found");
            }
            return 1;
        }else{

            return Redirect::to('dashboard');

        }
    }

    public function approveInterview()
    {
        if(Request::ajax()){

            $id = Input::get('id');
            $interview = Interview::find($id);
            if(count($interview))
            {
                $interview->interview_date = Input::get('interview_date');
                $interview->interview_time = Input::get('interview_time');
                $interview->interview_duration = Input::get('interview_duration');
                $interview->status = "Approved";
                $interview->save();
                Helpers::addMessage(200, 'Interview Approved');
            }else{

                Helpers::addMessage(400, " No interview found");
            }
            
            return 1;

        }else{

            return Redirect::to('dashboard');

        }
    }

    public function approveAcceptInterview()
    {
        if(Request::ajax()){

            $id = Input::get('id');
            $interview = Interview::find($id);
            if(count($interview))
            {
               /* $interview->interview_date = Input::get('interview_date');
                $interview->interview_time = Input::get('interview_time');
                $interview->interview_duration = Input::get('interview_duration');*/
                $interview->status = "Accepted";
                $interview->save();
                Helpers::addMessage(200, 'Interview Accepted');
            }else{

                Helpers::addMessage(400, " No interview found");
            }

            return 1;

        }else{

            return Redirect::to('dashboard');

        }
    }

    public function replyInterview()
    {
        if(Request::ajax())
        {
            $id = Input::get('id');
            $replyText = Input::get('reply');
            $interview = Interview::find($id);
            if(count($interview))
            {

                $interview->save();
                $interviewReply = new InterviewReply();
                $interviewReply->interview_id = $interview->interview_id;
                if($this->_userSession->user_type == 'Employer')
                {
                    $interviewReply->replier_id = $this->_userSession->user_id;
                    $interviewReply->replier_type = "Employer";
                    $interviewReply->reply = $replyText;
                    $interviewReply->status = $interview->status;
                    $interviewReply->save();

                }elseif($this->_userSession->user_type == 'Admin')
                {
                    $interviewReply->replier_id = $this->_userSession->id;
                    $interviewReply->replier_type = "Admin";
                    $interviewReply->reply = $replyText;
                    $interviewReply->status = 'Replied by admin';
                    $interviewReply->save();
                    $interview->status = 'Replied by admin';
                    $interview->save();
                }
                else
                {
                    $interviewReply->replier_id = $interview->candidate_id;
                    $interviewReply->replier_type = "Candidate";
                    $interviewReply->reply = $replyText;
                    $interviewReply->status = "Pending";
                    $interviewReply->save();    
                }
                

            }else{

                Helpers::addMessage(400, " No interview found");
            }
            return 1;
        }else{

            return Redirect::to('dashboard');

        }
    }

    public function acceptInterview(){
        if(Request::ajax())
        {
            $id = Input::get('id');

            $interview = Interview::find($id);
            if(count($interview))
            {
                $interview->status = Interview::$AcceptedPending;
                $interview->save();
                
            }else{

                Helpers::addMessage(400, " No interview found");
            }
            return 1;
        }else{

            return Redirect::to('dashboard');

        }
    }

    public function declineInterview(){
        if(Request::isMethod('post'))
        {
            $id = Input::get('ir_id');

            $interview = Interview::find($id);

            if(count($interview))
            {
                $interview->status = "Declined";
                $interview->save();
                
                $interviewReply = new InterviewReply();
                $interviewReply->interview_id = $interview->interview_id;
                $interviewReply->replier_id   = $interview->candidate_id;
                $interviewReply->replier_type = "Candidate";
                $interviewReply->reply = Input::get('comment'); 
                $interviewReply->status = "Pending";
                $interviewReply->save();
                
                
            }else{

                Helpers::addMessage(400, " No interview found");
            }

            return Redirect::to('interview/details/'.$interview->interview_id);
        }else{

            return Redirect::to('dashboard');

        }
    }

    public function progressInterview(){
        if(Request::ajax())
        {
            $id = Input::get('id');

            $interview = Interview::find($id);
            if(count($interview))
            {
                $interview->status = Interview::$Progressing;
                $interview->save();
            }else{

                Helpers::addMessage(400, " No interview found");
            }
            return 1;
        }else{

            return Redirect::to('dashboard');

        }
    }

    public function completedInterview(){
        if(Request::ajax())
        {
            $id = Input::get('id');

            $interview = Interview::find($id);
            if(count($interview))
            {
                $interview->status = Interview::$Completed;
                $interview->save();
            }else{

                Helpers::addMessage(400, " No interview found");
            }
            return 1;
        }else{

            return Redirect::to('dashboard');

        }
    }

    public function cancelInterview(){
        if(Request::isMethod('post'))
        {
            $id = Input::get('interview_id');

            $interview = Interview::find($id);
            if(count($interview))
            {

                $interview->status = Interview::$Canceled;
                $interview->save();
                $interviewReply = new InterviewReply();
                $interviewReply->interview_id = $interview->interview_id;
                $interviewReply->replier_id = $this->_userSession->id;
                $interviewReply->replier_type = "Admin";
                $interviewReply->reply = Input::get('comment');
                $interviewReply->status = 'Canceled';
                $interviewReply->save();
              

                Helpers::addMessage(200, 'Interview Canceled');
            }else{

                Helpers::addMessage(400, " No interview found");
            }
            return Redirect::to('interview/details/'.$interview->interview_id);
        }else{

            return Redirect::to('dashboard');

        }
    }

    public function hiredInterview(){
        if(Request::ajax())
        {
            $id = Input::get('id');
            $feedback = Input::get('feedback');
            $interview = Interview::find($id);
            if(count($interview))
            {
                $userObj = User::where('user_id',$interview->candidate_id)->where('user_type','Candidate')->first();
                $data['receiver_id'] = $userObj->id;
                $data['candidate_id'] = $interview->candidate_id;
                $data['description'] = $interview->topics;
                $data['amount'] = $interview->salary;
                $data['title'] = $interview->Job->job_title;
                //Helpers::debug($data);die();
                $interviewFeedback = InterviewFeedback::firstOrNew(array('interview_id'=>$interview->interview_id));
                $interviewFeedback->feedback = $feedback;
                $interviewFeedback->save();

                AgreementBLL::CreateConfrimAgreement($this->_userSession->id,$interview->emp_id,$data);
                $interview->status = Interview::$Offered;
                $interview->save();
            }else{

                Helpers::addMessage(400, " No interview found");
            }
            return 1;
        }else{

            return Redirect::to('dashboard');

        }
    }

    public function offerInterview(){
        if(Request::isMethod('post'))
        {
            $id = Input::get('id');
            //$feedback = Input::get('feedback');
            $interview = Interview::find($id);
            if(count($interview))
            {
                $userObj = User::where('user_id',$interview->candidate_id)->where('user_type','Candidate')->first();
                $data['receiver_id'] = $userObj->id;
                $data['candidate_id'] = $interview->candidate_id;
                $data['description'] = $interview->topics;
                $data['amount'] = $interview->salary;
                $data['title'] = $interview->Job->job_title;
                //Helpers::debug($data);die();
                $interviewFeedback = InterviewFeedback::firstOrNew(array('interview_id'=>$interview->interview_id));
                $interviewFeedback->feedback = Input::get('feedback');
                $interviewFeedback->save();

                AgreementBLL::sentAgreement($this->_userSession->id,$interview->emp_id);
                $interview->status = Interview::$Offered;
                $interview->save();

                Helpers::addMessage(200, "Offer Sent successfully");

                return Redirect::to('dashboard');
            }else{

                Helpers::addMessage(400, " No interview found");
            }
            return 1;
        }else{

            return Redirect::to('dashboard');

        }
    }

    public function replyTproject()
    {
        if(Request::ajax())
        {
            $id = Input::get('id');
            $replyText = Input::get('reply');
            if(preg_match('/((^[0][1-9]|\d+){6,14}|http:\/\/.+\.([a-z]{3,}|[a-z]{3,}\.[a-z]{2,3})|[a-z]+\.[a-z]{3,}|[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-z]{2,})/', $replyText))
            {
                Helpers::addMessage(400,"Sorry! You cannot write any phone no, email or web url");
                return false;
            }  
            
            $tproject = TestProject::find($id);
            if(count($tproject))
            {

                
                $tprojectReply = new TestProjectReply();
                $tprojectReply->tproject_id = $tproject->id;
                if($this->_userSession->user_type == 'Employer')
                {
                    $tproject->save();
                    $tprojectReply->replier_id = $this->_userSession->user_id;
                    $tprojectReply->replier_type = "Employer";
                }
                elseif($this->_userSession->user_type=="Candidate")
                {
                    $tproject->save();
                    $tprojectReply->replier_id = $tproject->candidate_id;
                    $tprojectReply->replier_type = "Candidate";
                    $tprojectReply->status = TestProjectReply::$Pending;
                }else{

                    $tproject->project_status = "Replied by admin";
                    $tproject->save();

                    $tprojectReply->replier_id = $this->_userSession->id;
                    $tprojectReply->replier_type = "Admin";
                    $tprojectReply->status = "Replied by admin";
                }
                $tprojectReply->reply = $replyText;
                
                $tprojectReply->save();

            }else{

                Helpers::addMessage(400, " No inquiry found");
            }
            return 1;
        }else{

            return Redirect::to('dashboard');

        }
    }


}
