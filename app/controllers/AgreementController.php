<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class AgreementController extends BaseController {

    protected $layout;

    /**
     * for set default route
     * @var string
     */
    protected $default_route;

    /**
     * for set user session data
     * @var string
     */
    protected $_userSession;

    //protected $pageLimit;

    public function __construct() {

        $this->layout = Theme::getLayout();
        $this->default_route = 'activities/index';
        $this->pageLimit = 20;
        $this->_userSession = Authenticate::check();  // check is user logged in
        if(!empty($this->_userSession) && ($this->_userSession->remember_me))
        {
            $expireTime = (60*24*360);
            Config::set('session.lifetime',$expireTime);
        }
        $inbox = Inbox::where('receiver_id', $this->_userSession->id)->where('read_status', 0)->orderBy('inbox_id','desc')->take(5)->get();
        View::share('messages', $inbox);
        
             $comment = Comment::where('receiver_id', $this->_userSession->id)->where('read_status', 0)->get();
        View::share('comments', $comment);

        if(!empty($this->_userSession->user_id))
            $jobCount = Jobs::where('created_by',$this->_userSession->user_id)->count();
        else
            $jobCount = 0;
        View::share('jobCount',$jobCount);
    }

    /* Agreement sent function started */

    public function sentAgreement() {

        if (Request::ajax()) {
            $user = $this->_userSession;
            $sender_id = $user->id;
            $user_id = $user->user_id;
            $id = AgreementBLL::sentAgreement($sender_id, $user_id);

            return $id;
        }
    }

    public function sentDisagreement() {

        if (Request::ajax()) {
            $user = $this->_userSession;
            $sender_id = $user->id;
            $user_id = $user->user_id;
            $user_type = $user->user_type;
            
            $id = AgreementBLL::sentDisAgreement($sender_id, $user_id, $user_type);

            if($id)
            { 
               $agreementReply = new AgreementReply();
               $agreementReply->agreement_id = Input::get('dis_agreement_id');
               $agreementReply->reply = Input::get('disdescription');
               $agreementReply->save();
            }
            
            return $id;
        }
    }

    public function getAgreement() {

        if (Request::ajax()) {
            $user = $this->_userSession;

            $agreement_id = Input::get("agreement_id");

            $agreements = Agreements::find($agreement_id)
                            ->leftJoin('users','users.id','=','agreements.sender_id')
                            ->leftJoin('employer', 'users.user_id', '=', 'employer.emp_id')
                            ->leftJoin('candidate', 'agreements.receiver_id', '=', 'candidate.candidate_id')->first();
                         
                            //Helpers::debug($agreements);
            return $agreements;
        }
    }

    public function agreementDtl() {

        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession
        );
        $user = $this->_userSession;


        $agreement_id = Request::segment(3);

        $agreements = Agreements::find($agreement_id);
                       
        $viewModel["agreements"] = $agreements;
        return Theme::make('agreement.agreementDtl', $viewModel);
    }

    public function agreementList() {

        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession
        );

        // echo $this->_userSession->user_id;
        //echo $this->_userSession->user_type;
        //die();
        $viewModel['lists'] = "";
        if ($this->_userSession->user_type == "Admin") {
            $viewModel['lists'] = Agreements::orderBy('agreement_id','DESC')->get();
        } else if ($this->_userSession->user_type == "Candidate") {
            $viewModel['lists'] = Agreements::where("receiver_id", $this->_userSession->user_id)->get();
        }
        return Theme::make('agreement.agreements', $viewModel);
    }

    /* Agreement sent function started */

    public function updateAgreement() {

        if (Request::isMethod('post') || Request::ajax()) {
            $id = Input::get("agreement_id");
            $amount = Input::get('charge');
            $user = $this->_userSession;
            $sender_id = $user->id;

            if ($user->user_type == "Admin")
            {
                AgreementBLL::updateAgreement("Confirmed", $id, $sender_id, $this->_userSession->user_type,$amount);
                return Redirect::to('agreement/details/'.$id);  
            }else
            {
                AgreementBLL::updateAgreement("Accepted", $id, $sender_id, $this->_userSession->user_type,$amount);
                return Redirect::to('agreement/agreementDtl/'.$id);   
            }
        }
    }

    public function lists() {

        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession
        );

        if ($this->_userSession->user_type == "Candidate")
            $viewModel["lists"] = Agreements::where("receiver_id", $this->_userSession->id)->where("status", "Accepted")->orWhere("status", "Confirmed")->orderBy('agreement_id','DESC')->get();
        else
            $viewModel["lists"] = Agreements::orderBy('agreement_id','DESC')->get();


        return Theme::make('agreement.list', $viewModel);
    }

    public function details()
    {
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession
        );
        $id = Request::segment(3);
        $agreement = Agreements::find($id);
        if(count($agreement))
        {
            $viewModel['agreement'] = $agreement;
            $viewModel['payments'] = $agreement->Payments;
            return Theme::make('agreement.details',$viewModel);
        }else{
            Helpers::addMessage(400, " Agreement not found");
            return Redirect::to('agreement/lists');
        }
    }

    public function agreementRating()
    {
        if(Request::ajax())
        {
            $rating = Input::get('rating');
            $comment = Input::get('comment');
            $agreementId = Input::get('agreementId');
            $agreement = Agreements::find($agreementId);
            if(count($agreement) && $agreement->status=='Accepted')
            {
                return  Agreements::where('agreement_id',$agreementId)->update(array(
                    'rating'=>$rating,
                    'rating_comment' => (!empty($comment))? $comment : ''
                ));
            }


        }else{
            Helpers::addMessage(500, " Bad Request");
            return Redirect::to('agreement/lists');
        }
    }

    public function agreeFrm($agreement_id)
    {
       $agreement = Agreements::find($agreement_id);
       $viewModel['agreement'] = $agreement;
       return Theme::make('agreement.agree',$viewModel);
    }

}

?>
