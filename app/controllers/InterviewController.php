<?php
/**
 * Created by PhpStorm.
 * User: HimelC51
 * Date: 11/17/14
 * Time: 4:10 PM
 */

class InterviewController extends BaseController{

    protected $layout;
    protected $default_route;
    protected $_userSession;
    protected $pageLimit = 20;

    public function __construct() {

        $this->layout = Theme::getLayout();
        $this->default_route = 'interview/index';
        $this->_userSession = Authenticate::check();

        if(!empty($this->_userSession) && ($this->_userSession->remember_me))
        {
            $expireTime = (60*24*360);
            Config::set('session.lifetime',$expireTime);
        }
        $inbox = Inbox::where('receiver_id',$this->_userSession->id)->where('read_status',0)->orderBy('inbox_id','desc')->take(5)->get();

        View::share('messages',$inbox);

        $comment = Comment::where('receiver_id', $this->_userSession->id)->where('read_status', 0)->get();
        View::share('comments', $comment);

        if(!empty($this->_userSession->user_id))
            $jobCount = Jobs::where('created_by',$this->_userSession->user_id)->count();
        else
            $jobCount = 0;
        View::share('jobCount',$jobCount);

    }

    public function index(){

        return Redirect::to('interview/lists');

    }

    public function lists()
    {
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user'  => $this->_userSession
        );
        if($this->_userSession->user_type == 'Employer')
        {
            $empId = $this->_userSession->user_id;
            $jobs       =  Jobs::where('created_by',$empId)->get();
            if(count($jobs) <= 0)
            {
                Helpers::addMessage(400,' You must create at least one job before make interview');
                return Redirect::to('dashboard');
            }
            $interviews = Interview::with('Candidate','Employer','Job')->where('emp_id',$empId)->orderBy('interview_id','DESC')->get();

        }else if($this->_userSession->user_type == 'Admin')
        {
            $interviews = Interview::with('Candidate','Employer','Job')->orderBy('interview_id','DESC')->get();

        }else{

            $candidateId = $this->_userSession->user_id;
            $interviews = Interview::with('Candidate','Employer','Job')->where(function($q){
                $q->where('status','=','Offered')->where('status','=','Approved','OR')->where('status','=','Replied','OR')->where('status','=','Declined','OR')->where('status','=','Completed','OR')->where('status','=','Accepted Pending','OR')->where('status','=','Accepted','OR');
            })->where('candidate_id',$candidateId)->orderBy('interview_id','DESC')->get();
        }
        $viewModel['interviews'] = $interviews;

        return Theme::make('interview.new.interview-list',$viewModel);
    }

    public function create()
    {
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user'  => $this->_userSession
        );

        if($this->_userSession->user_type!='Employer')
        {
            return Redirect::to('dashboard');
        }

        $candidateId = Request::segment(3);
        $candidate  =  Candidate::find($candidateId);
        $employer   =  Employer::find($this->_userSession->user_id);

         if(Request::isMethod('post'))
            $viewModel['job'] = Input::get('job_id');

        $jobs       =  Jobs::where('created_by',$employer->emp_id)->get();
        
        if(count($jobs) <= 0)
        {
            Helpers::addMessage(400,' You must create at least one job before make interview');
            return Redirect::to('dashboard');
        }

        $myjobs = array();
        foreach($jobs as $job)
        {
            $myjobs[$job->jobs_id]['title'] = $job->job_title;
            $myjobs[$job->jobs_id]['job_id'] = $job->jobs_id;

            $jobResponsibilities = (!empty($job->job_responsibility))? json_decode($job->job_responsibility) : array();
            $jobEduQualification = (!empty($job->eduactional_requirement))? json_decode($job->eduactional_requirement) : array();
            $jobExpRequirement   = (!empty($job->experience_requirment))? json_decode($job->experience_requirment) : array();
            $jobNature = $job->job_nature;


            $myjobs[$job->jobs_id]['responsibilities'] = implode("\n",$jobResponsibilities);
            $myjobs[$job->jobs_id]['edu'] = implode("\n",$jobEduQualification);
            $myjobs[$job->jobs_id]['exp'] = implode("\n",$jobExpRequirement);
            $myjobs[$job->jobs_id]['nature'] = $jobNature;
            $myjobs[$job->jobs_id]['salary'] = $job->salary_range;

        }

        if(count($candidate))
        {
            $viewModel['employer'] = $employer;
            $viewModel['candidate'] = $candidate;
            $viewModel['jobs']      = json_encode($myjobs);
            return Theme::make('interview.new.interview-create',$viewModel);
        }else{
            return Redirect::to('resume/display-resume/'.$candidateId);
        }

    }

    public function sendInterview()
    {
        if(Request::isMethod('post'))
        {
            if($this->_userSession->user_type!='Employer')
            {
                return Redirect::to('dashboard');
            }
            $cid = Input::get('cid');
            $jobid = Input::get('joblist');

            $responsibilities = Input::get('responsibilities');
            $edu_qualification = Input::get('edu_qualification');
            $exp = Input::get('exp');
            $salary = Input::get('salary');
            $nature = Input::get('nature');
            $message = Input::get('message');

            if(preg_match('/((^[0][1-9]|\d+){6,14}|http:\/\/.+\.([a-z]{3,}|[a-z]{3,}\.[a-z]{2,3})|[a-z]+\.[a-z]{3,}|[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-z]{2,})/', $message))
            {
                 Helpers::addMessage(400,"Sorry! You cannot write any phone no, email or web url");
                return Redirect::to('interview/index');
            }
            
            $interviewDate = Input::get('interview_date');
            $interviewTime = Input::get('interview_time');
            $interviewDuration = Input::get('interview_duration');

            $empId = $this->_userSession->user_id;
            $interview = Interview::firstOrNew(array('emp_id'=>$empId,'candidate_id'=>$cid,'job_id'=>$jobid,'status'=>'Pending'));
            
            if(!$interview->exists)
            {
                $interview->topics  = "Responsibilities: ".$responsibilities;
                $interview->topics .= "\n Academic Qualification".$edu_qualification;
                $interview->topics .= "\n Experience ".$exp;
                $interview->salary  = $salary;

                $interview->interview_date     = $interviewDate;
                $interview->interview_time     = $interviewTime;
                $interview->interview_duration = $interviewDuration;

                $interview->type    = $nature;
                $interview->message = $message;
                $interview->status  = 'Pending';
                $interview->save();

                Helpers::addMessage(200, ' Interview request send successfully');

            }else{

                Helpers::addMessage(400, ' There is a pending interview');

            }
            return Redirect::to('interview/index');
        }
        else
        {
            return Redirect::to('dashboard');
        }
    }

    public function details()
    {
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user'  => $this->_userSession
        );

        $permission = Permission::getPermission($this->_userSession->role_id,"Interview",'view');

        $editPermission = Permission::getPermission($this->_userSession->role_id,"Interview",'edit');

        if($this->_userSession->role_id != 1)
        {

           if(!$permission)
           {
                Helpers::addMessage(500, " You don't have permission to access that page");
                return Redirect::to('dashboard');
           }
        }

        $id = Request::segment(3);
        $interview = Interview::find($id);
        if(count($interview))
        {
            if(($this->_userSession->user_type == "Candidate") )
            {
                if(!in_array($interview->status,array('Declined','Offered','Completed','Approved','Replied','Accepted','Accepted Pending'))){
                    Helpers::addMessage(400, "Access Denied");
                    return Redirect::to('interview/lists');
                }
                $viewModel['interviewResponse'] = InterviewReply::where('interview_id',$interview->interview_id)->where('replier_type','Candidate')
                                            ->where(function($q){
                                                $q->where('status','=','Approved')->where('status','=','Pending','or');
                                            })->where('replier_id',$this->_userSession->user_id)->count();
               
            }

            $employer   =  Employer::find($interview->emp_id);
            $viewModel['employer'] = $employer;
            $viewModel['interview'] = $interview;
            $viewModel['editPermission'] = $editPermission;
            return Theme::make('interview.new.interview-details',$viewModel);
        }
        else{
            return Redirect::to('interview/lists');
        }


    }

    public function offer()
    {
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user'  => $this->_userSession
        );

        $id = Request::segment(3);
        $interview = Interview::find($id);

        if(count($interview))
        {

            $employer   =  Employer::find($interview->emp_id);
           // Helpers::debug($interview->Candidate,1);
            $viewModel['employer'] = $employer;
            $viewModel['interview'] = $interview;
            return Theme::make('interview.new.offer-details',$viewModel);
        }
        else{
            return Redirect::to('inquiry/lists');
        }
    }

    public function viewReplies($id)
    {
        $interview = Interview::find($id);
        $replies = $interview->InterviewReplies;
        $viewModel = array(
            'user' => $this->_userSession
        );
       
        $viewModel['replies'] = $replies;

        return Theme::make('interview.new.replies',$viewModel);
    }

    public function approveReply()
    {
        if(Request::isMethod('post'))
        {
            $id = Input::get('id');
            $interviewResponse = InterviewReply::find($id);
            $interviewResponse->status = "Approved";
            $interviewResponse->save();
            $interview = Interview::find($interviewResponse->interview_id);
            
            $interview->status = "Replied";
            $interview->save();
            echo 1;
            exit();

        }else{
            return Redirect::to('dashboard');
        }
        
    }

    public function candidateReply()
    {
        if(Request::isMethod('post'))
        {
            $all = Input::all();
            $interviewId = $all['id'];
            $interview = Interview::find($interviewId);
            $interviewReply = array(
                'replier_id' => $this->_userSession->user_id,
                'replier_type' => 'Candidate',
                'reply'        => json_encode(array('date'=>$all['my_date'],'time'=>$all['my_time'])),
                'status' => "Pending",
                'interview_id' => $interview->interview_id
            );
            $interviewReplyObj = new InterviewReply($interviewReply);
            $interviewReplyObj->save();
            return $interviewReplyObj->interview_id;
        }else{
            return Redirect::to('dashboard');
        }
    }

    public function acceptInterviewSchedule()
    {
        if(Request::isMethod('post')){
            $all = Input::all();
            $interviewReply = InterviewReply::find($all['ir_id']);
            $interviewReply->status= "Approved";
            $interviewReply->save();
            $interview = $interviewReply->Interview;
            if(!empty($all['approve_date']) && !empty($all['approve_time']))
            {
                $interview->candidate_prefered_datetime = json_encode(array('date'=>$all['approve_date'],'time'=>$all['approve_time']));
            }
            $interview->status = "Accepted";
            $interview->save();
            return Redirect::to('interview/details/'.$interview->interview_id);
        }else{
            return Redirect::to('dashboard');
        }
    }

    public function acceptInterviewReply()
    {
        if(Request::isMethod('post')){
            $all = Input::all();
            $interviewReply = InterviewReply::find($all['ir_id']);
            $interviewReply->status= "Approved";
            $interviewReply->save();
            
            $interview = $interviewReply->Interview;
            
            
            return Redirect::to('interview/details/'.$interview->interview_id);
        }else{
            return Redirect::to('dashboard');
        }
    }

    public function get_interview_by_date()
    {
        $interviews = null;
        if(Request::isMethod('post'))
        {
            $createdDate = Input::get('date');
            if($this->_userSession->user_type == 'Employer')
            {
                $empId = $this->_userSession->user_id;
                
                $interviews = Interview::with('Candidate','Employer','Job')->where('created_at',$createdDate)->where('emp_id',$empId)->orderBy('interview_id','DESC')->get();

            }else if($this->_userSession->user_type == 'Admin')
            {
                $interviews = Interview::with('Candidate','Employer','Job')->where('created_at',$createdDate)->orderBy('interview_id','DESC')->get();

            }else{

                $candidateId = $this->_userSession->user_id;
                $interviews = Interview::with('Candidate','Employer','Job')->where(function($q){
                    $q->where('status','=','Offered')->where('status','=','Approved','OR')->where('status','=','Replied','OR')->where('status','=','Declined','OR')->where('status','=','Completed','OR')->where('status','=','Accepted Pending','OR')->where('status','=','Accepted','OR');
                })->where('candidate_id',$candidateId)->where('created_at',$createdDate)->orderBy('interview_id','DESC')->get();
            }
        }

        return $interviews;
        exit();
    }

} 