<?php
/**
 * Created by PhpStorm.
 * User: Himel
 * Date: 7/8/14
 * Time: 3:39 PM
 */

class MessageController extends BaseController{

    /**
     * for set layout
     * @var string
     */
    protected $layout;

    /**
     * for set default route
     * @var string
     */
    protected $default_route;

    /**
     * for set user session data
     * @var string
     */
    protected $_userSession;

    protected $pageLimit;

    public function __construct()
    {
        $this->layout = Theme::getLayout();
        $this->default_route = 'message/index';
        $this->pageLimit = 20;
        $this->_userSession = Authenticate::check();  // check is user logged in
        if(!empty($this->_userSession) && ($this->_userSession->remember_me))
        {
            $expireTime = (60*24*360);
            Config::set('session.lifetime',$expireTime);
        }
     $inbox = Inbox::where('receiver_id', $this->_userSession->id)->where('read_status', 0)->orderBy('inbox_id','desc')->take(5)->get();
        View::share('messages', $inbox);
        
                     $comment = Comment::where('receiver_id', $this->_userSession->id)->where('read_status', 0)->get();
        View::share('comments', $comment);

        if(!empty($this->_userSession->user_id))
            $jobCount = Jobs::where('created_by',$this->_userSession->user_id)->count();
        else
            $jobCount = 0;
        View::share('jobCount',$jobCount);
   
    }

    
    public function index()
    {
        return Redirect::to("message/lists");
    }
    public function lists()
    {
        $sessionUId = $this->_userSession->id;
        $viewModel = array(
            'theme'=> Theme::getTheme(),
            'user' => $this->_userSession,
            'commentall' => Comment::where('receiver_id',$sessionUId)->where('type','Notification')->OrderBy('sender_id','DESC')->get()
        );
        return Theme::make('messagebox.index',$viewModel);
    }

    public function sentItems()
    {
        $sessionUId = $this->_userSession->id;
       
        $viewModel = array(
            'theme'=> Theme::getTheme(),
            'user' => $this->_userSession,
            'sentitems' => Discussion::where('sender_id',$sessionUId)->OrderBy('discussion_id','DESC')->get()
        );
        return Theme::make('messagebox.sent-items',$viewModel);
    }

    public function compose()
    {
        $receivers=  array();
        if($this->_userSession->user_type=="Admin"){
        
            $receivers =  Employer::all();
        
        }else{
            $receivers=User::where("user_type","Admin")->get();
        }
        /*Helpers::LastQuery();
        Helpers::debug($receivers);die();*/
        $viewModel = array(
            'theme' =>  Theme::getTheme(),
            'user'  => $this->_userSession,
            'receivers' => $receivers
        );

        return Theme::make('messagebox.compose',$viewModel);
    }

    public function saveMessage()
    {
      $response = MessageSystem::saveDiscussion($this->_userSession->id);
      Helpers::addMessage($response['status'],$response['msg']);

      return Redirect::to($this->default_route);
    }


    public function details()
    {
        $id = Request::segment(3);
        $cid = Request::segment(4);
        $discussion = Discussion::find($id);


        $viewModel = array(
          'theme' => Theme::getTheme(),
          'user'  => $this->_userSession
        );

        if(count($discussion))
        {
            $comment = Comment::find($cid);
            if(count($comment))
            {
                Comment::where('comment_id',$comment->comment_id)->update(array('read_status'=>1));
            }

            $inbox = Comment::where('receiver_id',$this->_userSession->id)->where('read_status',0)->where('type','Notification')->get();
            View::share('comments',$inbox);

            $viewModel['message'] = $discussion;
            return Theme::make('messagebox.details',$viewModel);
        }else{
            Helpers::addMessage(500, " Bad Request");
            return Redirect::to($this->default_route);
        }
    }

    public function reply()
    {
        $response = MessageSystem::commentToDiscussion();
        Helpers::addMessage($response['status'],$response['msg']);

        return Redirect::to($response['refurl']);
    }

    public function bulkDeleteComment()
    {
        if(Request::ajax())
        {
            $message_id = Input::get('message_id');
            if(count($message_id))
            {
                foreach($message_id as $mid)
                {
                   $notification = Comment::find($mid);
                   $notification->delete();
                }

            }
        }
        return 1;
    }

    public function bulkDeleteMessage()
    {
        if(Request::ajax())
        {
            $message_id = Input::get('message_id');
            if(count($message_id))
            {
                foreach($message_id as $mid)
                {
                    $message = Discussion::find($mid);
                    $message->delete();
                }

            }
        }
        return 1;
    }

    public function downloadAttachment($fileName)
    {
        $file_url = 'data/attachment/'.urldecode($fileName);
        header('Content-Type: application/octet-stream');
        header('Content-Transfer-Encoding: Binary');
        header('Content-disposition: attachment; filename="' . basename($file_url) . '"');
        readfile($file_url);
    }
} 