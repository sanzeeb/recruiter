<?php

class RolesController extends BaseController
{
	protected $layout;

    /**
     * for set default route
     * @var string
     */
    protected $default_route;

    /**
     * for set user session data
     * @var string
     */
    protected $_userSession;

    //protected $pageLimit;

    public function __construct()
    {

        $this->layout = Theme::getLayout();
        $this->default_route = 'roles/index';
        $this->pageLimit = 20;
        $this->_userSession = Authenticate::check();  // check is user logged in
        if(!empty($this->_userSession) && ($this->_userSession->remember_me))
        {
            $expireTime = (60*24*360);
            Config::set('session.lifetime',$expireTime);
        }
        $inbox = Inbox::where('receiver_id', $this->_userSession->id)->where('read_status', 0)->orderBy('inbox_id','desc')->take(5)->get();
        View::share('messages', $inbox);

        $comment = Comment::where('receiver_id', $this->_userSession->id)->where('read_status', 0)->get();
        View::share('comments', $comment);

        if(!empty($this->_userSession->user_id))
            $jobCount = Jobs::where('created_by',$this->_userSession->user_id)->count();
        else
            $jobCount = 0;
        View::share('jobCount',$jobCount);
    }

    public function index()
    {
    	return Redirect::to('roles/lists');
    }

	public function lists()
	{
		$viewModel = array(
            'theme' => Theme::getTheme(),
            'user'  => $this->_userSession
        );
		
		$viewModel['roles'] = Role::all();

		return Theme::make('role.index',$viewModel);
	}    

	public function create()
	{
		if(Request::isMethod('post'))
		{
			$post_all = Input::all();

			$role = Role::firstOrNew(array('role_name'=>$post_all['role_name']));
			$role->editable = 1;
			$role->save();
			if($role->role_id)
				Helpers::addMessage(200, 'Role '. $role->role_name. ' created');
		}

		return Redirect::to($this->default_route);
	}

	public function permission($id=null)
	{
		$viewModel = array(
            'theme' => Theme::getTheme(),
            'user'  => $this->_userSession
        );

        if($id != null)
        {
        	$role = Role::find($id);
        	
        	$viewModel['role'] = $role;
        	$viewModel['modules'] = Module::getModules();
        	$viewModel['permissions'] = Permission::getModulePermissions($role->role_id);
			return Theme::make('role.permission',$viewModel);
        }else{
        	return Redirect::to('roles/lists');
        }
	}

	public function updatePermission()
	{
		if(Request::isMethod('post'))
		{
			$post_all = Input::all();
			$role = $post_all['role'];
			
			
			$permission = Permission::where('role_id',$role);
			$permission->delete();

			foreach($post_all['permission'] as $module => $permissions)
			{
				$actions = null;
				foreach($permissions as $action=>$role_id){
					
					$permission = Permission::firstOrNew(array('module'=>$module,'role_id'=>$role_id));
					
					$actions[$action] = 1;
					
				}
				$permission->action = json_encode($actions);
				$permission->save();
			}
			
			
			return Redirect::to('roles/permission/'.$role);
			
		}else{
			return Redirect::to($this->default_route);
		}

		
	}

}