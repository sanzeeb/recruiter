<?php
/**
 * Created by PhpStorm.
 * User: Himel
 * Date: 6/2/14
 * Time: 5:20 PM
 */

class CvmgmController extends BaseController{

    protected $layout;
    protected $default_route;
    protected $_userSession;

    public function __construct() {

        $this->layout = Theme::getLayout();
        $this->default_route = 'dashboard/index';
        $this->_userSession = Authenticate::check();
        if(!empty($this->_userSession) && ($this->_userSession->remember_me))
        {
            $expireTime = (60*24*360);
            Config::set('session.lifetime',$expireTime);
        }
        $this->_userSession->user_type;
    }

    public function index()
    {
        error_reporting(0);
        $data = new SpreadsheetExcelReader("data/Job Seeker Profile Format.xls");
        $currentSheet = array_shift($data->sheets);

        $cv = new Cv($currentSheet);
        $cv->getIndex();
        $saved = $cv->SaveCv();


        exit;
    }

    public function uploadCv()
    {
        error_reporting(0);
        if(Request::isMethod('post'))
        {
            if(Input::hasFile('cvfile'))
            {

                $ext = Input::file('cvfile')->getClientOriginalExtension();
                $name = Input::file('cvfile')->getClientOriginalName();

                $path = 'data/cv/';
                $name = time().'_'.$name;
                if(in_array(strtolower($ext),array('xls'))){
                    Input::file('cvfile')->move($path,$name);
                    $data = new SpreadsheetExcelReader($path.$name);
                    $currentSheet = array_shift($data->sheets);

                    $cv = new Cv($currentSheet);
                    $cv->getIndex();
                    $saved = $cv->SaveCv();

                }
            }
        }
    }

} 