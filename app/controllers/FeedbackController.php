<?php

class FeedbackController extends BaseController {

    protected $layout;
    protected $default_route;
    protected $_userSession;

    public function __construct() {

        $this->layout = Theme::getLayout();
        $this->default_route = 'dashboard/index';
        $this->_userSession = Authenticate::check();
        if(!empty($this->_userSession) && ($this->_userSession->remember_me))
        {
            $expireTime = (60*24*360);
            Config::set('session.lifetime',$expireTime);
        }
        $inbox = Inbox::where('receiver_id', $this->_userSession->id)->where('read_status', 0)->orderBy('inbox_id','desc')->take(5)->get();

        View::share('messages', $inbox);
        
                             $comment = Comment::where('receiver_id', $this->_userSession->id)->where('read_status', 0)->get();
        View::share('comments', $comment);

        if(!empty($this->_userSession->user_id))
            $jobCount = Jobs::where('created_by',$this->_userSession->user_id)->count();
        else
            $jobCount = 0;
        View::share('jobCount',$jobCount);
    }

    public function index() {

        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession
        );

        $cv_tbl_id = Request::segment(3);


        $cvtbl = CvTable::find($cv_tbl_id);
        if (count($cvtbl) > 0) {
            $candidate = $cvtbl->Candidate;
            $speacs = $cvtbl->CvSpecialization;
            $viewModel["candidate"] = $candidate;
            $viewModel["speacs"] = $speacs;
            return Theme::make('feedback.list', $viewModel);
        } else {
            Helpers::addMessage(400, " Candidate cv not found");
            return Redirect::to("resume/bank-resume/");
        }
    }
    
    
        public function feedbackSave(){
            
            if(Request::ajax()){
                
              $spec_id = Input::get("spec_id");
              
              $ratingStar = Input::get("ratingStar");
              
              $data=array("rating"=>$ratingStar);
              
             return CvSpecialization::where("cv_specialization_id",$spec_id)->update($data);
            }
        
    }

}
