<?php

class CandidateController extends BaseController {
    
    /*
      |--------------------------------------------------------------------------
      | Default Home Controller
      |--------------------------------------------------------------------------
      |
      | You may wish to use controllers instead of, or in addition to, Closure
      | based routes. That's great! Here is an example controller method to
      | get you started. To route to this controller, just add the route:
      |
      |	Route::get('/', 'HomeController@login');
      |
     */

    
    protected $layout;

    /**
     * for set default route
     * @var string
     */
    protected $default_route;

    /**
     * for set user session data
     * @var string
     */
    protected $_userSession;

    //protected $pageLimit;

    public function __construct()
    {

        $this->layout = Theme::getLayout();
        $this->default_route = 'activities/index';
        $this->pageLimit = 20;
        $this->_userSession = Authenticate::check();  // check is user logged in
        if(!empty($this->_userSession) && ($this->_userSession->remember_me))
        {
            $expireTime = (60*24*360);
            Config::set('session.lifetime',$expireTime);
        }
        $inbox = Inbox::where('receiver_id', $this->_userSession->id)->where('read_status', 0)->orderBy('inbox_id','desc')->take(5)->get();
        View::share('messages', $inbox);

        $comment = Comment::where('receiver_id', $this->_userSession->id)->where('read_status', 0)->orderBy('comment_id','desc')->get();
        View::share('comments', $comment);
    }

    public function dashboard() {

     $viewModel = array(
            'theme' => Theme::getTheme(),
            'user'  => $this->_userSession
        );
     
        $skills = Skills::where('status',1)->orderBy("display_order","asc")->paginate(20);
        
        $viewModel["skills"] = $skills;
        
        $candidates = User::where('user_type', 'Candidate')->get();
        $employers  = User::where('user_type', 'Employer')->get();
        $jobs       = Jobs::all();
        $resumes    = CvTable::all();
        
        $viewModel['candidates'] = count($candidates);
        $viewModel['employers']  = count($employers);
        $viewModel['company']    = Employer::all();
        $viewModel['jobs']       = count($jobs);
        $viewModel['resumes']    = count($resumes);
        
        return Theme::make('candidate_dashboard', $viewModel);
        
        
    }

    public function jobs()
    {
      $viewModel = array(
            'theme' => Theme::getTheme(),
            'user'  => $this->_userSession
        );
        $skill=base64_decode(Request::segment(3));
        $skillObj = Skills::where('skill_name',$skill)->first();
        
        $jobsObj = Jobs::where('skill_id',$skillObj->skill_id)
        ->whereRaw("last_date >= CURRENT_DATE()")->get();
        $jobsObj = DB::table('jobs')
                ->select(
                  DB::raw('GROUP_CONCAT(candidate_id) AS candidates,jobs_id, job_title , job_vacancies, salary_range ,last_date ,title, jobs.created_at as job_post_date')
                )
                ->join('employer','jobs.created_by','=','employer.emp_id','left')
                ->join('job_candidates','job_candidates.job_id','=','jobs.jobs_id','left')
                ->where('jobs.skill_id',$skillObj->skill_id)
                ->whereRaw("last_date >= CURRENT_DATE()")
                ->groupBy('jobs_id')
                ->get();

      // Helpers::LastQuery();
        
        $viewModel['jobs'] = $jobsObj;
        $viewModel['skill'] = $skill;

        return Theme::make('job.candidate_jobs',$viewModel);
    }

    public function relatedJobs()
    {
        $viewModel = array(
          'theme'=> Theme::getTheme(),
          'user' => $this->_userSession
        );
        $candidate = Candidate::find($this->_userSession->user_id);
        $result = array();
        if(count($candidate))
        {
           $result =  DB::table('cv_specialization')
                ->select(
                  DB::raw('GROUP_CONCAT(candidate_id) AS candidates,jobs_id, job_title , job_vacancies, salary_range ,last_date , jobs.created_at as job_post_date')
                )
                ->join('skills','cv_specialization.key','=','skills.skill_name')
                ->join('jobs','skills.skill_id','=','jobs.skill_id')
                ->join('job_candidates','job_candidates.job_id','=','jobs.jobs_id','left')
                ->whereRaw("last_date >= CURRENT_DATE()")
                
                ->where('cv_tbl_id',$candidate->cv_tbl_id)
                ->groupBy('jobs_id')
                ->get();

        }

        //Helpers::LastQuery();
      //Helpers::debug($result,1);
        $viewModel['jobs'] = $result;

        return Theme::make('job.related-jobs',$viewModel);
    }
    
    
    

}
