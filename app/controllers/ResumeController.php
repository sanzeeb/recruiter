<?php

class ResumeController extends BaseController {
    /*
      |--------------------------------------------------------------------------
      | Default Home Controller
      |--------------------------------------------------------------------------
      |
      | You may wish to use controllers instead of, or in addition to, Closure
      | based routes. That's great! Here is an example controller method to
      | get you started. To route to this controller, just add the route:
      |
      |	Route::get('/', 'HomeController@login');
      |
     */

    protected $layout;

    /**
     * for set default route
     * @var string
     */
    protected $default_route;

    /**
     * for set user session data
     * @var string
     */
    protected $_userSession;

    //protected $pageLimit;

    public function __construct() {
        
        $this->layout = Theme::getLayout();
        $this->default_route = 'activities/index';
        $this->pageLimit = 20;
        $this->_userSession = Authenticate::check();  // check is user logged in
        if(!empty($this->_userSession) && ($this->_userSession->remember_me))
        {
            $expireTime = (60*24*360);
            Config::set('session.lifetime',$expireTime);
        }
        $inbox = Inbox::where('receiver_id', $this->_userSession->id)->where('read_status', 0)->orderBy('inbox_id','desc')->take(5)->get();
        View::share('messages', $inbox);
                             $comment = Comment::where('receiver_id', $this->_userSession->id)->where('read_status', 0)->get();
        View::share('comments', $comment);

        if(!empty($this->_userSession->user_id))
            $jobCount = Jobs::where('created_by',$this->_userSession->user_id)->count();
        else
            $jobCount = 0;
        View::share('jobCount',$jobCount);
    }

    public function candidateResumeSave() {
        $permission = Authenticate::getPermission($this->_userSession);
        if (!$permission->create) {
            Helpers::addMessage(500, " You don't have permission to access that page");
            return Redirect::to('dashboard');
        }
        $resumeModel = new ResumeModel();
        $saved = $resumeModel->saveCanResume();
        return Redirect::to('resume/can-create-resume');
    }

    public function deleteAttr() {

        if (Request::ajax()) {
            $type = Input::get("type");
            $id = Input::get("id");

            if ($type == "j") {


                CvJob::where('cv_jobs_id', $id)->delete();
                return 200;
            } else if ($type == "e") {

                CvEdu::where('cv_edu_id', $id)->delete();
                return 200;
            } else if ($type == "q") {
                CvQualification::where('cv_qualification_id', $id)->delete();
                return 200;
            }
        }
    }

    public function import() {

        $permission = Authenticate::getPermission($this->_userSession);
        if (!$permission->create) {

            Helpers::addMessage(500, " You don't have permission to access that page");
            return Redirect::to('dashboard');
        }

        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession
        );

        if (Request::isMethod('post')) {
            error_reporting(0);
            if (Input::hasFile('cvfile')) {
                $ext = Input::file('cvfile')->getClientOriginalExtension();
                $name = Input::file('cvfile')->getClientOriginalName();

                $path = 'data/cv/';
                $name = time() . '_' . str_replace(" ", "_", $name);
                if (in_array(strtolower($ext), array('xls'))) {
                    Input::file('cvfile')->move($path, $name);
                    $fileName = $path . $name;
                    $data = new SpreadsheetExcelReader($fileName);
                    $currentSheet = array_shift($data->sheets);

                    $cv = new Cv($currentSheet);
                    $cv->getIndex();
                    $saved = $cv->SaveCv();
                    if ($saved) {
                        // Helpers::addMessage(200, 'Successfully cv imported');
                        return Redirect::to('resume/import');
                    }
                } else {
                    Helpers::addMessage(400, 'File Type not supported. Please verify that file extension is .xls');
                }
            }
        }


        return Theme::make('resume.import', $viewModel);
    }

    public function createResume() {

        $permission = Authenticate::getPermission($this->_userSession);
        if (!$permission->create) {
            Helpers::addMessage(500, " You don't have permission to access that page");
            return Redirect::to('dashboard');
        }
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession,
            'specs' => array(
                'Oracle', 'Linux', 'Joomla', 'Microcontroller programming',
                'ASP.Net', 'C++/VC++', 'Java', 'C#', 'Python', 'Ruby', 'Perl', 'HTML/DHTML', 'MS SQL Server', 'JavaScript', 'PHP', 'MySQL'
                , 'XML'),
            'genders' => array('Male', 'Female'),
            'marital_status' => array('Married', 'Unmarried')
        );


        return Theme::make('resume.creates', $viewModel);
    }

    public function canCreateResume() {

        $permission = Authenticate::getPermission($this->_userSession);
        if (!$permission->create) {
            Helpers::addMessage(500, " You don't have permission to access that page");
            return Redirect::to('dashboard');
        }
        $user = Authenticate::check();
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession,
            'specs' => array(
                'Oracle', 'Linux', 'Joomla', 'Microcontroller programming',
                'ASP.Net', 'C++/VC++', 'Java', 'C#', 'Python', 'Ruby', 'Perl', 'HTML/DHTML', 'MS SQL Server', 'JavaScript', 'PHP', 'MySQL'
                , 'XML'),
            'genders' => array('Male', 'Female'),
            'marital_status' => array('Married', 'Unmarried')
        );

        $candidate = Candidate::find($user->user_id);

        $viewModel['candidate'] = $candidate;

        return Theme::make('resume.can_create', $viewModel);
    }

    public function saveResume() {
        $permission = Authenticate::getPermission($this->_userSession);
        if (!$permission->create) {
            Helpers::addMessage(500, " You don't have permission to access that page");
            return Redirect::to('dashboard');
        }
        $resumeModel = new ResumeModel();
        $saved = $resumeModel->saveResume();
        return Redirect::to('resume/create-resume');
    }

    public function updateResume() {
        $resumeModel = new ResumeModel();
        $updated = $resumeModel->updateResume();
        if ($this->_userSession->user_type == "Candidate") {

            return Redirect::to('profile/index/' . $updated);
        }
        return Redirect::to('resume/view-resume/' . $updated);
    }

    public function updateOthers() {
        $resumeModel = new ResumeModel();
        $updated = $resumeModel->updateOthers();
        if ($this->_userSession->user_type == "Candidate") {

            return Redirect::to('profile/index/' . $updated);
        }
        return Redirect::to('resume/view-resume/' . $updated);
    }

    public function changeSpec() {
        ResumeModel::updateSpecs();
        return 0;
    }

    public function viewResume() {

        $permission = Authenticate::getPermission($this->_userSession);
        if (!$permission->read) {
            Helpers::addMessage(500, " You don't have permission to access that page");
            return Redirect::to('dashboard');
        }
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession,
            'specs' => array(
                'Oracle', 'Linux', 'Joomla', 'Microcontroller programming',
                'ASP.Net', 'C++/VC++', 'Java', 'C#', 'Python', 'Ruby', 'Perl', 'HTML/DHTML', 'MS SQL Server', 'JavaScript', 'PHP', 'MySQL'
                , 'XML'),
            'genders' => array('Male', 'Female'),
            'marital_status' => array('Married', 'Unmarried')
        );


        $cvTblId = Request::segment(3);
        $candidate = Candidate::where('cv_tbl_id', $cvTblId)->first();
        if (count($candidate)) {
            $cvTblObj = $candidate->CvTable;
            $cvJob = $cvTblObj->CvJob;
            $cvEdu = $cvTblObj->CvEdu;
            $cvQualification = $cvTblObj->CvQualification;
            $cvSpecialization = $cvTblObj->CvSpecialization;

            $viewModel['candidate'] = $candidate;
            $viewModel['cvInfo'] = $cvTblObj;
            $viewModel['cvJob'] = $cvJob;
            $viewModel['cvEdu'] = $cvEdu;
            $viewModel['cvQ'] = $cvQualification;
            $specs = array();
            if (!empty($cvSpecialization)) {
                foreach ($cvSpecialization as $cSpec) {
                    $specs[$cSpec->key] = $cSpec;
                }
            }
            $viewModel['cvSpecs'] = $specs;
            return Theme::make('resume.view', $viewModel);
        } else {
            Helpers::addMessage(400, ' No resume found');
            return Redirect::to('resume/bank-resume');
        }
    }

    public function displayResume() {

        $permission = Authenticate::getPermission($this->_userSession);

        if (!$permission->read) {
            Helpers::addMessage(500, " You don't have permission to access that page");
            return Redirect::to('dashboard');
        }

        $skills = Skills::where('status',1)->distinct()->get(array('skill_name'));

        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession,
            'specs' => $skills,
            'genders' => array('Male', 'Female'),
            'marital_status' => array('Married', 'Unmarried')
        );


        $cvTblId = Request::segment(3);
        $candidate = Candidate::where('cv_tbl_id', $cvTblId)->first();
        if (count($candidate)) {
            $cvTblObj = $candidate->CvTable;
            $cvJob = $cvTblObj->CvJob;
            $cvEdu = $cvTblObj->CvEdu;
            $cvQualification = $cvTblObj->CvQualification;
            $cvSpecialization = $cvTblObj->CvSpecialization;
            $job_id = Request::segment(4);
            if(!empty($job_id))
            {
                $job_id = Helpers::EncodeDecode($job_id,false);
                $viewModel['job'] = Jobs::find($job_id);
                
            }else{
                $viewModel['job'] = null;
            }
            
            $viewModel['candidate'] = $candidate;
            $viewModel['cvInfo'] = $cvTblObj;
            $viewModel['cvJob'] = $cvJob;
            $viewModel['cvEdu'] = $cvEdu;
            $viewModel['cvQ'] = $cvQualification;
            
            $specs = array();

            if (!empty($cvSpecialization)) {
                foreach ($cvSpecialization as $cSpec) {
                    $specs[$cSpec->key] = $cSpec;
                }
            }

            $viewModel['cvSpecs'] = $specs;
            return Theme::make('resume.display', $viewModel);

        } else {

            Helpers::addMessage(400, ' No resume found');
            return Redirect::to('resume/bank-resume');

        }
    }

    public function bankResume() {
        
        if(in_array($this->_userSession->user_type, array("Admin","Employer")))
        {
            return Redirect::to('dashboard/top-engineer');

        }else{
            return Redirect::to('dashboard');
        }
        
        $permission = Authenticate::getPermission($this->_userSession);
        if (!$permission->read) {
            Helpers::addMessage(500, " You don't have permission to access that page");
            return Redirect::to('dashboard');
        }
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession,
            'recruiters' => RecruiterBLL::getRecruiters(),
            'specs' => array(
                'Oracle', 'Linux', 'Joomla', 'Microcontroller programming',
                'ASP.Net', 'C++/VC++', 'Java', 'C#', 'Python', 'Ruby', 'Perl', 'HTML/DHTML', 'MS SQL Server', 'JavaScript', 'PHP', 'MySQL'
                , 'XML')
        );
        $jobs = '';
        if ($this->_userSession->user_type == 'Employer') {
            $jobs = JobModel::getJobs($this->_userSession);
        } else {
            $jobs = JobModel::getJobs($this->_userSession);
        }
        $viewModel['jobs'] = $jobs;
        return Theme::make('resume.bank', $viewModel);
    }

    public function searchResume() {
        if (Request::ajax()) {
            $permission = Authenticate::getPermission($this->_userSession);
            if (!$permission->read) {

                return "500";
            }

            $result = ResumeModel::search();

            return (!empty($result)) ? $result : json_encode(array());
        } else {
            Helpers::addMessage(500, " Bad Request");
            return Redirect::to('resume/bank-resume');
        }
    }

    public function pdf() {
        if (Request::isMethod('post')) {
            $permission = Authenticate::getPermission($this->_userSession);
            if (!$permission->read) {
                Helpers::addMessage(500, " You don't have permission to access that page");
                return Redirect::to('dashboard');
            }
            $user = $this->_userSession;
            if (Request::isMethod('post')) {
                $cvTblId = Input::get('cv_tbl_id');
                $candidate = Candidate::where('cv_tbl_id', $cvTblId)->first();

                $cvTblObj = $candidate->CvTable;
                $cvJob = $cvTblObj->CvJob;
                $CvEdu = $cvTblObj->CvEdu;
                $CvQualification = $cvTblObj->CvQualification;
                $CvSpecialization = $cvTblObj->CvSpecialization;

                $cvPdf = new CvDownload();
                $cvPdf->SetAutoPageBreak('auto', 30);
                $cvPdf->SetFont("Arial", "B", 10);
                $cvPdf->AddPage();
                $cvPdf->setHeader($candidate, Theme::getTheme(), $user->user_type);
                $cvPdf->employmentHistory($cvJob, $cvTblObj);
                $cvPdf->academicQualification($CvEdu);
                $cvPdf->professionalQualification($CvQualification);
                $cvPdf->specialization($CvSpecialization);
                $cvPdf->personalInfo($candidate);
                $cvPdf->output();
            } else {
                
            }
        } else {
            Helpers::addMessage(500, ' Bad Request');
            return Redirect::to('resume/bank-resume');
        }
    }

    public function addToFavorite()
    {
        if(Request::isMethod('post'))
        {
            $userType = $this->_userSession->user_type;
            if(in_array($userType,array("Admin","Employer")))
            {

                $empId = $this->_userSession->user_id;
                $candidateId = Input::get('cid');
                $jobs       =  Jobs::where('created_by',$empId)->get();
                
                if(count($jobs) <= 0)
                {
                    Helpers::addMessage(400,' You must create at least one job before add to favorite');
                    return Redirect::to('dashboard');
                }

                $candidate = Candidate::find($candidateId);
                $employerFavorite = EmployerFavorite::firstOrNew(array('emp_id'=>$empId,'candidate_id'=>$candidateId));
                if(!$employerFavorite->exists && count($candidate))
                {
                    $employerFavorite->save();
                    Helpers::addMessage(200, " Candidate added to your favorite list");
                }else{
                    Helpers::addMessage(200, " Candidate already exist in your favorite list");
                }
            }
        }

        return Redirect::to('resume/display-resume/'.$candidate->cv_tbl_id);
    }

}
