<?php

class NotificationController extends BaseController
{
	protected $layout;
    protected $default_route;
    protected $_userSession;
    protected $pageLimit = 20;
    public function __construct() {

        $this->layout = Theme::getLayout();
        $this->default_route = 'notification/call';
        $this->_userSession = Authenticate::check();
        
        if(!empty($this->_userSession) && ($this->_userSession->remember_me))
        {
           $expireTime = (60*24*360);
           Config::set('session.lifetime',$expireTime);
        }
        $inbox = Inbox::where('receiver_id',$this->_userSession->id)->where('read_status',0)->orderBy('inbox_id','DESC')->take(5)->get();
        
        View::share('messages',$inbox);
        
        $comment = Comment::where('receiver_id', $this->_userSession->id)->where('read_status', 0)->get();
        View::share('comments', $comment);

        if(!empty($this->_userSession->user_id))
            $jobCount = Jobs::where('created_by',$this->_userSession->user_id)->count();
        else
            $jobCount = 0;
        View::share('jobCount',$jobCount);
        
    }	

    public function index()
    {
    	return Redirect::to($this->default_route);
    }

    public function all()
    {
    	$user_type = $this->_userSession->user_type;
    	$viewModel = array(
    		'user' => $this->_userSession,
    		'theme' => Theme::getTheme()
		);

    	if($user_type == 'Candidate')
    	{
    		$notifications = Inbox::where('receiver_id',$this->_userSession->id)->orderBy('inbox_id','DESC')->paginate($this->pageLimit);
    	}else if($user_type == 'Employer')
    	{
    		$notifications = Inbox::where('receiver_id',$this->_userSession->id)->orderBy('inbox_id','DESC')->paginate($this->pageLimit);
    	}else
    	{
    		$notifications = Inbox::where('receiver_id',$this->_userSession->id)->orderBy('inbox_id','DESC')->paginate($this->pageLimit);
    	}

    	$viewModel['notifications'] = $notifications;
    	return Theme::make('notification',$viewModel);
    }
}