<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class JobseekerController extends BaseController{
    
    
          protected $layout;

    /**
     * for set default route
     * @var string
     */
    protected $default_route;

    /**
     * for set user session data
     * @var string
     */
    protected $_userSession;

    //protected $pageLimit;

    public function __construct()
    {
        Theme::setTheme('jobseeker');
        $this->layout = Theme::getLayout();
        $this->default_route = 'activities/index';
        $this->pageLimit = 20;
        $this->_userSession = Authenticate::hasSession();  // check is user logged in
        if(!empty($this->_userSession) && ($this->_userSession->remember_me))
        {
            $expireTime = (60*24*360);
            Config::set('session.lifetime',$expireTime);
        }
    }
    
    
     public function topEngineers() {
         
        Theme::setTheme('recruit');
         $viewModel = array(
                'theme' => Theme::getTheme(),
                'user'=>  $this->_userSession
            );

        $skill=   Request::segment(3);
       
        $expertises=  CvSpecialization::where('key',$skill)->get();
        
        $viewModel['expertises']=$expertises;
        return Theme::make('top_engineers', $viewModel);
        
        
    }
    
    
    public function registerJobSeeker() {
        $viewModel = array(
            'theme' => Theme::getTheme()
        );
        $msg="";
        if (Request::isMethod("post")) {
            
           $msg=  JobseekerBLL::register();
           
           
           
           $viewModel["msg"]=$msg;
           

        }
        $viewModel['ref'] = Input::get('ref');


                 $id=   Request::segment(3);
        $viewModel['cv_id']=$id;
        return Theme::make('login-registration', $viewModel);
    }

    /*Render home */
    
     public function index() {

     $viewModel = array(
            'theme' => Theme::getTheme()
        );
     
        $skills = Skills::where('status',1)->orderBy("display_order","asc")->paginate(20);
        
        $viewModel["skills"] = $skills;
        
        $candidates = User::where('user_type', 'Candidate')->get();
        $employers  = User::where('user_type', 'Employer')->get();
        $jobs       = Jobs::all();
        $resumes    = CvTable::all();
        
        $viewModel['candidates'] = count($candidates);
        $viewModel['employers']  = count($employers);
        $viewModel['company']    = Employer::all();
        $viewModel['jobs']       = count($jobs);
        $viewModel['resumes']    = count($resumes);
        
        return Theme::make('jobseeker_home', $viewModel);
        
        
    }


    
    /*End home */


    public function jobs()
    {
        $viewModel = array(
            'theme' => Theme::getTheme()
        );
        $skill=base64_decode(Request::segment(3));
        $skillObj = Skills::where('skill_name',$skill)->first();
        $jobsObj = Jobs::where('skill_id',$skillObj->skill_id)->paginate($this->pageLimit);
        $viewModel['jobs'] = $jobsObj;
        $viewModel['skill'] = $skill;
        return Theme::make('jobs',$viewModel);
    }

    public function about() {

        Theme::setTheme('jobseeker');
     $viewModel = array(
            'theme' => Theme::getTheme()
        );
     
         $about=  About::where('type', 'jobseeker')->first();
         $viewModel['about']=$about;
        
        return Theme::make('about', $viewModel);
        
        
    }
    
    
    
    public function contact() {
    Theme::setTheme('jobseeker');
     $viewModel = array(
            'theme' => Theme::getTheme()
        );


        return Theme::make('contact', $viewModel);
        
        
    }
    
    
    /* Render login form */

    public function login() {

        $user = Authenticate::hasSession();

        if ($user) {
            $userExist = User::find($user->id);
            if ($userExist)
                return Redirect::to('dashboard');
        }

        $viewModel = array(
            'theme' => Theme::getTheme()
        );

        return Theme::make('login', $viewModel);
    }

    /* Action for authentication */

    public function authenticate() {

        if (Request::isMethod('post')) {

            $userId = Input::get('userId');
            $password = Input::get('pass');
            $type_log=Input::get('log_type');
            $ref=Input::get('ref');
            Session::put('type_log', $type_log);
            try {
                if(Input::has('remember'))
                {
                    $login = Authenticate::login($userId, $password,1);
                }else{
                    $login = Authenticate::login($userId, $password,0);
                }
                if(is_array($login)){

                   
                    if($login['type']  == 'Candidate')
                        return Redirect::to(JOB_DOMAIN.'jobseeker/register-job-seeker');
                    else
                        return Redirect::to(ASIAN_DOMAIN.'recruiter/register-recruiter'); 

                }    
                if ($login == 1) {

                    if(!empty($ref))
                        return Redirect::to($ref);

                    $cv_tbl_id = Input::get('cv_tbl_id');

                    if($cv_tbl_id!=""){
                               return Redirect::to('resume/display-resume/'.$cv_tbl_id);
                    }

                    $user = Session::get('user');
                    if($user->user_type == "Candidate")
                        return Redirect::to('candidate/dashboard');
                    else
                        return Redirect::to('dashboard');

                }elseif($login == 2){

                    Session::flash('message', 'Your account not verified yet. please confirm your email address to verify your account');
                    return Redirect::to('recruiter/register-recruiter');

                }else {
                    //Session::flash('flash', 'User and password is incorrect');
                    Session::flash('message', 'User and password incorrect');
                    return Redirect::to('jobseeker/register-job-seeker');
                }
            } catch (DbConnectionException $e) {
                echo $e->getMessage();
                die();
            }
        }
    }

    /* Logout a user and redirect to login page */

    public function logout() {
        if (Authenticate::check()) {

            Session::forget('user');
            return Redirect::to('/');
            exit();
        }
    }

    public function forgetPass()
    {

        $viewModel = array(
            'theme' => Theme::getTheme(),
            'gallery'=> Gallery::first(),
            'Msg' =>''
        );

        if(Request::isMethod('post'))
        {

            $email = Input::get('email');
            $user = User::where('user_type','Candidate')->where('user_email',$email)->first();

            if(!empty($email))
            {
                if(count($user) && $user->user_email == $email)
                {
                    $data= array();
                    $data['token'] = $token = sha1($user->username.$user->user_email.time());

                    $expire_time = time()+(60*60*24);
                    $user->reset_token = $token;
                    $user->token_expire = $expire_time;
                    $user->save();
                    $data['user'] = $user;
                    $data['link'] = ((preg_match('/'.$_SERVER['SERVER_NAME'].'/',ASIAN_DOMAIN))? ASIAN_DOMAIN.'recruiter/reset-pass/'.$token : JOB_DOMAIN.'jobseeker/reset-pass/'.$token); 
                    // send email
                    Email::sendResetPasswordUrl($user,$data);
                    return Redirect::to('jobseeker/sent-reset-link');
                }else{

                    $viewModel['Msg'] = ' Sorry your email not matched';
                }
            }else{
                $viewModel['Msg'] = ' Sorry email required';
            }


        }else{
            $viewModel['Msg'] = '';
        }

        return Theme::make('forget-pass',$viewModel);
    }

    public function sentResetLink()
    {
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'gallery'=> Gallery::first(),
            'Msg' =>''
        );
        return Theme::make('sent-forget-pass-request',$viewModel);
    }

    public function resetPass()
    {
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'gallery'=> Gallery::first(),
            'Msg' =>''
        );
        $token = Request::segment(3);
        $user = User::where('user_type','Candidate')->where('reset_token',$token)->first();

        $viewModel['user'] = $user;
        $viewModel['Msg'] = '';
        if((!count($user)))
        {
            $viewModel['Msg'] = ' Sorry token expired. If you need reset your password then sent request again. <a href="'.url('jobseeker/forget-pass').'">Click Here</a>';

        }
        return Theme::make('reset-pass',$viewModel);
    }

    public function updatePassword()
    {
        if(Request::isMethod('post'))
        {
            $newPass = Input::get('newpass');
            $confirmPass = Input::get('confirmPass');

            $id = Input::get('id');
            $user = User::find($id);
            if($newPass == $confirmPass)
            {
                $user->password = md5($newPass);
                $user->reset_token = '';
                $user->token_expire = '';
                $user->save();
                $data = array('user' => $user);
                Email::passwordChanged($user,$data);
                
                Session::flash('message', 'Your password changed successfully');
                return Redirect::to('jobseeker/register-job-seeker');
            }else{
                Session::flash('message', 'Sorry! Your password not changed, try again later');
                return Redirect::to('jobseeker/reset-pass/'.$user->reset_token)->withInput();
            }
        }else{
            return Redirect::to('/');
        }
    }


}
?>