<?php
/**
 * Created by PhpStorm.
 * User: HimelC51
 * Date: 11/13/14
 * Time: 12:45 PM
 */

class InquiryController extends BaseController{

    protected $layout;
    protected $default_route;
    protected $_userSession;
    protected $pageLimit = 20;

    public function __construct() {

        $this->layout = Theme::getLayout();
        $this->default_route = 'inquiry/index';
        $this->_userSession = Authenticate::check();
        if(!empty($this->_userSession) && ($this->_userSession->remember_me))
        {
            $expireTime = (60*24*360);
            Config::set('session.lifetime',$expireTime);
        }

        $inbox = Inbox::where('receiver_id',$this->_userSession->id)->where('read_status',0)->orderBy('inbox_id','desc')->take(5)->get();

        View::share('messages',$inbox);

        $comment = Comment::where('receiver_id', $this->_userSession->id)->where('read_status', 0)->get();
        View::share('comments', $comment);

        if(!empty($this->_userSession->user_id))
            $jobCount = Jobs::where('created_by',$this->_userSession->user_id)->count();
        else
            $jobCount = 0;
        View::share('jobCount',$jobCount);

    }

    public function index()
    {
        return Redirect::to('inquiry/lists');
    }

    public function lists()
    {
        $permission = Permission::getPermission($this->_userSession->role_id,"Inquiry",'view');
                
        if($this->_userSession->role_id != 1)
        {

           if(!$permission)
           {
                Helpers::addMessage(500, " You don't have permission to access that page");
                return Redirect::to('dashboard');
           }
        }

        $viewModel = array(
          'theme' => Theme::getTheme(),
          'user'=>  $this->_userSession
        );

        if($this->_userSession->user_type == 'Employer')
        {
            $empId = $this->_userSession->user_id;
            $jobs       =  Jobs::where('created_by',$empId)->get();
            if(count($jobs) <= 0)
            {
                Helpers::addMessage(400,' You must create at least one job before make inquiry');
                return Redirect::to('dashboard');
            }
            $inqueries = Inquiry::with('InqueryRepliedResponse','InqueryRepliedAdminResponse','Candidate','Employer','Job')->where('emp_id',$empId)->orderBy('id','DESC')->get();
           
        }else if($this->_userSession->user_type == 'Admin')
        {
            $inqueries = Inquiry::with('Candidate','Employer','Job')->orderBy('id','DESC')->get();
        }else{
            $candidateId = $this->_userSession->user_id;
            $inqueries = Inquiry::with('Candidate','Employer','Job')->where(function($q){
                $q->where('status','=','Approved')->where('status','=','Replied','OR');
            })->where('candidate_id',$candidateId)->orderBy('id','DESC')->get();
        }//Helpers::debug($inqueries);

       // Helpers::debug($inqueries);die();
        $viewModel['inqueries'] = $inqueries;
        return Theme::make('inquiry.lists',$viewModel);
    }

    public function details()
    {
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user'=>  $this->_userSession
        );

        $permission = Permission::getPermission($this->_userSession->role_id,"Inquiry",'view');
        $editPermission = Permission::getPermission($this->_userSession->role_id,"Inquiry",'edit');
                
        if($this->_userSession->role_id != 1)
        {

           if(!$permission)
           {
                Helpers::addMessage(500, " You don't have permission to access that page");
                return Redirect::to('dashboard');
           }
        }

        $id = Request::segment(3);
        $inquiry = Inquiry::find($id);
        if(count($inquiry))
        {
            if(($this->_userSession->user_type == "Candidate") )
            {
                if(!in_array($inquiry->status,array('Approved','Replied'))){
                    Helpers::addMessage(400, "Access Denied");
                    return Redirect::to('inquiry/lists');
                }
                $viewModel['inquiryResponse'] = InqueryResponse::where('inquery_id',$inquiry->id)->where('replier_type','Candidate')
                                            ->where(function($q){
                                                $q->where('status','=','Approved')->where('status','=','Pending','or');
                                            })->where('replier_id',$this->_userSession->user_id)->count();
               
            }

            $employer   =  Employer::find($inquiry->emp_id);
            $viewModel['employer'] = $employer;
            $viewModel['inquiry'] = $inquiry;
            $viewModel['onlyApprovedResponse'] = count($inquiry->InqueryRepliedResponse);
            $viewModel['editPermission'] = $editPermission;

            return Theme::make('inquiry.details',$viewModel);
        }
        else{
            return Redirect::to('inquiry/lists');
        }

    }

    public function create()
    {
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user'=>  $this->_userSession
        );
        
        $permission = Permission::getPermission($this->_userSession->role_id,"Inquiry",'view');
                
        if($this->_userSession->role_id != 1)
        {

           if(!$permission)
           {
                Helpers::addMessage(500, " You don't have permission to access that page");
                return Redirect::to('dashboard');
           }
        }

        if($this->_userSession->user_type!='Employer')
        {
            return Redirect::to('dashboard');
        }

        $candidateId = Request::segment(3);
        $candidate  =  Candidate::find($candidateId);
        $employer   =  Employer::find($this->_userSession->user_id);

        if(Request::isMethod('post'))
            $viewModel['job'] = Input::get('job_id');

        $jobs       =  Jobs::where('created_by',$employer->emp_id)->get();
        if(count($jobs) <= 0)
        {
            Helpers::addMessage(400,' You must create at least one job before make inquiry');
            return Redirect::to('dashboard');
        }
        $myjobs = array();
        foreach($jobs as $job)
        {
            $myjobs[$job->jobs_id]['title'] = $job->job_title;
            $myjobs[$job->jobs_id]['job_id'] = $job->jobs_id;
            $description = "Responsiblities: ";
            $jobResponsibilities = json_decode($job->job_responsibility);
            $jobNature = $job->job_nature;


            $myjobs[$job->jobs_id]['description'] = implode("\n",$jobResponsibilities)."\n Job Nature:".$jobNature;

        }
        if(count($candidate))
        {
            $viewModel['employer'] = $employer;
            $viewModel['candidate'] = $candidate;
            $viewModel['jobs']      = json_encode($myjobs);
            return Theme::make('inquiry.create',$viewModel);
        }else{
            return Redirect::to('resume/display-resume/'.$candidateId);
        }

    }

    public function sendinquiry()
    {
        if(Request::isMethod('post'))
        {
            if($this->_userSession->user_type!='Employer')
            {
                return Redirect::to('dashboard');
            }
            $cid = Input::get('cid');
            $jobid = Input::get('joblist');
            $askSourceCode = (boolean)Input::get('source_code');
            $academicCertificate = (boolean)Input::get('academic_certificate');
            $job_description = Input::get('job_description');
            $message = Input::get('inquiry');
            if(preg_match('/((^[0][1-9]|\d+){6,14}|http:\/\/.+\.([a-z]{3,}|[a-z]{3,}\.[a-z]{2,3})|[a-z]+\.[a-z]{3,}|[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-z]{2,})/', $message))
                return Redirect::to('inquiry/create/'.$cid);
            $empId = $this->_userSession->user_id;
            $inquiry = Inquiry::firstOrNew(array('message'=>$message,'emp_id'=>$empId,'candidate_id'=>$cid,'job_id'=>$jobid));
            if(!$inquiry->exists)
            {
                $inquiry->description = $job_description;
                //$inquiry->message = $message;
                $inquiry->status = 'Pending';
                $inquiry->ask_source_code = $askSourceCode;
                $inquiry->ask_academic_certificate = $academicCertificate;
                $inquiry->save();
                Helpers::addMessage(200, ' Inquiry request send successfully');
            }else{
                Helpers::addMessage(400, ' Inquiry message was similar with old inquiry please change the message of inquriy');
            }
            return Redirect::to('inquiry/index');
        }
        else
        {
            return Redirect::to('dashboard');
        }
    }

    public function cancelReply()
    {
        if(Request::isMethod('post'))
        {
            $id = Input::get('ir_id');
            $comment = trim(Input::get('comment'));

            $inquiryResponse = InqueryResponse::find($id);
            if(count($inquiryResponse))
            {

                if($comment == "")
                {
                    Helpers::addMessage(400, "Sorry! you have to give a reason");
                    
                    return Redirect::to('inquiry/details/'.$inquiryResponse->inquery_id);
                }
                
                $inquiryResponse->inquery_reason = Input::get('comment');
                $inquiryResponse->status = "Canceled";
                $inquiryResponse->cancel_by = $this->_userSession->username;
                $inquiryResponse->save();
                return Redirect::to('inquiry/details/'.$inquiryResponse->inquery_id);
            }else{
                Helpers::addMessage(400, "Sorry! No reply found to cancel");

                return Redirect::to('inquiry/details/'.$inquiryResponse->inquery_id);
            }

        }else{
            return Redirect::to('dashboard');
        }
    }

    public function approveReply()
    {
        if(Request::isMethod('post'))
        {
            $id = Input::get('id');
            $inquiryResponse = InqueryResponse::find($id);
            $inquiryResponse->status = "Approved";
            $inquiryResponse->save();
            $inquiry = Inquiry::find($inquiryResponse->inquery_id);
            
            $inquiry->status = "Replied";
            $inquiry->save();
            echo 1;
            exit();

        }else{
            return Redirect::to('dashboard');
        }
        
    }

    public function get_inquiry_by_date()
    {
        $inqueries = null;
        if(Request::isMethod('post'))
        {
            $createdDate = Input::get('date');

            if($this->_userSession->user_type == 'Employer')
            {
                $empId = $this->_userSession->user_id;
              
                
                $inqueries = Inquiry::with('InqueryRepliedResponse','InqueryRepliedAdminResponse','Candidate','Employer','Job')->where('emp_id',$empId)->where('created_at',$createdDate)->orderBy('id','DESC')->get();
               
            }else if($this->_userSession->user_type == 'Admin')
            {
                $inqueries = Inquiry::with('Candidate','Employer','Job')->where('created_at',$createdDate)->orderBy('id','DESC')->get();
            }else{
                $candidateId = $this->_userSession->user_id;
                $inqueries = Inquiry::with('Candidate','Employer','Job')->where(function($q){
                    $q->where('status','=','Approved')->where('status','=','Replied','OR');
                })->where('candidate_id',$candidateId)->where('created_at',$createdDate)->orderBy('id','DESC')->get();
            }
        }
        
        return $inqueries;
        exit();
    }


} 