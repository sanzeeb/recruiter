<?php

class InterviewController extends BaseController {
    /*
      |--------------------------------------------------------------------------
      | Default Home Controller
      |--------------------------------------------------------------------------
      |
      | You may wish to use controllers instead of, or in addition to, Closure
      | based routes. That's great! Here is an example controller method to
      | get you started. To route to this controller, just add the route:
      |
      |	Route::get('/', 'HomeController@login');
      |
     */

    protected $layout;
    /**
     * for set default route
     * @var string
     */
    protected $default_route;
    /**
     * for set user session data
     * @var string
     */
    protected $_userSession;

    //protected $pageLimit;

    public function __construct() {

        $this->layout = Theme::getLayout();
        $this->default_route = 'activities/index';
        $this->pageLimit = 20;
        $this->_userSession = Authenticate::check();  // check is user logged in
        $inbox = Inbox::where('receiver_id',$this->_userSession->id)->where('read_status',0)->get();
        View::share('messages',$inbox);
        
                             $comment = Comment::where('receiver_id', $this->_userSession->id)->where('read_status', 0)->get();
        View::share('comments', $comment);
        
    }

    public function getScheduleTime() {
        if (Request::ajax()) {
            $id = Input::get("id");
            $schedule = Schedule::where('avail_id', '=', $id)->first();

            return $schedule;
        }
    }

    public function inviteForInterview() {

        if (Request::ajax()) {

            
            $candidateArray = Input::get("candidates");

            $recruiter = ($this->_userSession->user_type=="Admin")? Input::get('recruiter') : $this->_userSession->user_id;
            $job = Input::get('joblist');
            $purpose = Input::get('purpose');
            //$candidateArray = explode(",", $candidates);
            $interviewRequest = new InterviewRequest();
            $interviewRequest->emp_id = $recruiter;
            $interviewRequest->job_id = $job;
            $interviewRequest->purpose = $purpose;
            $interviewRequest->save();
            if($interviewRequest->id)
            {
                foreach($candidateArray as $i => $candiateId) {

                    $approve = New Aproval();
                    $approve->candidate_id = $candiateId;
                    $approve->employer_id = $recruiter;
                    $approve->status = 0;
                    $approve->interview_request_id = $interviewRequest->id;
                    $approve->save();
                }
                return array('status'=>200,'msg'=>' Your request accepted');
            }
            
        }else{
            Helpers::addMessage(500," Bad Request");
            return Redirect::to('interview/manage-interview');
        }
        
    }

    public function requestedInterviewers() {
        $permission = Authenticate::getPermission($this->_userSession);
        if (!$permission->create) {
            Helpers::addMessage(500, " You don't have permission to access that page");
            return Redirect::to('dashboard');
        }
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession
        );

        $viewModel["requesteds"] = InterviewRequest::OrderBy('id','desc')->paginate(20);

        return Theme::make('interview.requested_candidate', $viewModel);
    }
    
    
    public function candidateReqPdf()
    {
        $permission = Authenticate::getPermission($this->_userSession);
        if (!$permission->read) {
            Helpers::addMessage(500, " You don't have permission to access that page");
            return Redirect::to('dashboard');
        }
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user'  => $this->_userSession
        );
        
        $id =Input::get("emp_id");
        $interviewRequest = InterviewRequest::find($id);

        if(count($interviewRequest))
        {
            $employer = Employer::find($interviewRequest->emp_id);
            $requests=Aproval::where('interview_request_id',$interviewRequest->id)->get();
             
                $cvPdf = new RequestedlistPdf();
                
                $cvPdf->SetAutoPageBreak('auto', 30);
                $cvPdf->SetFont("Arial", "B", 10);
                $cvPdf->AddPage();
                $cvPdf->setHeader($employer, Theme::getTheme());
                $cvPdf->requestedCandidate($requests);
                
                $cvPdf->Output();
        }else{
            return Redirect::to('interview/requested-interviewers');
        }
        
        return Theme::make('interview.details',$viewModel);
    }
    
    
     public function candidateConPdf()
    {
        $permission = Authenticate::getPermission($this->_userSession);
        if (!$permission->read) {
            Helpers::addMessage(500, " You don't have permission to access that page");
            return Redirect::to('dashboard');
        }
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user'  => $this->_userSession
        );
        
        $id =Input::get("emp_id");
        $interviewRequest = InterviewRequest::find($id);

        if(count($interviewRequest))
        {
            $employer = Employer::find($interviewRequest->emp_id);
            $requests=Aproval::where('interview_request_id',$interviewRequest->id)->where('status', '=', 1)->get();
             
                $cvPdf = new RequestedlistPdf();
                
                $cvPdf->SetAutoPageBreak('auto', 30);
                $cvPdf->SetFont("Arial", "B", 10);
                $cvPdf->AddPage();
                $cvPdf->setHeader($employer, Theme::getTheme());
                $cvPdf->conCandidate($requests);
                
                $cvPdf->Output();
        }else{
            return Redirect::to('interview/requested-interviewers');
        }
        
        return Theme::make('interview.details',$viewModel);
    }
    
    public function details()
    {
        $permission = Authenticate::getPermission($this->_userSession);
        if (!$permission->read) {
            Helpers::addMessage(500, " You don't have permission to access that page");
            return Redirect::to('dashboard');
        }
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user'  => $this->_userSession
        );
        
        $id = Request::segment(3);
        $interviewRequest = InterviewRequest::find($id);

        if(count($interviewRequest))
        {
            $employer = Employer::find($interviewRequest->emp_id);

            $viewModel['employer'] = $employer;
            $viewModel['emp_id'] = $id;
            $job = Jobs::find($interviewRequest->job_id);
            $viewModel['jobs']  = (!empty($job))? $job->job_title : $interviewRequest->purpose;
            $viewModel['requests'] = Aproval::where('interview_request_id',$interviewRequest->id)->get();
            
        }else{
            return Redirect::to('interview/requested-interviewers');
        }
        
        return Theme::make('interview.details',$viewModel);
    }

    public function sendNotification()
    {
        if(Request::isMethod('post'))
        {
            Helpers::debug($_POST);

            $time = Input::get('time');
            $recruiter_time = Input::get('recruiter_time');
            $recruiter_id   = Input::get('recruiter_id');
            $candidate_id   = Input::get('candidate_id');
            if(count($time))
            {
                foreach($time as $i=> $t)
                {

                    $userCand = User::find($candidate_id[$i]);


                    $candidate = Candidate::where('candidate_id',$userCand->user_id)->first();

                    $inbox = new Inbox();
                    $inbox->subject     = "Interview Invitation";
                    $inbox->sender_id   = $this->_userSession->id;
                    $inbox->receiver_id = $candidate_id[$i];
                    $message = "$candidate->firstname You have a job interview invitation at your following available Time.";
                    $message .= "<p>".$time[$i]."</p>";
                    $inbox->body = $message;
                    $inbox->save();
                    $approval = Aproval::find($i);
                    Aproval::where('aprov_id',$approval->aprov_id)->update(array('status'=>1));
                    $interviewRequest = $approval->InterviewRequest;
                    InterviewRequest::where('id',$interviewRequest->id)->update(array('status'=>1));
                }
                foreach($recruiter_time as $i => $rt)
                {
                    
                    $userCand = User::find($candidate_id[$i]);
                    $userEmp = User::find($recruiter_id[$i]);
                    $candidate = Candidate::where('candidate_id',$userCand->user_id)->first();
                    $employer = Employer::where('emp_id',$userEmp->user_id)->first();
                    $inbox = new Inbox();
                    $inbox->subject     = "Interview Request Accepted";
                    $inbox->sender_id   = $this->_userSession->id;
                    $inbox->receiver_id = $recruiter_id[$i];
                    $message = "You have a job interview with $candidate->firstname at your following available Time.";
                    $message .= "<p>".$rt."</p>";
                    $inbox->body = $message;
                    $inbox->save();
                }


            }

            Helpers::addMessage(200,' Request approved and notification send');
            return Redirect::to('interview/requested-interviewers');
        }else{
            Helpers::addMessage(500, " Bad Request");
            return Redirect::to('interview/requested-interviewers');
        }
    }

    public function manageInterview() {

        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession,
            'recruiters' => RecruiterBLL::getRecruiters()
        );
        $jobs = '';
        if ($this->_userSession->user_type == 'Employer') {
            $jobs = JobModel::getJobs($this->_userSession);
            $recruiter = Employer::find($this->_userSession->user_id);
            $viewModel['recruiter'] = $recruiter;
        } else {
            $jobs = JobModel::getJobs($this->_userSession);
        }
        $viewModel['jobs'] = $jobs;

        $id = Session::get("id");
        $viewModel["skills"] = Skills::all();
        $viewModel["titles"] = Schedule::where('user_id', $id)->get();

        return Theme::make('interview.interview-list', $viewModel);
    }

    public function getAvailableCandidates() 
    {
        
        $collectiionObjects = array();
        if (Request::isMethod("post")) {

            $schedule_title = Input::get("schedule_title");

            if ($schedule_title != "0") {

                $available_date = Input::get("available_date");
                $from_time = Input::get("availtime");
                $to_time = Input::get("availtimeto");
                $checkMySchedule = Input::get('checkMySchedule');
                $fromYear = Input::get('fromYear');
                $toYear = Input::get('toYear');

                /* $dt_from=  strtotime(date("Y-m-d"). $from_time);
                  $dt_to=  strtotime(date("Y-m-d"). $to_time);
                 * 
                 */
                $dt_from = strtotime($available_date . $from_time);
                $dt_to = strtotime($available_date . $to_time);

                $dt = Helpers::dateTimeFormat("Y-m-d", $available_date);

                $sql = "SELECT * FROM schedule 

                         LEFT JOIN users
                            ON users.id = schedule.user_id
                         LEFT JOIN candidate
                            ON candidate.candidate_id = users.user_id
                         LEFT JOIN cv_tbl
                            ON cv_tbl.cv_tbl_id = candidate.cv_tbl_id


                        WHERE
                            (cv_tbl.year_of_exp >= $fromYear AND cv_tbl.year_of_exp < $toYear) ";

                if ($checkMySchedule) {
                    $sql .=" AND ( (timestamp_from>'$dt_from' AND timestamp_to<'$dt_to') OR
                                 (timestamp_from <'$dt_from' AND timestamp_to < '$dt_to' AND timestamp_to > '$dt_from'  )  OR
                                 (timestamp_from >'$dt_from' AND timestamp_to > '$dt_to' AND timestamp_from < '$dt_to') OR
                                 (timestamp_from <'$dt_from' AND timestamp_to > '$dt_to')) AND available_date ='$dt' ";
                }

                $schedules = DB::select(DB::raw($sql));

                foreach ($schedules as $sch) {
                    $obj = Schedule::find($sch->avail_id);
                    if ($obj->User->user_type == 'Candidate') {

                        $candidateObj = Candidate::find($obj->User->user_id);
                        $obj->candidateObj = $candidateObj->getAttributes();
                        $obj->cv_tbl = $candidateObj->CvTable->getAttributes();
                        
                        
                        //$obj->candidateObj
                        $collectiionObjects[] = $obj->getAttributes();
                    }
                }
            } else {
               return array('status'=>400,'msg'=>"Select schedule first than try again");
            }
            
            return (!empty($collectiionObjects))? array('status'=>200,'data'=>$collectiionObjects) : array();
        }else{
            Helpers::addMessage(500, " Bad Request");
            return Redirect::to('interview/manage-interview');
        }
    }

}
