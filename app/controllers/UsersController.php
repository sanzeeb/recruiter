<?php

class UsersController extends BaseController {
    /*
      |--------------------------------------------------------------------------
      | Default Home Controller
      |--------------------------------------------------------------------------
      |
      | You may wish to use controllers instead of, or in addition to, Closure
      | based routes. That's great! Here is an example controller method to
      | get you started. To route to this controller, just add the route:
      |
      |	Route::get('/', 'HomeController@login');
      |
     */

    protected $layout;

    /**
     * for set default route
     * @var string
     */
    protected $default_route;

    /**
     * for set user session data
     * @var string
     */
    protected $_userSession;

    //protected $pageLimit;

    public function __construct() {

        $this->layout = Theme::getLayout();
        $this->default_route = 'activities/index';
        $this->pageLimit = 20;
        $this->_userSession = Authenticate::check();  // check is user logged in
        if(!empty($this->_userSession) && ($this->_userSession->remember_me))
        {
            $expireTime = (60*24*360);
            Config::set('session.lifetime',$expireTime);
        }
        $inbox = Inbox::where('receiver_id',$this->_userSession->id)->where('read_status',0)->orderBy('inbox_id','desc')->take(5)->get();
        View::share('messages',$inbox);
        
                             $comment = Comment::where('receiver_id', $this->_userSession->id)->where('read_status', 0)->get();
        View::share('comments', $comment);
        
        if(!empty($this->_userSession->user_id))
            $jobCount = Jobs::where('created_by',$this->_userSession->user_id)->count();
        else
            $jobCount = 0;
        View::share('jobCount',$jobCount);
    }

    public function index()
    {
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user'=>  $this->_userSession
        );

       /* $permission = Permission::getPermission($this->_userSession->role_id,'User','view');
        if(!$permission)
         {
            Helpers::addMessage(400, 'Access Denied');
            return Redirect::to('dashboard');
         }  */
       
        $keyword = Request::get('key');

        $userObj = User::with('Role')->where('user_type','Admin')->where('created_by',$this->_userSession->id);
        
        $userObj->where(function($query)use ($keyword){
            $query->where('username','like',$keyword.'%','and');
            $query->where('user_email','like',$keyword.'%','or');


        });


        $users = $userObj->get();
        $viewModel['edit_permission']   = Permission::getPermission($this->_userSession->role_id,'User','edit');
        $viewModel['delete_permission'] = Permission::getPermission($this->_userSession->role_id,'User','delete');
        
        $viewModel["users"] = $users;
        return Theme::make('users.list', $viewModel);

    }

    public function checkUsernameExist()
    {
        if(Request::isMethod('post'))
        {
           $username = Input::get('username');
           echo $user = User::where('username',$username)->count();
           exit();
        }
    }

    public function changePassword(){
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user'=>  $this->_userSession
        );

        if (Request::isMethod("post")) {

            $msg = UserBLL::userChangePassword();
            $viewModel["msg"] = $msg;

        }
        return Theme::make('users.change-password', $viewModel);
    }

    public function deleteUser() {


        if (Request::ajax()) {

            $permission = Permission::getPermission($this->_userSession->role_id,'User','delete');
            if($this->_userSession->role_id != 1)
            {
                if (!$permission) {
                    return "500";
                }
            }
            $id = Input::get("id");
            $user = User::find($id);

            $user_id = $user->user_id;


            $msg = User::where('id', $id)->delete();
            $msg2 = Candidate::where('candidate_id', $user_id)->delete();

            return $msg;
        }
    }
    
    public function createUser() {

        
        $permission = Permission::getPermission($this->_userSession->role_id,'User','view');
        if($this->_userSession->role_id != 1)
        {
            if (!$permission) {
                Helpers::addMessage(500, " You don't have permission to access that page");
                return Redirect::to('dashboard');
            }
        }
        
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user'=>  $this->_userSession,
            'roles' => Role::all()
        );

        if (Request::isMethod("post")) {
            $userType = Input::get('user_type');
           
            $msg =UserBLL::userRegister();
            
            
            if($msg=="1"){
            $viewModel["msg"]="Account created successfully";     
            }
            if($msg=="2"){
            $viewModel["msg"]="Email already exist ";     
             return Theme::make('users.create', $viewModel);
            }
            if($msg=="3"){
            $viewModel["msg"]="User already exist by this user name"; 
             return Theme::make('users.create', $viewModel);
            }
            if($msg=="4"){
            $viewModel["msg"]="Please enter all required field"; 
             return Theme::make('users.create', $viewModel);
            }
            if($msg=="5"){
            $viewModel["msg"]="Password does not match"; 
             return Theme::make('users.create', $viewModel);
            
            }
            
            if($userType == 'Employer')
                return Redirect::to('users/viewEmployerUser');
            elseif($userType == "Candidate")
                return Redirect::to('users/viewUser');
            else
                return Redirect::to('users/index');
        }

        return Theme::make('users.create', $viewModel);
    }

    public function editUser() {

        $permission = Permission::getPermission($this->_userSession->role_id,'User','edit');
        
        if($this->_userSession->role_id != 1)
        {
            if (!$permission) {
                Helpers::addMessage(500, " You don't have permission to access that page");
                return Redirect::to('dashboard');
            }
        }

        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user'=>  $this->_userSession,
            'roles' => Role::all()
        );
          $id=   Request::segment(3);
          if (Request::isMethod("post")) {

              
            $msg = UserBLL::userEdit();
            if($msg)
                return Redirect::to('users/index');
            $viewModel["msg"] = $msg;
        }
        $users = User::where('id', $id)->first();
        $viewModel["users"] = $users;
        
        return Theme::make('users.edit', $viewModel);
        
        
    }

    public function viewEmployerUser() {

        $permission = Permission::getPermission($this->_userSession->role_id,'User','view');
        if($this->_userSession->role_id != 1)
        {
            if (!$permission) {
                Helpers::addMessage(500, " You don't have permission to access that page");
                return Redirect::to('dashboard');
            }
        }
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user'=>  $this->_userSession
        );

        $keyword = Request::get('key');
        $userObj = User::with('Employer','Role')->where('user_type','Employer');
        $userObj = $userObj->join('employer','employer.emp_id','=','users.user_id');
        if(!empty($keyword))
        { 
            $userObj->where(function($query)use ($keyword){
                $query->where('username','like',$keyword.'%','and');
                $query->where('user_email','like',$keyword.'%','or');


            })->where('employer.title','like',"%".$keyword."%",'or');

            $users = $userObj->take(100)->orderBy('id','DESC')->get();
        }else{
            $users =  DB::table('users')->join('employer','employer.emp_id','=','users.user_id')->get();
        }
        $viewModel['edit_permission']   = Permission::getPermission($this->_userSession->role_id,'User','edit');
        $viewModel['delete_permission'] = Permission::getPermission($this->_userSession->role_id,'User','delete');
        $viewModel["users"] = $users;
        return Theme::make('users.employer-list', $viewModel);
    }

    public function viewUser() {

        
        $permission = Permission::getPermission($this->_userSession->role_id,'User','view');
        if($this->_userSession->role_id != 1)
        {
            if (!$permission) {
                Helpers::addMessage(500, " You don't have permission to access that page");
                return Redirect::to('dashboard');
            }
        }

        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user'=>  $this->_userSession
        );

        $keyword = trim(Request::get('key'));
        $userObj = User::with('Candidate','Employer','Role')->where('user_type','Candidate');
        $userObj = $userObj->join('candidate','candidate.candidate_id','=','users.user_id');
        if(!empty($keyword))
        {
            $userObj->where(function($query)use ($keyword){
            $query->where('username','like',$keyword.'%','and');
            $query->where('user_email','like',$keyword.'%','or');


        })->where('candidate.firstname','like',"%".$keyword."%",'or');
        $userObj= $userObj->take(1000);
        $users = $userObj->orderBy('id','DESC')->get();
        }else{
          $users =  DB::table('users')->join('candidate','candidate.candidate_id','=','users.user_id')->get();
        }

        
        $viewModel['edit_permission']   = Permission::getPermission($this->_userSession->role_id,'User','edit');
        $viewModel['delete_permission'] = Permission::getPermission($this->_userSession->role_id,'User','delete');
        $viewModel["users"] = $users;
        return Theme::make('users.candidate-list', $viewModel);
    }

    public function role()
    {
        $permission = Authenticate::getPermission($this->_userSession);
        if (!$permission->read) {
            Helpers::addMessage(500, " You don't have permission to access that page");
            return Redirect::to('dashboard');
        }
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user'  => $this->_userSession,
            'modules' => Modules::all()
        );
        return Theme::make('users.role',$viewModel);
    }

    public function updateRole()
    {
        if(Request::ajax())
        {
           $type = Input::get('request-type');
           $moduleId = Input::get('module_id');
           $field = Input::get('field');
           $userType = Input::get('user_type');

           $userRole = UserRole::firstOrNew(array('user_type'=>$userType,'module_id'=>$moduleId));

           if(!$userRole->exists)
           {

               if($type == 'add')
                   $userRole->$field = 1;
               else
                   $userRole->$field = 0;

               $userRole->save();
           }else{
               $updateData = array();

               if($type == 'add')
                   $updateData[$field] = 1;
               else
                   $updateData[$field] = 0;
               UserRole::where('user_type',$userType)->where('module_id',$moduleId)->update($updateData);
           }

        }
        die();
    }

    public function getRole()
    {
        if(Request::ajax())
        {
            $user_type = Input::get('user_type');
            $userRoles = UserRole::where('user_type',$user_type)->get();
            $modules = Modules::all();
            if(!empty($modules))
            {
                foreach($modules as $i=> $module)
                {
                    foreach($userRoles as $userRole){
                        if($module->module_id == $userRole->module_id)
                        {
                            $modules[$i]['role'] = $userRole->getAttributes();
                        }

                    }

                }
            }
            return $modules;
        }
        return array();
    }

}
