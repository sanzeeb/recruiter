<?php

class PaymentController extends BaseController {

    protected $layout;

    /**
     * for set default route
     * @var string
     */
    protected $default_route;

    /**
     * for set user session data
     * @var string
     */
    protected $_userSession;

    //protected $pageLimit;

    public function __construct() {

        $this->layout = Theme::getLayout();
        $this->default_route = 'payment/index';
        $this->pageLimit = 20;
        $this->_userSession = Authenticate::check();  // check is user logged in
        if(!empty($this->_userSession) && ($this->_userSession->remember_me))
        {
            $expireTime = (60*24*360);
            Config::set('session.lifetime',$expireTime);
        }
        $inbox = Inbox::where('receiver_id', $this->_userSession->id)->where('read_status', 0)->orderBy('inbox_id','desc')->take(5)->get();
        View::share('messages', $inbox);
        
                             $comment = Comment::where('receiver_id', $this->_userSession->id)->where('read_status', 0)->get();
        View::share('comments', $comment);

        if(!empty($this->_userSession->user_id))
            $jobCount = Jobs::where('created_by',$this->_userSession->user_id)->count();
        else
            $jobCount = 0;
        View::share('jobCount',$jobCount);
    }

    public function index() {
        return Redirect::to('agreement/lists');
    }

    public function paynow() {

        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession,
        );
        $agreementId = Request::segment(3);
        $agreement = Agreements::find($agreementId);
        if (count($agreement)) {

            $viewModel['agreement'] = $agreement;

            $viewModel['payments'] = $agreement->Payments;
            return Theme::make('payment.index', $viewModel);
        } else {
            Helpers::addMessage(400, " Agreement not found");
            return Redirect::to('agreement/lists');
        }
    }

    public function approve() {
        if (Request::isMethod('post')) {

            $id = Input::get('agreement_id');
            $agreementAmount = Input::get('amountToPay');
            $agreement = Agreements::find($id);

            if (!is_numeric($agreementAmount)) {
                Helpers::addMessage(400, " Amount must be in number");
                return Redirect::to('payment/paynow/' . $id)->withInput();
            }
            if (count($agreement) && $agreement->status == 'Accepted') {

                //$agreementAmount = $agreement->amount;
                $agreementTitle = $agreement->agreement_title;
                $sdkConfig = array(
                    "mode" => "sandbox"
                );
                $clientId = "AVSQFOjk4bHR6taw9yhTxtz17TJty8CUWsIBKhYNXm9UsdSkmJe-OqUz8YSLoJ0-KdKZELYxXG5nie5s";
                $secretCode = "EHBD38w84BNcQ8qlz5aefRs95X25Xw3OC0jYh8i7lVYy3VhP2LLdjHmZe0doYekI8Q8JKIvVFmZ-obeg";
                $cred = new OAuthTokenCredential($clientId, $secretCode, $sdkConfig);
                $accessToken = $cred->getAccessToken($sdkConfig);


                $apiContext = new ApiContext($cred, 'Request' . time());
                $apiContext->setConfig($sdkConfig);

                $payer = new Payer();
                $payer->setPaymentMethod("paypal");

                $amount = new Amount();
                $amount->setCurrency("USD");
                $amount->setTotal($agreementAmount);

                $item = new Item();
                $item->setQuantity(1);
                $item->setName("$agreementTitle");
                $item->setPrice($agreementAmount);
                $item->setCurrency("USD");

                $itemList = new ItemList();
                $itemList->setItems(array($item));

                $transaction = new Transaction();
                $transaction->setDescription("$agreementTitle");

                $transaction->setAmount($amount);
                $transaction->setItemList($itemList);



                $redirectUrls = new RedirectUrls();
                $redirectUrls->setReturnUrl(ASIAN_DOMAIN.'payment/success/' . '?AgreementId=' . $agreement->agreement_id);
                $redirectUrls->setCancelUrl(ASIAN_DOMAIN.'payment/cancel/' . '?AgreementId=' . $agreement->agreement_id);
                
                
                
                $payment = new Payment();
                $payment->setIntent("sale");
                $payment->setPayer($payer);
                $payment->setRedirectUrls($redirectUrls);
                $payment->setTransactions(array($transaction));
                
                $response = $payment->create($apiContext);
                

                //Helpers::debug($payment);die();
                $linkObj = '';


                foreach ($payment->getLinks() as $link) {
                    if ($link->rel == 'approval_url') {
                        $linkObj = $link;
                        break;
                    }
                }
                Session::put('paymentid', $payment->id);

                return Redirect::to($linkObj->href);


                exit(1);
            } else {
                Helpers::addMessage(400, " Agreement not found or not accepted by Candidate");
                return Redirect::to('agreement/lists');
            }
        } else {
            Helpers::addMessage(500, " Bad Request");
            return Redirect::to('agreement/lists');
        }
    }

    public function success() {

        $sdkConfig = array(
            "mode" => "sandbox"
        );
        $clientId = "AWmkcBD3ZI_nzHw7an0nSfeCNiCBjks7t93F6HOhPRxB4jv8iFqTg_18HGNZ";
        $secretCode = "ENwr9xDBjY52u3aZAzeXZdYGhzMt5KeXPT7Ib7zGtYnlo77XPJkaY8QjHur5";

        $cred = new OAuthTokenCredential($clientId, $secretCode, $sdkConfig);

        $apiContext = new ApiContext($cred);
        $apiContext->setConfig($sdkConfig);
        $payerId = Request::get('PayerID');
        $paymentId = session::get('paymentid');
        $agreementId = Request::get('AgreementId');
        $agreement = Agreements::find($agreementId);

        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession
        );

        if (!empty($payerId) && !empty($paymentId) && !empty($agreement->agreement_id)) {
            $payment = Payment::get($paymentId, $apiContext);
            $execution = new PaymentExecution();
            $execution->setPayerId($payerId);
            try {

                $payment->execute($execution, $apiContext);


                $transactions = $payment->getTransactions();
                $title = '';
                $amount = '';
                if (count($transactions)) {
                    foreach ($transactions as $transaction) {
                        $title = $transaction->getDescription();
                        $amount = ($transaction->amount->getTotal());
                    }
                }

                $agreementPayment = new Payments();
                $agreementPayment->agreement_id = $agreement->agreement_id;
                $agreementPayment->sender_id = $agreement->sender_id;
                $agreementPayment->receiver_id = $agreement->receiver_id;
                $agreementPayment->amount = $amount;
                $agreementPayment->status = 'Pending';
                $agreementPayment->save();

                // put here a notification to candidate that employer made a payment and djit also

                //for candidate inbox
                $inbox = new Inbox();
                $inbox->subject = "Payment sent successfully";
                $inbox->sender_id = $agreement->sender_id;
                $inbox->receiver_id = $agreement->receiver_id;
                $inbox->in_type = "payment";
                $message = "<h5>Title:" . $agreement->title . "</h5>";
                $message .= "<h6>Payment amount:" . $amount . "</h6>";
                $message .= "<p>Payment made by &nbsp;".$agreement->SenderUserAcc->Employer->title."</p>";
                $message .="<p><a class='btn btn-info' href='".url('agreement/details/'.$agreement->agreement_id)."'>view details</a></p>";
             
                $inbox->body = $message;
                $inbox->save();
                
                //Helpers::debug($inbox);
               // die();
                
                
                //notify to admin  inbox
                $inbox2 = new Inbox();
                $inbox2->subject = "Payment sent successfully";
                $inbox2->sender_id =$agreement->receiver_id;
                $inbox2->receiver_id = 1;
                $inbox2->in_type = "payment";
                $message = "<h5>Title:" . $agreement->title . "</h5>";
                $message .= "<h6>Payment amount:" . $amount . "</h6>";
                $message .= "<p>Payment made by &nbsp;".$agreement->SenderUserAcc->Employer->title."</p>";
                $message .="<p><a class='btn btn-info' href='".url('agreement/details/'.$agreement->agreement_id)."'>view details</a></p>";
                $inbox2->body = $message;
                $inbox2->save();
                
                
                


                $viewModel['agreement'] = $agreement;
                $viewModel['title'] = $title;
                $viewModel['amount'] = $amount;
                return Theme::make('payment.payment-success', $viewModel);
            } Catch (Exception $ex) {
                return Theme::make('payment.payment-expired', $viewModel);
            }
        }
    }
    
    public function paymentApprove(){
        
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession
        );
        $viewModel["payments"]=  Payments::paginate(20);
                        return Theme::make('payment.payment-approve', $viewModel);
        
    }

    public function paidCancel(){
        
        if (Request::ajax()) {
            $user = $this->_userSession;
            $sender_id = $user->id;
            $user_id = $user->user_id;
            $user_type = $user->user_type;
            $dataId = Input::get('dataId');
            $id = AgreementBLL::cancelPaid('Canceled',$dataId);

            return $id;
        }
    }
    
    public function paidAmount(){
        if (Request::ajax()) {
            $user = $this->_userSession;
            $sender_id = $user->id;
            $user_id = $user->user_id;
            $user_type = $user->user_type;
            $dataId = Input::get('dataId');
            $id = AgreementBLL::paymentPaid('Paid', $dataId);

            return $id;
        }
        
    }

        public function cancel() {
        Helpers::addMessage(400, ' Your request for payment was canceled, please try again later');
        return Redirect::to('agreement/lists');
    }

}