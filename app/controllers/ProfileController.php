<?php

class ProfileController extends BaseController {
    /*
      |--------------------------------------------------------------------------
      | Default Home Controller
      |--------------------------------------------------------------------------
      |
      | You may wish to use controllers instead of, or in addition to, Closure
      | based routes. That's great! Here is an example controller method to
      | get you started. To route to this controller, just add the route:
      |
      |	Route::get('/', 'HomeController@login');
      |
     */

    protected $layout;

    /**
     * for set default route
     * @var string
     */
    protected $default_route;

    /**
     * for set user session data
     * @var string
     */
    protected $_userSession;

    //protected $pageLimit;

    public function __construct() {

        $this->layout = Theme::getLayout();
        $this->default_route = 'activities/index';
        $this->pageLimit = 20;
        $this->_userSession = Authenticate::check();  // check is user logged in
        if(!empty($this->_userSession) && ($this->_userSession->remember_me))
        {
            $expireTime = (60*24*360);
            Config::set('session.lifetime',$expireTime);
        }
        $inbox = Inbox::where('receiver_id',$this->_userSession->id)->where('read_status',0)->orderBy('inbox_id','desc')->take(5)->get();
        View::share('messages',$inbox);
                             $comment = Comment::where('receiver_id', $this->_userSession->id)->where('read_status', 0)->get();
        View::share('comments', $comment);

        if(!empty($this->_userSession->user_id))
            $jobCount = Jobs::where('created_by',$this->_userSession->user_id)->count();
        else
            $jobCount = 0;
        View::share('jobCount',$jobCount);
    }

    public function index() {

        $id = Request::segment(3);

        $cvSpecialization = DB::table('cv_specialization')
            ->select('key')
            ->groupBy('key')->get();
        $specs = array();

        //Helpers::debug($cvSpecialization);die();
        if (!empty($cvSpecialization)) {
            foreach ($cvSpecialization as $cSpec) {
                $specs[$cSpec->key] = $cSpec->key;
            }
        }

        $skills = Skills::all();

        foreach($skills as $skill)
        {
            $key = trim(strtolower($skill->skill_name));

            if(!array_key_exists($key, $specs))
            {
                $specs[$key] = $key;
            }
        }
        
        sort($specs);
       // Helpers::debug($this->_userSession);die();
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession,
            'specs' => $specs,
            'genders' => array('Male', 'Female'),
            'marital_status' => array('Married', 'Unmarried')
        );

        if ($this->_userSession->user_type == "Candidate" || $this->_userSession->user_type == "Admin") {


            $cvTblId = Request::segment(3);
            $candidate = Candidate::where('cv_tbl_id', $cvTblId)->first();
            if (count($candidate)) {
                $cvTblObj = $candidate->CvTable;
                $cvJob = $cvTblObj->CvJob;
                $cvEdu = $cvTblObj->CvEdu;
                $cvQualification = $cvTblObj->CvQualification;
                $cvSpecialization = $cvTblObj->CvSpecialization;

                $viewModel['candidate'] = $candidate;
                $viewModel['cvInfo'] = $cvTblObj;
                $viewModel['cvJob'] = $cvJob;
                $viewModel['cvEdu'] = $cvEdu;
                $viewModel['cvQ'] = $cvQualification;
                $specs = array();
                if (!empty($cvSpecialization)) {
                    foreach ($cvSpecialization as $cSpec) {
                        $specs[$cSpec->key] = $cSpec;
                    }
                }
                
                $viewModel['cvSpecs'] = $specs;
                return Theme::make('profiles.candidate', $viewModel);
            }

        } else if ($this->_userSession->user_type == "Employer" || $this->_userSession->user_type == "Admin") {

            $id = Request::segment(3);
          
        $users = User::where('id', '=', $id)->first();
        $viewModel["users"] = $users;
            return Theme::make('profiles.employer-view', $viewModel);
        }
    }

    public function edit($id)
    {
         $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession,
            
            'genders' => array('Male', 'Female'),
            'marital_status' => array('Married', 'Unmarried')
        );
          
        $users = User::where('id', '=', $id)->first();
        $viewModel["users"] = $users;
        return Theme::make('profiles.employer', $viewModel);
    }

}
