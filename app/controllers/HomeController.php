<?php

class HomeController extends BaseController {
    /*
      |--------------------------------------------------------------------------
      | Default Home Controller
      |--------------------------------------------------------------------------
      |
      | You may wish to use controllers instead of, or in addition to, Closure
      | based routes. That's great! Here is an example controller method to
      | get you started. To route to this controller, just add the route:
      |
      |	Route::get('/', 'HomeController@login');
      |
     */

    
        protected $layout;

    /**
     * for set default route
     * @var string
     */
    protected $default_route;

    /**
     * for set user session data
     * @var string
     */
    protected $_userSession;

    //protected $pageLimit;

    public function __construct()
    {
        Theme::setTheme('recruit');
        $this->layout = Theme::getLayout();
        $this->default_route = 'activities/index';
        $this->pageLimit = 20;
     //  $this->_userSession = Authenticate::check();  // check is user logged in
    }
    
    
    public function registerRecruiter() {
        $viewModel = array(
            'theme' => Theme::getTheme()
        );
        $msg="";
        if (Request::isMethod("post")) {

           $msg=Home::register();
           $viewModel["msg"]=$msg;
           $isAdmin = Input::get("admin");
           
           if(isset($isAdmin)){
           
           if($isAdmin=="int"){
               
                 return Redirect::to('users/create-user');
           }
           
           }
        }
 
        return Theme::make('login-registration', $viewModel);
    }

    /*Render home */
    
     public function index() {

     $viewModel = array(
            'theme' => Theme::getTheme()
        );


        return Theme::make('recruit_home', $viewModel);
        
        
    }
    
    
    
    public function about() {

        Theme::setTheme('jobseeker');
     $viewModel = array(
            'theme' => Theme::getTheme()
        );
     
        return Theme::make('about', $viewModel);
        
        
    }
    
    
    
    public function contact() {
    Theme::setTheme('jobseeker');
     $viewModel = array(
            'theme' => Theme::getTheme()
        );


        return Theme::make('contact', $viewModel);
        
        
    }
    
    /*End home */
    
    
    
     /*Render home */
    
     public function topEngineers() {

     $viewModel = array(
            'theme' => Theme::getTheme(),
            'user'=>  $this->_userSession
        );

    $skill=   Request::segment(3);
   
    $expertises=  CvSpecialization::groupBy('cv_tbl_id')->get();
    
    $viewModel['expertises']=$expertises;
    return Theme::make('top_engineers', $viewModel);
        
        
    }
    
    /*End home */
    
    /* Render login form */

     public function confirm() {

        $viewModel = array(
            'theme'    => Theme::getTheme(),
            'user'     => $this->_userSession,
            
        );
        
        $username= base64_decode(Request::segment(3));
        
        $password=  base64_decode(Request::segment(4));
        
         $user = User::firstOrNew(array('username' => $username,'password'=>md5($password)));
               if ($user->exists) {

                    $userdt = array(
                        'deleted_at' => '0'
                    );
                    
            User::where('id', $user->id)->update($userdt);
            
            
             try {
                $login = Authenticate::login($username, $password);
                if ($login) {
                    
                    return Redirect::to('dashboard');
                } else {
                    return Redirect::to('home/register-recruiter');
                }
            } catch (DbConnectionException $e) {
                echo $e->getMessage();
                die();
            }
       
            }
        
        return Theme::make('dashboard',$viewModel);
    }

    public function confirmEmail()
    {
        $user = User::where('token',Request::segment(3))->first();
        if(count($user))
        {
          $user->user_status = 1;
          $user->token = null;
          $user->save();
          if($user->user_type == "Employer")
          {
              Session::flash('message', 'Your email verified successfully');
              return Redirect::to('recruiter/register-recruiter');
          }    
          else
          {
              Session::flash('message', 'Your email verified successfully');
              return Redirect::to('jobseeker/register-job-seeker');
          }
        }else{
          Session::flash('message','Sorry! invalid token or token expired');
          return Redirect::to('/');
        }

    }

    public function login() {

        $user = Authenticate::hasSession();

        if ($user) {
            $userExist = User::find($user->id);
            if ($userExist){
                return Redirect::to('dashboard');
            }
        }

        $viewModel = array(
            'theme' => Theme::getTheme()
        );

        //Helpers::debug(Theme::getTheme());exit();  
        return Theme::make('login', $viewModel);
    }

    /* Action for authentication */

    public function authenticate() {

        if (Request::isMethod('post')) {

            $userId = Input::get('userId');
            $password = Input::get('password');

            
            try {
                $login = Authenticate::login($userId, $password);
                if ($login) {
                    
                    return Redirect::to('dashboard');
                } else {
                    return Redirect::to('home/register-recruiter');
                }
            } catch (DbConnectionException $e) {
                echo $e->getMessage();
                die();
            }
        }
    }

    /* Logout a user and redirect to login page */

    public function logout() {
        if (Authenticate::check()) {

            Session::forget('user');
            Session::forget('id');
            if(Session::get('type_log')=="recruit"){
            return Redirect::to('/recruiter/index');
            exit();
            }
            return Redirect::to('/');
            exit();
        }
    }



}