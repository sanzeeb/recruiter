<?php
/**
 * Created by PhpStorm.
 * User: HimelC51
 * Date: 11/13/14
 * Time: 12:48 PM
 */

class TprojectController extends BaseController{

    protected $layout;
    protected $default_route;
    protected $_userSession;
    protected $pageLimit = 20;

    public function __construct() {

        $this->layout = Theme::getLayout();
        $this->default_route = 'tproject/index';
        $this->_userSession = Authenticate::check();

        if(!empty($this->_userSession) && ($this->_userSession->remember_me))
        {
            $expireTime = (60*24*360);
            Config::set('session.lifetime',$expireTime);
        }
        $inbox = Inbox::where('receiver_id',$this->_userSession->id)->where('read_status',0)->orderBy('inbox_id','desc')->take(5)->get();

        View::share('messages',$inbox);

        $comment = Comment::where('receiver_id', $this->_userSession->id)->where('read_status', 0)->get();
        View::share('comments', $comment);

        if(!empty($this->_userSession->user_id))
            $jobCount = Jobs::where('created_by',$this->_userSession->user_id)->count();
        else
            $jobCount = 0;
        View::share('jobCount',$jobCount);
    }

    public function index()
    {
        return Redirect::to('tproject/lists');
    }

    public function lists()
    {
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user'  => $this->_userSession
        );
        if($this->_userSession->user_type == 'Employer')
        {
            $empId = $this->_userSession->user_id;
            $jobs       =  Jobs::where('created_by',$empId)->get();
            if(count($jobs) <= 0)
            {
                Helpers::addMessage(400,' You must create at least one job before make test project');
                return Redirect::to('dashboard');
            }
            $tprojects = TestProject::with('Candidate')->where(function($q){
                $q->where('project_status','=','Pending')->where('project_status','=','Approved','OR')
                ->where('project_status','=','Review','OR')->where('project_status','=','Submitted','OR')
                ->where('project_status','=','Canceled','OR')->where('project_status','=','Replied','OR')
                ->where('project_status','=','Replied by admin','OR')->where('project_status','=','Submit Canceled','OR');
            })->where('emp_id',$empId)->orderBy('id','DESC')->get();

        }else if($this->_userSession->user_type == 'Admin')
        {
            $tprojects = TestProject::with('Candidate','Employer')->orderBy('id','DESC')->get();
        }else{
            $candidateId = $this->_userSession->user_id;
            $tprojects = TestProject::with('Candidate','Employer')->where(function($q){
                $q->where('project_status','=','Approved')->where('project_status','=','Review','OR')->where('project_status','=','Submitted','OR');
            })->where('candidate_id',$candidateId)->where('project_status','=','Submit Canceled','OR')->orderBy('id','DESC')->get();
        }
       /* echo $candidateId;
        Helpers::debug($tprojects);die();*/
        $viewModel['tprojects'] = $tprojects;
        return Theme::make('tproject.lists',$viewModel);
    }

    public function create()
    {
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user'=>  $this->_userSession
        );
        if($this->_userSession->user_type!='Employer')
        {
            return Redirect::to('dashboard');
        }
        $candidateId = Request::segment(3);
        $candidate  =  Candidate::find($candidateId);
        $employer   =  Employer::find($this->_userSession->user_id);


        if(count($candidate))
        {

            if(Request::isMethod('post'))
                $viewModel['jobId'] = Input::get('job_id');

            $jobs       =  Jobs::where('created_by',$employer->emp_id)->get();
            if(count($jobs) <= 0)
            {
                Helpers::addMessage(400,' You must create at least one job before make test project');
                return Redirect::to('dashboard');
            }

            $myjobs = array();
            foreach($jobs as $job)
            {
                $myjobs[$job->jobs_id]['title'] = $job->job_title;
                $myjobs[$job->jobs_id]['job_id'] = $job->jobs_id;

                $jobResponsibilities = (!empty($job->job_responsibility))? json_decode($job->job_responsibility) : array();
                $jobEduQualification = (!empty($job->eduactional_requirement))? json_decode($job->eduactional_requirement) : array();
                $jobExpRequirement   = (!empty($job->experience_requirment))? json_decode($job->experience_requirment) : array();
                $jobNature = $job->job_nature;


                $myjobs[$job->jobs_id]['responsibilities'] = implode("\n",$jobResponsibilities);
                $myjobs[$job->jobs_id]['edu'] = implode("\n",$jobEduQualification);
                $myjobs[$job->jobs_id]['exp'] = implode("\n",$jobExpRequirement);
                $myjobs[$job->jobs_id]['nature'] = $jobNature;
                $myjobs[$job->jobs_id]['salary'] = $job->salary_range;

            }
            $viewModel['employer'] = $employer;
            $viewModel['candidate'] = $candidate;
            $viewModel['jobs'] = $myjobs;
            //Helpers::debug($myjobs);die();
            return Theme::make('tproject.create',$viewModel);
        }else{
            return Redirect::to('resume/display-resume/'.$candidateId);
        }

    }

    public function sendRequest()
    {
        if(Request::isMethod('post'))
        {
            if($this->_userSession->user_type!='Employer')
            {
                return Redirect::to('dashboard');
            }
            $eid = Input::get('eid');
            $cid = Input::get('cid');


            $project_description = Input::get('project_description');
            $project_title = Input::get('project_title');

            $empId = $this->_userSession->user_id;
            $tproject = TestProject::firstOrNew(array('emp_id'=>$empId,'candidate_id'=>$cid,'project_name'=>$project_title,'project_description'=>$project_description));
            if(!$tproject->exists)
            {
                $tproject->project_name = $project_title;
               // $tproject->project_description = $project_description;

                if(Input::hasFile('attachment'))
                {

                    $ext = Input::file('attachment')->getClientOriginalExtension();
                    $fileName = Input::file('attachment')->getClientOriginalName();

                    $path = 'data/tproject/attachment/';


                    $fileName = time().'_'.str_replace(" ","_",$fileName);
                    if(in_array(strtolower($ext),array('jpg','jpeg','png','gif','pdf','doc','docx','xls','xlsx','ppt'))){
                        Input::file('attachment')->move($path,$fileName);
                        $tproject->project_attachment = $fileName;

                    }
                  
                }
                $tproject->project_status = 'Pending';
                

                $tproject->save();
                Helpers::addMessage(200, ' Test Project sent to admin to approve');
            }
            return Redirect::to('tproject/index');
        }
        else
        {
            return Redirect::to('dashboard');
        }
    }

    public function details()
    {
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user'  => $this->_userSession
        );

        $id = Request::segment(3);
        $tProject = TestProject::find($id);

        if(count($tProject))
        {
            $employer   =  Employer::find($tProject->emp_id);
            if($this->_userSession->user_type == "Candidate")
                $viewModel['anyApprovedReply'] = TestProjectReply::where(function($q){
                    $q->where('status','=','Replied')->where('status','=','Pending','OR');
                })->where('tproject_id',$tProject->id)->count(); 
            $viewModel['employer'] = $employer;
            $viewModel['tproject'] = $tProject;
            return Theme::make('tproject.details',$viewModel);
        }
        else{

            return Redirect::to('tproject/lists');

        }

    }

    public function replyAccept()
    {
        if(Request::isMethod('post'))
        {
            $id = Input::get('ir_id');
            $tprojectReply = TestProjectReply::find($id);
            $tproject = $tprojectReply->TestProject;
            if(count($tprojectReply))
            {
                $tproject->project_status = "Replied";
                $tproject->save();

                $tprojectReply->status = "Replied";
                $tprojectReply->save();
                Helpers::addMessage(200, 'Test Project reply approved');

            }else{

                Helpers::addMessage(400, " No Test Project found");

            }
            return 1;

        }else{

            return Redirect::to('dashboard');

        }
    }

    public function replyCancel()
    {
        if(Request::isMethod('post'))
        {
            $id = Input::get('ir_id');
            $comment = trim(Input::get('comment'));
            $tprojectReply = TestProjectReply::find($id);
            $tProject = $tprojectReply->TestProject;
            if(count($tprojectReply))
            {
                if($comment == "")
                {
                    Helpers::addMessage(400, "Sorry! you have to give a reason");
                    
                    return Redirect::to('tproject/details/'.$tProject->id);
                }

                $tprojectReply->status = "Canceled";
                $tprojectReply->reason = $comment;
                $tprojectReply->save();
                Helpers::addMessage(200, 'Test Project reply approved');

            }else{

                Helpers::addMessage(400, " No Test Project found");

            }
            return Redirect::to('tproject/details/'.$tProject->id);

        }else{

            return Redirect::to('dashboard');

        }
    }

    public function submittionCancel()
    {
        if(Request::isMethod('post'))
        {
            $id = Input::get('id');
            $tproject = TestProject::find($id);
            $comment  = trim(Input::get('comment'));
            if(count($tproject))
            {
                if($comment == "")
                {
                    Helpers::addMessage(400, "Sorry! you have to give a reason");
                    
                    return Redirect::to('tproject/details/'.$tProject->id);
                }

                $tproject->project_status = "Submit Canceled";
               
                $tproject->save();
                $testProjectReply = new TestProjectReply();
                $testProjectReply->tproject_id = $tproject->id;
                $testProjectReply->replier_type = 'Admin';
                $testProjectReply->replier_id =$this->_userSession->id;
                $testProjectReply->reply="Cancel";
                $testProjectReply->reason = $comment;
                $testProjectReply->status = "Submit Canceled";
                $testProjectReply->save();

                Helpers::addMessage(200, 'Test Project reply approved');

            }else{

                Helpers::addMessage(400, " No Test Project found");

            }
            return Redirect::to('tproject/details/'.$tproject->id);

        }else{

            return Redirect::to('dashboard');

        }
    }

    public function approve()
    {
        if(Request::ajax()){

            $id = Input::get('id');
            $tproject = TestProject::find($id);
            if(count($tproject))
            {
                $tproject->project_status = TestProject::$Approved;
                $tproject->save();
                Helpers::addMessage(200, 'Test Project Approved');
            }else{

                Helpers::addMessage(400, " No Test Project found");
            }
            return 1;
        }else{

            return Redirect::to('dashboard');

        }
    }

    public function cancel()
    {
        if(Request::isMethod('post')){

            $id = Input::get('id');
            $tproject = TestProject::find($id);
            $comment  = trim(Input::get('comment'));
            if(count($tproject))
            {
                if($comment == "")
                {
                    Helpers::addMessage(400, "Sorry! you have to give a reason");
                    
                    return Redirect::to('tproject/details/'.$id);
                }

                $tproject->project_status = TestProject::$Canceled;
                $tproject->save();
                $tprojectReply = new TestProjectReply();
                $tprojectReply->replier_id = $this->_userSession->id;
                $tprojectReply->replier_type = 'Admin';
                $tprojectReply->tproject_id = $tproject->id;
                $tprojectReply->status = "Replied by admin";
                $tprojectReply->reply = $comment; 
                $tprojectReply->save();

                Helpers::addMessage(200, 'Test Project Canceled');
            }else{

                Helpers::addMessage(400, " No Test Project found");
            }
            return Redirect::to('tproject/details/'.$id);
        }else{

            return Redirect::to('dashboard');

        }
    }

    public function reviewApprove()
    {
        if(Request::isMethod('post'))
        {
            $id = Input::get('id');
            $tproject = TestProject::find($id);
            if(count($tproject))
            {
                $tproject->project_status = TestProject::$Submitted;
                $tproject->save();
                Helpers::addMessage(200, 'Test Project project submission Accepted');
            }else{

                Helpers::addMessage(400, " No Test Project found");
            }
            return 1;
        }else{

            return Redirect::to('dashboard');
        }
    }

    public function submitted()
    {
        if(Request::isMethod('post'))
        {
            $id = Input::get('id');
            $tproject = TestProject::find($id);

            if(count($tproject))
            {
                $sourceLink = Input::get('source_link');

                if($sourceLink)
                    $tproject->project_submission_link = $sourceLink;

                $tproject->project_status = TestProject::$Review;

                if(Input::hasFile('project'))
                {
                    $ext = Input::file('project')->getClientOriginalExtension();
                    $fileName = Input::file('project')->getClientOriginalName();

                    $path = 'data/tproject/project/';


                    $fileName = time().'_'.str_replace(" ","_",$fileName);
                    if(in_array(strtolower($ext),array('zip'))){

                        Input::file('project')->move($path,$fileName);
                        $tproject->project_zip = $fileName;

                    }else{
                        Helpers::addMessage(400, "only zip file supported");
                        return Redirect::to('tproject/details/'.$tproject->id);
                    }
                }
                $tproject->Candidate->source_code = $tproject->project_submission_link;
                $tproject->Candidate->save();
                $tproject->save();

                Helpers::addMessage(200, " Test project submitted");

            }else{

                Helpers::addMessage(400, " No test project found");

            }
            return Redirect::to('tproject');
        }
        else
        {
            return Redirect::to('dashboard');
        }
    }

    public function get_tproject_by_date()
    {
        $tprojects = null;
        if(Request::isMethod('post'))
        {
            $createdDate = Input::get('date');
            if($this->_userSession->user_type == 'Employer')
            {
                $empId = $this->_userSession->user_id;
                

                $tprojects = TestProject::with('Candidate')->where(function($q){
                    $q->where('project_status','=','Pending')->where('project_status','=','Approved','OR')
                    ->where('project_status','=','Review','OR')->where('project_status','=','Submitted','OR')
                    ->where('project_status','=','Canceled','OR')->where('project_status','=','Replied','OR')
                    ->where('project_status','=','Replied by admin','OR')->where('project_status','=','Submit Canceled','OR');
                })->where('emp_id',$empId)->where('created_at',$createdDate)->orderBy('id','DESC')->get();

            }else if($this->_userSession->user_type == 'Admin')
            {
                $tprojects = TestProject::with('Candidate','Employer')->where('created_at',$createdDate)->orderBy('id','DESC')->get();
            }else{
                $candidateId = $this->_userSession->user_id;
                $tprojects = TestProject::with('Candidate','Employer')->where(function($q){
                    $q->where('project_status','=','Approved')->where('project_status','=','Review','OR')->where('project_status','=','Submitted','OR');
                })->where('candidate_id',$candidateId)->where('created_at',$createdDate)->where('project_status','=','Submit Canceled','OR')->orderBy('id','DESC')->get();
            }
        }  
        return $tprojects;
        exit(); 

    }
} 