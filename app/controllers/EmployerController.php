<?php

class EmployerController extends BaseController {
    /*
      |--------------------------------------------------------------------------
      | Default Home Controller
      |--------------------------------------------------------------------------
      |
      | You may wish to use controllers instead of, or in addition to, Closure
      | based routes. That's great! Here is an example controller method to
      | get you started. To route to this controller, just add the route:
      |
      |	Route::get('/', 'HomeController@login');
      |
     */

    protected $layout;

    /**
     * for set default route
     * @var string
     */
    protected $default_route;

    /**
     * for set user session data
     * @var string
     */
    protected $_userSession;

    //protected $pageLimit;

    public function __construct() {

        $this->layout = Theme::getLayout();
        $this->default_route = 'activities/index';
        $this->pageLimit = 20;
        $this->_userSession = Authenticate::check();  // check is user logged in
        if(!empty($this->_userSession) && ($this->_userSession->remember_me))
        {
            $expireTime = (60*24*360);
            Config::set('session.lifetime',$expireTime);
        }
        $inbox = Inbox::where('receiver_id',$this->_userSession->id)->where('read_status',0)->orderBy('inbox_id','desc')->take(5)->get();
        View::share('messages',$inbox);
        
                             $comment = Comment::where('receiver_id', $this->_userSession->id)->where('read_status', 0)->get();
        View::share('comments', $comment);

        if(!empty($this->_userSession->user_id))
            $jobCount = Jobs::where('created_by',$this->_userSession->user_id)->count();
        else
            $jobCount = 0;
        View::share('jobCount',$jobCount);
    }

    public function deleteEmp() {


        if (Request::ajax()) {

            $permission = Authenticate::getPermission($this->_userSession);
            if (!$permission->delete) {
                return "500";
            }
            $id = Input::get("id");
            $user = User::find($id);

            $user_id = $user->user_id;


            $msg = User::where('id', $id)->delete();
            $msg2 = Employer::where('emp_id', $user_id)->delete();

            return $msg;
        }
    }

    public function editEmployer() {

        
        
        
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user'=>  $this->_userSession
        );
        
        $permission = Authenticate::getPermission($this->_userSession);
        
        if (!$permission->update) {

            Helpers::addMessage(500, " You don't have permission to access that page");
            return Redirect::to('dashboard');
        }
        
        $id = Request::segment(3);
        if (Request::isMethod("post")) {

            $msg = EmployerBLL::empEdit();
            if ($this->_userSession->user_type == "Employer") {

                return Redirect::to('profile/index/' . $this->_userSession->id);
            } else {

                return Redirect::to('employer/list-employer');
                $viewModel["msg"] = $msg;
            }
        }
        
        $users = User::where('id', '=', $id)->first();
        $viewModel["users"] = $users;

        return Theme::make('employer.edit', $viewModel);
    }

    public function createEmployer() {

        $permission = Authenticate::getPermission($this->_userSession);
        if (!$permission->create) {

            Helpers::addMessage(500, " You don't have permission to access that page");
            return Redirect::to('dashboard');
        }
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession
        );

        if (Request::isMethod("post")) {

            $msg = EmployerBLL::empRegister();
            $viewModel["msg"] = $msg;
            
             if($msg=="1"){
            $viewModel["msg"]="Account created successfully";     
            }
            if($msg=="2"){
            $viewModel["msg"]="Email already exist ";     
             return Theme::make('employer.create', $viewModel);
            }
            if($msg=="3"){
            $viewModel["msg"]="Recruiter already exist by this user name"; 
             return Theme::make('employer.create', $viewModel);
            }
            if($msg=="4"){
            $viewModel["msg"]="Please enter all required field"; 
             return Theme::make('employer.create', $viewModel);
            }
            if($msg=="5"){
            $viewModel["msg"]="Password does not match"; 
            return Theme::make('employer.create', $viewModel);
            
            }

            return Redirect::to('employer/list-employer');
        }
        return Theme::make('employer.create', $viewModel);
    }

    public function listEmployer() {

        $permission = Authenticate::getPermission($this->_userSession);
        if (!$permission->read) {

            Helpers::addMessage(500, " You don't have permission to access that page");
            return Redirect::to('dashboard');
        }
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user'=>  $this->_userSession
        );

        $users = User::where('user_type', '=', 'Employer')->paginate(10);
        $viewModel["users"] = $users;
        return Theme::make('employer.list', $viewModel);
    }

    public function favorites(){
       /* $viewModel = array(
            'theme' => Theme::getTheme(),
            'user'=>  $this->_userSession
        );

        $cvTbl  = new CvTable();
         $skill=base64_decode(Request::segment(3));
        $empId      = $this->_userSession->user_id;
        $expertises = $cvTbl->FavoriteCv($empId);
        $jobs       =  Jobs::where('created_by',$empId)->get();
        $skills = array();
        if(empty($distinctSkills))
        {
            $allSkills = Skills::all();
            foreach($allSkills as $sk)
            {
                $distinctSkills[$sk->skill_name] =strtolower(trim($sk->skill_name));
            }

        }

        if(count($jobs) <= 0)
        {
            Helpers::addMessage(400,' You must create at least one job before make favorites');
            return Redirect::to('dashboard');
        }
        $viewModel['skill']=strtolower(trim($skill));
        $viewModel['skills'] = $skills;
        $viewModel['disSkills'] =$distinctSkills;
        $viewModel['expertises']=$expertises;
        $viewModel['gallery']= Gallery::first();*/

        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user'=>  $this->_userSession
        );
    $created_by = $this->_userSession->user_id;
    $skill=base64_decode(Request::segment(3));

    $keyword      = trim(Request::get('name'));
    $age          = trim(Request::get('age'));
    $yxp          = trim(Request::get('yxp'));
    $cvTbl        = new CvTable();
    $searchskills = Request::get('skills');
    
    
   // $expertises=  CvSpecialization::where('key',  $skill)->get();
   if(!empty($searchskills))
   {

        $expertises=$cvTbl->FavoriteCv($created_by,$searchskills,$keyword,$age,$yxp);

   }else
   {
        $expertises=$cvTbl->FavoriteCv($created_by,$skill,$keyword,$age,$yxp);
   }

   $skills = array();
   $distinctSkills = array();
   foreach($expertises as $exp)
   {
        $cvSpecializations = CvSpecialization::where('cv_tbl_id',$exp->cv_tbl_id)->get();
        $skills[$exp->cv_tbl_id] = $cvSpecializations;

        foreach($cvSpecializations as $spec)
        {
            $distinctSkills[$spec->key] = strtolower(trim($spec->key));
        }
   }

   if(empty($distinctSkills))
   {
        $allSkills = Skills::all();
        foreach($allSkills as $sk)
        {
            $distinctSkills[$sk->skill_name] =strtolower(trim($sk->skill_name));
        }

   }

  //Helpers::debug($expertises);
  // die();
    $viewModel['expertises']=$expertises;
    $viewModel['searchedSkills'] = $searchskills;
    $viewModel['skill']=strtolower(trim($skill));
    $viewModel['skills'] = $skills;
    $viewModel['disSkills'] =$distinctSkills;
    $viewModel['gallery']= Gallery::first();
        return Theme::make('employer.favorite_lists', $viewModel);
    }

    public function removeFavorite()
    {
        if(Request::isMethod('post'))
        {
            $cid = Input::get('cid');
            $employeeFreelancer = EmployerFavorite::where('emp_id',$this->_userSession->user_id)->where('candidate_id',$cid)->first();
            $employeeFreelancer->delete();

        }
        return Redirect::to('employer/favorites');
    }
}
