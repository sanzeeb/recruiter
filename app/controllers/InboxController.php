<?php
/**
 * Created by PhpStorm.
 * User: Himel
 * Date: 6/22/14
 * Time: 2:57 PM
 */

class InboxController extends BaseController{

    protected $layout;
    protected $default_route;
    protected $_userSession;

    public function __construct() {

        $this->layout = Theme::getLayout();
        $this->default_route = 'dashboard/index';
        $this->_userSession = Authenticate::check();
        if(!empty($this->_userSession) && ($this->_userSession->remember_me))
        {
            $expireTime = (60*24*360);
            Config::set('session.lifetime',$expireTime);
        }
        $this->_userSession->user_type;
        $inbox = Inbox::where('receiver_id',$this->_userSession->id)->where('read_status',0)->orderBy('inbox_id','desc')->take(5)->get();
        View::share('messages',$inbox);
        $comment = Comment::where('receiver_id', $this->_userSession->id)->where('read_status', 0)->get();
        View::share('comments', $comment);

        if(!empty($this->_userSession->user_id))
            $jobCount = Jobs::where('created_by',$this->_userSession->user_id)->count();
        else
            $jobCount = 0;
        View::share('jobCount',$jobCount);
        
    }

    public function index()
    {
        $viewModel = array(
            'theme' => Theme::getTheme(),
            'user' => $this->_userSession,
        );
        $id = Request::segment(3);
        $inbox = Inbox::find($id);
        Inbox::where('inbox_id',$inbox->inbox_id)->update(array('read_status'=>1));
       
        $inboxCount = Inbox::where('receiver_id',$this->_userSession->id)->where('read_status',0)->get();
      
        View::share('messages',$inboxCount);
        $viewModel['inbox'] = $inbox;
        return Theme::make('inbox.index',$viewModel);
    }
} 